<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSenMediationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sen_mediations', function (Blueprint $table) {
            $table->id();
            $table->integer('id_hist_scen')->nullable();
            $table->string('lieu_med')->nullable();
            $table->string('adress_med')->nullable();
            $table->datetime('date_med')->nullable();
            $table->string('mediateur')->nullable();
            $table->string('debiteur_mandataire')->nullable();
            $table->string('promesse')->nullable();
            $table->string('date_promi')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sen_mediations');
    }
}
