<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSituationJudiciairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('situation_judiciaires', function (Blueprint $table) {
            $table->id();
            $table->integer('id_hist_scen')->nullable();
            $table->string('type_debiteur')->nullable();
            $table->string('type_situation_judic')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('situation_judiciaires');
    }
}
