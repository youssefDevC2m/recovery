<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSenVisiteDomicilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sen_visite_domiciles', function (Blueprint $table) {
            $table->id();
            $table->integer('id_hist_scen')->nullable();
            $table->string('adress_perso')->nullable();
            $table->string('adress_pro')->nullable();
            $table->string('adress_proche')->nullable();
            $table->datetime('date_H_visite')->nullable();
            $table->string('visiteur')->nullable();
            $table->text('promess')->nullable();
            $table->string('date_promi')->nullable();
            $table->text('comment')->nullable();
            $table->string('domicile')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sen_visite_domiciles');
    }
}
