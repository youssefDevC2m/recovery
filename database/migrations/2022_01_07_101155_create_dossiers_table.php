<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDossiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dossiers', function (Blueprint $table) {
            $table->id();
            $table->integer('id_client')->nullable();
            $table->integer('id_dbtr')->nullable();
            $table->integer('creator_id')->nullable();
            $table->string("represante_legal")->nullable();
            $table->string("tele_represante_legal")->nullable();
            $table->string("contact_represante_legal")->nullable();
            $table->datetime('date_resep')->nullable();
            $table->datetime('date_ouverture')->nullable();
            $table->string('ref')->nullable();
            $table->string('phase')->nullable();
            $table->float('mt_glob_creance')->nullable();
            $table->text('observation')->nullable();
            $table->datetime('date_db_doss')->nullable();
            $table->datetime('date_fin_doss')->nullable();
            $table->integer('is_jurid')->nullable();
            $table->integer('doss_exp_vu')->nullable();
           
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dossiers');
    }
}
