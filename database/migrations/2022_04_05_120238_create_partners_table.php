<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->id();
            $table->string('type_partn')->nullable();
            $table->integer('agent_id')->nullable();
            $table->string('nom_part')->nullable();
            $table->string('prenom_part')->nullable();
            $table->text('adrs_part')->nullable();
            $table->text('adrs_act_part')->nullable();
            $table->string('tele_part')->nullable();
            $table->string('tele_wts_part')->nullable();
            $table->string('email_part')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
