<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('partner_details', function (Blueprint $table) {
            $table->id();
            $table->integer('id_part')->nullable();
            $table->integer('doss')->nullable();
            $table->integer('dbteur')->nullable();
            $table->text('vill_debtr')->nullable();
            $table->float('mt_creance')->nullable();
            $table->text('motif_creance')->nullable();
            $table->text('nature_creance')->nullable();
            $table->date('date_trans')->nullable();
            $table->text('remarque')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('partner_details');
    }
}
