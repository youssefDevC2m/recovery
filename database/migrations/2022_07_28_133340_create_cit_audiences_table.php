<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitAudiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cit_audiences', function (Blueprint $table) {
            $table->id();
            $table->integer('id_citaton');
            $table->date('cit_date_audc')->nullable();
            $table->time('cit_heur_audc')->nullable();
            $table->string('cit_lieu_audc')->nullable();
            $table->text('cit_resume')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cit_audiences');
    }
}
