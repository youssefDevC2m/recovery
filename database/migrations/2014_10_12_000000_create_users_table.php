<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nom')->nullable();
            $table->string('prenom')->nullable();
            $table->string('email')->unique();
            $table->string('tele')->nullable();
            $table->String('address')->nullable();
            $table->string('temp_pwd')->unique();
            $table->string('photo')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('role')->nullable();
            $table->string('password');
            $table->string('id_client')->nullable();
//////////////////////////////////////////////////////////
            $table->integer('acs_tabord')->nullable();
//////////////////////////////////////////////////////////
            $table->integer('acs_user')->nullable();
            $table->integer('give_priv_user')->nullable();
            $table->integer('droit_de_valid_acts')->nullable();
//////////////////////////////////////////////////////////
            $table->integer('acs_cli')->nullable();
            $table->integer('add_cli')->nullable();
            $table->integer('edit_cli')->nullable();
            $table->integer('delet_cli')->nullable();
            $table->integer('list_cli')->nullable();
            $table->integer('read_only_cli')->nullable();
            $table->integer('see_doss_cli')->nullable();
///////////////////////////////////////////////////////////
            $table->integer('acs_doss')->nullable();
            $table->integer('see_all_doss')->nullable();
            $table->integer('acs_scens_doss')->nullable();
            $table->integer('add_doss')->nullable();
            $table->integer('edit_doss')->nullable();
            $table->integer('delet_doss')->nullable();
            $table->integer('list_doss')->nullable();
            $table->integer('read_only_doss')->nullable();
            $table->integer('edit_info_cli_doss')->nullable();
            $table->integer('see_info_cli_doss')->nullable();
            $table->integer('edit_info_dbitr_doss')->nullable();
            $table->integer('see_info_dbitr_doss')->nullable();
            $table->integer('see_fermetur_doss')->nullable();
            $table->integer('edit_fermetur_doss')->nullable();
            $table->integer('acs_phas_ami')->nullable();
            $table->integer('acs_phas_judis')->nullable();
            
//////////////////////////////////////////////////////////////
            $table->integer('acs_devis')->nullable();
            $table->integer('add_devis')->nullable();
            $table->integer('edit_devis')->nullable();
            $table->integer('delet_devis')->nullable();
            $table->integer('list_devis')->nullable();
            $table->integer('print_devis')->nullable();
            $table->integer('see_devis')->nullable();
//////////////////////////////////////////////////////////////
            $table->integer('acs_fact')->nullable();
            $table->integer('add_fact')->nullable();
            $table->integer('edit_fact')->nullable();
            $table->integer('delet_fact')->nullable();
            $table->integer('list_fact')->nullable();
            $table->integer('see_fact')->nullable();
/////////////////////////////////////////////////////////////
            $table->integer('acs_rglmnt')->nullable();
            $table->integer('add_rglmnt')->nullable();
            $table->integer('edit_rglmnt')->nullable();
            $table->integer('delet_rglmnt')->nullable();
            $table->integer('list_rglmnt')->nullable();
            $table->integer('see_rglmnt')->nullable();
////////////////////////////////////////////////////////////
            $table->integer('acs_charge')->nullable();
            $table->integer('add_charge')->nullable();
            $table->integer('edit_charge')->nullable();
            $table->integer('delet_charge')->nullable();
            $table->integer('list_charge')->nullable();
            $table->integer('see_charge')->nullable();
//////////////////////////////////////////////////////////////
            $table->integer('acs_parten')->nullable();
            $table->integer('add_parten')->nullable();
            $table->integer('edit_parten')->nullable();
            $table->integer('delet_parten')->nullable();
            $table->integer('list_parten')->nullable();
            $table->integer('see_infos_parten')->nullable();
            $table->integer('see_parten')->nullable();
///////////////////////////////////////////////////////////////            
            $table->integer('acs_task')->nullable();
            $table->integer('add_task')->nullable();
            $table->integer('valid_task')->nullable();
            $table->integer('edit_task')->nullable();
            $table->integer('delet_task')->nullable();
            $table->integer('list_task')->nullable();
            $table->integer('suivi_task')->nullable();
            $table->integer('see_dt_task')->nullable();
///////////////////////////////////////////////////////////////
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /*

    ALTER TABLE `users`
	ADD COLUMN `acs_cli` INT NULL DEFAULT NULL AFTER `id_client`,
	ADD COLUMN `add_cli` INT NULL DEFAULT NULL AFTER `acs_cli`,
	ADD COLUMN `edit_cli` INT NULL DEFAULT NULL AFTER `add_cli`,
	ADD COLUMN `delet_cli` INT(11) NULL DEFAULT NULL AFTER `edit_cli`,
	ADD COLUMN `list_cli` INT(11) NULL DEFAULT NULL AFTER `delet_cli`,
	ADD COLUMN `read_only_cli` INT(11) NULL DEFAULT NULL AFTER `list_cli`,
	ADD COLUMN `see_doss_cli` INT(11) NULL DEFAULT NULL AFTER `read_only_cli`,
    ADD COLUMN `acs_doss` INT(11) NULL DEFAULT NULL AFTER `see_doss_cli`,
	ADD COLUMN `add_doss` INT(11) NULL DEFAULT NULL AFTER `acs_doss`,
	ADD COLUMN `edit_doss` INT(11) NULL DEFAULT NULL AFTER `add_doss`,
	ADD COLUMN `delet_doss` INT(11) NULL DEFAULT NULL AFTER `edit_doss`,
	ADD COLUMN `list_doss` INT(11) NULL DEFAULT NULL AFTER `delet_doss`,
	ADD COLUMN `read_only_doss` INT(11) NULL DEFAULT NULL AFTER `list_doss`,
	ADD COLUMN `edit_info_cli_doss` INT(11) NULL DEFAULT NULL AFTER `read_only_doss`,
	ADD COLUMN `see_info_cli_doss` INT(11) NULL DEFAULT NULL AFTER `edit_info_cli_doss`,
	ADD COLUMN `edit_info_dbitr_doss` INT(11) NULL DEFAULT NULL AFTER `see_info_cli_doss`,
	ADD COLUMN `see_info_dbitr_doss` INT(11) NULL DEFAULT NULL AFTER `edit_info_dbitr_doss`,
	ADD COLUMN `see_fermetur_doss` INT(11) NULL DEFAULT NULL AFTER `see_info_dbitr_doss`,
	ADD COLUMN `edit_fermetur_doss` INT(11) NULL DEFAULT NULL AFTER `see_fermetur_doss`,
	ADD COLUMN `acs_devis` INT(11) NULL DEFAULT NULL AFTER `edit_fermetur_doss`,
	ADD COLUMN `add_devis` INT(11) NULL DEFAULT NULL AFTER `acs_devis`,
	ADD COLUMN `edit_devis` INT(11) NULL DEFAULT NULL AFTER `add_devis`,
	ADD COLUMN `delet_devis` INT(11) NULL DEFAULT NULL AFTER `edit_devis`,
	ADD COLUMN `list_devis` INT(11) NULL DEFAULT NULL AFTER `delet_devis`,
	ADD COLUMN `print_devis` INT(11) NULL DEFAULT NULL AFTER `list_devis`,
	ADD COLUMN `see_devis` INT(11) NULL DEFAULT NULL AFTER `print_devis`,
	ADD COLUMN `acs_rglmnt` INT(11) NULL DEFAULT NULL AFTER `see_devis`,
	ADD COLUMN `add_rglmnt` INT(11) NULL DEFAULT NULL AFTER `acs_rglmnt`,
	ADD COLUMN `edit_rglmnt` INT(11) NULL DEFAULT NULL AFTER `add_rglmnt`,
	ADD COLUMN `delet_rglmnt` INT(11) NULL DEFAULT NULL AFTER `edit_rglmnt`,
	ADD COLUMN `list_rglmnt` INT(11) NULL DEFAULT NULL AFTER `delet_rglmnt`,
	ADD COLUMN `see_rglmnt` INT(11) NULL DEFAULT NULL AFTER `list_rglmnt`,
    ADD COLUMN `acs_charge` INT(11) NULL DEFAULT NULL AFTER `see_rglmnt`,
	ADD COLUMN `add_charge` INT(11) NULL DEFAULT NULL AFTER `acs_charge`,
	ADD COLUMN `edit_charge` INT(11) NULL DEFAULT NULL AFTER `add_charge`,
	ADD COLUMN `delet_charge` INT(11) NULL DEFAULT NULL AFTER `edit_charge`,
	ADD COLUMN `list_charge` INT(11) NULL DEFAULT NULL AFTER `delet_charge`,
	ADD COLUMN `see_charge` INT(11) NULL DEFAULT NULL AFTER `list_charge`;

     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
