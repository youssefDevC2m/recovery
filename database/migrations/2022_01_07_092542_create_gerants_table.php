<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGerantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gerants', function (Blueprint $table) {
            $table->id();
            $table->integer('id_dbtr')->nullable();
            $table->string('nom')->nullable();
            $table->string('prenom')->nullable();
            $table->string('alias')->nullable();
            $table->string('cin')->nullable();
            $table->string('adress_pincipale')->nullable();
            $table->string('ville')->nullable();
            $table->string('tele')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gerants');
    }
}
