<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebiteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debiteurs', function (Blueprint $table) {
            $table->id();
            $table->string('type_dtbr')->nullable();
            $table->string('nom')->nullable();
            $table->string('prenom')->nullable();
            $table->string('cin')->nullable();
            $table->string('alias')->nullable();
            $table->string('employeur')->nullable();
            $table->text('activite')->nullable();
            $table->text('adress_prv')->nullable();
            $table->string('ville_adress_prv')->nullable();
            $table->text('adress_pro')->nullable();
            $table->string('ville_adress_pro')->nullable();
            
            // banque infos
            $table->string('forme_jurdq')->nullable();
            $table->string('rc')->nullable();
            $table->string('if')->nullable();
            $table->string('patent')->nullable();
            $table->text('adress_siege')->nullable();
            $table->string('ville')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debiteurs');
    }
}
