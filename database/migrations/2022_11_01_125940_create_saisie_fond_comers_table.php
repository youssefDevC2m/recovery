<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaisieFondComersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saisie_fond_comers', function (Blueprint $table) {
            $table->id();
            $table->integer('id_hist_scen');
            $table->string('tribunal_competant')->nullable();
            $table->string('non_juge')->nullable();
            $table->string('ref')->nullable();
            // parties
            // 1
            $table->string('benificiaire')->nullable();
            $table->string('avoca_benif')->nullable();
            $table->string('charger_suivi_binif')->nullable();
            // 2
            $table->string('le_tireur')->nullable();
            $table->string('address_tireur')->nullable();
            $table->string('cin_treur')->nullable();
            $table->string('avocat_tireur')->nullable();
            // end parties
            // montant creance came from dossier
            $table->float('montant_creance')->nullable();
            $table->date("date_depot")->nullable();
            $table->integer("accep_demande")->nullable();
            $table->string("huissier_designie")->nullable();
            $table->text("reslt_notif")->nullable();

            $table->integer('vald_pieces')->nullable();
            $table->string('provi')->nullable();
            $table->date('date_debut_plant')->nullable();
            $table->date('date_fin_plant')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saisie_fond_comers');
    }
}
