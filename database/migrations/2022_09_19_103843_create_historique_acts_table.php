<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriqueActsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historique_acts', function (Blueprint $table) {
            $table->id();

            $table->integer('id_user')->nullable();
            $table->string('what')->nullable();
            $table->string('where')->nullable();
            $table->integer('id_action')->nullable();
            $table->string('username')->nullable();
            $table->string('userlastname')->nullable();
            $table->integer('id_valid')->nullable();
            $table->text('other_data')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historique_acts');
    }
}
