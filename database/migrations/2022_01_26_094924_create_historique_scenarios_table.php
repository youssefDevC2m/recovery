<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriqueScenariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historique_scenarios', function (Blueprint $table) {
            $table->id();
            $table->integer('id_doss')->nullable();
            $table->string('mediateur')->nullable();
            $table->string('agence')->nullable();
            $table->date('date_remise')->nullable();
            $table->date('date_retoure')->nullable();
            $table->integer('nombre_tele_max')->nullable();
            $table->integer('nombre_courr_max')->nullable();
            $table->integer('nombre_visit_max')->nullable();
            $table->integer('nombre_convo_max')->nullable();
            $table->integer('nombre_valid_adrs_max')->nullable();
            $table->integer('nombre_medi_max')->nullable();
            $table->integer('nombre_situJud_max')->nullable();
            $table->integer('nombre_solvaDebit_max')->nullable();
            $table->integer('tel_is_sel')->nullable();
            $table->integer('courr_is_sel')->nullable();
            $table->integer('valid_adrs_is_sel')->nullable();
            $table->integer('visit_is_sel')->nullable();
            $table->integer('convo_is_sel')->nullable();
            $table->integer('cheque_ref')->nullable();
            $table->integer('injoncDpay')->nullable();
            $table->integer('medi_is_sel')->nullable();
            $table->integer('solvaDebit_is_sel')->nullable();
            $table->integer('situJud_is_sel')->nullable();
            $table->integer('result_exe_req_inj_pay')->nullable();
            $table->integer('SituationDirect')->nullable();
            $table->integer('reqinjoncDpay')->nullable();
            $table->integer('id_creator')->nullable();
            $table->integer('is_jurid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historique_scenarios');
    }
}
