<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFermetureDossiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fermeture_dossiers', function (Blueprint $table) {
            $table->id();
            
            $table->integer('id_doss')->nullable();
            $table->date('date_fermetur')->nullable();
            $table->string('statuFerm')->nullable();
            $table->text('rapportDecis')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fermeture_dossiers');
    }
}
