<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReqinjoncDpaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reqinjonc_dpays', function (Blueprint $table) {
            $table->id();
            
            $table->integer('id_hist_scen')->nullable();
            $table->text('proc_dep_req_inj_pay')->nullable();

            
            $table->string('les_proc_req_inj_pay')->nullable();
            $table->text('les_audins_req_inj_pay')->nullable();
            $table->text('les_preces_req_inj_pay')->nullable();
            
            $table->string('les_proc_app_req_inj_pay')->nullable();
            $table->date('dat_dep_req_inj_pay')->nullable();
            $table->string('num_app_req_inj_pay')->nullable();
            $table->date('dat_app_req_inj_pay')->nullable();
            $table->string('nom_consltnt_req_inj_pay')->nullable();
            $table->string('trib_competnt_req_inj_pay')->nullable();
            $table->text('note_et_audience_req_inj_pay')->nullable();
            $table->date('dat_dep_doss_au_trib_req_inj_pay')->nullable();
            $table->string('dep_doss_au_trib_req_inj_pay')->nullable();

            $table->string('swt_proc_notif_req_inj_pay')->nullable();
            $table->date('date_dep_notif_req_inj_pay')->nullable();
            $table->string('num_doss_notif_req_inj_pay')->nullable();
            $table->string('huissier_des_req_inj_pay')->nullable();
            $table->string('reslt_notif_req_inj_pay')->nullable();
            $table->date('date_appl_notif_req_inj_pay')->nullable();
            $table->text('proces_notif_req_inj_pay')->nullable();
            
            $table->string('swt_proc_exe_req_inj_pay')->nullable();
            $table->string('num_dos_exe_req_inj_pay')->nullable();
            $table->date('date_dos_exe_req_inj_pay')->nullable();
            $table->string('huissier_designe_exe_req_inj_pay')->nullable();
            $table->date('date_exe_req_inj_pay')->nullable();
            $table->string('expiration_exe_req_inj_pay')->nullable();
            $table->date('dat_saisie_req_inj_pay')->nullable(); 
            $table->string('type_saisie_req_inj_pay')->nullable();
            $table->date('date_exe_judic_req_inj_pay')->nullable();  
            $table->string('nom_expert_exe_req_inj_pay')->nullable(); 
            $table->string('pub_jal_req_inj_pay')->nullable(); 
            $table->string('nom_journal_req_inj_pay')->nullable(); 
            $table->string('vent_aux_encher_public_req_inj_pay')->nullable();
            $table->date('date_vent_exe_req_inj_pay')->nullable();
            $table->text('result_exe_req_inj_pay')->nullable();

            $table->integer('vald_pieces')->nullable();
            $table->string('provi')->nullable();
            $table->integer('avocat')->nullable();
            $table->date('date_debut_plant')->nullable();
            $table->date('date_fin_plant')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reqinjonc_dpays');
    }
}
