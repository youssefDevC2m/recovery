<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScenJudicInfoCheqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scen_judic_info_cheqs', function (Blueprint $table) {
            $table->id();
            $table->integer('id_hist_scen')->nullable();
            $table->float('montant_cheq')->nullable();
            $table->string('n_cheq')->nullable();
            $table->date('date_emission')->nullable();
            $table->string('motif')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scen_judic_info_cheqs');
    }
}
