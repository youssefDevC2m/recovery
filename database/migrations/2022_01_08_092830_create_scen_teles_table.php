<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScenTelesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scen_teles', function (Blueprint $table) {
            $table->id();

            $table->string('id_hist_scen')->nullable();
            $table->string('nom_charge')->nullable();
            $table->datetime('date_H_contact')->nullable();
            $table->string('type_tele')->nullable();
            $table->string('n_tele')->nullable();
            $table->text("desc_num_tel")->nullable();
            $table->text('promess_pay')->nullable();
            $table->string('date_promi')->nullable();
            $table->text('observation')->nullable();
            $table->integer('id_creator')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scen_teles');
    }
}
