<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citations', function (Blueprint $table) {
            $table->id();
            $table->integer('id_hist_scen');
            $table->string('cit_ref')->nullable();
            $table->string('cit_cod_jud')->nullable();
            $table->string('cit_trib_comp')->nullable();
            $table->string('cit_client')->nullable();
            $table->string('cit_advers')->nullable();
            $table->text('cit_sujet')->nullable();
            $table->text('cit_jugemen')->nullable();
            $table->string('cit_phas_app')->nullable();
            $table->string('cit_code_app')->nullable();
            $table->date('cit_date_app')->nullable();
            $table->text('cit_jugemen_difinit')->nullable();

            $table->integer('vald_pieces')->nullable();
            $table->string('provi')->nullable();
            $table->integer('avocat')->nullable();
            $table->date('date_debut_plant')->nullable();
            $table->date('date_fin_plant')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citations');
    }
}
