<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInjonctionDePayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('injonction__de__payers', function (Blueprint $table) {
            $table->id();
            $table->string('id_hist_scen')->nullable();
            $table->string('ref_inj_p')->nullable();
            $table->string('trib_com_inj_p')->nullable();
            $table->string('nom_jug_inj_p')->nullable();
            $table->string('tirer_inj_p')->nullable();
            $table->string('adrs_tirer_inj_p')->nullable();
            $table->string('Cin_tirer_inj_p')->nullable();
            $table->string('proc_tirer_inj_p')->nullable();
            $table->string('benef_inj_p')->nullable();
            $table->string('proc_benf_inj_p')->nullable();
            $table->string('Chrg_inj_p')->nullable();
            $table->string('bank_inj_p')->nullable();
            $table->string('adrs_bnk_inj_p')->nullable();
            $table->string('num_cmpt_bnk_inj_p')->nullable();
            $table->float('montant_glob_inj_pay')->nullable();
            $table->String('proc_depo_inj_pay')->nullable();
            $table->text('explication_dep_inj_pay')->nullable();
            $table->String('num_doss_inj_pay')->nullable();
            $table->date('date_jug_inj_pay')->nullable();
            $table->string('Accept_demnd_inj_pay')->nullable();
            $table->date('dt_retrt_inj_pay')->nullable();
            $table->string('nr_retrt_inj_pay')->nullable();
            $table->string('proc_notif_inj_pay')->nullable();
            $table->date('date_depot_inj_pay')->nullable();
            $table->string('nr_dos_notif_inj_pay')->nullable();
            $table->string('huiss_designe_inj_pay')->nullable();
            $table->string('result_notif_inj_pay')->nullable();
            $table->date('date_app_inj_pay')->nullable();
            $table->text('procd_proc_notif')->nullable();

            $table->string('proc_exec_inj_pay')->nullable();
            $table->string('nr_execution_inj_pay')->nullable();
            $table->date('date_execution_inj_pay')->nullable();
            $table->string('huiss_des_proc_exec_inj_pay')->nullable();
            $table->string('attes_del_pay_inj_pay')->nullable();
            $table->string('attes_defo_pay_inj_pay')->nullable();
            $table->string('expiration_inj_pay')->nullable();
            $table->date('dt_saisi_proc_exe_inj_p')->nullable();
            $table->string('type_saisie_inj_p')->nullable();
            $table->date('date_expert_judic_inj_p')->nullable();
            $table->string('nom_expert_inj_p')->nullable();
            $table->string('Public_j_a_l')->nullable();
            $table->string('nom_journal')->nullable();
            $table->string('vent_encher_publc')->nullable();
            $table->date('date_vent')->nullable();
            $table->text('result_inj_pay')->nullable();
            
            $table->string('proc_appel_inj_pay')->nullable();
            $table->date('date_depo_proc_appel')->nullable();
            $table->string('num_appel_proc_appel')->nullable();
            $table->date('date_apel_inj_p')->nullable();
            $table->string('nom_consult_inj_p')->nullable();
            $table->string('trib_comp_inj_p')->nullable();
            $table->text('note_audience')->nullable();

            $table->integer('vald_pieces')->nullable();
            $table->string('provi')->nullable();
            $table->integer('avocat')->nullable();
            $table->date('date_debut_plant')->nullable();
            $table->date('date_fin_plant')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('injonction__de__payers');
    }
}
