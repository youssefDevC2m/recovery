<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSenConvocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sen_convocations', function (Blueprint $table) {
            $table->id();
            $table->integer('id_hist_scen')->nullable();
            $table->string('remise_par')->nullable();
            $table->datetime('date_H_conv')->nullable();
            $table->datetime('date_echeance')->nullable();
            $table->string('obtenu_par')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sen_convocations');
    }
}
