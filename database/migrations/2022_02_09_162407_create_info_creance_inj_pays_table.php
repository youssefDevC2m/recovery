<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfoCreanceInjPaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_creance_inj_pays', function (Blueprint $table) {
            $table->id();
            $table->integer('id_inj_pays')->nullable();
            $table->float('montant_creanc_inj_pay')->nullable();
            $table->string('n_crance_inj_pay')->nullable();
            $table->date('date_echc_crenc_inj_pay')->nullable();
            $table->string('motif_crenc_inj__pay')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_creance_inj_pays');
    }
}
