<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskManagmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_managments', function (Blueprint $table) {
            $table->id();
            
            $table->integer('id_agent')->nullable();
            $table->integer('id_doss')->nullable();
            $table->string('commande')->nullable();
            $table->datetime('date_db')->nullable();
            $table->datetime('date_fin')->nullable();
            $table->integer('valid_task')->nullable();
            $table->integer('task_ag_vu')->nullable();
            $table->integer('creator')->nullable();
            $table->integer('admin_exp_vu')->nullable();
            $table->text('remarque')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_managments');
    }
}
