<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailDossiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_dossiers', function (Blueprint $table) {
            $table->id();
            $table->integer('id_doss');
            $table->string('type_pay')->nullable();
            $table->string('numero')->nullable();
            $table->string('bank')->nullable();
            $table->string('ville')->nullable();
            $table->string('motif')->nullable();
            $table->date('date_creation')->nullable();
            $table->date('date_ancent')->nullable();
            $table->integer('vue_alert_anc')->nullable();
            $table->string('date_rejet')->nullable();
            $table->float('mt_creance')->nullable();
            $table->float('mt_reclame')->nullable();
            $table->text('remarque')->nullable();
            $table->datetime('date_db_doss')->nullable();#added from scenario part
            $table->datetime('date_fin_doss')->nullable();#added from scenario part
            
            $table->timestamps();#added from scenario part
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_dossiers');
    }
}
