<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->String('type_cli')->nullable();
            $table->String('raison_sos')->nullable();
            $table->String('iterloc_prev')->nullable();
            $table->String('code_cli')->nullable();
            $table->String('qualit')->nullable();
            $table->float('capital')->nullable();
            $table->string('rc')->nullable();
            $table->string('adres_sos1')->nullable();
            $table->string('adres_sos2')->nullable();
            $table->string('statue_jurdq')->nullable();
            $table->string('activite')->nullable();
            $table->string('adres1')->nullable();
            $table->string('adres2')->nullable();
            $table->string('mndatr_partic')->nullable();
            $table->string('gerant1')->nullable();
            $table->string('gerant2')->nullable();
            $table->string('brand')->nullable();
            $table->string('ice_camp')->nullable();
            
            $table->string('sec_a_act')->nullable();
            $table->string('rc_client')->nullable();
            $table->date('date_parten')->nullable();
            $table->date('encenre_camp')->nullable();
            $table->date('date_paten')->nullable();
            $table->string('rep_legal_camp')->nullable();
            $table->string('ice')->nullable();
            
            // tele
            // contacte
            // interlocuteur
            $table->string('email')->nullable();

            $table->String('nom')->nullable();
           
            $table->String('nom_ar')->nullable();
            $table->String('prenom')->nullable(); 
            $table->String('prenom_ar')->nullable();
            $table->String('ville_perso')->nullable();
            $table->String('ville_perso_ar')->nullable();
            $table->String('ville_sos')->nullable();
            $table->String('ville_sos_ar')->nullable();
            $table->String('adress1')->nullable();
            $table->String('adress1_ar')->nullable();
            $table->String('adress2')->nullable();
            $table->String('adress2_ar')->nullable();
            $table->String('adressTrav1')->nullable();
            $table->String('adressTrav1_ar')->nullable();
            $table->String('adressTrav2')->nullable();
            $table->String('adressTrav2_ar')->nullable();
            $table->string('cin')->nullable();
            $table->string('pass_port')->nullable();
            $table->string('carte_sejour')->nullable();
            $table->string('nationalite')->nullable();
            $table->String('gender')->nullable();
            $table->date('date_naissance')->nullable();

            $table->integer('id_user')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
