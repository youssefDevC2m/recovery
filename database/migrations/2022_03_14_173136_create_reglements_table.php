<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReglementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reglements', function (Blueprint $table) {
            $table->id();
            $table->integer('id_doss')->nullable();
            $table->integer('id_cli')->nullable();
            $table->integer('id_debit')->nullable();
            $table->string('distin')->nullable();
            $table->string('mode_regl')->nullable();
            $table->string('type_regl')->nullable();
            $table->float('montant_reg')->nullable();
            $table->date('date_echeance')->nullable();
            $table->string('affectation')->nullable();
            $table->string('nature_reglement')->nullable();
            $table->text('remarque')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reglements');
    }
}
