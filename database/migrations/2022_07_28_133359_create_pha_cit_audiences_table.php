<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhaCitAudiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pha_cit_audiences', function (Blueprint $table) {
            $table->id();
            $table->integer('id_citaton');
            $table->date('pha_cit_date_audc')->nullable();
            $table->time('pha_cit_heur_audc')->nullable();
            $table->string('pha_cit_lieu_audc')->nullable();
            $table->text('pha_cit_resume')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pha_cit_audiences');
    }
}
