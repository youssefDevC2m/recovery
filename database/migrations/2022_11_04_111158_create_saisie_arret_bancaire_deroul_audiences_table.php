<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaisieArretBancaireDeroulAudiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saisie_arret_bancaire_deroul_audiences', function (Blueprint $table) {
            $table->id();
            $table->integer('id_arrb');
            $table->date('date_result')->nullable();
            $table->text('result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saisie_arret_bancaire_deroul_audiences');
    }
}
