<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScenJudicCheqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scen_judic_cheqs', function (Blueprint $table) {
            $table->id();
            $table->integer('id_hist_scen')->nullable();
            $table->string('ref_st_cheqe')->nullable();
            $table->string('tribunal_spese')->nullable();
            $table->string('tireur')->nullable();
            $table->string('adresse_cheq')->nullable();
            $table->string('cin_cheq')->nullable();
            $table->string('procureur_tir')->nullable();
            $table->string('benefic')->nullable();
            $table->string('procureur_tir_benif')->nullable();
            $table->string('charge_suiv')->nullable();
            $table->string('banque_tire')->nullable();
            $table->string('adrs_tire')->nullable();
            $table->string('n_compt')->nullable();
            $table->float('montant_glob')->nullable();
            //Procédure De La Plainte
            $table->string('plainte_ck')->nullable();
            $table->string('tribnal_compet_cheq')->nullable();
            $table->date('date_plaint_cheq')->nullable();
            $table->string('num_plaint_cheq')->nullable();
            $table->string('num_dos_penal')->nullable();
            $table->date('date_trans_autorite_center_cheq')->nullable();
            $table->date('DTAC')->nullable();
            $table->string('num_trans_cheq')->nullable();
            $table->string('sort_plainte_cheq')->nullable();
            $table->date('date_rappel_cheq')->nullable();
            $table->text('sort_rappel_cheq')->nullable();
            $table->string('DAI_cheq')->nullable();
            $table->date('date_audience')->nullable();
            $table->text('audiences_suivantes')->nullable();
            $table->text('jugement')->nullable();
            $table->date('DAJPI')->nullable();
            $table->string('OTCA')->nullable();
            $table->text('Rprt_plc_judic')->nullable();
            $table->string('polic_judic')->nullable();
            $table->string('chargeDossJurid')->nullable();
            $table->string('NCT')->nullable();
            $table->date('DTP_cheq')->nullable();
            $table->date('date_honorer')->nullable();
            $table->date('date_non_honor')->nullable();
            $table->text('p_j_faits_cheq')->nullable();
            $table->text('entiteDate')->nullable();
            // PHASE D’APPEL
            $table->string('appel_ck')->nullable();
            $table->string('tribunal_competnt_fazeApl_cheque')->nullable();
            $table->string('Ndos_fazeApl_cheque')->nullable();
            $table->date('dt_p_aud_fazeApl_cheque')->nullable();
            $table->text('aud_suivan_fazeApl_cheque')->nullable();
            $table->date('date_delib_fazeApl_cheque')->nullable();
            $table->text('dispo_deci_fazeApl_cheque')->nullable();
            $table->date('dt_retrait_jug_fazeApl_cheque')->nullable();
            $table->date('dt_dos_fazeApl_cheque')->nullable();
            $table->string('depo_dos_tribunal_fazeApl_cheque')->nullable();
            
            $table->integer('vald_pieces')->nullable();
            $table->string('provi')->nullable();
            $table->integer('avocat')->nullable();
            $table->date('date_debut_plant')->nullable();
            $table->date('date_fin_plant')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scen_judic_cheqs');
    }
}
