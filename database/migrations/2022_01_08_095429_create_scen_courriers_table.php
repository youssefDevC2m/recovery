<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScenCourriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scen_courriers', function (Blueprint $table) {
            $table->id();
            $table->integer('id_hist_scen')->nullable();
            $table->datetime('date_env')->nullable();
            $table->string('mise_dem_par')->nullable();
            $table->text('comment')->nullable();
            $table->integer('id_creator')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scen_courriers');
    }
}
