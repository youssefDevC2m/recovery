<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->integer('id_client')->nullable();
            $table->integer('id_dbtr')->nullable();
            $table->integer('id_partic')->nullable();//=id_client
            $table->integer('id_mandataire')->nullable();
            $table->string('tele')->nullable();
            $table->string('cellPhone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->text('contact')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
