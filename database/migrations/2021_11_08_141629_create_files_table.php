<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            
            $table->integer('id_detail')->nullable();
            $table->integer('id_ferm_dos')->nullable();
            $table->integer('id_client')->nullable();
            $table->integer('id_devis')->nullable();
            $table->integer('id_reg')->nullable();
            $table->integer('id_dt_partn')->nullable();
            $table->integer('id_task')->nullable(); 
            $table->integer('id_facture')->nullable();
            $table->integer('id_sfc')->nullable();
            $table->integer('id_td')->nullable();
            $table->integer('id_arrb')->nullable();
            $table->integer('id_aps')->nullable();
            $table->integer('id_smob')->nullable();
            $table->integer('id_imob')->nullable();
            
            $table->string('file_name')->nullable();
            $table->string('org_file_name')->nullable();
            $table->string('is_exicte')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
