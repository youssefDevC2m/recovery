-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           10.4.24-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Listage de la structure de table recouvrement. agents
CREATE TABLE IF NOT EXISTS `agents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.agents : ~0 rows (environ)

-- Listage de la structure de table recouvrement. banks
CREATE TABLE IF NOT EXISTS `banks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_dbtr` int(11) DEFAULT NULL,
  `banque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `n_compte` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.banks : ~9 rows (environ)
INSERT INTO `banks` (`id`, `id_dbtr`, `banque`, `agence`, `ville`, `n_compte`, `created_at`, `updated_at`) VALUES
	(2, 2, 'ss', 'test', 'casablanca', 'test', '2022-03-16 12:52:17', '2022-03-16 12:52:17'),
	(3, 3, '41', '41', '41', '41', '2022-04-08 15:32:01', '2022-04-08 15:32:01'),
	(4, 3, '41', '41', '41', '41', '2022-04-08 15:32:01', '2022-04-08 15:32:01'),
	(5, 5, '1001', '101', '010', '1010', '2022-04-10 16:27:07', '2022-04-10 16:27:07'),
	(6, 8, '4141', '414', '14', '1', '2022-04-12 14:03:59', '2022-04-12 14:03:59'),
	(7, 8, '41', '41', '141', '4', '2022-04-12 14:03:59', '2022-04-12 14:03:59'),
	(8, 9, '100', '10', '10', '10', '2022-04-12 14:09:15', '2022-04-12 14:09:15'),
	(9, 9, '100', '1', '1', '1', '2022-04-12 14:09:15', '2022-04-12 14:09:15'),
	(10, 10, 'test', 'dd', '100', '100', '2022-05-18 13:24:13', '2022-05-18 13:24:13');

-- Listage de la structure de table recouvrement. charges
CREATE TABLE IF NOT EXISTS `charges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_doss` int(11) DEFAULT NULL,
  `owner_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mode_reg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `montant` double(8,2) DEFAULT NULL,
  `nature_reg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `affect` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarque` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.charges : ~3 rows (environ)
INSERT INTO `charges` (`id`, `id_doss`, `owner_type`, `mode_reg`, `montant`, `nature_reg`, `affect`, `remarque`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Avocat', 'Espèces', 100.00, 'test', 'Compte Cabinet', 'test-300', '2022-03-28 10:53:17', '2022-04-04 14:21:32'),
	(5, 1, 'Avocat', 'Espèces', 100.00, '1000', 'Compte Cabinet', 'test', '2022-03-28 11:54:50', '2022-03-28 11:54:50'),
	(6, 1, 'Avocat', 'Espèces', 100.00, 'test', 'Caisse', 'test', '2022-04-04 14:20:52', '2022-04-04 14:20:52');

-- Listage de la structure de table recouvrement. clients
CREATE TABLE IF NOT EXISTS `clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_cli` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `raison_sos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iterloc_prev` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_cli` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capital` double DEFAULT NULL,
  `rc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adres_sos1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adres_sos2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statue_jurdq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activite` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adres1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adres2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mndatr_partic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gerant1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gerant2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville_perso` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville_perso_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville_sos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville_sos_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress1_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress2_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adressTrav1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adressTrav1_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adressTrav2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adressTrav2_ar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pass_port` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carte_sejour` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationalite` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.clients : ~2 rows (environ)
INSERT INTO `clients` (`id`, `type_cli`, `raison_sos`, `iterloc_prev`, `code_cli`, `qualit`, `capital`, `rc`, `adres_sos1`, `adres_sos2`, `statue_jurdq`, `activite`, `adres1`, `adres2`, `mndatr_partic`, `gerant1`, `gerant2`, `email`, `nom`, `nom_ar`, `prenom`, `prenom_ar`, `ville_perso`, `ville_perso_ar`, `ville_sos`, `ville_sos_ar`, `adress1`, `adress1_ar`, `adress2`, `adress2_ar`, `adressTrav1`, `adressTrav1_ar`, `adressTrav2`, `adressTrav2_ar`, `cin`, `pass_port`, `carte_sejour`, `nationalite`, `gender`, `date_naissance`, `id_user`, `created_at`, `updated_at`) VALUES
	(1, 'entre', 'c2m', 'wdwd', '1589', 'wdwdw', 999999.99, 'wdwdw', 'dwd', 'wdwwd', 'wdwdw', 'dwdd', NULL, NULL, NULL, 'wdw', 'dwdw', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-28 16:36:34', '2022-02-28 16:59:47'),
	(2, 'entre', '1414', NULL, '1414', '1414', 141, '1414', '1414', '1414', '141', '1414', NULL, NULL, NULL, '1414', '14414', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-03-01 11:03:25', '2022-03-01 11:03:25');

-- Listage de la structure de table recouvrement. contacts
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_client` int(11) DEFAULT NULL,
  `id_dbtr` int(11) DEFAULT NULL,
  `id_partic` int(11) DEFAULT NULL,
  `tele` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.contacts : ~10 rows (environ)
INSERT INTO `contacts` (`id`, `id_client`, `id_dbtr`, `id_partic`, `tele`, `fax`, `email`, `contact`, `created_at`, `updated_at`) VALUES
	(33, NULL, NULL, 1, '2525', NULL, NULL, NULL, '2022-02-28 16:59:47', '2022-02-28 16:59:47'),
	(34, NULL, NULL, 1, '5252', NULL, NULL, NULL, '2022-02-28 16:59:47', '2022-02-28 16:59:47'),
	(36, 2, NULL, NULL, '1414', NULL, NULL, NULL, '2022-03-01 11:03:25', '2022-03-01 11:03:25'),
	(37, 2, NULL, NULL, NULL, '41', NULL, NULL, '2022-03-01 11:03:25', '2022-03-01 11:03:25'),
	(38, 2, NULL, NULL, NULL, NULL, '141', NULL, '2022-03-01 11:03:25', '2022-03-01 11:03:25'),
	(39, 2, NULL, NULL, NULL, NULL, NULL, '4141', '2022-03-01 11:03:25', '2022-03-01 11:03:25'),
	(40, NULL, NULL, 2, '1414', NULL, NULL, NULL, '2022-03-01 11:03:25', '2022-03-01 11:03:25'),
	(41, NULL, NULL, 2, NULL, '1414', NULL, NULL, '2022-03-01 11:03:25', '2022-03-01 11:03:25'),
	(42, NULL, NULL, 2, NULL, NULL, '14', NULL, '2022-03-01 11:03:25', '2022-03-01 11:03:25'),
	(43, NULL, NULL, 2, NULL, NULL, NULL, '4141', '2022-03-01 11:03:25', '2022-03-01 11:03:25');

-- Listage de la structure de table recouvrement. conversations
CREATE TABLE IF NOT EXISTS `conversations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_task` int(11) DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seen` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.conversations : ~103 rows (environ)
INSERT INTO `conversations` (`id`, `id_user`, `id_task`, `message`, `seen`, `created_at`, `updated_at`) VALUES
	(1, 1, 4, '1111', 0, '2022-04-22 15:19:09', '2022-04-22 15:19:09'),
	(2, 1, 4, '1111111111', 0, '2022-04-22 15:19:41', '2022-04-22 15:19:41'),
	(3, 1, 4, 'ccccc', 0, '2022-04-22 15:20:36', '2022-04-22 15:20:36'),
	(4, 1, 4, '112122132121', 0, '2022-04-22 15:22:41', '2022-04-22 15:22:41'),
	(5, 1, 4, 'sxsxsx', 0, '2022-04-22 15:23:31', '2022-04-22 15:23:31'),
	(6, 1, 4, 'dcdcdc', 0, '2022-04-22 15:24:57', '2022-04-22 15:24:57'),
	(7, 2, 4, 'testfwef', 0, '2022-04-22 15:25:59', '2022-04-22 15:25:59'),
	(8, 1, 4, 'febvjebve', 0, '2022-04-22 15:26:03', '2022-04-22 15:26:03'),
	(9, 1, 4, 'vfvfdvfvfv', 0, '2022-04-22 15:26:08', '2022-04-22 15:26:08'),
	(10, 2, 4, 'rvesvev', 0, '2022-04-22 15:26:11', '2022-04-22 15:26:11'),
	(11, 2, 4, 'dede', 0, '2022-04-22 15:28:41', '2022-04-22 15:28:41'),
	(12, 2, 4, 'dsdsd', 0, '2022-04-22 15:29:45', '2022-04-22 15:29:45'),
	(13, 2, 4, 's', 0, '2022-04-22 15:29:59', '2022-04-22 15:29:59'),
	(14, 2, 4, '12121', 0, '2022-04-22 15:30:50', '2022-04-22 15:30:50'),
	(15, 2, 4, 'r', 0, '2022-04-22 15:31:36', '2022-04-22 15:31:36'),
	(16, 2, 4, 'rrr', 0, '2022-04-22 15:31:39', '2022-04-22 15:31:39'),
	(17, 2, 4, 'dfjbsdjfv', 0, '2022-04-22 15:32:30', '2022-04-22 15:32:30'),
	(18, 1, 4, 'egwrgwerg', 0, '2022-04-22 15:32:33', '2022-04-22 15:32:33'),
	(19, 2, 4, 'ergergerg', 0, '2022-04-22 15:32:36', '2022-04-22 15:32:36'),
	(20, 1, 4, 'wgwfew', 0, '2022-04-22 15:32:39', '2022-04-22 15:32:39'),
	(21, 1, 4, 'dfbdfbdfb', 0, '2022-04-22 15:32:59', '2022-04-22 15:32:59'),
	(22, 2, 4, 'dbdfbdfb', 0, '2022-04-22 15:33:01', '2022-04-22 15:33:01'),
	(23, 2, 4, 'hi i am youssef', 0, '2022-04-22 15:33:12', '2022-04-22 15:33:12'),
	(24, 1, 4, 'how ARE YOU', 0, '2022-04-22 15:33:22', '2022-04-22 15:33:22'),
	(25, 2, 4, 'do already know me', 0, '2022-04-22 15:33:36', '2022-04-22 15:33:36'),
	(26, 1, 4, 'yes i know', 0, '2022-04-22 15:33:49', '2022-04-22 15:33:49'),
	(27, 1, 4, 'how do you know', 0, '2022-04-22 15:34:34', '2022-04-22 15:34:34'),
	(28, 1, 4, 'it\'s impossible', 0, '2022-04-22 15:34:50', '2022-04-22 15:34:50'),
	(29, 1, 4, 'am a gost hahaha]', 0, '2022-04-22 15:34:57', '2022-04-22 15:34:57'),
	(30, 2, 4, NULL, 0, '2022-04-22 15:51:31', '2022-04-22 15:51:31'),
	(31, 2, 4, 'efef', 0, '2022-04-22 15:55:12', '2022-04-22 15:55:12'),
	(32, 1, 4, '111', 0, '2022-05-09 08:46:05', '2022-05-09 08:46:05'),
	(33, 1, 4, '222', 0, '2022-05-09 08:46:15', '2022-05-09 08:46:15'),
	(34, 1, 4, '222', 0, '2022-05-09 08:46:25', '2022-05-09 08:46:25'),
	(35, 1, 4, '888', 0, '2022-05-09 08:46:28', '2022-05-09 08:46:28'),
	(36, 1, 4, '1111', 0, '2022-05-09 12:08:51', '2022-05-09 12:08:51'),
	(37, 1, 4, '4777474744444', 0, '2022-05-09 12:09:34', '2022-05-09 12:09:34'),
	(38, 1, 4, 'test', 0, '2022-05-10 11:07:33', '2022-05-10 11:07:33'),
	(39, 1, 4, 'test', 0, '2022-05-10 11:07:40', '2022-05-10 11:07:40'),
	(40, 1, 4, '1141', 0, '2022-05-10 11:09:48', '2022-05-10 11:09:48'),
	(41, 1, 4, 'sss', 0, '2022-05-10 11:10:31', '2022-05-10 11:10:31'),
	(42, 1, 4, 'xddx', 0, '2022-05-10 11:11:38', '2022-05-10 11:11:38'),
	(43, 1, 4, 'ddx', 0, '2022-05-10 11:11:45', '2022-05-10 11:11:45'),
	(44, 1, 4, 'dddd', 0, '2022-05-10 11:13:28', '2022-05-10 11:13:28'),
	(45, 1, 4, 'cdcd', 0, '2022-05-10 11:20:01', '2022-05-10 11:20:01'),
	(46, 1, 4, 'cdcdc', 0, '2022-05-10 11:20:18', '2022-05-10 11:20:18'),
	(47, 1, 4, '114', 0, '2022-05-10 11:21:50', '2022-05-10 11:21:50'),
	(48, 1, 4, '1441', 0, '2022-05-10 11:24:44', '2022-05-10 11:24:44'),
	(49, 1, 4, 'ef', 0, '2022-05-10 11:26:01', '2022-05-10 11:26:01'),
	(50, 1, 4, 'fef', 0, '2022-05-10 11:26:04', '2022-05-10 11:26:04'),
	(51, 1, 4, '11', 0, '2022-05-10 11:34:08', '2022-05-10 11:34:08'),
	(52, 1, 4, '1111', 0, '2022-05-10 11:34:11', '2022-05-10 11:34:11'),
	(53, 1, 4, '1', 0, '2022-05-10 11:34:13', '2022-05-10 11:34:13'),
	(54, 1, 4, '1', 0, '2022-05-10 11:34:15', '2022-05-10 11:34:15'),
	(55, 1, 4, '1', 0, '2022-05-10 11:34:18', '2022-05-10 11:34:18'),
	(56, 1, 4, 'cdcdc', 0, '2022-05-10 11:50:33', '2022-05-10 11:50:33'),
	(57, 1, 4, 'frfrfrf', 0, '2022-05-10 11:54:33', '2022-05-10 11:54:33'),
	(58, 1, 4, 'xsx', 0, '2022-05-10 11:55:42', '2022-05-10 11:55:42'),
	(59, 1, 4, 'sxsx', 0, '2022-05-10 11:55:47', '2022-05-10 11:55:47'),
	(60, 1, 4, '112', 0, '2022-05-10 11:56:48', '2022-05-10 11:56:48'),
	(61, 1, 4, '11224', 0, '2022-05-10 11:56:52', '2022-05-10 11:56:52'),
	(62, 1, 4, '11224', 0, '2022-05-10 11:56:53', '2022-05-10 11:56:53'),
	(63, 1, 4, 'eded', 0, '2022-05-10 11:57:22', '2022-05-10 11:57:22'),
	(64, 1, 4, '1121', 0, '2022-05-10 12:00:35', '2022-05-10 12:00:35'),
	(65, 1, 4, '1414', 0, '2022-05-10 12:04:11', '2022-05-10 12:04:11'),
	(66, 1, 4, 'ededed', 0, '2022-05-10 13:24:01', '2022-05-10 13:24:01'),
	(67, 1, 4, '255', 0, '2022-05-10 13:24:21', '2022-05-10 13:24:21'),
	(68, 1, 4, 'ttrt', 0, '2022-05-10 13:46:24', '2022-05-10 13:46:24'),
	(69, 1, 4, 'ds', 0, '2022-05-10 14:05:53', '2022-05-10 14:05:53'),
	(70, 1, 4, 'sdsd', 0, '2022-05-10 14:58:13', '2022-05-10 14:58:13'),
	(71, 1, 4, '1441', 0, '2022-05-10 15:00:06', '2022-05-10 15:00:06'),
	(72, 1, 4, 'xs', 0, '2022-05-10 15:01:38', '2022-05-10 15:01:38'),
	(73, 2, 4, 'deded', 0, '2022-05-10 15:23:53', '2022-05-10 15:23:53'),
	(74, 2, 4, 'rfrfrffr', 0, '2022-05-10 15:25:22', '2022-05-10 15:25:22'),
	(75, 2, 4, 'frfrf', 0, '2022-05-10 15:26:52', '2022-05-10 15:26:52'),
	(76, 2, 4, '141414', 0, '2022-05-10 15:27:07', '2022-05-10 15:27:07'),
	(77, 2, 4, '1441', 0, '2022-05-10 15:28:55', '2022-05-10 15:28:55'),
	(78, 2, 4, '414141', 0, '2022-05-10 15:29:06', '2022-05-10 15:29:06'),
	(79, 1, 4, 'sdsds', 0, '2022-05-10 15:33:03', '2022-05-10 15:33:03'),
	(80, 2, 4, 'dsdsd', 0, '2022-05-10 15:33:16', '2022-05-10 15:33:16'),
	(81, 2, 4, 'tgtgtg', 0, '2022-05-10 15:34:18', '2022-05-10 15:34:18'),
	(82, 2, 4, 'eded', 0, '2022-05-10 15:34:55', '2022-05-10 15:34:55'),
	(83, 2, 4, 'rfrf', 0, '2022-05-10 15:39:28', '2022-05-10 15:39:28'),
	(84, 2, 4, 'dsdsd', 0, '2022-05-10 15:44:47', '2022-05-10 15:44:47'),
	(85, 2, 4, 'dd', 0, '2022-05-10 15:45:04', '2022-05-10 15:45:04'),
	(86, 2, 4, 'youssef', 0, '2022-05-10 15:45:23', '2022-05-10 15:45:23'),
	(87, 2, 4, 'e', 0, '2022-05-10 15:45:30', '2022-05-10 15:45:30'),
	(88, 2, 4, 'deded', 0, '2022-05-10 15:47:04', '2022-05-10 15:47:04'),
	(89, 1, 4, 'dedded', 0, '2022-05-10 15:47:45', '2022-05-10 15:47:45'),
	(90, 2, 4, 'yhyhy', 0, '2022-05-10 15:48:47', '2022-05-10 15:48:47'),
	(91, 2, 4, 'test', 0, '2022-05-10 15:49:57', '2022-05-10 15:49:57'),
	(92, 1, 4, 'test', 0, '2022-05-10 15:58:14', '2022-05-10 15:58:14'),
	(93, 2, 4, 'fdfdf', 0, '2022-05-10 15:58:19', '2022-05-10 15:58:19'),
	(94, 1, 4, 'sdsd', 0, '2022-05-10 15:59:24', '2022-05-10 15:59:24'),
	(95, 2, 4, '123123', 0, '2022-05-10 15:59:39', '2022-05-10 15:59:39'),
	(96, 1, 4, '11', 0, '2022-05-10 16:00:12', '2022-05-10 16:00:12'),
	(97, 2, 4, 'sdsd', 0, '2022-05-11 15:28:59', '2022-05-11 15:28:59'),
	(98, 2, 4, 'sdsd', 0, '2022-05-11 15:29:04', '2022-05-11 15:29:04'),
	(99, 2, 4, 'test', 0, '2022-05-13 09:13:01', '2022-05-13 09:13:01'),
	(100, 2, 4, 'bien recu', 0, '2022-05-13 09:14:24', '2022-05-13 09:14:24'),
	(101, 1, 9, 'tset', 0, '2022-05-13 09:55:21', '2022-05-13 09:55:21'),
	(102, 1, 4, 'rest', 0, '2022-05-13 14:14:15', '2022-05-13 14:14:15'),
	(103, 1, 4, 'dwdw', 0, '2022-05-13 14:22:22', '2022-05-13 14:22:22'),
	(104, 2, 9, 'test', 0, '2022-05-23 14:07:25', '2022-05-23 14:07:25'),
	(105, 2, 16, 'test', 0, '2022-05-24 11:38:18', '2022-05-24 11:38:18'),
	(106, 1, 17, 'test', 0, '2022-05-26 16:10:30', '2022-05-26 16:10:30'),
	(107, 2, 17, 'rtes', 0, '2022-05-26 16:11:31', '2022-05-26 16:11:31');

-- Listage de la structure de table recouvrement. debiteurs
CREATE TABLE IF NOT EXISTS `debiteurs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_dtbr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employeur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activite` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress_prv` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville_adress_prv` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress_pro` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville_adress_pro` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forme_jurdq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `if` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress_siege` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.debiteurs : ~10 rows (environ)
INSERT INTO `debiteurs` (`id`, `type_dtbr`, `nom`, `prenom`, `cin`, `alias`, `employeur`, `activite`, `adress_prv`, `ville_adress_prv`, `adress_pro`, `ville_adress_pro`, `forme_jurdq`, `rc`, `if`, `patent`, `adress_siege`, `ville`, `created_at`, `updated_at`) VALUES
	(1, 'perso', 'amhile', 'youssef', '1414', 'first step', 'test', 'dwdd', 'hay el oroude villa des roses nr 63', 'casablanca', 'hay el oroude villa des roses nr 63', NULL, '', '', '', '', '', '', '2022-03-01 08:48:09', '2022-03-01 08:48:09'),
	(2, 'entre', 'amhile', '', '', '', '', 'ss', '', 'casablanca', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2022-03-01 08:52:36', '2022-03-16 12:52:16'),
	(3, 'perso', '14', '41', '41', '41', '41', '4141', '41', '41', '41', '41', '', '', '', '', '', '', '2022-04-08 15:32:01', '2022-04-08 15:32:01'),
	(4, 'perso', 'amhile', 'youssef', NULL, NULL, NULL, NULL, 'hay el oroude villa des roses nr 63', 'casablanca', 'hay el oroude villa des roses nr 63', NULL, '', '', '', '', '', '', '2022-04-10 16:26:05', '2022-04-10 16:26:05'),
	(5, 'perso', 'amhile', 'youssef', NULL, NULL, NULL, NULL, 'hay el oroude villa des roses nr 63', 'casablanca', 'hay el oroude villa des roses nr 63', NULL, '', '', '', '', '', '', '2022-04-10 16:27:07', '2022-04-10 16:27:07'),
	(6, 'perso', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '2022-04-11 12:35:57', '2022-04-11 12:35:57'),
	(7, 'perso', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '2022-04-11 12:38:31', '2022-04-11 12:38:31'),
	(8, 'perso', 'amhile', 'youssef', '414', '141', NULL, NULL, 'hay el oroude villa des roses nr 63', 'casablanca', 'hay el oroude villa des roses nr 63', NULL, '', '', '', '', '', '', '2022-04-12 14:03:59', '2022-04-12 14:03:59'),
	(9, 'perso', '100', '100', '100', '100', '100', '10', '100', '100', '100', '111', '', '', '', '', '', '', '2022-04-12 14:09:15', '2022-04-12 14:09:15'),
	(10, 'entre', 'c2m', '', '', '', '', 'active', '', '', '', '', 'FJ 123', 'R.C TEST', 'IF :551', 'dd', 'test', 'test', '2022-05-18 13:24:13', '2022-05-18 13:24:13'),
	(11, 'perso', 'test', 'test', '100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '2022-05-26 15:22:52', '2022-05-26 15:22:52');

-- Listage de la structure de table recouvrement. detail_dossiers
CREATE TABLE IF NOT EXISTS `detail_dossiers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_doss` int(11) NOT NULL,
  `type_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numero` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motif` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_creation` date DEFAULT NULL,
  `date_ancent` date DEFAULT NULL,
  `vue_alert_anc` int(11) DEFAULT NULL,
  `date_rejet` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_creance` double DEFAULT NULL,
  `mt_reclame` double DEFAULT NULL,
  `remarque` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.detail_dossiers : ~9 rows (environ)
INSERT INTO `detail_dossiers` (`id`, `id_doss`, `type_pay`, `numero`, `bank`, `ville`, `motif`, `date_creation`, `date_ancent`, `vue_alert_anc`, `date_rejet`, `mt_creance`, `mt_reclame`, `remarque`, `created_at`, `updated_at`) VALUES
	(63, 2, 'Effet', '41', '3', '41', 'Endos irrégulier', '2022-04-08', '2022-04-08', NULL, '2022-04-08', 41, 41, '4', '2022-04-08 15:32:25', '2022-04-08 15:32:25'),
	(64, 3, 'Facture', NULL, '0', NULL, 'Insuffisance de provision', '2022-04-10', '2022-04-10', NULL, '2022-04-10', NULL, NULL, '', '2022-04-10 16:26:43', '2022-04-10 16:26:43'),
	(69, 1, 'Effet', 'test', '0', 'casablanca', 'Opposition pour perte ou vol', '1900-03-01', '1900-04-15', 1, '1900-03-01', 1000, 200, '', '2022-04-12 14:03:00', '2022-04-16 09:43:27'),
	(70, 1, 'Chèque', 'test', '0', '10', 'Endos irrégulier', '2022-03-14', '2022-03-12', NULL, '2022-03-14', 10, 10, '', '2022-04-12 14:03:00', '2022-04-12 14:03:00'),
	(72, 7, '0', NULL, '0', NULL, 'Insuffisance de provision', '2022-04-12', '1000-04-12', NULL, '2022-04-12', NULL, NULL, '', '2022-04-12 14:08:39', '2022-04-12 14:08:39'),
	(73, 8, 'Chèque', '10', '8', '100', 'Insuffisance de provision', '2022-04-12', '1111-04-12', 1, '2022-04-12', 100, 100, '1', '2022-04-12 14:09:46', '2022-05-23 09:49:38'),
	(74, 9, 'Chèque', '100', '10', 'casablanca', 'Non conformité de la somme en lettre et en chiffre', '2022-05-18', '2022-05-01', NULL, '2022-05-18', 100, 233, '', '2022-05-18 13:25:11', '2022-05-18 13:25:11'),
	(76, 6, 'Chèque', NULL, '0', NULL, 'Insuffisance de provision', '2022-04-12', '2022-04-12', NULL, '2022-04-12', 1100, 10, '', '2022-05-23 12:33:05', '2022-05-23 12:33:05'),
	(77, 5, '0', NULL, '0', NULL, 'Insuffisance de provision', '2022-04-11', '1998-04-11', NULL, '2022-04-11', NULL, NULL, '', '2022-05-23 12:33:31', '2022-05-23 12:33:31'),
	(78, 4, 'Chèque', '1010', '5', '1010', 'Opposition pour perte ou vol', '2022-04-10', '2022-04-10', NULL, '2022-04-10', 100, 1000, '1', '2022-05-23 12:33:55', '2022-05-23 12:33:55'),
	(79, 10, 'Chèque', '1000', '0', NULL, 'Insuffisance de provision', '2022-05-26', '2022-05-26', NULL, '2022-05-26', NULL, NULL, '', '2022-05-26 15:24:31', '2022-05-26 15:24:31');

-- Listage de la structure de table recouvrement. detail_factures
CREATE TABLE IF NOT EXISTS `detail_factures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_facture` int(11) DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `montant` double(8,2) DEFAULT NULL,
  `remise` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.detail_factures : ~12 rows (environ)
INSERT INTO `detail_factures` (`id`, `id_facture`, `designation`, `montant`, `remise`, `created_at`, `updated_at`) VALUES
	(14, 1, 'p-1', 10.00, 1, '2022-05-13 16:45:08', '2022-05-13 16:45:08'),
	(15, 1, '10', 10.00, 10, '2022-05-13 16:45:08', '2022-05-13 16:45:08'),
	(16, 2, 'p - 2', 10.00, 2, '2022-05-13 16:50:54', '2022-05-13 16:50:54'),
	(17, 2, 'p - 3', 10.00, NULL, '2022-05-13 16:50:54', '2022-05-13 16:50:54'),
	(18, 3, '100', 100.00, 1, '2022-05-24 14:14:50', '2022-05-24 14:14:50'),
	(19, 3, '100', 100.00, 12, '2022-05-24 14:14:50', '2022-05-24 14:14:50'),
	(20, 4, '100', 100.00, 100, '2022-05-24 14:15:52', '2022-05-24 14:15:52'),
	(21, 6, '110', 100.00, 10, '2022-05-24 14:50:47', '2022-05-24 14:50:47'),
	(22, 7, '100', 100.00, 22, '2022-05-24 15:56:49', '2022-05-24 15:56:49'),
	(23, 10, 'test', 10.00, 1, '2022-05-25 09:27:52', '2022-05-25 09:27:52'),
	(24, 10, 'produit', 10.00, 2, '2022-05-25 09:27:52', '2022-05-25 09:27:52'),
	(26, 11, '100', 233.00, 200, '2022-05-25 09:29:43', '2022-05-25 09:29:43'),
	(27, 12, 'des', 100.00, NULL, '2022-05-26 15:55:00', '2022-05-26 15:55:00');

-- Listage de la structure de table recouvrement. devis
CREATE TABLE IF NOT EXISTS `devis` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_doss` int(11) DEFAULT NULL,
  `date_devie` date DEFAULT NULL,
  `remarque` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valuer` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.devis : ~5 rows (environ)
INSERT INTO `devis` (`id`, `id_doss`, `date_devie`, `remarque`, `valuer`, `created_at`, `updated_at`) VALUES
	(1, 1, '2022-03-15', 'e Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard', 'Chèque', '2022-03-15 15:00:58', '2022-04-05 10:49:13'),
	(3, 1, '2022-03-15', '121', NULL, '2022-03-15 16:05:37', '2022-03-15 16:05:37'),
	(4, 1, '2022-03-21', '123', 'Chèque', '2022-03-21 10:22:21', '2022-04-05 11:05:51'),
	(5, 1, '2022-04-05', 'test', 'Chèque', '2022-04-05 10:57:49', '2022-04-05 10:57:49'),
	(6, 1, '2022-05-14', '10000', 'Chèque', '2022-05-14 10:22:07', '2022-05-14 10:26:26');

-- Listage de la structure de table recouvrement. dossiers
CREATE TABLE IF NOT EXISTS `dossiers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_client` int(11) DEFAULT NULL,
  `id_dbtr` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `see_all_doss` int(11) DEFAULT NULL,
  `represante_legal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tele_represante_legal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_represante_legal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_resep` datetime DEFAULT NULL,
  `date_ouverture` datetime DEFAULT NULL,
  `ref` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phase` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_glob_creance` double DEFAULT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date_db_doss` datetime DEFAULT NULL,
  `date_fin_doss` datetime DEFAULT NULL,
  `doss_exp_vu` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.dossiers : ~9 rows (environ)
INSERT INTO `dossiers` (`id`, `id_client`, `id_dbtr`, `creator_id`, `see_all_doss`, `represante_legal`, `tele_represante_legal`, `contact_represante_legal`, `date_resep`, `date_ouverture`, `ref`, `phase`, `mt_glob_creance`, `observation`, `created_at`, `updated_at`, `date_db_doss`, `date_fin_doss`, `doss_exp_vu`) VALUES
	(1, 1, 2, 1, NULL, NULL, NULL, NULL, '2022-03-01 00:00:00', '2022-03-01 09:52:15', '1001-g', 'Clôturée', 19999, NULL, '2022-03-01 08:52:36', '2022-05-31 11:53:05', '2022-05-22 14:19:00', '2022-05-19 14:13:00', 1),
	(2, 2, 3, 1, NULL, NULL, NULL, NULL, '2022-04-08 00:00:00', '2022-04-08 15:31:32', '1414', 'Clôturée', 41111, NULL, '2022-04-08 15:32:01', '2022-05-23 12:12:38', '2022-05-18 17:30:00', '2022-05-17 17:30:00', 1),
	(4, 1, 5, 0, NULL, NULL, NULL, NULL, '2022-04-10 00:00:00', '2022-04-10 16:26:56', '1000uu4', 'Clôturée', 0, NULL, '2022-04-10 16:27:07', '2022-05-23 12:35:05', '2022-05-23 13:33:00', '2022-05-22 13:33:00', 0),
	(5, 1, 6, 1, NULL, NULL, NULL, NULL, '2022-04-11 00:00:00', '2022-04-11 12:35:52', '1000uu433', 'Prise en charge', 0, NULL, '2022-04-11 12:35:57', '2022-05-26 14:39:25', '2022-05-23 13:18:00', '2022-05-22 13:18:00', 1),
	(6, 1, 7, 1, NULL, NULL, NULL, NULL, '2022-04-12 00:00:00', '2022-04-11 12:38:27', '1000uu43', 'Prise en charge', 0, NULL, '2022-04-11 12:38:31', '2022-05-31 12:08:04', '2022-05-26 15:39:00', '2022-05-26 15:39:00', 1),
	(7, 2, 8, 1, NULL, NULL, NULL, NULL, '2022-04-12 00:00:00', '2022-04-12 14:03:43', '100-oo', 'Clôturée', 1000, NULL, '2022-04-12 14:03:59', '2022-05-30 11:52:58', '2022-05-26 15:57:00', '2022-05-28 15:57:00', 1),
	(8, 2, 9, 1, NULL, NULL, NULL, NULL, '2022-04-12 00:00:00', '2022-04-12 14:08:43', '100-o', 'Prise en charge', 100, NULL, '2022-04-12 14:09:15', '2022-04-12 14:09:46', NULL, NULL, 0),
	(9, 2, 10, 2, NULL, NULL, NULL, NULL, '2022-05-18 00:00:00', '2022-05-18 14:22:42', '100455-G', 'Prise en charge', 100000, NULL, '2022-05-18 13:24:13', '2022-05-25 08:45:46', '2022-05-18 14:25:00', '2022-05-21 14:25:00', 1),
	(10, 1, 11, 1, NULL, NULL, NULL, NULL, '2022-05-26 00:00:00', '2022-05-26 16:22:17', '1000uu33', 'Prise en charge', 1000, NULL, '2022-05-26 15:22:52', '2022-05-26 15:25:29', '2022-05-26 16:25:00', '2022-07-31 16:25:00', 0);

-- Listage de la structure de table recouvrement. dossier_factures
CREATE TABLE IF NOT EXISTS `dossier_factures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_doss` int(11) DEFAULT NULL,
  `id_fact` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.dossier_factures : ~18 rows (environ)
INSERT INTO `dossier_factures` (`id`, `id_doss`, `id_fact`, `created_at`, `updated_at`) VALUES
	(1, 1, 7, '2022-05-24 16:17:02', '2022-05-24 16:17:02'),
	(2, 4, 7, '2022-05-24 16:17:03', '2022-05-24 16:17:03'),
	(3, 5, 7, '2022-05-24 16:17:03', '2022-05-24 16:17:03'),
	(10, 1, 9, '2022-05-25 09:12:30', '2022-05-25 09:12:30'),
	(11, 4, 9, '2022-05-25 09:12:30', '2022-05-25 09:12:30'),
	(12, 5, 9, '2022-05-25 09:12:30', '2022-05-25 09:12:30'),
	(13, 7, 9, '2022-05-25 09:12:30', '2022-05-25 09:12:30'),
	(22, 2, 5, '2022-05-25 09:22:13', '2022-05-25 09:22:13'),
	(23, 7, 5, '2022-05-25 09:22:13', '2022-05-25 09:22:13'),
	(24, 8, 5, '2022-05-25 09:22:13', '2022-05-25 09:22:13'),
	(25, 9, 5, '2022-05-25 09:22:13', '2022-05-25 09:22:13'),
	(26, 1, 10, '2022-05-25 09:27:52', '2022-05-25 09:27:52'),
	(27, 4, 10, '2022-05-25 09:27:52', '2022-05-25 09:27:52'),
	(28, 5, 10, '2022-05-25 09:27:52', '2022-05-25 09:27:52'),
	(29, 6, 10, '2022-05-25 09:27:52', '2022-05-25 09:27:52'),
	(30, 1, 11, '2022-05-25 09:29:43', '2022-05-25 09:29:43'),
	(31, 4, 11, '2022-05-25 09:29:43', '2022-05-25 09:29:43'),
	(32, 5, 11, '2022-05-25 09:29:43', '2022-05-25 09:29:43'),
	(33, 6, 11, '2022-05-25 09:29:43', '2022-05-25 09:29:43'),
	(34, 1, 12, '2022-05-26 15:55:29', '2022-05-26 15:55:29'),
	(35, 4, 12, '2022-05-26 15:55:29', '2022-05-26 15:55:29'),
	(36, 6, 12, '2022-05-26 15:55:29', '2022-05-26 15:55:29');

-- Listage de la structure de table recouvrement. factures
CREATE TABLE IF NOT EXISTS `factures` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `doss` int(11) DEFAULT NULL,
  `type_facture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_facture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarque` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `montant_total` double(8,2) DEFAULT NULL,
  `id_client` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.factures : ~10 rows (environ)
INSERT INTO `factures` (`id`, `doss`, `type_facture`, `date_facture`, `remarque`, `montant_total`, `id_client`, `created_at`, `updated_at`) VALUES
	(5, NULL, '877', '2022-05-24', '100', NULL, 2, '2022-05-24 14:49:28', '2022-05-25 09:22:13'),
	(6, NULL, '877', '2022-05-24', '100', NULL, 2, '2022-05-24 14:50:47', '2022-05-24 14:50:47'),
	(7, NULL, '100', '2022-05-24', 'test', NULL, 1, '2022-05-24 15:56:49', '2022-05-24 15:56:49'),
	(8, NULL, 'test', '2022-05-24', 'test', NULL, 1, '2022-05-24 16:23:15', '2022-05-24 16:23:15'),
	(9, NULL, 'test', '2022-05-24', 'test', NULL, 1, '2022-05-24 16:24:37', '2022-05-25 09:12:30'),
	(10, NULL, 'yesy1231', '2022-05-24', NULL, NULL, 1, '2022-05-24 16:31:31', '2022-05-25 09:27:52'),
	(11, NULL, '100', '2022-05-25', 'test', NULL, 1, '2022-05-25 09:29:02', '2022-05-25 09:29:43'),
	(12, NULL, 'type', '2022-05-26', NULL, NULL, 1, '2022-05-26 15:55:00', '2022-05-26 15:55:00'),
	(13, NULL, '100', '2022-05-25', '744', NULL, 1, '2022-05-30 09:56:29', '2022-05-30 09:56:29'),
	(14, NULL, '100', '2022-05-25', '744', NULL, 1, '2022-05-30 09:57:23', '2022-05-30 10:13:41');

-- Listage de la structure de table recouvrement. fermeture_dossiers
CREATE TABLE IF NOT EXISTS `fermeture_dossiers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_doss` int(11) DEFAULT NULL,
  `date_fermetur` date DEFAULT NULL,
  `statuFerm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rapportDecis` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.fermeture_dossiers : ~0 rows (environ)
INSERT INTO `fermeture_dossiers` (`id`, `id_doss`, `date_fermetur`, `statuFerm`, `rapportDecis`, `created_at`, `updated_at`) VALUES
	(1, 1, '2022-03-16', 'Cloture pour non resultat', 'test-236', '2022-03-16 11:14:14', '2022-05-18 13:43:28'),
	(2, 2, '2022-05-20', 'Retour au client', 'test', '2022-05-20 16:11:57', '2022-05-20 16:11:57'),
	(3, 4, '2022-05-23', 'Retour au client', '102', '2022-05-23 12:35:05', '2022-05-23 12:35:05'),
	(4, 7, '2022-05-26', 'Transfere au tribunal', 'test', '2022-05-26 14:58:22', '2022-05-26 14:58:22');

-- Listage de la structure de table recouvrement. files
CREATE TABLE IF NOT EXISTS `files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_detail` int(11) DEFAULT NULL,
  `id_ferm_dos` int(11) DEFAULT NULL,
  `id_client` int(11) DEFAULT NULL,
  `id_devis` int(11) DEFAULT NULL,
  `id_dt_partn` int(11) DEFAULT NULL,
  `id_reg` int(11) DEFAULT NULL,
  `id_task` int(11) DEFAULT NULL,
  `id_facture` int(11) DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org_file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_exicte` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.files : ~7 rows (environ)
INSERT INTO `files` (`id`, `id_detail`, `id_ferm_dos`, `id_client`, `id_devis`, `id_dt_partn`, `id_reg`, `id_task`, `id_facture`, `file_name`, `org_file_name`, `is_exicte`, `created_at`, `updated_at`) VALUES
	(43, NULL, NULL, NULL, NULL, NULL, 11, NULL, NULL, '6231f830edd73image_processing20210802-2754-uc47v2.gif', 'image_processing20210802-2754-uc47v2.gif', NULL, '2022-03-16 14:46:08', '2022-03-16 14:46:08'),
	(44, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, '624c20ad9e89bcanvas-eleves.xlsx', 'canvas-eleves.xlsx', NULL, '2022-04-05 10:57:49', '2022-04-05 10:57:49'),
	(56, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '624d8c3f4c63f624d7b858dc81canvas-eleves.xlsx', '624d7b858dc81canvas-eleves.xlsx', NULL, '2022-04-06 12:49:03', '2022-04-06 12:49:03'),
	(57, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, '625a9aa74be0bcanvas-classes.xlsx', 'canvas-classes.xlsx', NULL, '2022-04-16 10:29:59', '2022-04-16 10:29:59'),
	(64, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL, '628b939d64cd2cpnl.txt', 'cpnl.txt', NULL, '2022-05-23 14:01:01', '2022-05-23 14:01:01'),
	(69, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14, '629498bb1731862320c7618873image1.png', '62320c7618873image1.png', NULL, '2022-05-30 10:13:15', '2022-05-30 10:13:15'),
	(71, NULL, NULL, NULL, NULL, NULL, NULL, 19, NULL, '6295e0806cd6b62320c7618873image1.png', '62320c7618873image1.png', NULL, '2022-05-31 09:31:44', '2022-05-31 09:31:44');

-- Listage de la structure de table recouvrement. file_paths
CREATE TABLE IF NOT EXISTS `file_paths` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.file_paths : ~2 rows (environ)
INSERT INTO `file_paths` (`id`, `file_path`, `type`, `created_at`, `updated_at`) VALUES
	(1, 'files/users/', 'users', '2022-01-10 12:05:16', '2022-01-10 12:05:19'),
	(2, '/files/joints/', 'joints', '2022-01-10 12:08:32', '2022-01-10 12:08:32');

-- Listage de la structure de table recouvrement. gerants
CREATE TABLE IF NOT EXISTS `gerants` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_dbtr` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress_pincipale` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tele` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.gerants : ~0 rows (environ)

-- Listage de la structure de table recouvrement. historique_scenarios
CREATE TABLE IF NOT EXISTS `historique_scenarios` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_doss` int(11) DEFAULT NULL,
  `mediateur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agence` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_remise` date DEFAULT NULL,
  `date_retoure` date DEFAULT NULL,
  `nombre_tele_max` int(11) DEFAULT NULL,
  `nombre_courr_max` int(11) DEFAULT NULL,
  `nombre_visit_max` int(11) DEFAULT NULL,
  `nombre_valid_adrs_max` int(11) DEFAULT NULL,
  `nombre_medi_max` int(11) DEFAULT NULL,
  `nombre_convo_max` int(11) DEFAULT NULL,
  `nombre_situJud_max` int(11) DEFAULT NULL,
  `nombre_solvaDebit_max` int(11) DEFAULT NULL,
  `tel_is_sel` int(11) DEFAULT NULL,
  `courr_is_sel` int(11) DEFAULT NULL,
  `valid_adrs_is_sel` int(11) DEFAULT NULL,
  `visit_is_sel` int(11) DEFAULT NULL,
  `convo_is_sel` int(11) DEFAULT NULL,
  `cheque_ref` int(11) DEFAULT NULL,
  `injoncDpay` int(11) DEFAULT NULL,
  `medi_is_sel` int(11) DEFAULT NULL,
  `solvaDebit_is_sel` int(11) DEFAULT NULL,
  `situJud_is_sel` int(11) DEFAULT NULL,
  `reqinjoncDpay` int(11) DEFAULT NULL,
  `result_exe_req_inj_pay` int(11) DEFAULT NULL,
  `SituationDirect` int(11) DEFAULT NULL,
  `id_creator` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.historique_scenarios : ~8 rows (environ)
INSERT INTO `historique_scenarios` (`id`, `id_doss`, `mediateur`, `agence`, `date_remise`, `date_retoure`, `nombre_tele_max`, `nombre_courr_max`, `nombre_visit_max`, `nombre_valid_adrs_max`, `nombre_medi_max`, `nombre_convo_max`, `nombre_situJud_max`, `nombre_solvaDebit_max`, `tel_is_sel`, `courr_is_sel`, `valid_adrs_is_sel`, `visit_is_sel`, `convo_is_sel`, `cheque_ref`, `injoncDpay`, `medi_is_sel`, `solvaDebit_is_sel`, `situJud_is_sel`, `reqinjoncDpay`, `result_exe_req_inj_pay`, `SituationDirect`, `id_creator`, `created_at`, `updated_at`) VALUES
	(1, 1, '2', 'test', '2022-03-16', '2022-03-16', 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 1, '2022-03-16 16:09:27', '2022-05-18 14:06:03'),
	(4, 2, '0', NULL, '2022-04-08', '2022-04-08', 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 1, '2022-04-08 15:37:54', '2022-05-18 16:31:00'),
	(5, 4, '0', 'test', '2022-04-10', '2022-04-10', 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 1, '2022-04-10 16:27:52', '2022-05-23 12:34:10'),
	(6, 5, '0', 'test', '2022-04-11', '2022-04-11', 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 1, '2022-04-11 12:36:51', '2022-05-23 12:18:48'),
	(7, 6, '["0"]', 'tesst', '2022-04-12', '2022-04-12', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, NULL, 0, 1, '2022-04-12 10:22:09', '2022-05-26 14:40:39'),
	(8, 9, '2', 'tesst', '2022-05-18', '2022-05-18', 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 1, '2022-05-18 13:25:50', '2022-05-20 16:41:40'),
	(9, 7, '2', 'test', '2022-05-26', '2022-05-26', 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 1, '2022-05-26 14:57:26', '2022-05-26 14:57:53'),
	(10, 10, '2', 'test', '2022-05-26', '2022-05-26', 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 1, '2022-05-26 15:25:29', '2022-05-26 15:25:29');

-- Listage de la structure de table recouvrement. info_creance_inj_pays
CREATE TABLE IF NOT EXISTS `info_creance_inj_pays` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_inj_pays` int(11) DEFAULT NULL,
  `montant_creanc_inj_pay` double DEFAULT NULL,
  `n_crance_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_echc_crenc_inj_pay` date DEFAULT NULL,
  `motif_crenc_inj__pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.info_creance_inj_pays : ~11 rows (environ)
INSERT INTO `info_creance_inj_pays` (`id`, `id_inj_pays`, `montant_creanc_inj_pay`, `n_crance_inj_pay`, `date_echc_crenc_inj_pay`, `motif_crenc_inj__pay`, `created_at`, `updated_at`) VALUES
	(8, 1, NULL, NULL, '2022-03-01', NULL, '2022-03-01 12:22:40', '2022-03-01 12:22:40'),
	(9, 2, NULL, NULL, '2022-03-14', NULL, '2022-03-14 11:31:18', '2022-03-14 11:31:18'),
	(10, 1, 414, '14141', '2022-04-09', '4141', '2022-04-09 10:27:01', '2022-04-09 10:27:01'),
	(11, 2, NULL, NULL, '2022-04-09', NULL, '2022-04-09 10:29:50', '2022-04-09 10:29:50'),
	(12, 3, NULL, NULL, '2022-04-09', NULL, '2022-04-09 10:31:22', '2022-04-09 10:31:22'),
	(13, 4, NULL, NULL, '2022-04-09', NULL, '2022-04-09 10:31:42', '2022-04-09 10:31:42'),
	(14, 5, NULL, NULL, '2022-04-09', NULL, '2022-04-09 10:32:00', '2022-04-09 10:32:00'),
	(21, 6, 100, '100', '2022-04-09', 'oioio', '2022-04-09 11:25:33', '2022-04-09 11:25:33'),
	(22, 6, 1010, '10101', '2022-04-09', '788', '2022-04-09 11:25:33', '2022-04-09 11:25:33'),
	(24, 7, NULL, NULL, '2022-04-11', NULL, '2022-04-11 09:52:18', '2022-04-11 09:52:18'),
	(25, 8, NULL, NULL, '2022-04-11', NULL, '2022-04-11 09:54:13', '2022-04-11 09:54:13');

-- Listage de la structure de table recouvrement. injonction__de__payers
CREATE TABLE IF NOT EXISTS `injonction__de__payers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trib_com_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_jug_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tirer_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adrs_tirer_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Cin_tirer_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proc_tirer_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `benef_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proc_benf_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Chrg_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adrs_bnk_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_cmpt_bnk_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `montant_glob_inj_pay` double DEFAULT NULL,
  `proc_depo_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `explication_dep_inj_pay` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_doss_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_jug_inj_pay` date DEFAULT NULL,
  `Accept_demnd_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dt_retrt_inj_pay` date DEFAULT NULL,
  `nr_retrt_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proc_notif_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_depot_inj_pay` date DEFAULT NULL,
  `nr_dos_notif_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `huiss_designe_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result_notif_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_app_inj_pay` date DEFAULT NULL,
  `procd_proc_notif` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proc_exec_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nr_execution_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_execution_inj_pay` date DEFAULT NULL,
  `huiss_des_proc_exec_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attes_del_pay_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attes_defo_pay_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dt_saisi_proc_exe_inj_p` date DEFAULT NULL,
  `type_saisie_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_expert_judic_inj_p` date DEFAULT NULL,
  `nom_expert_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Public_j_a_l` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_journal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vent_encher_publc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_vent` date DEFAULT NULL,
  `result_inj_pay` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proc_appel_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_depo_proc_appel` date DEFAULT NULL,
  `num_appel_proc_appel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_apel_inj_p` date DEFAULT NULL,
  `nom_consult_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trib_comp_inj_p` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_audience` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.injonction__de__payers : ~0 rows (environ)

-- Listage de la structure de table recouvrement. migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.migrations : ~42 rows (environ)
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(64, '2014_10_12_000000_create_users_table', 1),
	(65, '2014_10_12_100000_create_password_resets_table', 1),
	(66, '2021_11_08_141629_create_files_table', 1),
	(67, '2021_11_09_085720_create_file_paths_table', 1),
	(68, '2022_01_07_085725_create_clients_table', 1),
	(69, '2022_01_07_091024_create_contacts_table', 1),
	(70, '2022_01_07_092542_create_gerants_table', 1),
	(71, '2022_01_07_092842_create_debiteurs_table', 1),
	(72, '2022_01_07_100056_create_banks_table', 1),
	(73, '2022_01_07_101155_create_dossiers_table', 1),
	(74, '2022_01_07_105009_create_detail_dossiers_table', 1),
	(75, '2022_01_08_092830_create_scen_teles_table', 1),
	(76, '2022_01_08_095429_create_scen_courriers_table', 1),
	(77, '2022_01_08_101802_create_sen_visite_domiciles_table', 1),
	(78, '2022_01_08_101927_create_sen_mediations_table', 1),
	(79, '2022_01_08_105801_create_sen_convocations_table', 1),
	(80, '2022_01_10_090847_create_situation_judiciaires_table', 1),
	(81, '2022_01_14_105039_create_temp_users_table', 1),
	(82, '2022_01_20_093338_create_type_creances_table', 1),
	(83, '2022_01_26_094924_create_historique_scenarios_table', 1),
	(84, '2022_02_01_095520_create_scen_judic_cheqs_table', 1),
	(85, '2022_02_01_103733_create_scen_judic_info_cheqs_table', 1),
	(86, '2022_02_04_102146_create_validation__dadresses_table', 1),
	(87, '2022_02_07_162914_create_solvabilite_debiteurs_table', 1),
	(88, '2022_02_08_110351_create_fermeture_dossiers_table', 1),
	(89, '2022_02_09_161618_create_injonction__de__payers_table', 1),
	(90, '2022_02_09_162407_create_info_creance_inj_pays_table', 1),
	(91, '2022_02_28_145057_create_reqinjonc_dpays_table', 1),
	(92, '2022_03_14_130233_create_situation_directs_table', 2),
	(93, '2022_03_14_130922_create_situation_direct_files_table', 2),
	(94, '2022_03_14_172943_create_devis_table', 3),
	(95, '2022_03_14_173136_create_reglements_table', 3),
	(96, '2022_03_14_174753_create_produits_table', 3),
	(130, '2022_03_25_104727_create_charges_table', 4),
	(131, '2022_04_05_120238_create_partners_table', 5),
	(132, '2022_04_06_093833_create_partner_details_table', 6),
	(133, '2022_04_16_105636_create_task_managments_table', 7),
	(137, '0000_00_00_000000_create_websockets_statistics_entries_table', 8),
	(138, '2022_04_17_184714_create_agents_table', 8),
	(139, '2022_04_18_223318_create_conversations_table', 8),
	(140, '2022_05_13_152912_create_factures_table', 9),
	(143, '2022_05_13_153328_create_detail_factures_table', 10),
	(144, '2022_05_24_164401_create_dossier_factures_table', 11);

-- Listage de la structure de table recouvrement. partners
CREATE TABLE IF NOT EXISTS `partners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_partn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `nom_part` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom_part` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adrs_part` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adrs_act_part` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tele_part` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tele_wts_part` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_part` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.partners : ~2 rows (environ)
INSERT INTO `partners` (`id`, `type_partn`, `agent_id`, `nom_part`, `prenom_part`, `adrs_part`, `adrs_act_part`, `tele_part`, `tele_wts_part`, `email_part`, `created_at`, `updated_at`) VALUES
	(2, 'Huissier', NULL, 'test', 'test-2', 'test', 'test', '060606060', '0606060', '0@0', '2022-04-05 15:24:01', '2022-04-05 15:52:33'),
	(7, 'Collaborateur', 5, 'agent 2', 'agent 2', 'test', 'test', 'test', '00000', 'agent2@gmail.com', '2022-05-26 14:33:14', '2022-05-26 14:33:14'),
	(8, 'Collaborateur', 2, 'agent', 'just do it', 'test', '123', '0606060', '100000', 'agent@gmail.com', '2022-05-26 14:37:01', '2022-05-26 14:37:01');

-- Listage de la structure de table recouvrement. partner_details
CREATE TABLE IF NOT EXISTS `partner_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_part` int(11) DEFAULT NULL,
  `doss` int(11) DEFAULT NULL,
  `dbteur` int(11) DEFAULT NULL,
  `vill_debtr` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mt_creance` float DEFAULT NULL,
  `motif_creance` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nature_creance` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_trans` date DEFAULT NULL,
  `remarque` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.partner_details : ~5 rows (environ)
INSERT INTO `partner_details` (`id`, `id_part`, `doss`, `dbteur`, `vill_debtr`, `mt_creance`, `motif_creance`, `nature_creance`, `date_trans`, `remarque`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, NULL, 'casablanca', 100, 'Endos irrégulier', 'Chèque', '2022-04-08', '1414', '2022-04-06 11:37:41', '2022-04-16 10:26:59'),
	(2, 2, 1, NULL, 'casablanca', 100, 'Non conformité de la somme en lettre et en chiffre', 'Facture', '2022-04-08', '1414', '2022-04-06 11:42:30', '2022-04-16 10:24:14'),
	(5, 2, 2, 3, '41', 100, 'Décédé', NULL, '2022-04-16', NULL, '2022-04-16 10:32:46', '2022-04-16 10:32:46'),
	(6, 2, 2, 3, '41', 1000, 'Prescrit', 'Facture', '2022-04-16', NULL, '2022-04-16 10:34:25', '2022-04-16 10:34:25'),
	(7, 3, 1, 2, 'casablanca', 100, 'Opposition pour perte ou vol', 'Effect', '2022-05-18', NULL, '2022-05-18 11:16:52', '2022-05-18 11:16:52');

-- Listage de la structure de table recouvrement. password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.password_resets : ~0 rows (environ)

-- Listage de la structure de table recouvrement. produits
CREATE TABLE IF NOT EXISTS `produits` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_devis` int(11) DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remise` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.produits : ~8 rows (environ)
INSERT INTO `produits` (`id`, `id_devis`, `montant`, `designation`, `remise`, `created_at`, `updated_at`) VALUES
	(20, 3, 212, 'hhhh', 121, '2022-03-15 16:05:37', '2022-03-15 16:05:37'),
	(44, 1, 101, '1010-655', 101, '2022-04-05 10:49:13', '2022-04-05 10:49:13'),
	(45, 1, 414, 'test', 141, '2022-04-05 10:49:13', '2022-04-05 10:49:13'),
	(46, 1, 100, 'test- 3', NULL, '2022-04-05 10:49:13', '2022-04-05 10:49:13'),
	(47, 5, 10, '10', NULL, '2022-04-05 10:57:49', '2022-04-05 10:57:49'),
	(48, 4, 100, 'tes-3', NULL, '2022-04-05 11:05:51', '2022-04-05 11:05:51'),
	(49, 4, 1000, 'test-2', NULL, '2022-04-05 11:05:51', '2022-04-05 11:05:51'),
	(56, 6, 100, '100', NULL, '2022-05-14 10:26:27', '2022-05-14 10:26:27');

-- Listage de la structure de table recouvrement. reglements
CREATE TABLE IF NOT EXISTS `reglements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_doss` int(11) DEFAULT NULL,
  `id_cli` int(11) DEFAULT NULL,
  `id_debit` int(11) DEFAULT NULL,
  `distin` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mode_regl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_regl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `montant_reg` double DEFAULT NULL,
  `date_echeance` date DEFAULT NULL,
  `affectation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nature_reglement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarque` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.reglements : ~4 rows (environ)
INSERT INTO `reglements` (`id`, `id_doss`, `id_cli`, `id_debit`, `distin`, `mode_regl`, `type_regl`, `montant_reg`, `date_echeance`, `affectation`, `nature_reglement`, `remarque`, `created_at`, `updated_at`) VALUES
	(8, 1, 1, NULL, 'cli', 'Espèces', 'Partial', 100, '2022-03-16', 'tesst', 'honoraire de clôture dossier', 'etes', '2022-03-16 14:18:14', '2022-03-16 14:18:14'),
	(9, 1, NULL, 2, 'avoc', 'Espèces', '', 100, '2022-03-16', '100', 'Honoraire de médiation', '100', '2022-03-16 14:20:42', '2022-03-16 14:20:42'),
	(10, 1, 1, NULL, 'cli', 'Espèces', 'Solder', 100.3, '2022-03-16', '10011', 'frais d\'overture', '100-2', '2022-03-16 14:35:45', '2022-03-16 14:40:17'),
	(11, 1, NULL, 2, 'debit', 'Carte bancaire', 'Solder', 100.03, '2022-03-16', 'test', 'Règlement créance', 'test', '2022-03-16 14:46:08', '2022-03-16 14:46:08');

-- Listage de la structure de table recouvrement. reqinjonc_dpays
CREATE TABLE IF NOT EXISTS `reqinjonc_dpays` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `proc_dep_req_inj_pay` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `les_proc_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `les_audins_req_inj_pay` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `les_preces_req_inj_pay` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `les_proc_app_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dat_dep_req_inj_pay` date DEFAULT NULL,
  `num_app_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dat_app_req_inj_pay` date DEFAULT NULL,
  `nom_consltnt_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trib_competnt_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_et_audience_req_inj_pay` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dat_dep_doss_au_trib_req_inj_pay` date DEFAULT NULL,
  `dep_doss_au_trib_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `swt_proc_notif_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_dep_notif_req_inj_pay` date DEFAULT NULL,
  `num_doss_notif_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `huissier_des_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reslt_notif_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_appl_notif_req_inj_pay` date DEFAULT NULL,
  `proces_notif_req_inj_pay` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `swt_proc_exe_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_dos_exe_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_dos_exe_req_inj_pay` date DEFAULT NULL,
  `huissier_designe_exe_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_exe_req_inj_pay` date DEFAULT NULL,
  `expiration_exe_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dat_saisie_req_inj_pay` date DEFAULT NULL,
  `type_saisie_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_exe_judic_req_inj_pay` date DEFAULT NULL,
  `nom_expert_exe_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pub_jal_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_journal_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vent_aux_encher_public_req_inj_pay` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_vent_exe_req_inj_pay` date DEFAULT NULL,
  `result_exe_req_inj_pay` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.reqinjonc_dpays : ~4 rows (environ)
INSERT INTO `reqinjonc_dpays` (`id`, `id_hist_scen`, `proc_dep_req_inj_pay`, `les_proc_req_inj_pay`, `les_audins_req_inj_pay`, `les_preces_req_inj_pay`, `les_proc_app_req_inj_pay`, `dat_dep_req_inj_pay`, `num_app_req_inj_pay`, `dat_app_req_inj_pay`, `nom_consltnt_req_inj_pay`, `trib_competnt_req_inj_pay`, `note_et_audience_req_inj_pay`, `dat_dep_doss_au_trib_req_inj_pay`, `dep_doss_au_trib_req_inj_pay`, `swt_proc_notif_req_inj_pay`, `date_dep_notif_req_inj_pay`, `num_doss_notif_req_inj_pay`, `huissier_des_req_inj_pay`, `reslt_notif_req_inj_pay`, `date_appl_notif_req_inj_pay`, `proces_notif_req_inj_pay`, `swt_proc_exe_req_inj_pay`, `num_dos_exe_req_inj_pay`, `date_dos_exe_req_inj_pay`, `huissier_designe_exe_req_inj_pay`, `date_exe_req_inj_pay`, `expiration_exe_req_inj_pay`, `dat_saisie_req_inj_pay`, `type_saisie_req_inj_pay`, `date_exe_judic_req_inj_pay`, `nom_expert_exe_req_inj_pay`, `pub_jal_req_inj_pay`, `nom_journal_req_inj_pay`, `vent_aux_encher_public_req_inj_pay`, `date_vent_exe_req_inj_pay`, `result_exe_req_inj_pay`, `created_at`, `updated_at`) VALUES
	(1, 2, 'cddc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-03-01 09:52:59', '2022-03-01 09:52:59'),
	(2, 4, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-10 15:37:12', '2022-04-10 15:37:12'),
	(3, 4, 'tesyt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-10 15:37:42', '2022-04-10 15:37:42'),
	(4, 4, 'test-13', 'on', '77878', '77878', 'on', '2022-04-10', '2211', '2022-04-10', '2222', '2221', '111', '2022-04-10', '12121', 'on', '2022-04-10', '2022-04-10', '2', '1212', '2022-04-10', '12121', 'on', '1212', '2022-04-10', '2121', '2022-04-02', '1212', '2022-04-10', '121', '2022-04-10', '12121', '12121', '212', '212121', '2022-04-10', '21212', '2022-04-10 15:39:54', '2022-04-10 16:15:20');

-- Listage de la structure de table recouvrement. scen_courriers
CREATE TABLE IF NOT EXISTS `scen_courriers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `date_env` datetime DEFAULT NULL,
  `mise_dem_par` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_creator` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.scen_courriers : ~1 rows (environ)
INSERT INTO `scen_courriers` (`id`, `id_hist_scen`, `date_env`, `mise_dem_par`, `comment`, `id_creator`, `created_at`, `updated_at`) VALUES
	(2, 7, '2022-04-12 12:10:00', '', NULL, 1, '2022-05-26 14:40:40', '2022-05-26 14:40:40');

-- Listage de la structure de table recouvrement. scen_judic_cheqs
CREATE TABLE IF NOT EXISTS `scen_judic_cheqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `ref_st_cheqe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tribunal_spese` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tireur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse_cheq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cin_cheq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `procureur_tir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `benefic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `procureur_tir_benif` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `charge_suiv` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banque_tire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adrs_tire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `n_compt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `montant_glob` double(8,2) DEFAULT NULL,
  `plainte_ck` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tribnal_compet_cheq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_plaint_cheq` date DEFAULT NULL,
  `num_plaint_cheq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_dos_penal` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_trans_autorite_center_cheq` date DEFAULT NULL,
  `DTAC` date DEFAULT NULL,
  `num_trans_cheq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_plainte_cheq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_rappel_cheq` date DEFAULT NULL,
  `sort_rappel_cheq` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DAI_cheq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_audience` date DEFAULT NULL,
  `audiences_suivantes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jugement` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DAJPI` date DEFAULT NULL,
  `OTCA` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Rprt_plc_judic` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `polic_judic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chargeDossJurid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NCT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DTP_cheq` date DEFAULT NULL,
  `date_honorer` date DEFAULT NULL,
  `date_non_honor` date DEFAULT NULL,
  `p_j_faits_cheq` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entiteDate` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appel_ck` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tribunal_competnt_fazeApl_cheque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Ndos_fazeApl_cheque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dt_p_aud_fazeApl_cheque` date DEFAULT NULL,
  `aud_suivan_fazeApl_cheque` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_delib_fazeApl_cheque` date DEFAULT NULL,
  `dispo_deci_fazeApl_cheque` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dt_retrait_jug_fazeApl_cheque` date DEFAULT NULL,
  `dt_dos_fazeApl_cheque` date DEFAULT NULL,
  `depo_dos_tribunal_fazeApl_cheque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.scen_judic_cheqs : ~0 rows (environ)

-- Listage de la structure de table recouvrement. scen_judic_info_cheqs
CREATE TABLE IF NOT EXISTS `scen_judic_info_cheqs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `montant_cheq` double(8,2) DEFAULT NULL,
  `n_cheq` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_emission` date DEFAULT NULL,
  `motif` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.scen_judic_info_cheqs : ~14 rows (environ)
INSERT INTO `scen_judic_info_cheqs` (`id`, `id_hist_scen`, `montant_cheq`, `n_cheq`, `date_emission`, `motif`, `created_at`, `updated_at`) VALUES
	(2, NULL, 414.00, '4414', '1970-01-01', '4141', '2022-04-08 15:08:48', '2022-04-08 15:08:48'),
	(3, NULL, 4114.00, '4114', '2022-04-08', '141', '2022-04-08 15:08:48', '2022-04-08 15:08:48'),
	(8, 1, 474.00, '74747', '2022-01-15', '7474', '2022-04-08 15:18:19', '2022-04-08 15:18:19'),
	(11, 4, NULL, NULL, '2022-04-08', NULL, '2022-04-08 15:46:51', '2022-04-08 15:46:51'),
	(12, 5, 414.00, '1414', '2022-04-08', '141', '2022-04-08 15:50:11', '2022-04-08 15:50:11'),
	(13, 6, NULL, NULL, '2022-04-08', NULL, '2022-04-08 15:53:33', '2022-04-08 15:53:33'),
	(14, 7, NULL, NULL, '2022-04-08', NULL, '2022-04-08 15:59:00', '2022-04-08 15:59:00'),
	(15, 8, 10101.00, '101', '2022-04-08', '101010', '2022-04-08 15:59:46', '2022-04-08 15:59:46'),
	(16, 9, NULL, NULL, '2022-04-09', NULL, '2022-04-09 09:28:40', '2022-04-09 09:28:40'),
	(17, 10, NULL, NULL, '2022-04-09', NULL, '2022-04-09 09:35:15', '2022-04-09 09:35:15'),
	(18, 9, NULL, NULL, '2022-04-09', NULL, '2022-04-10 16:17:00', '2022-04-10 16:17:00'),
	(19, 9, NULL, NULL, '2022-04-09', NULL, '2022-04-10 16:17:01', '2022-04-10 16:17:01'),
	(20, 9, 10.00, '101', '2022-04-09', '101', '2022-04-10 16:20:06', '2022-04-10 16:20:06'),
	(21, 10, NULL, NULL, '2022-04-09', NULL, '2022-04-10 16:25:34', '2022-04-10 16:25:34');

-- Listage de la structure de table recouvrement. scen_teles
CREATE TABLE IF NOT EXISTS `scen_teles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nom_charge` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_H_contact` datetime DEFAULT NULL,
  `type_tele` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `n_tele` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_num_tel` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promess_pay` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_promi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_creator` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.scen_teles : ~1 rows (environ)
INSERT INTO `scen_teles` (`id`, `id_hist_scen`, `nom_charge`, `date_H_contact`, `type_tele`, `n_tele`, `desc_num_tel`, `promess_pay`, `date_promi`, `observation`, `id_creator`, `created_at`, `updated_at`) VALUES
	(2, '7', NULL, '2022-04-12 12:10:00', '0', NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-26 14:40:39', '2022-05-26 14:40:39');

-- Listage de la structure de table recouvrement. sen_convocations
CREATE TABLE IF NOT EXISTS `sen_convocations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `remise_par` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_H_conv` datetime DEFAULT NULL,
  `date_echeance` datetime DEFAULT NULL,
  `obtenu_par` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.sen_convocations : ~1 rows (environ)
INSERT INTO `sen_convocations` (`id`, `id_hist_scen`, `remise_par`, `date_H_conv`, `date_echeance`, `obtenu_par`, `comment`, `created_at`, `updated_at`) VALUES
	(2, 7, NULL, '2022-04-12 12:10:00', NULL, '', NULL, '2022-05-26 14:40:40', '2022-05-26 14:40:40');

-- Listage de la structure de table recouvrement. sen_mediations
CREATE TABLE IF NOT EXISTS `sen_mediations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `lieu_med` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress_med` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_med` datetime DEFAULT NULL,
  `mediateur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `debiteur_mandataire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promesse` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_promi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.sen_mediations : ~1 rows (environ)
INSERT INTO `sen_mediations` (`id`, `id_hist_scen`, `lieu_med`, `adress_med`, `date_med`, `mediateur`, `debiteur_mandataire`, `promesse`, `date_promi`, `comment`, `created_at`, `updated_at`) VALUES
	(2, 7, '0', NULL, '2022-04-12 12:10:00', '0', NULL, NULL, NULL, NULL, '2022-05-26 14:40:40', '2022-05-26 14:40:40');

-- Listage de la structure de table recouvrement. sen_visite_domiciles
CREATE TABLE IF NOT EXISTS `sen_visite_domiciles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `adress_perso` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress_pro` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adress_proche` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_H_visite` datetime DEFAULT NULL,
  `visiteur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promess` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_promi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `domicile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.sen_visite_domiciles : ~1 rows (environ)
INSERT INTO `sen_visite_domiciles` (`id`, `id_hist_scen`, `adress_perso`, `adress_pro`, `adress_proche`, `date_H_visite`, `visiteur`, `promess`, `date_promi`, `comment`, `domicile`, `created_at`, `updated_at`) VALUES
	(2, 7, '', '', '', '2022-04-12 12:10:00', '2022-04-12 12:10:00', NULL, NULL, NULL, '0', '2022-05-26 14:40:40', '2022-05-26 14:40:40');

-- Listage de la structure de table recouvrement. situation_directs
CREATE TABLE IF NOT EXISTS `situation_directs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `remarque` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.situation_directs : ~0 rows (environ)

-- Listage de la structure de table recouvrement. situation_direct_files
CREATE TABLE IF NOT EXISTS `situation_direct_files` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_st_dir` int(11) DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org_file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.situation_direct_files : ~3 rows (environ)
INSERT INTO `situation_direct_files` (`id`, `id_st_dir`, `file_name`, `org_file_name`, `created_at`, `updated_at`) VALUES
	(6, 2, '622f4eea86215template.jpg', 'template.jpg', '2022-03-14 14:19:22', '2022-03-14 14:19:22'),
	(7, 3, '62320bb71c73aimage1.png', 'image1.png', '2022-03-16 16:09:27', '2022-03-16 16:09:27'),
	(8, 2, '62384fe6d21b4622f426a0bbc8exportation_11_03_22.xls', '622f426a0bbc8exportation_11_03_22.xls', '2022-03-21 10:13:58', '2022-03-21 10:13:58');

-- Listage de la structure de table recouvrement. situation_judiciaires
CREATE TABLE IF NOT EXISTS `situation_judiciaires` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `type_debiteur` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_situation_judic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.situation_judiciaires : ~1 rows (environ)
INSERT INTO `situation_judiciaires` (`id`, `id_hist_scen`, `type_debiteur`, `type_situation_judic`, `comment`, `created_at`, `updated_at`) VALUES
	(2, 7, NULL, '', NULL, '2022-05-26 14:40:40', '2022-05-26 14:40:40');

-- Listage de la structure de table recouvrement. solvabilite_debiteurs
CREATE TABLE IF NOT EXISTS `solvabilite_debiteurs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `bien_personnels` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `td` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hertage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `participation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_solv_deb` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.solvabilite_debiteurs : ~1 rows (environ)
INSERT INTO `solvabilite_debiteurs` (`id`, `id_hist_scen`, `bien_personnels`, `td`, `hertage`, `participation`, `comment_solv_deb`, `created_at`, `updated_at`) VALUES
	(2, 7, NULL, NULL, NULL, NULL, NULL, '2022-05-26 14:40:40', '2022-05-26 14:40:40');

-- Listage de la structure de table recouvrement. task_managments
CREATE TABLE IF NOT EXISTS `task_managments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_agent` int(11) DEFAULT NULL,
  `id_doss` int(11) DEFAULT NULL,
  `commande` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_db` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `valid_task` int(11) DEFAULT NULL,
  `task_ag_vu` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `admin_exp_vu` int(11) DEFAULT NULL,
  `remarque` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.task_managments : ~15 rows (environ)
INSERT INTO `task_managments` (`id`, `id_agent`, `id_doss`, `commande`, `date_db`, `date_fin`, `valid_task`, `task_ag_vu`, `creator`, `admin_exp_vu`, `remarque`, `created_at`, `updated_at`) VALUES
	(4, 2, 2, 'Visite domiciliaire', '2022-04-16 00:00:00', '2022-04-16 00:00:00', 0, NULL, NULL, NULL, 'scscscs', '2022-04-16 13:40:15', '2022-06-01 11:32:10'),
	(5, 2, 1, 'Validation d\'adresse', '2022-04-16 00:00:00', '2022-04-19 00:00:00', NULL, NULL, NULL, NULL, 'test', '2022-04-16 13:41:18', '2022-04-16 14:14:00'),
	(6, 2, 2, 'Visite domiciliaire', '2022-04-16 00:00:00', '2022-04-16 00:00:00', NULL, NULL, NULL, NULL, 'tfgdvajvdhwvfc', '2022-04-16 13:42:31', '2022-04-16 13:42:31'),
	(7, 5, 1, 'Visite domiciliaire', '2022-04-19 00:00:00', '2022-04-19 00:00:00', NULL, NULL, NULL, NULL, NULL, '2022-04-19 11:04:51', '2022-04-19 11:04:51'),
	(8, 2, 1, 'Notification', '2022-05-13 00:00:00', '2022-05-08 00:00:00', NULL, NULL, NULL, NULL, 'test', '2022-05-13 09:10:56', '2022-05-13 09:10:56'),
	(9, 2, 1, 'Notification', '2022-05-13 00:00:00', '2022-05-13 00:00:00', NULL, NULL, NULL, NULL, 'test', '2022-05-13 09:54:26', '2022-05-13 09:54:26'),
	(10, 2, 1, 'Validation d\'adresse', '2022-05-13 00:00:00', '2022-05-13 00:00:00', NULL, NULL, 1, NULL, NULL, '2022-05-13 09:56:05', '2022-05-13 09:56:05'),
	(11, 2, 1, 'Visite domiciliaire', '2022-05-23 15:00:00', '2022-05-23 15:02:00', 1, 1, NULL, 1, '123', '2022-05-23 14:01:01', '2022-05-24 11:19:08'),
	(12, 2, 5, 'Validation d\'adresse', '2022-05-24 12:20:00', '2022-05-26 12:20:00', 0, NULL, 1, NULL, 'test', '2022-05-24 11:20:16', '2022-05-24 12:14:18'),
	(13, 2, 2, 'Notification', '2022-05-24 12:24:00', '2022-06-25 12:24:00', 1, NULL, 1, 1, '100', '2022-05-24 11:25:04', '2022-05-30 11:52:36'),
	(14, 2, 4, 'Visite domiciliaire', '2022-05-24 12:27:00', '2022-05-24 12:27:00', NULL, NULL, 1, NULL, '1.032', '2022-05-24 11:27:35', '2022-05-24 11:27:35'),
	(15, 2, 6, 'Notification', '2022-05-24 12:29:00', '2022-05-24 12:29:00', 1, 1, 1, 1, 'zzz', '2022-05-24 11:29:31', '2022-05-24 11:41:27'),
	(16, 2, 8, 'Notification', '2022-05-24 12:30:00', '2022-05-24 12:30:00', 1, 1, 1, 1, 'ssss', '2022-05-24 11:30:14', '2022-05-24 11:52:13'),
	(17, 2, 5, 'Notification', '2022-05-26 17:00:00', '2022-05-26 17:00:00', 1, 1, 1, 1, 'test', '2022-05-26 16:08:12', '2022-05-26 16:11:20'),
	(18, 2, 4, 'Validation d\'adresse', '2022-05-26 17:11:00', '2022-05-26 17:11:00', 1, NULL, 1, 1, NULL, '2022-05-26 16:12:12', '2022-05-26 16:13:54'),
	(19, 5, 2, 'Validation d\'adresse', '2022-05-31 10:31:00', '2022-05-31 10:31:00', 1, NULL, 1, 1, 'Fond & consigne', '2022-05-31 09:31:44', '2022-05-31 12:08:08');

-- Listage de la structure de table recouvrement. temp_users
CREATE TABLE IF NOT EXISTS `temp_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tele` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `temp_users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.temp_users : ~0 rows (environ)

-- Listage de la structure de table recouvrement. type_creances
CREATE TABLE IF NOT EXISTS `type_creances` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duree_echeance` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.type_creances : ~0 rows (environ)

-- Listage de la structure de table recouvrement. users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tele` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `temp_pwd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_client` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acs_user` int(11) DEFAULT NULL,
  `acs_tabord` int(11) DEFAULT NULL,
  `give_priv_user` int(11) DEFAULT NULL,
  `acs_cli` int(11) DEFAULT NULL,
  `add_cli` int(11) DEFAULT NULL,
  `edit_cli` int(11) DEFAULT NULL,
  `delet_cli` int(11) DEFAULT NULL,
  `list_cli` int(11) DEFAULT NULL,
  `read_only_cli` int(11) DEFAULT NULL,
  `see_doss_cli` int(11) DEFAULT NULL,
  `acs_doss` int(11) DEFAULT NULL,
  `see_all_doss` int(11) DEFAULT NULL,
  `acs_scens_doss` int(11) DEFAULT NULL,
  `add_doss` int(11) DEFAULT NULL,
  `edit_doss` int(11) DEFAULT NULL,
  `delet_doss` int(11) DEFAULT NULL,
  `list_doss` int(11) DEFAULT NULL,
  `read_only_doss` int(11) DEFAULT NULL,
  `edit_info_cli_doss` int(11) DEFAULT NULL,
  `see_info_cli_doss` int(11) DEFAULT NULL,
  `edit_info_dbitr_doss` int(11) DEFAULT NULL,
  `see_info_dbitr_doss` int(11) DEFAULT NULL,
  `see_fermetur_doss` int(11) DEFAULT NULL,
  `edit_fermetur_doss` int(11) DEFAULT NULL,
  `acs_devis` int(11) DEFAULT NULL,
  `add_devis` int(11) DEFAULT NULL,
  `edit_devis` int(11) DEFAULT NULL,
  `delet_devis` int(11) DEFAULT NULL,
  `list_devis` int(11) DEFAULT NULL,
  `print_devis` int(11) DEFAULT NULL,
  `see_devis` int(11) DEFAULT NULL,
  `acs_fact` int(11) DEFAULT NULL,
  `add_fact` int(11) DEFAULT NULL,
  `edit_fact` int(11) DEFAULT NULL,
  `delet_fact` int(11) DEFAULT NULL,
  `list_fact` int(11) DEFAULT NULL,
  `see_fact` int(11) DEFAULT NULL,
  `acs_rglmnt` int(11) DEFAULT NULL,
  `add_rglmnt` int(11) DEFAULT NULL,
  `edit_rglmnt` int(11) DEFAULT NULL,
  `delet_rglmnt` int(11) DEFAULT NULL,
  `list_rglmnt` int(11) DEFAULT NULL,
  `see_rglmnt` int(11) DEFAULT NULL,
  `acs_charge` int(11) DEFAULT NULL,
  `add_charge` int(11) DEFAULT NULL,
  `edit_charge` int(11) DEFAULT NULL,
  `delet_charge` int(11) DEFAULT NULL,
  `list_charge` int(11) DEFAULT NULL,
  `see_charge` int(11) DEFAULT NULL,
  `acs_parten` int(11) DEFAULT NULL,
  `add_parten` int(11) DEFAULT NULL,
  `edit_parten` int(11) DEFAULT NULL,
  `delet_parten` int(11) DEFAULT NULL,
  `list_parten` int(11) DEFAULT NULL,
  `see_infos_parten` int(11) DEFAULT NULL,
  `see_parten` int(11) DEFAULT NULL,
  `acs_task` int(11) DEFAULT NULL,
  `add_task` int(11) DEFAULT NULL,
  `edit_task` int(11) DEFAULT NULL,
  `valid_task` int(11) DEFAULT NULL,
  `delet_task` int(11) DEFAULT NULL,
  `list_task` int(11) DEFAULT NULL,
  `suivi_task` int(11) DEFAULT NULL,
  `see_dt_task` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.users : ~3 rows (environ)
INSERT INTO `users` (`id`, `nom`, `prenom`, `email`, `tele`, `address`, `temp_pwd`, `photo`, `email_verified_at`, `role`, `password`, `id_client`, `acs_user`, `acs_tabord`, `give_priv_user`, `acs_cli`, `add_cli`, `edit_cli`, `delet_cli`, `list_cli`, `read_only_cli`, `see_doss_cli`, `acs_doss`, `see_all_doss`, `acs_scens_doss`, `add_doss`, `edit_doss`, `delet_doss`, `list_doss`, `read_only_doss`, `edit_info_cli_doss`, `see_info_cli_doss`, `edit_info_dbitr_doss`, `see_info_dbitr_doss`, `see_fermetur_doss`, `edit_fermetur_doss`, `acs_devis`, `add_devis`, `edit_devis`, `delet_devis`, `list_devis`, `print_devis`, `see_devis`, `acs_fact`, `add_fact`, `edit_fact`, `delet_fact`, `list_fact`, `see_fact`, `acs_rglmnt`, `add_rglmnt`, `edit_rglmnt`, `delet_rglmnt`, `list_rglmnt`, `see_rglmnt`, `acs_charge`, `add_charge`, `edit_charge`, `delet_charge`, `list_charge`, `see_charge`, `acs_parten`, `add_parten`, `edit_parten`, `delet_parten`, `list_parten`, `see_infos_parten`, `see_parten`, `acs_task`, `add_task`, `edit_task`, `valid_task`, `delet_task`, `list_task`, `suivi_task`, `see_dt_task`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin', 'admin@gmail.com', '0', 'hay el oroude villa des roses nr 63', 'ss', NULL, NULL, 'Administrateur', '$2y$10$/tLdqH/SUpaDNeECxWTFcufwKOsDBfJ4KiwsrsGa5JMIgfKRx4XAq', NULL, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, NULL, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 'H1vQuzJXkKxpSFtdizQrgmmCumukdG7FNA3IJPHzWE1iJtOoNxatColTZJRF', '2021-09-11 10:07:28', '2022-06-01 14:17:00'),
	(2, 'agent', 'just do it', 'agent@gmail.com', '0606060', 'test', '123456', NULL, NULL, 'Agent', '$2y$10$/tLdqH/SUpaDNeECxWTFcufwKOsDBfJ4KiwsrsGa5JMIgfKRx4XAq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-16 13:26:55', '2022-05-13 10:45:35'),
	(5, 'agent 2', 'agent 2', 'agent2@gmail.com', NULL, NULL, '123456', NULL, NULL, 'Agent', '$2y$10$uuhpWE7qijQoxi.nbtwamOHblflx37Qe9Zx15PyJTnTPzO8OI.59.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 11:03:30', '2022-04-19 11:03:30'),
	(8, 'test', 'test', 'test123@jj', NULL, NULL, '123456', NULL, NULL, 'Administrateur', '$2y$10$ZCmM29Vicgs3zPOmSd6fm.p8tV.yQutLQLxFCBUnAE3PPfG9.r6Zm', NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 1, 0, 0, 0, NULL, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-30 15:17:16', '2022-05-30 16:43:28'),
	(9, 'test', 'test', 'test123@jj7', NULL, NULL, '123456', NULL, NULL, 'Agent', '$2y$10$88fbHRm5q5urkRVgqL.MAOATtilJKj85QWzOmx8ZDBnbOKybyP2Qm', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, '2022-06-01 15:29:37', '2022-06-01 15:29:37');

-- Listage de la structure de table recouvrement. validation__dadresses
CREATE TABLE IF NOT EXISTS `validation__dadresses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_hist_scen` int(11) DEFAULT NULL,
  `date_valid_adres` date DEFAULT NULL,
  `effect_par` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_debit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_vali_adrs` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.validation__dadresses : ~2 rows (environ)
INSERT INTO `validation__dadresses` (`id`, `id_hist_scen`, `date_valid_adres`, `effect_par`, `type_debit`, `comment_vali_adrs`, `created_at`, `updated_at`) VALUES
	(4, 4, '2022-04-12', NULL, '', NULL, '2022-05-18 16:31:00', '2022-05-18 16:31:00'),
	(5, 7, '2022-04-12', NULL, '', NULL, '2022-05-26 14:40:40', '2022-05-26 14:40:40');

-- Listage de la structure de table recouvrement. websockets_statistics_entries
CREATE TABLE IF NOT EXISTS `websockets_statistics_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peak_connection_count` int(11) NOT NULL,
  `websocket_message_count` int(11) NOT NULL,
  `api_message_count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table recouvrement.websockets_statistics_entries : ~0 rows (environ)

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
