<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
    protected $fillable=[
        'doss',
        'id_client',
        'type_facture',
        'date_facture',
        'remarque',
        'montant_total'
    ];
}
