<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SenMediation extends Model
{
    protected $fillable=[
        'id_hist_scen',
        'lieu_med',
        'adress_med',
        'date_med',
        'mediateur',
        'debiteur_mandataire',
        'promesse',
        'date_promi',
        'comment'
    ];
   
}
