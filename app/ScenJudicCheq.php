<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScenJudicCheq extends Model
{
    protected $fillable=[
        'id_hist_scen',
        'ref_st_cheqe',
        'tribunal_spese',
        'tireur',
        'adresse_cheq',
        'cin_cheq',
        'procureur_tir',
        'benefic',
        'procureur_tir_benif',
        'charge_suiv',
        'banque_tire',
        'adrs_tire',
        'n_compt',
        'montant_glob',

        'plainte_ck',
        'tribnal_compet_cheq',
        'date_plaint_cheq',
        'num_plaint_cheq',
        'num_dos_penal',
        'date_trans_autorite_center_cheq',
        'DTAC',
        'num_trans_cheq',
        'sort_plainte_cheq',
        'date_rappel_cheq',
        'sort_rappel_cheq',
        'DAI_cheq',
        'date_audience',
        'date_honorer',
        'audiences_suivantes',
        'jugement',
        'DAJPI',
        'OTCA',
        'Rprt_plc_judic',
        'polic_judic',
        'chargeDossJurid',
        'NCT',
        'p_j_faits_cheq',
        'DTP_cheq',
        'date_non_honor',
        'entiteDate',

        'appel_ck',
        'tribunal_competnt_fazeApl_cheque',
        'Ndos_fazeApl_cheque',
        'dt_p_aud_fazeApl_cheque',
        'aud_suivan_fazeApl_cheque',
        'date_delib_fazeApl_cheque',
        'dispo_deci_fazeApl_cheque',
        'dt_retrait_jug_fazeApl_cheque',
        'dt_dos_fazeApl_cheque',
        'depo_dos_tribunal_fazeApl_cheque',
        
        'vald_pieces',
        'provi',
        'avocat',
        'date_debut_plant',
        'date_fin_plant'
    ];
    
}
