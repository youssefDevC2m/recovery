<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhaCitAudiences extends Model
{
    protected $fillable = [
        'id_citaton',
        'pha_cit_date_audc',
        'pha_cit_heur_audc',
        'pha_cit_lieu_audc',
        'pha_cit_resume'
    ];
}
