<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solvabilite_debiteur extends Model
{
    protected $fillable=[
        'id_hist_scen',
        'bien_personnels',
        'td',
        'hertage',
        'participation',
        'comment_solv_deb'
    ];
}
