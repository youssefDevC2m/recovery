<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SituationDirect extends Model
{
    protected $fillable=[
        'id_hist_scen',
        'remarque'
    ];
}
