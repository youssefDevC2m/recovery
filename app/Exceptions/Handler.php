<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Auth;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        // if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
        //     return route('login');
        // }
        
        return parent::render($request, $exception);
    }
    // protected function unauthenticated($request, AuthenticationException $exception)
    // {
    //     // dd(Auth::user());
    //     // return parent::render($request, $exception);
        
    //     // $guard = array_get($exception->guards(), 0);
    //     $var = (isset(Auth::user()->role))?Auth::user()->role : '';
    //     switch ($var) {
    //         case 'Agent':
    //             dd('agent');
    //         break;
    //         case 'Admin':
    //             dd('admin');
    //         break;
            
    //         default:
    //             return redirect()->guest(url('login'));
    //         break;
    //     }
        
    // }
}
