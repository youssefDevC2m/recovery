<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SenConvocation extends Model
{
    protected $fillable=[
        'id_hist_scen','remise_par','date_H_conv','date_echeance','obtenu_par','comment'
    ];
}
