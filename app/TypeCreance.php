<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeCreance extends Model
{
    protected $fillable=[
        'designation','description','duree_echeance'
    ];
}
