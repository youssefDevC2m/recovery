<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debiteur extends Model
{
    protected $fillable=[
        'type_dtbr',
        'nom',
        'prenom',
        'cin',
        'alias',
        'employeur',
        'activite',
        'adress_prv',
        'ville_adress_prv',
        'adress_pro',
        'ville_adress_pro',
        'forme_jurdq',
        'rc',
        'if',
        'patent',
        'adress_siege',
        'ville'
    ];
}
