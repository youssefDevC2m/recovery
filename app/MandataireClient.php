<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MandataireClient extends Model
{
    protected $fillable =[
        'id_client',
        'nom'
    ];
}
