<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard = 'admins';
    protected $fillable = [
        'nom',
        'prenom', 
        'email', 
        'password',
        'tele',
        'address',
        'temp_pwd',
        'role',
        'photo',
        'id_client',

// tab bor------------->
        'acs_tabord',
// end tab bor -------->

//previleges user -------------->
        'acs_user',
        'give_priv_user',
        'droit_de_valid_acts',
//end previleges user ---------->

// client priv---------->
        'acs_cli',
        'add_cli',
        'edit_cli',
        'delet_cli',
        'list_cli',
        'read_only_cli',
        'see_doss_cli',
// end client priv------>

// dossier privs-------->
        'acs_doss',
        'see_all_doss',
        'add_doss',
        'edit_doss',
        'delet_doss',
        'list_doss',
        'read_only_doss',
        'see_info_cli_doss',
        'edit_info_dbitr_doss',
        'see_info_dbitr_doss',
        'acs_scens_doss',
        'see_fermetur_doss',
        'edit_fermetur_doss',
        'acs_phas_ami',
        'acs_phas_judis',

// end dossier privs------->

// devis priv-------------->
        'acs_devis',
        'add_devis',
        'edit_devis',
        'delet_devis',
        'list_devis',
        'print_devis',
        'see_devis',
// end devis privs--------->

// facture prive--------->
        'acs_fact',
        'add_fact',
        'edit_fact',
        'delet_fact',
        'list_fact',
        'see_fact',
// end facture prive--------->

// reglement prive--------->
        'acs_rglmnt',
        'add_rglmnt',
        'edit_rglmnt',
        'delet_rglmnt',
        'list_rglmnt',
        'see_rglmnt',
// end reglement prive--------->

// charge prive--------->
        'acs_charge',
        'add_charge',
        'edit_charge',
        'delet_charge',
        'list_charge',
        'see_charge',
// end charge prive--------->

// partenaire priv----------->
        'acs_parten',
        'add_parten',
        'edit_parten',
        'delet_parten',
        'list_parten',
        'see_infos_parten',
        'see_parten',
// end partenaire priv----------->

// gestion taches priv------------>
        'acs_task',
        'add_task',
        'edit_task',
        'delet_task',
        'list_task',
        'valid_task',
        'suivi_task',
        'see_dt_task',
// end gestion taches priv-------->
    ];
 



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function conversation(){
        return $this->hasMany('App\Conversation','id_user');
    }
}
