<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Injonction_De_Payer extends Model
{
    protected $fillable=[
        'id_hist_scen',
        'ref_inj_p',
        'trib_com_inj_p',
        'nom_jug_inj_p',
        'tirer_inj_p',
        'adrs_tirer_inj_p',
        'Cin_tirer_inj_p',
        'proc_tirer_inj_p',
        'benef_inj_p',
        'proc_benf_inj_p',

        'Chrg_inj_p',
        'bank_inj_p',
        'adrs_bnk_inj_p',
        'num_cmpt_bnk_inj_p',
        'montant_glob_inj_pay',

        'proc_depo_inj_pay',
        'explication_dep_inj_pay',
        'num_doss_inj_pay',
        'date_jug_inj_pay',
        'Accept_demnd_inj_pay',
        'dt_retrt_inj_pay',
        'nr_retrt_inj_pay',

        'proc_notif_inj_pay',
        'date_depot_inj_pay',
        'nr_dos_notif_inj_pay',
        'huiss_designe_inj_pay',
        'result_notif_inj_pay',
        'date_app_inj_pay',
        'procd_proc_notif',
        
        'proc_exec_inj_pay',
        'nr_execution_inj_pay',
        'date_execution_inj_pay',
        'huiss_des_proc_exec_inj_pay',
        'attes_del_pay_inj_pay',
        'attes_defo_pay_inj_pay',
        'expiration_inj_pay',
        'dt_saisi_proc_exe_inj_p',
        'type_saisie_inj_p',
        'date_expert_judic_inj_p',
        'nom_expert_inj_p',
        'Public_j_a_l',
        'nom_journal',
        'vent_encher_publc',
        'date_vent',
        'result_inj_pay',
        'proc_appel_inj_pay',
        'date_depo_proc_appel',
        'num_appel_proc_appel',
        'date_apel_inj_p',
        'nom_consult_inj_p',
        'trib_comp_inj_p',
        'note_audience',

        'vald_pieces',
        'provi',
        'avocat',
        'date_debut_plant',
        'date_fin_plant'

    ];
}
