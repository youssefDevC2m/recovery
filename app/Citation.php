<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citation extends Model
{
    protected $fillable = [
        'id_hist_scen',
        'cit_ref',
        'cit_cod_jud',
        'cit_trib_comp',
        'cit_client',
        'cit_advers',
        'cit_sujet',
        'cit_jugemen',

        'cit_phas_app',

        'cit_code_app',
        'cit_date_app',
        'cit_jugemen_difinit',
        
        'vald_pieces',
        'provi',
        'avocat',
        'date_debut_plant',
        'date_fin_plant',
    ];
}
