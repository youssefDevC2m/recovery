<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SituationJudiciaire extends Model
{
    protected $fillable=[
        'id_hist_scen',
        'type_debiteur',
        'type_situation_judic',
        'comment'
    ];
     
}