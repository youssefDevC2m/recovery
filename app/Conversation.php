<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable=[
        'id_user','id_task','message','seen'
    ];

    public function users(){
        return belongsTo('App\User','id_user');
    }
}
