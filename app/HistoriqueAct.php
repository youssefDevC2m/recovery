<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoriqueAct extends Model
{
    protected $fillable=[
        'id_user',
        'what',
        'where',
        'id_action',
        'username',
        'userlastname',
        'id_valid',
        'other_data'
    ];
}
