<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaisieMobiler extends Model
{
    protected $fillable = [
        'id_hist_scen',
        'tribunal_competant',
        'non_juge',
        'ref',
        'benificiaire',
        'avoca_benif',
        'charger_suivi_binif',
        'le_tireur',
        'address_tireur',
        'cin_treur',
        'avocat_tireur',
        'montant_creance',
        'date_depot',
        'accep_demande',
        'huissier_designie',
        'reslt_notif',

        'vald_pieces',
        'provi',
        'date_debut_plant',
        'date_fin_plant',
    ];
}
