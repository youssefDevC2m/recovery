<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoCreance_inj_pay extends Model
{
    protected $fillable=[
        'id_inj_pays',
        'montant_creanc_inj_pay',
        'n_crance_inj_pay',
        'date_echc_crenc_inj_pay',
        'motif_crenc_inj__pay'
    ];
}
