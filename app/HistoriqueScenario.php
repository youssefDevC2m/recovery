<?php

namespace App;
use App\ScenJudicCheq;
use Illuminate\Database\Eloquent\Model;

class HistoriqueScenario extends Model
{
        protected $fillable=[
            'id_doss',
            'mediateur',
            'agence',
            'date_remise',
            'date_retoure',
            'nombre_tele_max',
            'nombre_courr_max',
            'nombre_visit_max',
            'nombre_convo_max',
            'nombre_valid_adrs_max',
            'nombre_medi_max',
            'nombre_situJud_max',
            'nombre_solvaDebit_max',
            'tel_is_sel',
            'courr_is_sel',
            'valid_adrs_is_sel',
            'visit_is_sel',
            'convo_is_sel',
            'medi_is_sel',
            'situJud_is_sel',
            'solvaDebit_is_sel',
            'cheque_ref',
            'id_creator',
            'injoncDpay',
            'reqinjoncDpay',
            'SituationDirect',
            'is_jurid'
        ];

        public function situCheque(){
            return $this->hasMany(ScenJudicCheq::class,'id_hist_scen');
        }
        
}
