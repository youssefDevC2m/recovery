<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Charges extends Model
{
    protected $fillable=[
        'id_doss',
        'owner_type',
        'mode_reg',
        'montant',
        'nature_reg',
        'affect',
         
        'remarque'
    ];
}
