<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailFacture extends Model
{
    protected $fillable= [
        'id_facture',
        'designation',
        'montant',
        'remise'
    ];
}
