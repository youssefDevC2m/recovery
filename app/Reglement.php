<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reglement extends Model
{
    protected $fillable=[
        'id_doss',
        'id_cli',
        'id_debit',
        'mode_regl',
        'type_regl',
        'montant_reg',
        'distin',
        'date_echeance',
        'affectation',
        'nature_reglement',
        'remarque'
    ];
}
