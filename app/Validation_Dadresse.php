<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Validation_Dadresse extends Model
{
    protected $fillable=[
        'id_hist_scen',
        'date_valid_adres',
        'effect_par',
        'type_debit',
        'comment_vali_adrs'
    ];
}
