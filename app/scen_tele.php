<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class scen_tele extends Model
{
    protected $fillable=[
        'id_hist_scen',
        'date_H_contact',
        'type_tele',
        'nom_charge',
        'n_tele',
        "desc_num_tel",
        'promess_pay',
    ];
}
