<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable=[
        'id_dbtr',
        'banque',
        'agence',
        'ville',
        'n_compte'
    ];
}
