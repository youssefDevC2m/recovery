<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SenVisiteDomicile extends Model
{
    protected $fillable=[
        'id_hist_scen',
        'adress_perso',
        'adress_pro',
        'adress_proche',
        'date_H_visite',
        'visiteur',
        'promess',
        'date_promi',
        'comment',
        'domicile'
    ];
}
