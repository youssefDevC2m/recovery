<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SituationDirectFiles extends Model
{
    protected $fillable=[
        'id_st_dir',
        'file_name',
        'org_file_name'
    ];
}
