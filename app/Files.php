<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected $fillable=[
        'id_detail',
        'id_client',
        'id_ferm_dos',
        'id_dt_partn',
        'id_devis',
        'id_reg',
        'id_task',
        'id_facture',
        'id_sfc',
        'id_td',
        'id_arrb',
        'id_aps',
        'id_smob',
        'id_imob',

        'file_name',
        'org_file_name',
        'is_exicte'
        
    ];
}
