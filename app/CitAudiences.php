<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CitAudiences extends Model
{
    protected $fillable = [
        'id_citaton',
        'cit_date_audc',
        'cit_heur_audc',
        'cit_lieu_audc',
        'cit_resume'
    ];
}
