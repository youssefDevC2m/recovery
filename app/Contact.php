<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable=[
        'id_client',
        'id_dbtr',
        'id_partic',//=id_client don't panic
        'id_mandataire',// id mandataire client
        'tele',
        'fax',
        'cellPhone',
        'email',
        'contact'
    ];
}
