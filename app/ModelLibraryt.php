<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelLibraryt extends Model
{
    protected $fillable = [
        'ref',
        'modle',
        'descrip'
    ];
}
