<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScenJudicInfoCheq extends Model
{
    protected $fillable=[
        'id_hist_scen',
        'montant_cheq',
        'n_cheq',
        'date_emission',
        'motif'
    ];
    
}
