<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FermetureDossier extends Model
{
    protected $fillable=[
        'id_doss',
        'date_fermetur',
        'statuFerm',
        'rapportDecis'
    ];
    
}
