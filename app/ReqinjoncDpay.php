<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqinjoncDpay extends Model
{
    protected $fillable = [
        'id_hist_scen',
        'proc_dep_req_inj_pay',

        'les_proc_req_inj_pay',
        'les_audins_req_inj_pay',
        'les_preces_req_inj_pay',

        'les_proc_app_req_inj_pay',
        'dat_dep_req_inj_pay',
        'num_app_req_inj_pay',
        'dat_app_req_inj_pay',
        'nom_consltnt_req_inj_pay',
        'trib_competnt_req_inj_pay',
        'note_et_audience_req_inj_pay',
        'dat_dep_doss_au_trib_req_inj_pay',
        'dep_doss_au_trib_req_inj_pay',

        'swt_proc_notif_req_inj_pay',
        'date_dep_notif_req_inj_pay',
        'num_doss_notif_req_inj_pay',
        'huissier_des_req_inj_pay',
        'reslt_notif_req_inj_pay',
        'date_appl_notif_req_inj_pay',
        'proces_notif_req_inj_pay',

        'swt_proc_exe_req_inj_pay',
        'num_dos_exe_req_inj_pay',
        'date_dos_exe_req_inj_pay',
        'huissier_designe_exe_req_inj_pay',
        'date_exe_req_inj_pay',
        'expiration_exe_req_inj_pay',
        'dat_saisie_req_inj_pay',
        'type_saisie_req_inj_pay',
        'date_exe_judic_req_inj_pay',
        'nom_expert_exe_req_inj_pay',
        'pub_jal_req_inj_pay',
        'nom_journal_req_inj_pay',
        'vent_aux_encher_public_req_inj_pay',
        'date_vent_exe_req_inj_pay',
        'result_exe_req_inj_pay',
        'vald_pieces',
        'provi',
        'avocat',
        'date_debut_plant',
        'date_fin_plant'
    ];
}
