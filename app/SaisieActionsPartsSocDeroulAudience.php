<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaisieActionsPartsSocDeroulAudience extends Model
{
    protected $fillable = [
        'id_asp',
        'date_result',
        'result'
    ];
}
