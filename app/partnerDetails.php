<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class partnerDetails extends Model
{
    protected $fillable=[
        'id_part',
        'doss',
        'dbteur',
        'vill_debtr',
        'mt_creance',
        'motif_creance',
        'nature_creance',
        'date_trans',
        'remarque'
    ];
}
