<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devis extends Model
{
    protected $fillable=[
        'id_doss','date_devie','remarque','joints','valuer'
    ];
}
