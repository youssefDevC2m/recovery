<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DossierFacture extends Model
{
    protected $fillable = [
        'id_doss',
        'id_fact'
    ];
}
