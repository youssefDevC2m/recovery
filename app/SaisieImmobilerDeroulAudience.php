<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaisieImmobilerDeroulAudience extends Model
{
    protected $fillable = [
        'id_imob',
        'date_result',
        'result'
    ];
}
