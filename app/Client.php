<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable=[
        'type_cli',
        'code_cli',
        'raison_sos',
        'iterloc_prev',
        'qualit',
        'capital',
        'rc',
        'gerant1',
        'gerant2',
        'statue_jurdq',
        'activite',
        'adres_sos1',
        'adres_sos2',
        'ville_sos',
        'ville_sos_ar',
        'email',
        'nom',
        'nom_ar',
        'prenom',
        'prenom_ar',
        'mndatr_partic',
        'cin',
        'pass_port',
        'carte_sejour',
        'nationalite',
        'ville_perso',
        'ville_perso_ar',
        'adress1',
        'adress1_ar',
        'adress2',
        'adress2_ar',
        'adressTrav1',
        'adressTrav1_ar',
        'adressTrav2',
        'adressTrav2_ar',
        'gender',
        'id_user',
        'date_naissance',

        'brand',
        'sec_a_act',
        'date_parten',
        'rc_client',
        'ice',

        'ice_camp',
        'encenre_camp',
        'date_paten',
        'rep_legal_camp'
    ];
}
