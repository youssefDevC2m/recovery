<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaisieTiersDetenteurDeroulAudience extends Model
{
    protected $fillable = [
        'id_td',
        'date_result',
        'result'
    ];
}
