<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotificationEv implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $notif;
    public $user;
    public $task;
    public $username;
    public $role;
    public function __construct($user, $notif, $task, $username, $role)
    {
        $this->notif = $notif;
        $this->user = $user;
        $this->task= $task;
        $this->username = $username;
        $this->role = $role;
    }
     
    public function broadcastOn()
    {
        return new Channel('NotificationChanl');
    }

    public function broadcastWith()
    {
        return 
        [
            'notif'=> $this->notif,
            'user'=>$this->user,
            'task'=>$this->task,
            'username'=>$this->username,
            'role' =>$this->role
        ];
    }
}
