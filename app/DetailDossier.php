<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailDossier extends Model
{
    protected $fillable=[
        'id_doss',
        'type_pay',
        'numero',
        'bank',
        'date_ancent',
        'vue_alert_anc',
        'ville',
        'motif',
        'date_creation',
        'date_rejet',
        'mt_creance',
        'mt_reclame',
        'remarque'
    ];
}
