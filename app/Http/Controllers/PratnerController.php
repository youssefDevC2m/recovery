<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dossier;
use App\DetailDossier;
use App\Debiteur;
use App\partner;
use App\User;
use App\partnerDetails;
use App\Files;
use App\File_path;
use App\myLib\myFun;
use Auth;
class PratnerController extends Controller
{
    
    public function partForm()
    {   $partner = partner::whereNotNull('agent_id')->where('type_partn','Collaborateur')->get();
        $partners = $partner->map(function($item){
            return $item->agent_id;
        });
        
        $agents = User::where('role','Agent')->whereNotIn('id',$partners)->get();
         
        return view('admin.partners.add')
        ->with('agents',$agents);
    }
    public function GetDebit(Request $req){
        $Debiteur = Debiteur::select('debiteurs.*')
        ->join('dossiers','dossiers.id_dbtr','debiteurs.id')
        ->where('dossiers.id',$req->id_dos)->first();
        return json_encode($Debiteur);
    }
    public function listForm()
    {
        $partner = partner::get();
        return view('admin.partners.list')
        ->with('partner',$partner); 
    }

    public function creatPart(Request $req)
    {
        $partner = partner::create([
            'type_partn'=>$req->type_partn,
            'agent_id'=>$req->agent_id,
            'nom_part'=>$req->nom_part,
            'prenom_part'=>$req->prenom_part,
            'adrs_part'=>$req->adrs_part,
            'adrs_act_part'=>$req->adrs_act_part,
            'tele_part'=>$req->tele_part,
            'tele_wts_part'=>$req->tele_wts_part,
            'email_part'=>$req->email_part
        ]);
        myFun::historiqueSaveData('create', 'partner', $partner->id, 'create');
        return redirect()->route('listForm');
    }
    
    public function showPart($id)
    {
        $partner = partner::where('id',$id)->first();

        return view('admin.partners.show_partner')
        ->with('partner',$partner);
    }

    public function part_work($id){
        $partnerDetails = partnerDetails::select('partner_details.*','debiteurs.nom','debiteurs.prenom','debiteurs.type_dtbr')
        ->leftjoin('dossiers','dossiers.id','partner_details.doss')
        ->leftjoin('debiteurs','debiteurs.id','dossiers.id_dbtr')
        ->where('id_part',$id)
        ->get();
        return view('admin.partners.workList')
        ->with('id_part',$id)
        ->with('partDt',$partnerDetails);
    }
    
    public function formDetail($id){
        $Dossier = Dossier::get();
        return view('admin.partners.form_data')
        ->with('id_part',$id)
        ->with('Dossier',$Dossier);
    }
    public function addDetail(Request $req){
        $partnerDetails = partnerDetails::create([
            'id_part'=>$req->id_part,
            'doss'=>$req->doss,
            'dbteur'=>$req->dbteur,
            'vill_debtr'=>$req->vill_debtr,
            'mt_creance'=>$req->mt_creance,
            'nature_creance'=>$req->nature_creance,
            'motif_creance'=>$req->motif_creance,
            'date_trans'=>$req->date_trans,
            'remarque'=>$req->remarque
        ]);
        
        if(isset($req->file_val))
        {
            foreach($req->file_val as $file)
            {
                $filenameorg = $file->getClientOriginalName();
                $filename =uniqid().$filenameorg;
                $file->move(public_path('files/joints/'),$filename);
                $data2= Files::create([
                    'id_dt_partn'=>$partnerDetails->id,
                    'file_name'=>$filename ,
                    'org_file_name'=>$filenameorg
                ]);
            }
        }
        return json_encode($partnerDetails); 
    }

    public function showDetail($id){
        $Dossier = Dossier::get();
        $partDet = partnerDetails::select('partner_details.*','debiteurs.nom','debiteurs.prenom','debiteurs.type_dtbr')
        ->leftjoin('dossiers','dossiers.id','partner_details.doss')
        ->leftjoin('debiteurs','debiteurs.id','dossiers.id_dbtr')
        ->where('partner_details.id',$id)
        ->first();
        // dd($partDet);
        $File_path = File_path::where('type','joints')->first();
        $files = Files::where('id_dt_partn',$id)->get();
        
        return view('admin.partners.show_details')
        ->with('id_part',$partDet->id_part)
        ->with('id_dt',$id)
        ->with('File_path',$File_path)
        ->with('files',$files)
        ->with('partDet',$partDet)
        ->with('Dossier',$Dossier);
    }

    public function editDetail(Request $req){
        
        $data = partnerDetails::where('id',$req->id_dt)->update([
            'doss'=>$req->doss,
            'dbteur'=>$req->dbteur,
            'vill_debtr'=>$req->vill_debtr,
            'mt_creance'=>$req->mt_creance,
            'motif_creance'=>$req->motif_creance,
            'nature_creance'=>$req->nature_creance,
            'date_trans'=>$req->date_trans,
            'remarque'=>$req->remarque
        ]);
       
        if(isset($req->file_exc)){
            Files::where('id_dt_partn',$req->id_dt)
            ->whereNotIn('file_name',$req->file_exc)
            ->delete();
        }else{
            Files::where('id_dt_partn',$req->id_dt)
            ->delete();
        }

        if(isset($req->file_val))
        {
            foreach($req->file_val as $file)
            {
                $filenameorg = $file->getClientOriginalName();
                $filename =uniqid().$filenameorg;
                $file->move(public_path('files/joints/'),$filename);
                $data2= Files::create([
                    'id_dt_partn'=>$req->id_dt,
                    'file_name'=>$filename ,
                    'org_file_name'=>$filenameorg
                ]);
            }
        }
        return json_encode($data);
    }

    public function deleteDetail($id){
        partnerDetails::where('id',$id)->delete();
        return back();
    }

    public function editPart(Request $req)
    {
        partner::where('id',$req->id_part)
        ->update([
            'type_partn'=>$req->type_partn,
            'nom_part'=>$req->nom_part,
            'prenom_part'=>$req->prenom_part,
            'adrs_part'=>$req->adrs_part,
            'adrs_act_part'=>$req->adrs_act_part,
            'tele_part'=>$req->tele_part,
            'tele_wts_part'=>$req->tele_wts_part,
            'email_part'=>$req->email_part
        ]);
        myFun::historiqueSaveData('create', 'partner', $req->id_reg, 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
       
        return redirect()->route('listForm');
    }
    
    public function deletePart($id)
    {
        $res = myFun::historiqueSaveData('delete', 'partner', $id, 'delete', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
        if($res == 'do_it'){
            partner::where('id',$id)->delete();
        }
        return redirect()->route('listForm');
    }
    
    public function getAgents(Request $req)
    {
        $agent=User::where('id',$req->agents)->first();
        
        return json_encode((!empty($agent)? $agent : 0));
    }

    public function destroy($id)
    {
        //
    }
}
