<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charges;
use App\Dossier;
use Auth;
use App\myLib\myFun;
class ChargeController extends Controller
{
     
   
    public function listCharge()
    {
        $charges=Charges::get();
        return view('admin.charges.list')->with('charges',$charges);
    }

    
    public function addIndex()
    {
        $dosses=Dossier::get();
        return view('admin.charges.add')->with('dosses',$dosses);
    }
    
    public function storeCharge(Request $req)
    {
        $data =  Charges::create([
            'id_doss'=>$req->id_doss,
            'owner_type'=>$req->owner_type,
            'mode_reg'=>$req->mode_reg,
            'montant'=>$req->montant,
            'nature_reg'=>$req->nature_reg,
            'affect'=>$req->affect,
            'remarque'=>$req->remarque
        ]);
        myFun::historiqueSaveData('create', 'Charges', $data->id, 'create');
        return json_encode($data);
    }
    
    public function showCharge($id)
    {
        $charges=Charges::where('id',$id)->first();
        $dosses=Dossier::get();
        return view('admin.charges.show')->with('charges',$charges)->with('dosses',$dosses);
        
    }
    
    public function updateCharge(Request $req)
    {
        $data =  Charges::where('id',$req->id_charge)->update([
            'id_doss'=>$req->id_doss,
            'owner_type'=>$req->owner_type,
            'mode_reg'=>$req->mode_reg,
            'montant'=>$req->montant,
            'nature_reg'=>$req->nature_reg,
            'affect'=>$req->affect,
            'remarque'=>$req->remarque
        ]);

        myFun::historiqueSaveData('create', 'Charges', $data->id_facture, 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
        
        return json_encode($data);
    }

    public function deleteCharge($id){
        
        $res = myFun::historiqueSaveData('delete', 'Charges', $id, 'delete', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
        
        if($res == 'do_it'){
            Charges::where('id',$id)->delete();  
        }
        
        return back();
    }

   
    public function destroy($id)
    {
        //
    }
}
