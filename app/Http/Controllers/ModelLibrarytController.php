<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelLibraryt;
use App\File_path;

class ModelLibrarytController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bib_model_index()
    {
        $data = ModelLibraryt::get();
        $path = File_path::where('type', 'joints')->first()->file_path;
        // dd($path);
        return view('admin.LibraryModel.add')
            ->with('path', $path)
            ->with('ModelLibraryt', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bib_model_add(Request $req)
    {
        $filename = '';

        if(isset($req->modle)){
            $filenameorg = $req->modle->getClientOriginalName();
            $filename = uniqid() . $filenameorg;
            $req->modle->move(public_path('files/joints/'), $filename);
        }
        
        ModelLibraryt::create([
            'ref' => $req->ref,
            'descrip' => $req->descrip,
            'modle' => $filename
        ]);

        $data = ModelLibraryt::get();
        $path = File_path::where('type', 'joints')->first()->file_path;
        $view = view('admin.LibraryModel.list')
        ->with('ModelLibraryt',$data)
        ->with('path',$path)
        ->render();
        return json_encode([
            'statue' => 'success',
            'message' => "L'opération s'est déroulée avec succès",
            'view' => $view,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete_biblio($id)
    {
        ModelLibraryt::where('id',$id)->delete();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
