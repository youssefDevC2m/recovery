<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Conversation;
use App\taskManagment;
use App\Dossier;
use App\User;
use App\Files;
use App\File_path;
use App\Events\NotificationEv;

class AgentConroller extends Controller
{
    
    public function index()
    {
        $task = taskManagment::select('dossiers.ref','task_managments.*')
        ->join('dossiers','dossiers.id','task_managments.id_doss')
        ->where('id_agent',Auth::user()->id)
        ->where('valid_task',1)
        ->take('4')
        ->get();
        
        return view('agent.dashborde')->with('task',$task);
    }

    
    public function task_list()
    {
        $task = taskManagment::select('dossiers.ref','task_managments.*')
        ->join('dossiers','dossiers.id','task_managments.id_doss')
        ->where('id_agent',Auth::user()->id)
        ->where('valid_task',1)
        ->get();
        return view('agent.tasks.task_list')
        ->with('task',$task);
    }

    public function show_task($id)
    {
        $task=taskManagment::select('task_managments.*','users.nom','users.prenom')
        ->join('users','users.id','task_managments.id_agent')
        ->where('task_managments.id',$id)->first();
        $Dossier=Dossier::get();
        $files=Files::where('id_task',$id)->get();
        $File_path=File_path::where('type','joints')->first();
        $Conver=Conversation::where('id_task',$id)->get();
      
        if(!empty($task) && $task->valid_task != 1 && Auth::user()->role!='Administrateur'){
             abort(404);
        }
        
        return view('agent.tasks.showTask')
        ->with('Dossier',$Dossier)
        ->with('Conver',$Conver)
        ->with('File_path',$File_path)
        ->with('files',$files)
        ->with('task',$task);
    }
    public function reloadmsg(Request $req){
        $Conver=Conversation::where('id_task',$req->id_task)->get();
        return json_encode($Conver);
    }
    public function send(Request $req){

        $data = Conversation::create([
            'id_user'=>$req->user,
            'id_task'=>$req->taskId,
            'message'=>$req->message,
            'seen'=>0
        ]);

        // event(new NotificationEv($req->user,$req->message,$req->taskId,$req->username,$req->role));
        return ['statue', $data];
    }
    
    public function typingAg(Request $req)
    {
        event(new NotificationEv($req->user,$req->message,$req->taskId,$req->username,$req->role));
        return [$req->role];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function vu_task(Request $req)
    {
        taskManagment::where('id',$req->id_task)->update([
            'task_ag_vu'=>1
        ]);
        return json_encode('gaat it!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
