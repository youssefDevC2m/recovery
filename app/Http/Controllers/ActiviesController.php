<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HistoriqueAct;
use App\Client;
use App\Dossier;
use App\ScenJudicCheq;
use App\ReqinjoncDpay;
use App\Citation;
use App\CitAudiences;
use App\PhaCitAudiences;
use App\Files;
use App\SaisieFondComers;
use App\SaisieFontComersDeroulAudience;
use App\SaisieTiersDetenteur;
use App\SaisieTiersDetenteurDeroulAudience;
use App\SaisieArretBancaire;
use App\SaisieArretBancaireDeroulAudience;
use App\SaisieActionsPartsSoc;
use App\SaisieActionsPartsSocDeroulAudience;
use App\SaisieMobiler;
use App\SaisieMobilerDeroulAudience;
use App\SaisieImobiler;
use App\SaisieImmobilerDeroulAudience;
use App\HistoriqueScenario;
use App\Devis;
use App\Produit;
use App\Facture;
use App\Reglement;
use App\Charges;
use App\partner;

class ActiviesController extends Controller
{
    
    public function admin_see_all_acts()
    {
        $HistoAct=HistoriqueAct::where('id_valid',0)
        ->orderby('created_at','desc')
        ->get();

        return view('admin.activites_history.show_history')
        ->with('HistoAct',$HistoAct);
    }
    
    public function aprouv_act(Request $req)
    {
        $HistoriqueAct = new HistoriqueAct();
        $HistoriqueAct->where('id',$req->id)->update(['id_valid'=>1]);
        $data = $HistoriqueAct->where('id',$req->id)->first();

        if($data->where == 'scenChequ'){
            $action = ScenJudicCheq::where('id',$data->id_action)->first();
            $for_id = HistoriqueScenario::where('id', $action->id_hist_scen)->first();
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }

        if($data->where == 'scenReqInjonctionPay'){
            $action = ReqinjoncDpay::where('id',$data->id_action)->first(); 
            $for_id = HistoriqueScenario::where('id', $action->id_hist_scen)->first();
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }
        
        if($data->where == 'citation_direct'){
            $action = Citation::where('id', $data->id_action)->first();
            $for_id = HistoriqueScenario::where('id', $action->id_hist_scen)->first();
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }

        if($data->where == 'FondComers'){
            $action = SaisieFondComers::where('id', $data->id_action)->first();
            $for_id = HistoriqueScenario::where('id', $action->id_hist_scen)->first();
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }

        if($data->where == 'SaisieTiersDetenteur'){
            $action = SaisieTiersDetenteur::where('id', $data->id_action)->first();
            $for_id = HistoriqueScenario::where('id', $action->id_hist_scen)->first();
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }

        if($data->where == 'SaisieArretBancaire'){
            $action = SaisieArretBancaire::where('id', $data->id_action)->first();
            $for_id = HistoriqueScenario::where('id', $action->id_hist_scen)->first();
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }

        if($data->where == 'SaisieActionsPartsSoc'){
            $action = SaisieActionsPartsSoc::where('id', $data->id_action)
            ->first();
            $for_id = HistoriqueScenario::where('id', $action->id_hist_scen)->first();
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }

        if($data->where == 'SaisieMobiler'){
            $action = SaisieMobiler::where('id', $data->id_action)->first();
            $for_id = HistoriqueScenario::where('id', $action->id_hist_scen)->first();
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }

        if($data->where == 'SaisieImobiler'){
            $action = SaisieImobiler::where('id', $data->id_action)->delete();
            $for_id = HistoriqueScenario::where('id', $action->id_hist_scen)->first();
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }

        if($data->what == 'delete' && $data->where == 'client'){
            Client::where('id',$data->id_action)->delete(); 
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','client')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'dossier'){
            Dossier::where('id',$data->id_action)->delete(); 
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','dossier')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'scenChequ'){
            ScenJudicCheq::where('id',$data->id_action)->delete(); 
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','scenChequ')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'scenReqInjonctionPay'){
            ReqinjoncDpay::where('id',$data->id_action)->delete(); 
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','scenReqInjonctionPay')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }
        if($data->what == 'delete' && $data->where == 'citation_direct'){
            Citation::where('id', $data->id_action)->delete();
            CitAudiences::where('id_citaton', $data->id_action)->delete();
            PhaCitAudiences::where('id_citaton', $data->id_action)->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','Citation')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }
        if($data->what == 'delete' && $data->where == 'FondComers'){
            SaisieFondComers::where('id', $data->id_action)->delete();
            SaisieFontComersDeroulAudience::where('id_fc', $data->id_action)->delete();
            Files::where('id_sfc',$data->id_action)->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','SaisieFondComers')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'SaisieTiersDetenteur'){
            SaisieTiersDetenteur::where('id', $data->id_action)->delete();
            SaisieTiersDetenteurDeroulAudience::where('id_td', $data->id_action)->delete();
            Files::where('id_td',$data->id_action)->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','SaisieTiersDetenteur')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'SaisieArretBancaire'){
            SaisieArretBancaire::where('id', $data->id_action)->delete();
            SaisieArretBancaireDeroulAudience::where('id_arrb', $data->id_action)->delete();
            Files::where('id_arrb',$data->id_action)->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','SaisieArretBancaire')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'SaisieActionsPartsSoc'){
            SaisieActionsPartsSoc::where('id', $data->id_action)
            ->delete();
            SaisieActionsPartsSocDeroulAudience::where('id_asp', $data->id_action)
            ->delete();
            Files::where('id_aps',$data->id_action)
            ->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','SaisieActionsPartsSoc')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'SaisieMobiler'){
            SaisieMobiler::where('id', $data->id_action)
            ->delete();
            SaisieMobilerDeroulAudience::where('id_smod', $data->id_action)
            ->delete();
            Files::where('id_smob',$data->id_action)
            ->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','SaisieMobiler')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'SaisieImobiler'){
            SaisieImobiler::where('id', $data->id_action)->delete();
            SaisieImmobilerDeroulAudience::where('id_imob', $data->id_action)->delete();
            Files::where('id_imob',$data->id_action)
            ->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','SaisieImobiler')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'Devis'){
            Devis::where('id', $data->id_action)->delete();
            Produit::where('id_devis', $data->id_action)->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','Devis')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'Facture'){
            Facture::where('id', $data->id_action)->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','Facture')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'Reglement'){
            Reglement::where('id',$data->id_action)->delete();
            Files::where('id_reg',$data->id_action)->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','Reglement')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'Charges'){
            Charges::where('id',$data->id_action)->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','Charges')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }

        if($data->what == 'delete' && $data->where == 'partner'){
            partner::where('id', $data->id_action)->delete();
            HistoriqueAct::where('id_action',$data->id_action)
            ->where('where','partner')
            ->where('id_valid',0)
            ->update(['id_valid'=>'-1']);
        }
        
        return json_encode($data);
    }

    
    public function reject_act(Request $req)
    {
        HistoriqueAct::where('id',$req->id)->update(['id_valid'=>'-1']);
        return json_encode($req->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
