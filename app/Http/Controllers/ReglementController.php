<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reglement;
use App\Dossier;
use App\Client;
use App\Files;
use App\File_path;
use App\myLib\myFun;
class ReglementController extends Controller
{
   
    public function listReglementp()
    {
        $Reglement=Reglement::select('clients.type_cli','clients.nom','clients.prenom','clients.raison_sos','reglements.*')
        ->leftjoin('clients','clients.id','reglements.id_cli')
        ->get();
        return view('admin.reglement.list')->with('Reglement',$Reglement);
    }

    
    public function addReglementp()
    {
        $folds = Dossier::where('phase','Prise en charge')->get();
        
        return view('admin.reglement.add')
        
        ->with('folds',$folds);
    }
    public function storeRegle(Request $req){

        $regle=Reglement::create([
            'id_cli'=>($req->distin=='cli')? $req->id_cli : null,
            'id_debit'=>($req->distin=='debit' || $req->distin=='avoc')? $req->id_debit : null,
            'type_regl'=>($req->distin!='avoc')?$req->type_regl: '',
            'id_doss'=>$req->id_doss,
            'distin'=>$req->distin,
            'mode_regl'=>$req->mode_regl,
            'montant_reg'=>$req->montant_reg,
            'date_echeance'=>$req->date_echeance,
            'affectation'=>$req->affectation,
            'nature_reglement'=>$req->nature_reglement,
            'remarque'=>$req->remarque
        ]);
        myFun::historiqueSaveData('create', 'Reglement', $regle->id, 'create');
        if(isset($req->file_val)){
            foreach($req->file_val as $file){
                $filenameorg = $file->getClientOriginalName();
                $filename =uniqid().$filenameorg;
                $file->move(public_path('files/joints/'),$filename);
                $data2= Files::create([
                    'id_reg'=>$regle->id,
                    'file_name'=>$filename ,
                    'org_file_name'=>$filenameorg
                ]);
            }
        }
       
        return json_encode($req->id_doss);
    }
    public function get_cli_reg(Request $req){
        $data=null;
        if($req->distin == 'cli' ){
            $data=Dossier::select('clients.*')
            ->leftjoin('clients','clients.id','dossiers.id_client')
            ->where('dossiers.id',$req->id_doss)->first();
        }
        if($req->distin == 'debit' || $req->distin == 'avoc'){
            $data=Dossier::select('debiteurs.*')
            ->leftjoin('debiteurs','debiteurs.id','dossiers.id_dbtr')
            ->where('dossiers.id',$req->id_doss)->first();
        }
        return json_encode($data);
    }
    public function showRglement($id){
        $folds = Dossier::where('phase','Prise en charge')->get();
        $regle = Reglement::
        select('clients.type_cli',
        'clients.nom',
        'clients.prenom',
        'clients.raison_sos',
        'clients.id as id_cli',
        'debiteurs.type_dtbr',
        'debiteurs.nom',
        'debiteurs.prenom',
        'reglements.*'
        )
        ->leftJoin('clients','clients.id','reglements.id_cli')
        ->leftJoin('debiteurs','debiteurs.id','reglements.id_debit')
        ->where('reglements.id',$id)
        ->first();
        $files=Files::where('id_reg',$id)->get();
        $File_path=File_path::where('type','joints')->first();
        // dd($regle);
        return view('admin.reglement.show')
        ->with('files',$files)
        ->with('File_path',$File_path)
        ->with('regle',$regle)
        ->with('folds',$folds);
    }
    public function updateRegle(Request $req){
        $data=Reglement::where('id',$req->id_reg)->update([
            'id_cli'=>($req->distin=='cli')? $req->id_cli : null,
            'id_debit'=>($req->distin=='debit' || $req->distin=='avoc')? $req->id_debit : null,
            'type_regl'=>($req->distin!='avoc')?$req->type_regl: '',
            'id_doss'=>$req->id_doss,
            'distin'=>$req->distin,
            'mode_regl'=>$req->mode_regl,
            'montant_reg'=>$req->montant_reg,
            'date_echeance'=>$req->date_echeance,
            'affectation'=>$req->affectation,
            'nature_reglement'=>$req->nature_reglement,
            'remarque'=>$req->remarque
        ]);
        myFun::historiqueSaveData('create', 'Reglement', $req->id_reg, 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
        
        if(isset($req->file_exc)){
            Files::where('id_reg',$req->id_reg)
            ->whereNotIn('file_name',$req->file_exc)
            ->delete();
        }else{
            Files::where('id_reg',$req->id_reg)->delete();
        }
        if(isset($req->file_val)){
            foreach($req->file_val as $file){
                $filenameorg = $file->getClientOriginalName();
                $filename =uniqid().$filenameorg;
                $file->move(public_path('files/joints/'),$filename);
                $data2= Files::create([
                    'id_reg'=>$req->id_reg,
                    'file_name'=>$filename,
                    'org_file_name'=>$filenameorg 
                ]);
            }
        }
        return json_encode($data);
    }
    
    public function deleteRegle($id)
    {
        $res = myFun::historiqueSaveData('delete', 'Reglement', $id, 'delete', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
        if($res == 'do_it'){
            Reglement::where('id',$id)->delete();
            Files::where('id_reg',$id)->delete();
        }
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
