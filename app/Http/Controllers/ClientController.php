<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Contact;
use App\tempUser;
use App\User;
use App\Files;
use App\File_path;
use App\MandataireClient;
use App\Dossier;
use App\HistoriqueAct;

use Auth;
use PDF;
class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $role;


   
    public function indexClient()
    {
        return view('admin.client.add_client');
    }

    // ///////////////////////////////////////////////
    // //////////////////////////////////////////////
    // /////////////////////////////////////////////
    // ||||||||||||||||||||||||||||||||||||||||||||
    // \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    // \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    // \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    
    public function listClient()
    {
        $clients=Client::get();
        return view('admin.client.list_client')
        ->with('clients',$clients);
    }

    public function createClient(Request $req)
    {
            
            if($req->user==0){
                $data=Client::create([
                    'code_cli'=>$req->code_cli,
                    'type_cli'=>$req->type_cli,
                    'raison_sos'=>$req->raison_sos,
                    'capital'=>$req->capital,
                    'qualit'=>$req->qualit,
                    'capital'=>$req->capital,
                    'adres_sos1'=>$req->adres_sos1,
                    'adres_sos2'=>$req->adres_sos2,
                    'gerant1'=>$req->gerant1,
                    'gerant2'=>$req->gerant2,
                    'rc'=>$req->rc,
                    'activite'=>$req->activite,
                    'statue_jurdq'=>$req->statue_jurdq,
                    'nom'=>$req->nom,
                    'nom_ar'=>$req->nom_ar,
                    'prenom'=>$req->prenom,
                    'mndatr_partic'=>$req->mndatr_partic,
                    'prenom_ar'=>$req->prenom_ar,
                    'adress1'=>$req->adress1,
                    'adress2'=>$req->adress2,
                    'adressTrav1'=>$req->adressTrav1,
                    'adressTrav2'=>$req->adressTrav2,
                    'ville_perso'=>$req->ville_perso,
                    'nationalite'=>$req->nation,
                    'gender'=>$req->gender,
                    'cin'=>$req->cin,
                    'pass_port'=>$req->pass_port,
                    'carte_sejour'=>$req->carte_sejour,

                    'brand'=>$req->brand,
                    'sec_a_act'=>$req->sec_a_act,
                    'date_parten'=>$req->date_parten,
                    'rc_client'=>$req->rc_client,
                    'ice'=>$req->ice,

                    'ice_camp'=>$req->ice_camp,
                    'encenre_camp'=>$req->encenre_camp,
                    'date_paten'=>$req->date_paten,
                    'rep_legal_camp'=>$req->rep_legal_camp
                ]);
            }else{
                $data=Client::create([
                    'code_cli'=>$req->code_cli,
                    'type_cli'=>$req->type_cli,
                    'raison_sos'=>$req->raison_sos,
                    'capital'=>$req->capital,
                    'qualit'=>$req->qualit,
                    'capital'=>$req->capital,
                    'adres_sos1'=>$req->adres_sos1,
                    'adres_sos2'=>$req->adres_sos2,
                    'gerant1'=>$req->gerant1,
                    'gerant2'=>$req->gerant2,
                    'rc'=>$req->rc,
                    'activite'=>$req->activite,
                    'statue_jurdq'=>$req->statue_jurdq,
                    'nom'=>$req->nom,
                    'nom_ar'=>$req->nom_ar,
                    'prenom'=>$req->prenom,
                    'prenom_ar'=>$req->prenom_ar,
                    'adress1'=>$req->adress1,
                    'adress2'=>$req->adress2,
                    'adressTrav1'=>$req->adressTrav1,
                    'adressTrav2'=>$req->adressTrav2,
                    'ville_perso'=>$req->ville_perso,
                    'nationalite'=>$req->nation,
                    'gender'=>$req->gender,
                    'cin'=>$req->cin,
                    'pass_port'=>$req->pass_port,
                    'carte_sejour'=>$req->carte_sejour,

                    'brand'=>$req->brand,
                    'sec_a_act'=>$req->sec_a_act,
                    'date_parten'=>$req->date_parten,
                    'rc_client'=>$req->rc_client,
                    'ice'=>$req->ice,

                    'ice_camp'=>$req->ice_camp,
                    'encenre_camp'=>$req->encenre_camp,
                    'date_paten'=>$req->date_paten,
                    'rep_legal_camp'=>$req->rep_legal_camp
                ]);
                $temp=tempUser::where('id',$req->user)->first();
                $users = User::create([
                    'nom' => $temp->nom,
                    'prenom'=> $temp->prenom,
                    'email' => $temp->email,
                    'tele'=>$temp->tele,
                    'address'=>$temp->adrs,
                    'password' => $temp->password,
                    'role'=>'Client',
                    'photo'=>$temp->photo,
                    'temp_pwd'=>$temp->password,
                    'id_client'=>$data->id
                ]);
            }

            if(isset($req->file_val)){
                foreach($req->file_val as $file){
                    $filenameorg = $file->getClientOriginalName();
                    $filename =uniqid().$filenameorg;
                    $file->move(public_path('files/joints/'),$filename);
                    $data2= Files::create([
                        'id_client'=>$data->id,
                        'file_name'=>$filename ,
                        'org_file_name'=>$filenameorg
                    ]);
                }
            }
            
            foreach($req->tele as $tele){
                if(!empty($tele)){
                    Contact::create([
                        'id_client'=>$data->id,
                        'tele'=>$tele
                    ]);
                }
            }
         
            foreach($req->cellPhone as $phone){
                if(!empty($phone)){
                    Contact::create([
                        'id_client'=>$data->id,
                        'cellPhone'=>$phone
                    ]);
                }
            }
            
            foreach($req->mail as $mail){
                if(!empty($mail)){
                    Contact::create([
                        'id_client'=>$data->id,
                        'email'=>$mail
                    ]);
                }
            }
            
            foreach($req->contact as $contact){
                if(!empty($contact)){
                    Contact::create([
                        'id_client'=>$data->id,
                        'contact'=>$contact
                    ]);
                }
            }
            ////////////////////////////////////////////
            // if(!empty($req->mndatr_partic) || !empty($req->iterloc_prev)){
            
            //     foreach($req->tele_mand as $tele){
            //         if(!empty($tele)){
            //             Contact::create([
            //                 'id_partic'=>$data->id,
            //                 'tele'=>$tele
            //             ]);
            //         }
            //     }
             
            //     foreach($req->cell_phone_mand as $phone2){
            //         if(!empty($phone2)){
            //             Contact::create([
            //                 'id_client'=>$data->id,
            //                 'cellPhone'=>$phone2
            //             ]);
            //         }
            //     }
                
            //     foreach($req->mail_mand as $mail){
            //         if(!empty($mail)){
            //             Contact::create([
            //                 'id_partic'=>$data->id,
            //                 'email'=>$mail
            //             ]);
            //         }
            //     }
                
            //     foreach($req->contact_mand as $contact){
            //         if(!empty($contact)){
            //             Contact::create([
            //                 'id_partic'=>$data->id,
            //                 'contact'=>$contact
            //             ]);
            //         }
            //     }
            // }
            
            $varTele = '';
            $varMail = '';
            $varCellPhon = '';
            $varOther = '';

            $phonIndex = 0;
            $mailIndex = 0;
            $CellPhonIndex = 0;
            $otherIndex = 0;
            if(isset($req->name_mondate)){
                foreach($req->name_mondate as $index => $info){

                    $mand = MandataireClient::create([
                        'id_client'=>$data->id,
                        'nom' =>$info
                    ]);
    
                    // start phone
                    $varTele = 'tele_mand'.$phonIndex;
                    while(!isset($req->$varTele)){
                        $phonIndex++;
                        $varTele = 'tele_mand'.$phonIndex;
                        
                    }
                    foreach($req->$varTele as $index2 => $item){
                        if(empty($item)){continue;}
                        $Contact=Contact::create([
                            'tele'=>$item,
                            'id_mandataire'=>$mand->id
                        ]);
                    }
                    // end phone
    
                    // start mail
                    $varMail = 'mail_mand'.$mailIndex;
                    while(!isset($req->$varMail)){
                        $mailIndex++;
                        $varMail = 'mail_mand'.$mailIndex;
                        
                    }
                    foreach($req->$varMail as $index3 => $item){
                        if(empty($item)){continue;}
                        $Contact=Contact::create([
                            'email'=>$item,
                            'id_mandataire'=>$mand->id
                        ]);
                    }
                    // end mail
    
                    // cell phone
                    $varCellPhon = 'cell_phone_mand'.$CellPhonIndex;
                    while(!isset($req->$varMail)){
                        $CellPhonIndex++;
                        $varCellPhon = 'cell_phone_mand'.$CellPhonIndex;
                        
                    }
                    foreach($req->$varCellPhon as $index4 => $item)
                    {
                        if(empty($item)){continue;}
                        $Contact=Contact::create([
                            'cellPhone'=>$item,
                            'id_mandataire'=>$mand->id
                        ]);
                    }
                    // end cell phone
    
                    // cell phone
                    $varOther = 'contact_mand'.$otherIndex;
                    while(!isset($req->$varMail)){
                        $otherIndex++;
                        $varOther = 'contact_mand'.$otherIndex;
                        
                    }
                    foreach($req->$varOther as $index5 => $item){
                        if(empty($item)){continue;}
                        $Contact=Contact::create([
                            'contact'=>$item,
                            'id_mandataire'=>$mand->id
                        ]);
                    }
                    // end cell phone
                    $otherIndex ++;
                    $CellPhonIndex++;
                    $mailIndex++;
                    $phonIndex++;
                }
            }
            HistoriqueAct::create([
                'id_user'=>Auth::user()->id,
                'what'=>'create',
                'where'=>'client',
                'id_action'=>$data->id,
                'username'=>Auth::user()->nom,
                'userlastname'=>Auth::user()->prenom,
                'id_valid' =>((Auth::user()->droit_de_valid_acts == 1)? 1 : 0),
            ]);

        return json_encode($data);
    }

    public function render_mondataireV(Request $req)
    {
        $monName = $req->mndatr_partic;
        $view = view('admin.client.mondate')
        ->with('id_cord',$req->id_cord)
        ->with('monName',$monName)
        ->render();
        $view2 = view('admin.client.modatair_cordon')
        ->with('id_cord',$req->id_cord)
        ->render();
        return json_encode(
            [
                'view'=>$view,
                'view2'=>$view2
            ]
        );
    }

    public function clientUser($id)
    {
        $user_temp=tempUser::where('id',$id)->first();
        return view('admin.client.add_client')
        ->with('user_temp',$user_temp);
    }
  
    public function showClient($id)
    {
        $client=Client::where('id',$id)->first();
        $contrat=Contact::where('id_client',$client->id)->get();
        $contmand=Contact::where('id_partic',$client->id)->get();
        $files=Files::where('id_client',$client->id)->get();
        $File_path=File_path::where('type','joints')->first();
        $MandataireClient = MandataireClient::where('id_client',$client->id)->get();
        return view('admin.client.edit_client')
        ->with('MandataireClient',$MandataireClient)
        ->with('client',$client)
        ->with('files',$files)
        ->with('File_path',$File_path)
        ->with('contmand',$contmand)
        ->with('contrat',$contrat);
    }

    public function editClient(Request $req)
    {
        
        Client::where('id',$req->id_client)->update([
            'code_cli'=>$req->code_cli,
            'type_cli'=>$req->type_cli,
            'raison_sos'=>$req->raison_sos,
            'iterloc_prev'=>$req->iterloc_prev,
            'qualit'=>$req->qualit,
            'capital'=>$req->capital,
            'adres_sos1'=>$req->adres_sos1,
            'adres_sos2'=>$req->adres_sos2,
            'gerant1'=>$req->gerant1,
            'gerant2'=>$req->gerant2,
            'rc'=>$req->rc,
            'activite'=>$req->activite,
            'statue_jurdq'=>$req->statue_jurdq,
            'mndatr_partic'=>$req->mndatr_partic,
            'nom'=>$req->nom,
            'nom_ar'=>$req->nom_ar,
            'prenom'=>$req->prenom,
            'prenom_ar'=>$req->prenom_ar,
            'adress1'=>$req->adress1,
            'adress2'=>$req->adress2,
            'adressTrav1'=>$req->adressTrav1,
            'adressTrav2'=>$req->adressTrav2,
            'ville_perso'=>$req->ville_perso,
            'nationalite'=>$req->nation,
            'gender'=>$req->gender,
            'cin'=>$req->cin,
            'pass_port'=>$req->pass_port,
            'carte_sejour'=>$req->carte_sejour,
            'brand'=>$req->brand,
            'sec_a_act'=>$req->sec_a_act,
            'date_parten'=>$req->date_parten,
            'rc_client'=>$req->rc_client,
            'ice'=>$req->ice,

            'ice_camp'=>$req->ice_camp,
            'encenre_camp'=>$req->encenre_camp,
            'date_paten'=>$req->date_paten,
            'rep_legal_camp'=>$req->rep_legal_camp
        ]);
        Contact::where('id_client',$req->id_client)->delete();
        if(isset($req->file_exc)){
            Files::where('id_client',$req->id_client)
            ->whereNotIn('file_name',$req->file_exc)
            ->delete();
        }else{
            Files::where('id_client',$req->id_client)
             
            ->delete();
        }
        
        if(isset($req->file_val)){
            foreach($req->file_val as $file){
                $filenameorg = $file->getClientOriginalName();
                $filename =uniqid().$filenameorg;
                $file->move(public_path('files/joints/'),$filename);
                $data2= Files::create([
                    'id_client'=>$req->id_client,
                    'file_name'=>$filename,
                    'org_file_name'=>$filenameorg 
                ]);
            }
        }
        
        foreach($req->tele as $tele){
            if(!empty($tele)){
                Contact::create([
                    'id_client'=>$req->id_client,
                    'tele'=>$tele
                ]);
            }
        }
     
        // foreach($req->fax as $fax){
        //     if(!empty($fax)){
        //         Contact::create([
        //             'id_client'=>$req->id_client,
        //             'fax'=>$fax
        //         ]);
        //     }
        // }


        foreach($req->cellPhone as $phone){
            if(!empty($phone)){
                Contact::create([
                    'id_client'=>$req->id_client,
                    'cellPhone'=>$phone
                ]);
            }
        }
    
    
        foreach($req->mail as $mail){
            if(!empty($mail)){
                Contact::create([
                    'id_client'=>$req->id_client,
                    'email'=>$mail
                ]);
            }
        }
        
        foreach($req->contact as $contact){
            if(!empty($contact)){
                Contact::create([
                    'id_client'=>$req->id_client,
                    'contact'=>$contact
                ]);
            }
        }
        ////////////////////////////////////////////////////
            $varTele = '';
            $varMail = '';
            $varCellPhon = '';
            $varOther = '';

            $phonIndex = 0;
            $mailIndex = 0;
            $CellPhonIndex = 0;
            $otherIndex = 0;
            $MandataireClient = MandataireClient::where('id_client',$req->id_client)->get();
            Contact::whereIn('id_mandataire',$MandataireClient->map(function($item){return $item->id;}))->delete();
            MandataireClient::where('id_client',$req->id_client)->delete();
            if(isset($req->name_mondate)){
                
                foreach($req->name_mondate as $index => $info){
                    
                    
                    $mand = MandataireClient::create([
                        'id_client'=>$req->id_client,
                        'nom' =>$info
                    ]);
    
                    // start phone
                    $varTele = 'tele_mand'.$phonIndex;
                    while(!isset($req->$varTele)){
                        $phonIndex++;
                        $varTele = 'tele_mand'.$phonIndex;
                        
                    }
                    foreach($req->$varTele as $index2 => $item){
                        if(empty($item)){continue;}
                        $Contact=Contact::create([
                            'tele'=>$item,
                            'id_mandataire'=>$mand->id
                        ]);
                    }
                    // end phone
    
                    // start mail
                    $varMail = 'mail_mand'.$mailIndex;
                    while(!isset($req->$varMail)){
                        $mailIndex++;
                        $varMail = 'mail_mand'.$mailIndex;
                        
                    }
                    foreach($req->$varMail as $index3 => $item){
                        if(empty($item)){continue;}
                        $Contact=Contact::create([
                            'email'=>$item,
                            'id_mandataire'=>$mand->id
                        ]);
                    }
                    // end mail
    
                    // cell phone
                    $varCellPhon = 'cell_phone_mand'.$CellPhonIndex;
                    
                    while(!isset($req->$varCellPhon)){
                        $CellPhonIndex++;
                        $varCellPhon = 'cell_phone_mand'.$CellPhonIndex;
                        
                    } 
                    foreach($req->$varCellPhon as $index4 => $item){
                        if(empty($item)){continue;}
                        $Contact=Contact::create([
                            'cellPhone'=>$item,
                            'id_mandataire'=>$mand->id
                        ]);
                    }
                    // end cell phone
    
                    // cell phone
                    $varOther = 'contact_mand'.$otherIndex;
                    while(!isset($req->$varOther)){
                        $otherIndex++;
                        $varOther = 'contact_mand'.$otherIndex;
                        
                    }
                    foreach($req->$varOther as $index5 => $item){
                        if(empty($item)){continue;}
                        $Contact=Contact::create([
                            'contact'=>$item,
                            'id_mandataire'=>$mand->id
                        ]);
                    }
                    // end cell phone
                    $otherIndex ++;
                    $CellPhonIndex++;
                    $mailIndex++;
                    $phonIndex++;
                }
            }
        if(Auth::user()->droit_de_valid_acts == 0){
            HistoriqueAct::where('id_action',$req->id_client)
            ->where('id_valid', 1)
            ->where('what','create')
            ->where('where','client')
            ->update(
                ['id_valid'=>'0']
            );
        }
        
        // if(!empty($req->mndatr_partic) || !empty($req->iterloc_prev)){
        //     $del=Contact::where('id_partic',$req->id_client)->delete();
        //     foreach($req->tele_mand as $tele){
        //         if(!empty($tele)){
        //             Contact::create([
        //                 'id_partic'=>$req->id_client,
        //                 'tele'=>$tele
        //             ]);
        //         }
        //     }
         
        //     foreach($req->fax_mand as $fax){
        //         if(!empty($fax)){
        //             Contact::create([
        //                 'id_partic'=>$req->id_client,
        //                 'fax'=>$fax
        //             ]);
        //         }
        //     }
            
        //     foreach($req->mail_mand as $mail){
        //         if(!empty($mail)){
        //             Contact::create([
        //                 'id_partic'=>$req->id_client,
        //                 'email'=>$mail
        //             ]);
        //         }
        //     }
            
        //     foreach($req->contact_mand as $contact){
        //         if(!empty($contact)){
        //             Contact::create([
        //                 'id_partic'=>$req->id_client,
        //                 'contact'=>$contact
        //             ]);
        //         }
        //     }
        // }
        return json_encode("scc");
    }
    
    public function destroyClient($id)
    {
        if(Auth::user()->droit_de_valid_acts == 0){
            $data = HistoriqueAct::where('what','delete')->where('id_user',Auth::user()->id)->where('where','client')->where('id_action',$id)->first();
            if(empty($data)){
                HistoriqueAct::create([
                    'id_user'=>Auth::user()->id,
                    'what'=>'delete',
                    'where'=>'client',
                    'id_action'=>$id,
                    'username'=>Auth::user()->nom,
                    'userlastname'=>Auth::user()->prenom,
                    'id_valid' =>((Auth::user()->droit_de_valid_acts == 1)? 1 : 0),
                ]);
            }
        }else{
            HistoriqueAct::create([
                'id_user'=>Auth::user()->id,
                'what'=>'delete',
                'where'=>'client',
                'id_action'=>$id,
                'username'=>Auth::user()->nom,
                'userlastname'=>Auth::user()->prenom,
                'id_valid' =>((Auth::user()->droit_de_valid_acts == 1)? 1 : 0),
            ]);
            Client::where('id',$id)->delete();
        }
        
        return back();
    }

    public function print_fich_cli($id){
        $dossiers = Dossier::select(
            'clients.nom as nomcli',
            'clients.prenom as prenomcli',
            'clients.type_cli',
            'clients.raison_sos',
            'debiteurs.nom as nomdbtr',
            'debiteurs.prenom as prenomdbtr',
            'debiteurs.type_dtbr',
            'dossiers.*'
        )
            ->leftJoin('clients', 'clients.id', 'dossiers.id_client')
            ->leftJoin('debiteurs', 'debiteurs.id', 'dossiers.id_dbtr')
            ->where('dossiers.id_client', $id);

        // if (Auth::user()->see_all_doss != 1) {
        //     $dossiers = $dossiers->where('dossiers.creator_id', Auth::user()->id);
        // }
        $dossiers = $dossiers->get();
        // dd($dossiers);
        $data = [
            'dossiers'=>$dossiers,
            'client'=>Client::where('id',$id)->first(),
            'contact'=>Contact::where('id_client',$id)->get(),
            'logo' => base64_encode(file_get_contents('public/files/joints/image1.png'))
        ];


        $pdf = PDF::loadView('admin.client.fiche_cli', array('data' => $data));
        return $pdf->stream('fiche_cli.pdf');
    }
}
