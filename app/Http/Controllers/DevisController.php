<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Devis;
use App\Dossier;
use App\File_path;
use App\Files;
use App\Produit;
use App\DetailDossier;
use App\nbr_covert\Number;
use App\myLib\myFun;
use Auth;
use PDF;

class DevisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $devis = Devis::select('devis.*', 'dossiers.ref')
            ->leftjoin('dossiers', 'dossiers.id', 'devis.id_doss');

        if (Auth::user()->see_all_doss == 0) {
            $devis = $devis->where('dossiers.creator_id', Auth::user()->id);
        }

        $devis = $devis->get();
        return view('admin.devis.list')->with('devis', $devis);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addIndex()
    {
        $folds = Dossier::where('phase', 'Prise en charge');

        if (Auth::user()->see_all_doss == 0) {
            $folds = $folds->where('dossiers.creator_id', Auth::user()->id);
        }

        $folds = $folds->get();

        return view('admin.devis.add')->with('folds', $folds);
    }

    public function get_list_value(Request $req)
    {
        $DetailDossier = DetailDossier::select('detail_dossiers.type_pay')->where('id_doss', $req->id)->groupBy('type_pay')->get();
        return json_encode($DetailDossier);
    }
    public function store(Request $req)
    {

        $data = Devis::create([
            'id_doss' => $req->id_doss,
            'date_devie' => $req->date_devie,
            'valuer' => $req->valuer,
            'remarque' => $req->remarque
        ]);

        $prod_rows = (!empty($req->prod_rows)) ? count($req->prod_rows) : 0;
        
        for ($i = 0; $i < $prod_rows; $i++) {
            Produit::create([
                'id_devis' => $data->id,
                'designation' => $req->designation[$i],
                'montant' => $req->montant[$i],
                'remise' => $req->remise[$i],
            ]);
        }

        myFun::historiqueSaveData('create', 'Devis', $data->id, 'create');

        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_devis' => $data->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        return json_encode($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showDevis($id)
    {
        $folds = Dossier::where('phase', 'Prise en charge');

        if (Auth::user()->see_all_doss == 0) {
            $folds = $folds->where('dossiers.creator_id', Auth::user()->id);
        }

        $folds = $folds->get();

        $devis = Devis::select('devis.*', 'dossiers.ref')
            ->leftjoin('dossiers', 'dossiers.id', 'devis.id_doss')
            ->where('devis.id', $id)
            ->first();

        $type_pay = DetailDossier::select('detail_dossiers.type_pay')
            ->leftJoin('devis', 'devis.id_doss', 'detail_dossiers.id_doss')
            ->where('devis.id', $id)
            ->groupBy('detail_dossiers.type_pay')
            ->get();

        $Produit = Produit::where('id_devis', $id)->orderby('id', 'desc')->get();
        $files = Files::where('id_devis', $id)->get();
        $File_path = File_path::where('type', 'joints')->first();
        return view('admin.devis.show')
            ->with('type_pay', $type_pay)
            ->with('produit', $Produit)
            ->with('files', $files)
            ->with('devis', $devis)
            ->with('folds', $folds)
            ->with('File_path', $File_path);
    }

    public function editDevis(Request $req)
    {
        $Devis = Devis::where('id', $req->id_devis)->update([
            'id_doss' => $req->id_doss,
            'date_devie' => $req->date_devie,
            'valuer' => $req->valuer,
            'remarque' => $req->remarque,
        ]);

        if (isset($req->file_exc)) {
            Files::where('id_devis', $req->id_devis)
                ->whereNotIn('file_name', $req->file_exc)
                ->delete();
        } else {
            Files::where('id_devis', $req->id_devis)->delete();
        }

        Produit::where('id_devis', $req->id_devis)->delete();
        $prod_rows = (!empty($req->prod_rows)) ? count($req->prod_rows) : 0;
        for ($i = 0; $i < $prod_rows; $i++) {
            Produit::create([
                'id_devis' => $req->id_devis,
                'designation' => $req->designation[$i],
                'montant' => $req->montant[$i],
                'remise' => $req->remise[$i],
            ]);
        }

        myFun::historiqueSaveData('create', 'Devis', $req->id_devis, 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
        
        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_devis' => $req->id_devis,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }
        return json_encode($Devis);
    }
    public function printDevie($id)
    {

        $devis = Devis::select('dossiers.ref', 'devis.*', 'debiteurs.nom', 'debiteurs.prenom', 'debiteurs.ville_adress_prv')
            ->leftJoin('dossiers', 'dossiers.id', 'devis.id_doss')
            ->join('debiteurs', 'debiteurs.id', 'dossiers.id_dbtr')
            ->where('devis.id', $id)
            ->first();
        // dd($devis->valuer);
        $dtDoss = DetailDossier::select('detail_dossiers.*')
            ->Join('devis', 'devis.id_doss', 'detail_dossiers.id_doss')
            ->where('devis.id', $id)
            ->where('detail_dossiers.type_pay', $devis->valuer)
            ->first();
        $nb = new Number;
        $prods = Produit::where('id_devis', $id)->get();
        $data = [
            'dtDoss' => $dtDoss,
            'mt_creance' => $dtDoss->where('detail_dossiers.type_pay', $devis->valuer)->sum('mt_creance'),
            'mt_reclame' => $dtDoss->where('detail_dossiers.type_pay', $devis->valuer)->sum('mt_reclame'),
            'nb' => $nb,
            'devis' => $devis,
            'prods' => $prods,
            'logo' => base64_encode(file_get_contents('public/files/joints/image1.png'))
        ];
        $pdf = PDF::loadView('admin.devis.devispdf', array('data' => $data));
        return $pdf->stream('devie.pdf');
    }

    public function deleteDevis(Request $request, $id)
    {
        $res = myFun::historiqueSaveData('delete', 'Devis', $id, 'delete', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
        if(isset($res) && $res == 'do_it'){
            Devis::where('id', $id)->delete();
            Produit::where('id_devis', $id)->delete();
        }
        
        return back();
    }
}
