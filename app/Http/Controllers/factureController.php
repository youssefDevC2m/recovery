<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dossier;
use App\Facture;
use App\DetailFacture;
use App\Client;
use App\nbr_covert\Number;
use App\DossierFacture;
use App\Files;
use App\File_path;
use App\myLib\myFun;
use Auth;
use PDF;
class factureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addFacture()
    {
        $dossier = new Dossier();
        $client = Client::get();
        if(Auth::user()->see_all_doss == 0){
            $dossier = $dossier->where('creator_id',Auth::user()->id);
        }
        $dossier = $dossier->get();
        // dd($client);
        return view('admin.facture.add_facture')
        
        ->with('client',$client)
        ->with('dossier',$dossier);
    }

    public function factuerContn($id,$id_fact){
        $dossier = Dossier::select('dossiers.id as id_doss','dossiers.ref','debiteurs.*')
        ->join('debiteurs','debiteurs.id','dossiers.id_dbtr')
        ->where('dossiers.id_client',$id);

        if(Auth::user()->see_all_doss == 0){
            $dossier = $dossier->where('creator_id',Auth::user()->id);
        }
        $dossier = $dossier->get();
        // dd($dossier);
        return view('admin.facture.pros_continu')
        ->with('id_fact',$id_fact)
        ->with('dossier',$dossier);
        
    }
    public function factuerlaststep(Request $req){
        $test = [];
        if (isset($req->dossier)) {
            for($i = 0 ; $i < count($req->dossier) ; $i++){
                $test = DossierFacture::create([
                    'id_doss' => $req->dossier[$i],
                    'id_fact' => $req->id_fact
                ]);
            }
        }
        
        return json_encode($test);
        // DossierFacture
    }
    public function createFacture(Request $req)
    {
        $f = Facture::create([
            'id_client'=>$req->client,
            'type_facture'=>$req->type_facture,
            'date_facture'=>$req->date_facture,
            'remarque'=>$req->remarque,
            // 'montant_total'=>$req->remarque
        ]);

        if(isset($req->prod_rows)){
            for($i=0;$i < count($req->prod_rows); $i++){
                DetailFacture::create([
                    'id_facture'=>$f->id,
                    'designation'=>$req->designation[$i],
                    'montant'=>$req->montant[$i],
                    'remise'=>$req->remise[$i]
                ]);
            }
        }

        $filenameorg='';
        if(isset($req->file_val)){
            foreach($req->file_val as $file){
                $filenameorg = $file->getClientOriginalName();
                $filename =uniqid().$filenameorg;
                $file->move(public_path('files/joints/'),$filename);
                Files::create([
                    'id_facture'=>$f->id,
                    'file_name'=>$filename,
                    'org_file_name'=>$filenameorg
                ]);
            }
        }
        myFun::historiqueSaveData('create', 'Facture', $f->id, 'create');
        return json_encode(route('factuerContn',[$f->id_client,$f->id]));
    }

    public function listFacture()
    {
        $Facture = Facture::select('factures.*')
        // ->join('dossiers','dossiers.id','factures.doss')
        ->get();
       
        return view('admin.facture.list_factures')
        ->with('Facture',$Facture);
    }

    public function deleteFacture($id)
    {
        $res = myFun::historiqueSaveData('delete', 'Facture', $id, 'delete', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
        if($res == 'do_it'){
            Facture::where('id',$id)->delete();
        }
        return back();
    }
    
    public function showFacture($id)
    {  
         
        // dd($alldoss);
        $dossier = DossierFacture::select('dossiers.*','dossier_factures.id_fact')
        ->leftjoin('dossiers','dossiers.id','dossier_factures.id_doss')
        ->where('id_fact',$id);
        if(Auth::user()->see_all_doss == 0){
            $dossier = $dossier->where('creator_id',Auth::user()->id);
        }
        // $dossier = $dossier->get();
       
        $dossier = $dossier->get()->map(function($item){
            return $item->id;
        })->all();
        $file=Files::where('id_facture',$id)->get();
        $File_path=File_path::where('type','joints')->first();

        $produit = DetailFacture::where('id_facture',$id)->get();
        $facture = Facture::where('id',$id)->first();
        $client = Client::where('id',$facture->id_client)->first();
        $alldoss=Dossier::where('id_client',$facture->id_client)->get();
        return view('admin.facture.show_facture')
        ->with('files',$file)
        ->with('File_path',$File_path)
        ->with('alldoss',$alldoss)
        ->with('dossier',$dossier)
        ->with('client',$client)
        ->with('facture',$facture)
        ->with('produit',$produit);
    }

    public function updateFacture(Request $req)
    {
        Facture::where('id',$req->id_facture)
        ->update([
            'type_facture'=>$req->type_facture,
            'date_facture'=>$req->date_facture,
            'remarque'=>$req->remarque
        ]);
        DetailFacture::where('id_facture',$req->id_facture)->delete();
        
        myFun::historiqueSaveData('create', 'Facture', $req->id_facture, 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
        
        if (isset($req->prod_rows)) {
            for($i = 0; $i<count($req->prod_rows); $i++){
                DetailFacture::create([
                    'id_facture'=>$req->id_facture,
                    'designation'=>$req->designation[$i],
                    'montant'=>$req->montant[$i],
                    'remise'=>$req->remise[$i]
                ]);
            }
        }
        if (isset($req->dossier)) {
            DossierFacture::where('id_fact',$req->id_facture)->delete();
            for($i = 0; $i<count($req->dossier); $i++){
                DossierFacture::create([
                    'id_doss' => $req->dossier[$i],
                    'id_fact' => $req->id_facture
                ]);
            }
        }
        if(!empty($req->file_exc)){
            Files::where('id_facture',$req->id_facture)->whereNotIn('file_name',$req->file_exc)->delete();
        }else{
            Files::where('id_facture',$req->id_facture)->delete();
        }
        if(isset($req->file_val)){
            foreach($req->file_val as $file){
                $filenameorg = $file->getClientOriginalName();
                $filename =uniqid().$filenameorg;
                $file->move(public_path('files/joints/'),$filename);
                Files::create([
                    'id_facture'=>$req->id_facture,
                    'file_name'=>$filename,
                    'org_file_name'=>$filenameorg
                ]);
            }
        }
        return json_encode($req->id_facture);
    }
    
    public function print($id)
    {
        $fact= Facture::select('factures.*')
        ->where('factures.id',$id)->first();
        $dt = DetailFacture::where('id_facture',$id)->get();
        $nb = new Number;
        // dd($fact->date_facture);
        $data=[
            'id_fact' => (!empty($fact->id))? $fact->id : '',
            // 'doss' => (!empty($fact->ref))? $fact->ref : '',
            'date_facture'=>$fact->date_facture,
            'dt' => $dt,
            'nb' =>$nb,
            'logo'=>base64_encode(file_get_contents('public/files/joints/image1.png')),
        ];

        $pdf = PDF::loadView('admin.facture.facturePdf', array('data' => $data));
        return $pdf->stream('devie.pdf');
    }
}
