<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\User;
use App\Dossier;
use App\Client;
use App\Charges;
use App\Devis;
use App\HistoriqueAct;
// use App\dasePath;

use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $User=User::count();
        $Dossier= new Dossier();
        $Client=Client::select('historique_acts.id_action')
        ->join('historique_acts','historique_acts.id_action','clients.id')
        ->where('what','create')
        ->where('where','client')
        ->where('id_valid',1)
        ->distinct('historique_acts.id_action');

        $Client = (Auth::user()->acs_cli == 1)? $Client->count() : 0;  
        
        $Charges=Charges::select('historique_acts.id_action')
        ->join('historique_acts','historique_acts.id_action','charges.id')
        ->where('what','create')
        ->where('where','Charges')
        ->where('id_valid',1)
        ->distinct('historique_acts.id_action');
        $Charges = (Auth::user()->acs_cli == 1)? $Charges->count() : 0;  

        $Devis=Devis::select('historique_acts.id_action')
        ->join('historique_acts','historique_acts.id_action','devis.id')
        ->where('what','create')
        ->where('where','Devis')
        ->where('id_valid',1)
        ->distinct('historique_acts.id_action');
        $Devis = (Auth::user()->acs_cli == 1)? $Devis->count() : 0;  
        

        $HistoAct=HistoriqueAct::where('id_valid',0)
        ->orderby('created_at','desc')
        ->take(10)
        ->get();

        if(Auth::user()->see_all_doss == 0){
            $Dossier = Dossier::where('creator_id',Auth::user()->id);
        }

        $Dossier = count($Dossier->get());
        
        return view('admin.dashborde')
        ->with('HistoAct',$HistoAct)
        ->with('User',$User)
        ->with('Dossier',$Dossier)
        ->with('Client',$Client)
        ->with('Charges',$Charges)
        ->with('Devis',$Devis);
    } 
    
}
