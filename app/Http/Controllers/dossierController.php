<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Client;
use App\Bank;
use App\Debiteur;
use App\Dossier;
use App\Gerant;
use App\Contact;
use App\TypeCreance;
use App\DetailDossier;
use App\Files;
use App\File_path;
use App\HistoriqueScenario;
use App\User;
use App\scen_tele;
use App\scen_courrier;
use App\SenVisiteDomicile;
use App\ScenJudicCheq;
use App\ScenJudicInfoCheq;
use App\Validation_Dadresse;
use App\SenConvocation;
use App\SenMediation;
use App\SituationJudiciaire;
use App\Solvabilite_debiteur;
use App\FermetureDossier;
use App\Injonction_De_Payer;
use App\InfoCreance_inj_pay;
use App\ReqinjoncDpay;
use App\SituationDirect;
use App\SituationDirectFiles;
use App\partner;
use App\Procedure_range;
use App\Citation;
use App\CitAudiences;
use App\PhaCitAudiences;
use App\HistoriqueAct;
use App\SaisieFondComers;
use App\SaisieFontComersDeroulAudience;
use App\SaisieTiersDetenteur;
use App\SaisieTiersDetenteurDeroulAudience;
use App\SaisieArretBancaire;
use App\SaisieArretBancaireDeroulAudience;
use App\SaisieActionsPartsSoc;
use App\SaisieActionsPartsSocDeroulAudience;
use App\SaisieMobiler;
use App\SaisieMobilerDeroulAudience;
use App\SaisieImobiler;
use App\SaisieImmobilerDeroulAudience;
// use App\myLib\myFun;
use Auth;
use Illuminate\Support\Facades\Validator;
use Whoops\Run;

use function Symfony\Component\String\b;

class dossierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $dossierInfoSaisie;
    protected $ClientInfoSaisie;
    protected $debitersInfoSaisie;

    public function ouvertureDoss()
    {
        $clients = Client::get();
        return view('dossier.ouverture_doss')->with('client', $clients);
    }


    public function createDossier(Request $req)
    {
        $debit = Debiteur::create([
            'type_dtbr' => $req->type_dtbr,
            'nom' => $req->nom,
            'prenom' => ($req->type_dtbr == 'perso') ? $req->prenom : '',
            'cin' => ($req->type_dtbr == 'perso') ? $req->cin : '',
            'alias' => ($req->type_dtbr == 'perso') ? $req->alias : '',
            'employeur' => ($req->type_dtbr == 'perso') ? $req->employeur : '',
            'activite' => $req->activite,
            'adress_prv' => ($req->type_dtbr == 'perso') ? $req->adress_prv : '',
            'ville_adress_prv' => ($req->type_dtbr == 'perso') ? $req->ville_adress_prv : '',
            'adress_pro' => ($req->type_dtbr == 'perso') ? $req->adress_pro : '',
            'ville_adress_pro' => ($req->type_dtbr == 'perso') ? $req->ville_adress_pro : '',
            'forme_jurdq' => ($req->type_dtbr != 'perso') ? $req->forme_jurdq : '',
            'rc' => ($req->type_dtbr != 'perso') ? $req->rc : '',
            'if' => ($req->type_dtbr != 'perso') ? $req->if : '',
            'patent' => ($req->type_dtbr != 'perso') ? $req->patent : '',
            'adress_siege' => ($req->type_dtbr != 'perso') ? $req->adress_siege : '',
            'ville' => ($req->type_dtbr != 'perso') ? $req->ville_entre : '',
        ]);

        $doss = Dossier::create([
            'id_client' => $req->creanc,
            'id_dbtr' => $debit->id,
            'creator_id' => Auth::user()->id,
            'phase' => 'Ouverture',
            'date_ouverture' => $req->date_ouverture
        ]);
        if (isset($req->banque)) {
            for ($i = 0; $i < count($req->banque); $i++) {
                if (isset($req->banque[$i])) {
                    $bank = Bank::create([
                        'id_dbtr' => $debit->id,
                        'banque' => $req->banque[$i],
                        'agence' => $req->agence[$i],
                        'ville' => $req->ville[$i],
                        'n_compte' => $req->n_compte[$i],
                    ]);
                }
            }
        }
        if (isset($req->nom_gerant)) {
            for ($i = 0; $i < count($req->nb_gerant); $i++) {
                if (isset($req->nom_gerant[$i])) {
                    $gerant = Gerant::create([
                        'id_dbtr' => $debit->id,
                        'nom' => $req->nom_gerant[$i],
                        'prenom' => $req->prenom_gerant[$i],
                        'alias' => $req->alias_gerant[$i],
                        'cin' => $req->alias_gerant[$i],
                        'adress_pincipale' => $req->adress_pincipale_gerant[$i],
                        'ville' => $req->ville_gerant[$i],
                        'tele' => $req->tele_gerant[$i],
                    ]);
                }
            }
        }

        foreach ($req->tele as $tele) {
            if (!empty($tele)) {
                Contact::create([
                    'id_dbtr' => $debit->id,
                    'tele' => $tele
                ]);
            }
        }

        foreach ($req->fax as $fax) {
            if (!empty($fax)) {
                Contact::create([
                    'id_dbtr' => $debit->id,
                    'fax' => $fax
                ]);
            }
        }

        foreach ($req->mail as $mail) {
            if (!empty($mail)) {
                Contact::create([
                    'id_dbtr' => $debit->id,
                    'email' => $mail
                ]);
            }
        }

        foreach ($req->contact as $contact) {
            if (!empty($contact)) {
                Contact::create([
                    'id_dbtr' => $debit->id,
                    'contact' => $contact
                ]);
            }
        }
        HistoriqueAct::create([
            'id_user' => Auth::user()->id,
            'what' => 'create',
            'where' => 'dossier',
            'id_action' => $doss->id,
            'username' => Auth::user()->nom,
            'userlastname' => Auth::user()->prenom,
            'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0),
        ]);

        return json_encode($doss);
    }

    public function listDossier()
    {
        if (Auth::user()->role == 'Client') {
            $dossiers = Dossier::select(
                'clients.nom as nomcli',
                'clients.prenom as prenomcli',
                'clients.type_cli',
                'clients.raison_sos',
                'debiteurs.nom as nomdbtr',
                'debiteurs.prenom as prenomdbtr',
                'debiteurs.type_dtbr',
                'dossiers.*'
            )
                ->leftJoin('clients', 'clients.id', 'dossiers.id_client')
                ->leftJoin('debiteurs', 'debiteurs.id', 'dossiers.id_dbtr')
                ->where('dossiers.id_client', Auth::user()->id)
                ->get();
        } else {
            $dossiers = Dossier::select(
                'clients.nom as nomcli',
                'clients.prenom as prenomcli',
                'clients.type_cli',
                'clients.raison_sos',
                'debiteurs.nom as nomdbtr',
                'debiteurs.prenom as prenomdbtr',
                'debiteurs.type_dtbr',
                'dossiers.*'
            )
                ->leftJoin('clients', 'clients.id', 'dossiers.id_client')
                ->leftJoin('debiteurs', 'debiteurs.id', 'dossiers.id_dbtr');

            if (Auth::user()->see_all_doss != 1) {
                $dossiers = $dossiers->where('dossiers.creator_id', Auth::user()->id);
            }

            $dossiers = $dossiers->get();
        }
        return view('dossier.list_dossier')
            ->with('dossiers', $dossiers);
    }
    public function list_alert_anc()
    {
        $dossiers = Dossier::select(
            'clients.nom as nomcli',
            'clients.prenom as prenomcli',
            'clients.type_cli',
            'clients.raison_sos',
            'debiteurs.nom as nomdbtr',
            'debiteurs.prenom as prenomdbtr',
            'debiteurs.type_dtbr',
            'dossiers.*'
        )
            ->leftJoin('clients', 'clients.id', 'dossiers.id_client')
            ->leftJoin('debiteurs', 'debiteurs.id', 'dossiers.id_dbtr')
            ->leftJoin('detail_dossiers', 'detail_dossiers.id_doss', 'dossiers.id')
            ->where('date_ancent', '<=', date('Y-m-d', strtotime('-48 months', strtotime(date('Y-m-d')))))
            ->orWhere('date_ancent', '<=', date('Y-m-d', strtotime('-36 months', strtotime(date('Y-m-d')))))
            ->distinct('dossiers.id')
            ->orderBy('dossiers.id', 'desc');

        $ex_doss = Dossier::select(
            'clients.nom as nomcli',
            'clients.prenom as prenomcli',
            'clients.type_cli',
            'clients.raison_sos',
            'debiteurs.nom as nomdbtr',
            'debiteurs.prenom as prenomdbtr',
            'debiteurs.type_dtbr',
            'dossiers.*'
        )
            ->leftJoin('clients', 'clients.id', 'dossiers.id_client')
            ->leftJoin('debiteurs', 'debiteurs.id', 'dossiers.id_dbtr')
            ->leftJoin('detail_dossiers', 'detail_dossiers.id_doss', 'dossiers.id')
            ->where("date_fin_doss", '<=', date('Y-m-d H:i:s'))
            ->distinct('dossiers.id')
            ->orderBy('dossiers.id', 'desc');

        if (Auth::user()->see_all_doss != 1) {
            $dossiers = $dossiers->where('dossiers.creator_id', Auth::user()->id);
            $ex_doss = $ex_doss->where('dossiers.creator_id', Auth::user()->id);
        }

        $dossiers = $dossiers->get();
        $ex_doss = $ex_doss->get();

        $res = $dossiers->merge($ex_doss);

        return view('dossier.list_dossier')
            ->with('bg_alert', 'background: #f82222 !important;color:white;')
            ->with('alert_messag', 'Liste des dossiers avec des (cheque/facture/effet) expiré')
            ->with('dossiers', $res);
    }

    public function showDebiteur($id)
    {
        $debits = Debiteur::find($id);
        $bank = Bank::where('id_dbtr', $id)->get();
        $gerant = Gerant::where('id_dbtr', $id)->get();
        $contact = Contact::where('id_dbtr', $id)->get();
        return view('dossier.debiteur')
            ->with('bank', $bank)
            ->with('gerant', $gerant)
            ->with('contact', $contact)
            ->with('debits', $debits);
    }

    public function showDossier($id)
    {
        $dossiers = Dossier::find($id);
        Dossier::where("date_fin_doss", '<=', date('Y-m-d H:i:s'))->where('id', $id)->update([
            'doss_exp_vu' => 1
        ]);
        $details = DetailDossier::where('id_doss', $id)->get();
        $files = Files::select('files.*')
            ->leftJoin('detail_dossiers', 'detail_dossiers.id', 'files.id_detail')
            ->leftJoin('dossiers', 'dossiers.id', 'detail_dossiers.id_doss')
            ->get();
        // dd($dossiers);
        $debiteur = Debiteur::select('debiteurs.*')
            ->leftjoin('dossiers', 'dossiers.id_dbtr', 'debiteurs.id')
            ->where('dossiers.id', $id)
            ->first();
        if (!empty($debiteur)) {
            $bank = Bank::where('id_dbtr', $debiteur->id)->get();
        }
        // dd($bank);
        $client = Client::get();
        $typeCreance = TypeCreance::get();
        $file_join = File_path::where('type', 'joints')->first();
        return view('dossier.show_dossier')
            ->with('typeCreance', $typeCreance)
            ->with('client', $client)
            ->with('bank', $bank)
            ->with('file_join', $file_join)
            ->with('debiteur', $debiteur)
            ->with('dossiers', $dossiers)
            ->with('details', $details)
            ->with('files', $files);
    }

    public function get_bank_info(Request $req)
    {
        $bank = Bank::where('id', $req->id_b)->first();
        return json_encode((!empty($bank)) ? $bank  : "0");
    }

    public function dateDifference($date_1, $date_2 /*, $differenceFormat = '%a-%h'*/)
    {
        // $datetime1 = date_create($date_1);
        // $datetime2 = date_create($date_2);
        // $interval = date_diff($datetime1, $datetime2);
        // return $interval->format($differenceFormat);
        $diff = abs(strtotime($date_2) - strtotime($date_1));
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
        $min = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / (60));
        $sec = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $min * 60));
        $date_inter = '';

        if ($years > 0) {
            $date_inter .= $years . ' a ';
        }
        if ($months > 0) {
            $date_inter .= $months . ' m ';
        }
        if ($days > 0) {
            $date_inter .= $days . ' j ';
        }
        // if($hours > 0){

        //     $date_inter.=$hours.' h ';
        // }if($min > 0){

        //     $date_inter.=$min .' min ';
        // }
        // if($sec > 0 && $min==0){
        //     $date_inter.=$sec.' sec ';
        // }

        return $date_inter;
    }

    public function editDossier(Request $req)
    {

        // for($i=0;$i < count($req->details_block_nb) ; $i++ ){

        //     if($req->id_dt[$i]==-1){

        //         if($req->type_pay[$i]=='Effet' || $req->type_pay[$i]=='Facture' || $req->type_pay[$i]=='Chèque'){
        //             if( date('Y-m-d',strtotime('-36 months',strtotime(date('Y-m-d')))) >= $req->date_ancent[$i]){

        //                 if($req->type_pay[$i]=='Chèque' && date('Y-m-d',strtotime('-48 months',strtotime(date('Y-m-d')))) >= $req->date_ancent[$i]){
        //                     return json_encode(['data'=>0,'err'=>" Date d'ancienneté de ".$req->type_pay[$i]." est non valide (".$req->date_ancent[$i].")"]);
        //                 }
        //                 return json_encode(['data'=>0,'err'=>" Date d'ancienneté de ".$req->type_pay[$i]." est non valide (".$req->date_ancent[$i].")"]);
        //             }
        //         }
        //     }

        // }


        $mt_glob_creance = (!empty($req->mt_glob_creance)) ? str_replace(' ', '', $req->mt_glob_creance) : 0;
        $mt_glob_creance = str_replace(',', '', $mt_glob_creance);
        $mt_glob_creance = preg_replace("/[A-z]/i", "", $mt_glob_creance);

        $rm_dts = DetailDossier::where('id_doss', $req->id_doss)->get();
        foreach ($rm_dts as $rm_dt) {
            Files::where('id_detail', $rm_dt->id)->delete();
        }
        DetailDossier::where('id_doss', $req->id_doss)->delete();
        // return 1;

        $dos = Dossier::where('id', $req->id_doss)->update([
            'date_resep' => $req->date_resep,
            'ref' => $req->ref,
            'id_client' => $req->id_client,
            'id_dbtr' => $req->id_dbtr,
            'phase' => 'Prise en charge',
            'mt_glob_creance' => $mt_glob_creance,
            'observation' => $req->observation
        ]);

        for ($i = 0; $i < count($req->details_block_nb); $i++) {

            $detail = DetailDossier::create([
                'id_doss' => $req->id_doss,
                'type_pay' => $req->type_pay[$i],
                'numero' => $req->numero[$i],
                'bank' => $req->bank[$i],
                'ville' => $req->ville[$i],
                'motif' => $req->motif[$i],
                'date_ancent' => $req->date_ancent[$i],
                'date_creation' => $req->date_creation[$i],
                'date_rejet' => $req->date_creation[$i],
                'mt_creance' => $req->mt_creance[$i],
                'mt_reclame' => $req->mt_reclame[$i],
                'remarque' => (isset($req->remarque[$i])) ? $req->remarque[$i] : ''
            ]);
            $filename = '';
            $j = 0;
            $a = 'file_val' . $i;
            $b = 'file_from_db' . $i;
            if (isset($req->$b)) {
                foreach ($req->$b as $file_from_db) {
                    Files::create([
                        'id_detail' => $detail->id,
                        'file_name' => uniqid() . $file_from_db,
                        'org_file_name' => $file_from_db
                    ]);
                }
            }
            if ($req->hasFile($a)) {
                foreach ($req->$a as $file) {
                    $filenameorg = $file->getClientOriginalName();
                    $filename = uniqid() . $filenameorg;
                    $file->move(public_path('files/joints/'), $filename);
                    Files::create([
                        'id_detail' => $detail->id,
                        'file_name' => $filename,
                        'org_file_name' => $filenameorg
                    ]);
                }
            }
        }
        if (Auth::user()->droit_de_valid_acts == 0) {
            HistoriqueAct::where('id_action', $req->id_doss)
                ->where('id_valid', 1)
                ->where('what', 'create')
                ->where('where', 'dossier')
                ->update(
                    ['id_valid' => '0']
                );
        }
        return json_encode(['data' => $detail]);
    }


    public function editDebiteurDossier(Request $req)
    {
        // return json_encode(Contact::find($req->id_dbtr));
        $debit = Debiteur::where('id', $req->id_dbtr)->update([
            'type_dtbr' => $req->type_dtbr,
            'nom' => $req->nom,
            'prenom' => ($req->type_dtbr == 'perso') ? $req->prenom : '',
            'cin' => ($req->type_dtbr == 'perso') ? $req->cin : '',
            'alias' => ($req->type_dtbr == 'perso') ? $req->alias : '',
            'employeur' => ($req->type_dtbr == 'perso') ? $req->employeur : '',
            'activite' => $req->activite,
            'adress_prv' => ($req->type_dtbr == 'perso') ? $req->adress_prv : '',
            'ville_adress_prv' => ($req->type_dtbr == 'perso') ? $req->ville_adress_prv : '',
            'adress_pro' => ($req->type_dtbr == 'perso') ? $req->adress_pro : '',
            'ville_adress_pro' => ($req->type_dtbr == 'perso') ? $req->ville_adress_pro : '',
            'forme_jurdq' => ($req->type_dtbr != 'perso') ? $req->forme_jurdq : '',
            'rc' => ($req->type_dtbr != 'perso') ? $req->rc : '',
            'if' => ($req->type_dtbr != 'perso') ? $req->if : '',
            'patent' => ($req->type_dtbr != 'perso') ? $req->patent : '',
            'adress_siege' => ($req->type_dtbr != 'perso') ? $req->adress_siege : '',
            'ville' => ($req->type_dtbr != 'perso') ? $req->ville_entre : '',
        ]);
        if (Auth::user()->role = "Administrateur") {
            $doss = Dossier::where('id', $req->id_dbtr)->update([
                'date_ouverture' => $req->date_ouverture
            ]);
        }
        Bank::where('id_dbtr', $req->id_dbtr)->delete();
        if (isset($req->banque)) {
            for ($i = 0; $i < count($req->banque); $i++) {
                if (isset($req->banque[$i])) {
                    $bank = Bank::create([
                        'id_dbtr' => $req->id_dbtr,
                        'banque' => $req->banque[$i],
                        'agence' => $req->agence[$i],
                        'ville' => $req->ville[$i],
                        'n_compte' => $req->n_compte[$i],
                    ]);
                }
            }
        }
        Gerant::where('id_dbtr', $req->id_dbtr)->delete();
        if (isset($req->nom_gerant)) {
            for ($i = 0; $i < count($req->nb_gerant); $i++) {
                if (isset($req->nom_gerant[$i])) {
                    $gerant = Gerant::create([
                        'id_dbtr' => $req->id_dbtr,
                        'nom' => $req->nom_gerant[$i],
                        'prenom' => $req->prenom_gerant[$i],
                        'alias' => $req->alias_gerant[$i],
                        'cin' => $req->alias_gerant[$i],
                        'adress_pincipale' => $req->adress_pincipale_gerant[$i],
                        'ville' => $req->ville_gerant[$i],
                        'tele' => $req->tele_gerant[$i],
                    ]);
                }
            }
        }
        Contact::where('id_dbtr', $req->id_dbtr)->delete();

        foreach ($req->tele as $tele) {
            if (!empty($tele)) {
                Contact::create([
                    'id_dbtr' => $req->id_dbtr,
                    'tele' => $tele
                ]);
            }
        }

        foreach ($req->fax as $fax) {
            if (!empty($fax)) {
                Contact::create([
                    'id_dbtr' => $req->id_dbtr,
                    'fax' => $fax
                ]);
            }
        }

        foreach ($req->mail as $mail) {
            if (!empty($mail)) {
                Contact::create([
                    'id_dbtr' => $req->id_dbtr,
                    'email' => $mail
                ]);
            }
        }

        foreach ($req->contact as $contact) {
            if (!empty($contact)) {
                Contact::create([
                    'id_dbtr' => $req->id_dbtr,
                    'contact' => $contact
                ]);
            }
        }

        return json_encode(Contact::where('id_dbtr', $req->id_dbtr)->delete());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDossier($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'dossier')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'dossier',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0),
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        } else {
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'dossier',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0),
            ]);
            Dossier::where('id', $id)->delete();
        }

        return back();
    }
    public function SceneList($id)
    {
        // $test = HistoriqueScenario::select('id')->with('situCheque')->find($id)->first();
        // dd($test);
        // return ;

        $agent = User::where('role', 'Agent')->get();
        $HistoriqueScenario = HistoriqueScenario::where('id_doss', $id)->first();
        $scen_tele = scen_tele::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->get();
        $scen_courr = scen_courrier::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->get();
        $SenVisDom = SenVisiteDomicile::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->get();
        $cheque = 0;
        $effet = 0;
        $facture = 0;
        $dossier = DetailDossier::where('detail_dossiers.id_doss', $id)->get();
        $Validation_Dadresse = Validation_Dadresse::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->get();
        $SenConvo = SenConvocation::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->get();
        // $ScenJudicCheq=ScenJudicCheq::where('id_hist_scen',(isset($HistoriqueScenario->id))? $HistoriqueScenario->id : 0)->first();
        // $infoCheq=ScenJudicInfoCheq::where('id_hist_scen',(isset($HistoriqueScenario->id))? $HistoriqueScenario->id : 0)->get();
        $SenMedi = SenMediation::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->get();
        $debiteur = Debiteur::select('debiteurs.nom', 'debiteurs.prenom')
            ->leftjoin('dossiers', 'dossiers.id_dbtr', 'debiteurs.id')
            ->where('dossiers.id', $id)
            ->first();
        $partner = partner::get();
        $SituJudic = SituationJudiciaire::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->get();
        $solvab_debi = Solvabilite_debiteur::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->get();
        $Inj_De_Payer = Injonction_De_Payer::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->first();

        $InfoCreance_inj_pay = InfoCreance_inj_pay::select('info_creance_inj_pays.*')
            ->join('injonction__de__payers', 'injonction__de__payers.id', 'info_creance_inj_pays.id_inj_pays')
            ->where('injonction__de__payers.id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)
            ->get();

        $SituationDirect = SituationDirect::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->first();
        // dd($SituationDirect);
        $SituationDirectFiles = SituationDirectFiles::where('id_st_dir', (isset($SituationDirect->id)) ? $SituationDirect->id : 0)->get();
        $ReqinjoncDpay = ReqinjoncDpay::where('id_hist_scen', (isset($HistoriqueScenario->id)) ? $HistoriqueScenario->id : 0)->first();
        $File_path = File_path::where('type', 'joints')->first();
        foreach ($dossier as $item) {
            if ($item->type_pay == 'Cheque') {
                $cheque = 1;
            }
            if ($item->type_pay == 'Effet') {
                $effet = 1;
            }
            if ($item->type_pay == 'Facture') {
                $facture = 1;
            }
        }
        $doss = Dossier::where('id', $id)->first();
        $Procedure_range = Procedure_range::where('id_doss', $id)->get();
        // dd($Procedure_range);
        return view('admin.scens.scens')
            ->with('Procedure_range', $Procedure_range)
            ->with('doss', $doss)
            ->with('partner', $partner)
            ->with('SituationDirect', $SituationDirect)
            ->with('SituationDirectFiles', $SituationDirectFiles)
            ->with('SenVisDom', $SenVisDom)
            ->with('id_doss', $id)
            ->with('histo', $HistoriqueScenario)
            ->with('scen_tele', $scen_tele)
            ->with('scen_courr', $scen_courr)
            ->with('facture', $facture)
            ->with('File_path', $File_path)
            ->with('cheque', $cheque)
            ->with('dossier', $dossier)
            ->with('ValidAdrs', $Validation_Dadresse)
            // ->with('ScenJudicCheq',$ScenJudicCheq)
            ->with('SenConvo', $SenConvo)
            // ->with('infoCheq',$infoCheq)
            ->with('effet', $effet)
            ->with('solvab_debi', $solvab_debi)
            ->with('SenMedi', $SenMedi)
            ->with('debiteur', $debiteur)
            ->with('SituJudic', $SituJudic)
            ->with('Inj_De_Payer', $Inj_De_Payer)
            ->with('InfoCreance_inj_pay', $InfoCreance_inj_pay)
            ->with('ReqinjoncDpay', $ReqinjoncDpay)
            ->with('agent', $agent);
    }
    public function createScene(Request $req)
    {
        $histo = "";
        // return json_encode(count($req->cheque_nb));
        $injoncDpayarr = [];
        $vareqinjoncDpay = [];
        Dossier::where('id', $req->id_doss)->update([
            'date_db_doss' => $req->date_db_doss,
            'date_fin_doss' => $req->date_fin_doss
        ]);
        if ($req->id_scenH == 0) {
            $histo = HistoriqueScenario::create([
                'id_doss' => $req->id_doss,
                'mediateur' => $req->mediateur,
                'agence' => $req->agence,
                'date_remise' => $req->date_remise,
                'date_retoure' => $req->date_retoure,
                'nombre_tele_max' => (isset($req->nombre_tele_max) ? $req->nombre_tele_max : 0),
                'nombre_courr_max' => (isset($req->nombre_courr_max) ? $req->nombre_courr_max : 0),
                'nombre_visit_max' => (isset($req->nombre_visit_max) ? $req->nombre_visit_max : 0),
                'nombre_valid_adrs_max' => (isset($req->valid_adrs_max) ? $req->valid_adrs_max : 0),
                'nombre_convo_max' => (isset($req->nombre_convo_max) ? $req->nombre_convo_max : 0),
                'nombre_medi_max' => (isset($req->nombre_medi_max) ? $req->nombre_medi_max : 0),
                'nombre_situJud_max' => (isset($req->nombre_situJud_max) ? $req->nombre_situJud_max : 0),
                'nombre_solvaDebit_max' => (isset($req->nombre_solvaDebit_max) ? $req->nombre_solvaDebit_max : 0),
                'tel_is_sel' => (isset($req->tel_is_sel) ? 1 : 0),
                'courr_is_sel' => (isset($req->courr_is_sel) ? 1 : 0),
                'valid_adrs_is_sel' => (isset($req->valid_adrs_is_sel) ? 1 : 0),
                'visit_is_sel' => (isset($req->visit_is_sel) ? 1 : 0),
                'convo_is_sel' => (isset($req->convo_is_sel) ? 1 : 0),
                'medi_is_sel' => (isset($req->medi_is_sel) ? 1 : 0),
                'situJud_is_sel' => (isset($req->situJud_is_sel) ? 1 : 0),
                'cheque_ref' => (isset($req->cheque_ref) ? 1 : 0),
                'injoncDpay' => (isset($req->injoncDpay) ? 1 : 0),
                'solvaDebit_is_sel' => (isset($req->solvaDebit_is_sel) ? 1 : 0),
                'reqinjoncDpay' => (isset($req->reqinjoncDpay) ? 1 : 0),
                'SituationDirect' => (isset($req->SituationDirect) ? 1 : 0),
                'id_creator' => Auth::user()->id
            ]);
            if (isset($req->tel_is_sel)) {
                for ($i = 0; $i < count($req->type_tele); $i++) {
                    scen_tele::create([
                        'id_hist_scen' => $req->id_scenH,
                        'nom_charge' => $req->nom_charge[$i],
                        'type_tele' => $req->type_tele[$i],
                        'date_H_contact' => $req->date_H_contact[$i],
                        'n_tele' => $req->n_tele[$i],
                        'desc_num_tel' => $req->desc_num_tel[$i],
                        'promess_pay' => $req->promess_pay[$i],
                        'id_creator' => Auth::user()->id
                    ]);
                }
            }
            if (isset($req->solvaDebit_is_sel)) {
                for ($i = 0; $i < count($req->nbsolva); $i++) {
                    Solvabilite_debiteur::create([
                        'id_hist_scen' => $histo->id,
                        'bien_personnels' => $req->bien_personnels[$i],
                        'td' => $req->td[$i],
                        'hertage' => $req->hertage[$i],
                        'participation' => $req->participation[$i],
                        'comment_solv_deb' => $req->comment_solv_deb[$i],
                    ]);
                }
            }
            if (isset($req->courr_is_sel)) {
                for ($i = 0; $i < count($req->date_env); $i++) {
                    scen_courrier::create([
                        'id_hist_scen' => $histo->id,
                        'date_env' => $req->date_env[$i],
                        'mise_dem_par' => (isset($req->mise_dem_par[$i])) ? $req->mise_dem_par[$i] : '',
                        'comment' => $req->comment[$i],
                        'id_creator' => Auth::user()->id
                    ]);
                }
            }
            if (isset($req->visit_is_sel)) {
                for ($i = 0; $i < count($req->domicile); $i++) {
                    SenVisiteDomicile::create([
                        'id_hist_scen' => $histo->id,
                        'adress_perso' => ($req->domicile[$i] == 'personnel') ? $req->adress[$i] : '',
                        'adress_pro' => ($req->domicile[$i] == 'pro') ? $req->adress[$i] : '',
                        'adress_proche' => ($req->domicile[$i] == 'proche') ? $req->adress[$i] : '',
                        'date_H_visite' => $req->date_H_visite[$i],
                        'visiteur' => $req->visiteur[$i],
                        'promess' => $req->promess[$i],
                        'comment' => $req->comment[$i],
                        'domicile' => $req->domicile[$i]
                    ]);
                }
            }
            if (isset($req->valid_adrs_is_sel)) {
                for ($i = 0; $i < count($req->date_valid_adres); $i++) {
                    if ($req->valid_adrs_max > $i) {
                        Validation_Dadresse::create([
                            'id_hist_scen' => $histo->id,
                            'date_valid_adres' => $req->date_valid_adres[$i],
                            'effect_par' => $req->effect_par[$i],
                            'type_debit' => (isset($req->type_debit[$i])) ? $req->type_debit[$i] : '',
                            'comment_vali_adrs' => $req->comment_vali_adrs[$i]
                        ]);
                    }
                }
            }
            if (isset($req->convo_is_sel)) {
                for ($i = 0; $i < count($req->date_H_conv); $i++) {
                    if ($req->nombre_convo_max > $i) {
                        SenConvocation::create([
                            'id_hist_scen' => $histo->id,
                            'remise_par' => $req->remise_par[$i],
                            'date_H_conv' => $req->date_H_conv[$i],
                            'obtenu_par' => (isset($req->obtenu_par[$i])) ? $req->obtenu_par[$i] : '',
                            'comment' => $req->comment_convo[$i]
                        ]);
                    }
                }
            }
            if (isset($req->medi_is_sel)) {
                for ($i = 0; $i < count($req->lieu_med); $i++) {
                    if ($req->nombre_medi_max > $i) {
                        SenMediation::create([
                            'id_hist_scen' => $histo->id,
                            'lieu_med' => $req->lieu_med[$i],
                            'adress_med' => $req->adress_med[$i],
                            'date_med' => $req->date_med[$i],
                            'mediateur' => $req->mediateur[$i],
                            'debiteur_mandataire' => $req->debiteur_mandataire[$i],
                            'promesse' => $req->promesse_medi[$i],
                            'comment' => $req->comment_medi[$i],
                        ]);
                    }
                }
            }
            if (isset($req->situJud_is_sel)) {
                for ($i = 0; $i < count($req->stj_nb); $i++) {
                    if ($req->nombre_situJud_max > $i) {
                        SituationJudiciaire::create([
                            'id_hist_scen' => $histo->id,
                            'type_situation_judic' => (isset($req->type_situation_judic[$i])) ? $req->type_situation_judic[$i] : '',
                            'comment' => $req->comment_stj[$i]
                        ]);
                    }
                }
            }

            if (isset($req->reqinjoncDpay)) {
                $vareqinjoncDpay = [
                    'id_hist_scen' => $histo->id,
                    'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                ];
                if (isset($req->les_proc_req_inj_pay)) {
                    $vareqinjoncDpay = [
                        'id_hist_scen' => $histo->id,
                        'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                        'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                        'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                        'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                    ];
                }
                if (isset($req->les_proc_app_req_inj_pay)) {
                    $vareqinjoncDpay = [
                        'id_hist_scen' => $histo->id,
                        'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                        'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                        'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                        'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                        'les_proc_app_req_inj_pay' => $req->les_proc_app_req_inj_pay,
                        'dat_dep_req_inj_pay' => $req->dat_dep_req_inj_pay,
                        'num_app_req_inj_pay' => $req->num_app_req_inj_pay,
                        'dat_app_req_inj_pay' => $req->dat_app_req_inj_pay,
                        'nom_consltnt_req_inj_pay' => $req->nom_consltnt_req_inj_pay,
                        'trib_competnt_req_inj_pay' => $req->trib_competnt_req_inj_pay,
                        'note_et_audience_req_inj_pay' => $req->note_et_audience_req_inj_pay,
                        'dat_dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                        'dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                    ];
                }
                if (isset($req->swt_proc_notif_req_inj_pay)) {
                    $vareqinjoncDpay = [
                        'id_hist_scen' => $histo->id,
                        'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                        'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                        'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                        'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                        'les_proc_app_req_inj_pay' => $req->les_proc_app_req_inj_pay,
                        'dat_dep_req_inj_pay' => $req->dat_dep_req_inj_pay,
                        'num_app_req_inj_pay' => $req->num_app_req_inj_pay,
                        'dat_app_req_inj_pay' => $req->dat_app_req_inj_pay,
                        'nom_consltnt_req_inj_pay' => $req->nom_consltnt_req_inj_pay,
                        'trib_competnt_req_inj_pay' => $req->trib_competnt_req_inj_pay,
                        'note_et_audience_req_inj_pay' => $req->note_et_audience_req_inj_pay,
                        'dat_dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                        'dep_doss_au_trib_req_inj_pay' => $req->dep_doss_au_trib_req_inj_pay,
                        'swt_proc_notif_req_inj_pay' => $req->swt_proc_notif_req_inj_pay,
                        'date_dep_notif_req_inj_pay' => $req->date_dep_notif_req_inj_pay,
                        'num_doss_notif_req_inj_pay' => $req->num_doss_notif_req_inj_pay,
                        'huissier_des_req_inj_pay' => $req->huissier_des_req_inj_pay,
                        'reslt_notif_req_inj_pay' => $req->reslt_notif_req_inj_pay,
                        'date_appl_notif_req_inj_pay' => $req->date_appl_notif_req_inj_pay,
                        'proces_notif_req_inj_pay' => $req->proces_notif_req_inj_pay,
                    ];
                }
                if (isset($req->swt_proc_exe_req_inj_pay)) {
                    $vareqinjoncDpay = [
                        'id_hist_scen' => $histo->id,
                        'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                        'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                        'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                        'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                        'les_proc_app_req_inj_pay' => $req->les_proc_app_req_inj_pay,
                        'dat_dep_req_inj_pay' => $req->dat_dep_req_inj_pay,
                        'num_app_req_inj_pay' => $req->num_app_req_inj_pay,
                        'dat_app_req_inj_pay' => $req->dat_app_req_inj_pay,
                        'nom_consltnt_req_inj_pay' => $req->nom_consltnt_req_inj_pay,
                        'trib_competnt_req_inj_pay' => $req->trib_competnt_req_inj_pay,
                        'note_et_audience_req_inj_pay' => $req->note_et_audience_req_inj_pay,
                        'dat_dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                        'dep_doss_au_trib_req_inj_pay' => $req->dep_doss_au_trib_req_inj_pay,
                        'swt_proc_notif_req_inj_pay' => $req->swt_proc_notif_req_inj_pay,
                        'date_dep_notif_req_inj_pay' => $req->date_dep_notif_req_inj_pay,
                        'num_doss_notif_req_inj_pay' => $req->num_doss_notif_req_inj_pay,
                        'huissier_des_req_inj_pay' => $req->huissier_des_req_inj_pay,
                        'reslt_notif_req_inj_pay' => $req->reslt_notif_req_inj_pay,
                        'date_appl_notif_req_inj_pay' => $req->date_appl_notif_req_inj_pay,
                        'proces_notif_req_inj_pay' => $req->proces_notif_req_inj_pay,
                        'swt_proc_exe_req_inj_pay' => $req->swt_proc_exe_req_inj_pay,
                        'num_dos_exe_req_inj_pay' => $req->num_dos_exe_req_inj_pay,
                        'date_dos_exe_req_inj_pay' => $req->date_dos_exe_req_inj_pay,
                        'huissier_designe_exe_req_inj_pay' => $req->huissier_designe_exe_req_inj_pay,
                        'date_exe_req_inj_pay' => $req->date_exe_req_inj_pay,
                        'expiration_exe_req_inj_pay' => $req->expiration_exe_req_inj_pay,
                        'dat_saisie_req_inj_pay' => $req->dat_saisie_req_inj_pay,
                        'type_saisie_req_inj_pay' => $req->type_saisie_req_inj_pay,
                        'date_exe_judic_req_inj_pay' => $req->date_exe_judic_req_inj_pay,
                        'nom_expert_exe_req_inj_pay' => $req->nom_expert_exe_req_inj_pay,
                        'pub_jal_req_inj_pay' => $req->pub_jal_req_inj_pay,
                        'nom_journal_req_inj_pay' => $req->nom_journal_req_inj_pay,
                        'vent_aux_encher_public_req_inj_pay' => $req->vent_aux_encher_public_req_inj_pay,
                        'date_vent_exe_req_inj_pay' => $req->date_vent_exe_req_inj_pay,
                        'result_exe_req_inj_pay' => $req->result_exe_req_inj_pay,
                    ];
                }
                $data = ReqinjoncDpay::create($vareqinjoncDpay);
            }
            if ($req->SituationDirect) {
                $stdir = SituationDirect::create([
                    'id_hist_scen' => $histo->id,
                    'remarque' => $req->situ_direc_rmarq
                ]);
                if (isset($req->file_val)) {
                    foreach ($req->file_val as $file) {
                        $filenameorg = $file->getClientOriginalName();
                        $filename = uniqid() . $filenameorg;
                        $file->move(public_path('files/joints/'), $filename);
                        $data2 = SituationDirectFiles::create([
                            'id_st_dir' => $stdir->id,
                            'file_name' => $filename,
                            'org_file_name' => $filenameorg
                        ]);
                    }
                }
            }
        } else {
            $histo = HistoriqueScenario::where('id', $req->id_scenH)->update([
                'id_doss' => $req->id_doss,
                'mediateur' => $req->mediateur,
                'agence' => $req->agence,
                'date_remise' => $req->date_remise,
                'date_retoure' => $req->date_retoure,
                'nombre_tele_max' => (isset($req->nombre_tele_max) ? $req->nombre_tele_max : 0),
                'nombre_courr_max' => (isset($req->nombre_courr_max) ? $req->nombre_courr_max : 0),
                'nombre_visit_max' => (isset($req->nombre_visit_max) ? $req->nombre_visit_max : 0),
                'nombre_valid_adrs_max' => (isset($req->valid_adrs_max) ? $req->valid_adrs_max : 0),
                'nombre_convo_max' => (isset($req->nombre_convo_max) ? $req->nombre_convo_max : 0),
                'nombre_medi_max' => (isset($req->nombre_medi_max) ? $req->nombre_medi_max : 0),
                'nombre_situJud_max' => (isset($req->nombre_situJud_max) ? $req->nombre_situJud_max : 0),
                'nombre_solvaDebit_max' => (isset($req->nombre_solvaDebit_max) ? $req->nombre_solvaDebit_max : 0),
                'tel_is_sel' => (isset($req->tel_is_sel) ? 1 : 0),
                'courr_is_sel' => (isset($req->courr_is_sel) ? 1 : 0),
                'valid_adrs_is_sel' => (isset($req->valid_adrs_is_sel) ? 1 : 0),
                'visit_is_sel' => (isset($req->visit_is_sel) ? 1 : 0),
                'convo_is_sel' => (isset($req->convo_is_sel) ? 1 : 0),
                'medi_is_sel' => (isset($req->medi_is_sel) ? 1 : 0),
                'situJud_is_sel' => (isset($req->situJud_is_sel) ? 1 : 0),
                'solvaDebit_is_sel' => (isset($req->solvaDebit_is_sel) ? 1 : 0),
                'cheque_ref' => (isset($req->cheque_ref) ? 1 : 0),
                'injoncDpay' => (isset($req->injoncDpay) ? 1 : 0),
                'reqinjoncDpay' => (isset($req->reqinjoncDpay) ? 1 : 0),
                'SituationDirect' => (isset($req->SituationDirect) ? 1 : 0)
            ]);

            scen_tele::where('id_hist_scen', $req->id_scenH)->delete();
            if (isset($req->tel_is_sel)) {
                for ($i = 0; $i < ((isset($req->type_tele)) ? (count($req->type_tele)) : 0); $i++) {
                    if ($req->nombre_tele_max > $i) {

                        scen_tele::create([
                            'id_hist_scen' => $req->id_scenH,
                            'nom_charge' => $req->nom_charge[$i],
                            'type_tele' => $req->type_tele[$i],
                            'date_H_contact' => $req->date_H_contact[$i],
                            'n_tele' => $req->n_tele[$i],
                            'desc_num_tel' => $req->desc_num_tel[$i],
                            'promess_pay' => $req->promess_pay[$i],
                            'id_creator' => Auth::user()->id
                        ]);
                    }
                }
            }
            Solvabilite_debiteur::where('id_hist_scen', $req->id_scenH)->delete();
            if (isset($req->solvaDebit_is_sel)) {
                for ($i = 0; $i <  ((isset($req->nbsolva)) ? count($req->nbsolva) : 0); $i++) {
                    if ($req->nombre_solvaDebit_max > $i) {
                        Solvabilite_debiteur::create([
                            'id_hist_scen' => $req->id_scenH,
                            'bien_personnels' => $req->bien_personnels[$i],
                            'td' => $req->td[$i],
                            'hertage' => $req->hertage[$i],
                            'participation' => $req->participation[$i],
                            'comment_solv_deb' => $req->comment_solv_deb[$i],
                        ]);
                    }
                }
            }
            scen_courrier::where('id_hist_scen', $req->id_scenH)->delete();
            if (isset($req->courr_is_sel)) {
                for ($i = 0; $i < ((isset($req->date_env)) ? count($req->date_env) : 0); $i++) {
                    if ($req->nombre_courr_max > $i) {
                        scen_courrier::create([
                            'id_hist_scen' => $req->id_scenH,
                            'date_env' => $req->date_env[$i],
                            'mise_dem_par' => (isset($req->mise_dem_par[$i])) ? $req->mise_dem_par[$i] : '',
                            'comment' => $req->comment[$i],
                            'id_creator' => Auth::user()->id
                        ]);
                    }
                }
            }

            Validation_Dadresse::where('id_hist_scen', $req->id_scenH)->delete();
            if (isset($req->valid_adrs_is_sel)) {
                for ($i = 0; $i < ((isset($req->date_valid_adres)) ? count($req->date_valid_adres) : 0); $i++) {
                    if ($req->valid_adrs_max > $i) {
                        Validation_Dadresse::create([
                            'id_hist_scen' => $req->id_scenH,
                            'date_valid_adres' => $req->date_valid_adres[$i],
                            'effect_par' => $req->effect_par[$i],
                            'type_debit' => (isset($req->type_debit[$i])) ? $req->type_debit[$i] : '',
                            'comment_vali_adrs' => $req->comment_vali_adrs[$i]
                        ]);
                    }
                }
            }
            SenConvocation::where('id_hist_scen', $req->id_scenH)->delete();
            if (isset($req->convo_is_sel)) {
                for ($i = 0; $i < ((isset($req->date_H_conv)) ? count($req->date_H_conv) : 0); $i++) {
                    if ($req->nombre_convo_max > $i) {
                        SenConvocation::create([
                            'id_hist_scen' => $req->id_scenH,
                            'remise_par' => $req->remise_par[$i],
                            'date_H_conv' => $req->date_H_conv[$i],
                            'obtenu_par' => (isset($req->obtenu_par[$i])) ? $req->obtenu_par[$i] : '',
                            'comment' => $req->comment_convo[$i]
                        ]);
                    }
                }
            }
            SenVisiteDomicile::where('id_hist_scen', $req->id_scenH)->delete();
            if (isset($req->visit_is_sel)) {
                for ($i = 0; $i < ((isset($req->domicile)) ? count($req->domicile) : 0); $i++) {
                    if ($req->nombre_visit_max > $i) {
                        SenVisiteDomicile::create([
                            'id_hist_scen' => $req->id_scenH,
                            'adress_perso' => ($req->domicile[$i] == 'personnel') ? $req->adress[$i] : '',
                            'adress_pro' => ($req->domicile[$i] == 'pro') ? $req->adress[$i] : '',
                            'adress_proche' => ($req->domicile[$i] == 'proche') ? $req->adress[$i] : '',
                            'date_H_visite' => $req->date_H_visite[$i],
                            'visiteur' => $req->visiteur[$i],
                            'promess' => $req->promess[$i],
                            'comment' => $req->comment[$i],
                            'domicile' => $req->domicile[$i]
                        ]);
                    }
                }
            }
            SenMediation::where('id_hist_scen', $req->id_scenH)->delete();
            if (isset($req->medi_is_sel)) {
                for ($i = 0; $i < ((isset($req->lieu_med)) ? count($req->lieu_med) : 0); $i++) {
                    if ($req->nombre_medi_max > $i) {
                        SenMediation::create([
                            'id_hist_scen' => $req->id_scenH,
                            'lieu_med' => $req->lieu_med[$i],
                            'adress_med' => $req->adress_med[$i],
                            'date_med' => $req->date_med[$i],
                            'mediateur' => $req->mediateur[$i],
                            'debiteur_mandataire' => $req->debiteur_mandataire[$i],
                            'promesse' => $req->promesse_medi[$i],

                            'comment' => $req->comment_medi[$i]
                        ]);
                    }
                }
            }
            SituationJudiciaire::where('id_hist_scen', $req->id_scenH)->delete();
            if (isset($req->situJud_is_sel)) {
                for ($i = 0; $i < ((isset($req->stj_nb)) ? count($req->stj_nb) : 0); $i++) {
                    if ($req->nombre_situJud_max > $i) {
                        SituationJudiciaire::create([
                            'id_hist_scen' => $req->id_scenH,
                            'type_situation_judic' => (isset($req->type_situation_judic[$i])) ? $req->type_situation_judic[$i] : '',
                            'comment' => $req->comment_stj[$i]
                        ]);
                    }
                }
            }
        }
        $HistoriqueAct = HistoriqueAct::where('where', 'NewScen')
            ->where('what', 'create')
            ->where('id_action', $req->id_doss)
            ->first();

        if (Auth::user()->droit_de_valid_acts == 0) {
            if (empty($HistoriqueAct)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'create',
                    'where' => 'NewScen',
                    'id_action' => $req->id_doss,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                HistoriqueAct::where('id_action', $req->id_doss)
                    ->where('id_valid', 1)
                    ->where('what', 'create')
                    ->where('where', 'NewScen')
                    ->update(
                        ['id_valid' => '0']
                    );
            }
        } else {
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'create',
                'where' => 'NewScen',
                'id_action' => $req->id_doss,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => 1,
            ]);
        }

        if (!isset($req->situJ)) {

            Injonction_De_Payer::where('id_hist_scen', $req->id_scenH)->delete();
            ScenJudicCheq::where('id_hist_scen', $req->id_scenH)->delete();
            return json_encode($req->situJ);
        }

        return json_encode($histo);
    }
    public function fermetureDoss($id)
    {
        $dossier = Dossier::where('id', $id)->first();
        $FermDoss = FermetureDossier::where('id_doss', $id)->first();
        $file = Files::where('id_ferm_dos', (isset($FermDoss->id) ? $FermDoss->id : 0))->get();
        $File_path = File_path::where('type', 'joints')->first();
        return view('dossier.fermDoss')
            ->with('File_path', $File_path)
            ->with('id_doss', $id)
            ->with('files', $file)
            ->with('FermDoss', $FermDoss)
            ->with('dossier', $dossier);
    }
    public function addFermInfos(request $req)
    {

        $fermedoss = FermetureDossier::create([
            'id_doss' => $req->id_doss,
            'date_fermetur' => $req->date_fermetur,
            'statuFerm' => $req->statuFerm,
            'rapportDecis' => $req->rapportDecis,
        ]);
        $dossier = Dossier::where('id', $req->id_doss)->update([
            'phase' => 'Clôturée'
        ]);
        $filenameorg = '';
        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                Files::create([
                    'id_ferm_dos' => $fermedoss->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        return json_encode($dossier);
    }
    public function updateFermInfos(Request $req)
    {
        // return json_encode($req->file_exc);
        $filenameorg = '';
        $fermedoss = FermetureDossier::where('id', $req->id_ferm)->update([
            'date_fermetur' => $req->date_fermetur,
            'statuFerm' => $req->statuFerm,
            'rapportDecis' => $req->rapportDecis,
        ]);
        if (!empty($req->file_exc)) {
            Files::where('id_ferm_dos', $req->id_ferm)->whereNotIn('file_name', $req->file_exc)->delete();
        } else {
            Files::where('id_ferm_dos', $req->id_ferm)->delete();
        }
        $dossier = Dossier::where('id', $req->id_doss)->update([
            'phase' => 'Clôturée'
        ]);
        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                Files::create([
                    'id_ferm_dos' => $req->id_ferm,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }
        return json_encode($fermedoss);
    }
    public function vu_anc(Request $req)
    {
        $DetailDossier = DetailDossier::where('id', $req->id_dt)->update(
            ['vue_alert_anc' => 1]
        );
        return json_encode($DetailDossier);
    }
    public function plante_Cheque($id)
    {
        $data = ScenJudicCheq::select('partners.nom_part', 'partners.prenom_part', 'scen_judic_cheqs.*')
            ->leftjoin('partners', 'partners.id', 'scen_judic_cheqs.procureur_tir')
            ->where('scen_judic_cheqs.id_hist_scen', $id)->get();
        $avocats = partner::where('type_partn', 'Avocat')->get();
        return view('admin.scens.list_cheques')
            ->with('avocats', $avocats)
            ->with('id_hist', $id)
            ->with('data', $data);
    }
    public function form_Cheque($id, Request $req)
    {

        $partner = partner::get();

        return view('admin.scens.form_Cheque')
            ->with('id_hist', $id)
            ->with('vald_pieces', $req->vald_pieces)
            ->with('provi', $req->provi)
            ->with('avocat', $req->avocat)
            ->with('date_debut_plant', $req->date_debut_plant)
            ->with('date_fin_plant', $req->date_fin_plant)
            ->with('partner', $partner);
    }
    public function add_cheques_ref(Request $req)
    {
        $link = '';
        if (isset($req->prem_inst)) {
            $data = ScenJudicCheq::create([
                'plainte_ck' => 'on',
                'id_hist_scen' => $req->id_hist,
                'num_dos_penal' => $req->num_dos_penal,
                'ref_st_cheqe' => $req->ref_st_cheqe,
                'tribunal_spese' => $req->tribunal_spese,
                'tireur' => $req->tireur,
                'adresse_cheq' => $req->adresse_cheq,
                'cin_cheq' => $req->cin_cheq,
                'procureur_tir' => $req->procureur_tir,
                'benefic' => $req->benefic,
                'procureur_tir_benif' => $req->procureur_tir_benif,
                'charge_suiv' => $req->charge_suiv,
                'banque_tire' => $req->banque_tire,
                'adrs_tire' => $req->adrs_tire,
                'n_compt' => $req->n_compt,
                'montant_glob' => $req->montant_glob,
                'tribnal_compet_cheq' => $req->tribnal_compet_cheq,
                'date_plaint_cheq' => $req->date_plaint_cheq,
                'num_plaint_cheq' => $req->num_plaint_cheq,
                'date_trans_autorite_center_cheq' => $req->date_trans_autorite_center_cheq,
                'DTAC' => $req->DTAC,
                'num_trans_cheq' => $req->num_trans_cheq,
                'sort_plainte_cheq' => $req->sort_plainte_cheq,
                'date_rappel_cheq' => $req->date_rappel_cheq,
                'sort_rappel_cheq' => $req->sort_rappel_cheq,
                'DAI_cheq' => $req->DAI_cheq,
                'date_audience' => $req->date_audience,
                'audiences_suivantes' => $req->audiences_suivantes,
                'jugement' => $req->jugement,
                'DAJPI' => $req->DAJPI,
                'OTCA' => $req->OTCA,
                'Rprt_plc_judic' => $req->Rprt_plc_judic,
                'polic_judic' => $req->polic_judic,
                'chargeDossJurid' => $req->chargeDossJurid,
                'NCT' => $req->NCT,
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
                'p_j_faits_cheq' => $req->p_j_faits_cheq,
                'entiteDate' => $req->entiteDate,
            ]);
        } elseif (isset($req->phase_appel)) {
            $data = ScenJudicCheq::create([
                'appel_ck' => 'on',
                'id_hist_scen' => $req->id_hist,
                'num_dos_penal' => $req->num_dos_penal,
                'ref_st_cheqe' => $req->ref_st_cheqe,
                'tribunal_spese' => $req->tribunal_spese,
                'tireur' => $req->tireur,
                'adresse_cheq' => $req->adresse_cheq,
                'cin_cheq' => $req->cin_cheq,
                'procureur_tir' => $req->procureur_tir,
                'benefic' => $req->benefic,
                'procureur_tir_benif' => $req->procureur_tir_benif,
                'charge_suiv' => $req->charge_suiv,
                'banque_tire' => $req->banque_tire,
                'adrs_tire' => $req->adrs_tire,
                'n_compt' => $req->n_compt,
                'montant_glob' => $req->montant_glob,
                'tribnal_compet_cheq' => $req->tribnal_compet_cheq,
                'date_plaint_cheq' => $req->date_plaint_cheq,
                'num_plaint_cheq' => $req->num_plaint_cheq,
                'date_trans_autorite_center_cheq' => $req->date_trans_autorite_center_cheq,
                'DTAC' => $req->DTAC,
                'num_trans_cheq' => $req->num_trans_cheq,
                'sort_plainte_cheq' => $req->sort_plainte_cheq,
                'date_rappel_cheq' => $req->date_rappel_cheq,
                'sort_rappel_cheq' => $req->sort_rappel_cheq,
                'DAI_cheq' => $req->DAI_cheq,
                'date_audience' => $req->date_audience,
                'audiences_suivantes' => $req->audiences_suivantes,
                'jugement' => $req->jugement,
                'DAJPI' => $req->DAJPI,
                'OTCA' => $req->OTCA,
                'Rprt_plc_judic' => $req->Rprt_plc_judic,
                'polic_judic' => $req->polic_judic,
                'chargeDossJurid' => $req->chargeDossJurid,
                'NCT' => $req->NCT,
                'p_j_faits_cheq' => $req->p_j_faits_cheq,
                'entiteDate' => $req->entiteDate,
                'tribunal_competnt_fazeApl_cheque' => $req->tribunal_competnt_fazeApl_cheque,
                'Ndos_fazeApl_cheque' => $req->Ndos_fazeApl_cheque,
                'dt_p_aud_fazeApl_cheque' => $req->dt_p_aud_fazeApl_cheque,
                'aud_suivan_fazeApl_cheque' => $req->aud_suivan_fazeApl_cheque,
                'date_delib_fazeApl_cheque' => $req->date_delib_fazeApl_cheque,
                'dispo_deci_fazeApl_cheque' => $req->dispo_deci_fazeApl_cheque,
                'dt_retrait_jug_fazeApl_cheque' => $req->dt_retrait_jug_fazeApl_cheque,
                'dt_dos_fazeApl_cheque' => $req->dt_dos_fazeApl_cheque,
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
                'depo_dos_tribunal_fazeApl_cheque' => $req->depo_dos_tribunal_fazeApl_cheque,
            ]);
        } else {
            $data = ScenJudicCheq::create([
                'id_hist_scen' => $req->id_hist,
                'num_dos_penal' => $req->num_dos_penal,
                'ref_st_cheqe' => $req->ref_st_cheqe,
                'tribunal_spese' => $req->tribunal_spese,
                'tireur' => $req->tireur,
                'adresse_cheq' => $req->adresse_cheq,
                'cin_cheq' => $req->cin_cheq,
                'procureur_tir' => $req->procureur_tir,
                'benefic' => $req->benefic,
                'procureur_tir_benif' => $req->procureur_tir_benif,
                'charge_suiv' => $req->charge_suiv,
                'banque_tire' => $req->banque_tire,
                'adrs_tire' => $req->adrs_tire,
                'n_compt' => $req->n_compt,
                'montant_glob' => $req->montant_glob,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant
            ]);
        }
        for ($t = 0; $t < count($req->cheque_nb); $t++) {
            ScenJudicInfoCheq::create([
                'id_hist_scen' => $data->id,
                'n_cheq' => $req->n_cheq[$t],
                'montant_cheq' => $req->montant_cheq[$t],
                'date_emission' => $req->date_emission[$t],
                'motif' => $req->motif[$t],
            ]);
        }
        $for_id = HistoriqueScenario::where('id', $data->id_hist_scen)->first();
        if(Auth::user()->droit_de_valid_acts == 1){
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }
        HistoriqueAct::create([
            'id_user' => Auth::user()->id,
            'what' => 'create',
            'where' => 'scenChequ',
            'id_action' => $data->id,
            'username' => Auth::user()->nom,
            'userlastname' => Auth::user()->prenom,
            'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'),
        ]);

        if ($data != null) {
            $link = route('SceneList', [$for_id->id_doss]);
            return json_encode($link);
        } else {
            return json_encode($data);
        }
    }
    public function show_Cheque($id)
    {
        $ScenJudicCheq = ScenJudicCheq::where('id', $id)->first();
        $infoCheq = ScenJudicInfoCheq::where('id_hist_scen', $id)->get();
        $partner = partner::get();
        return view('admin.scens.show_cheque')
            ->with('ScenJudicCheq', $ScenJudicCheq)
            ->with('partner', $partner)
            ->with('infoCheq', $infoCheq);
    }
    public function edit_cheques_ref(Request $req)
    {

        if (isset($req->prem_inst)) {
            ScenJudicCheq::where('id', $req->id_infos)->update([
                'plainte_ck' => 'on',
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'ref_st_cheqe' => $req->ref_st_cheqe,
                'num_dos_penal' => $req->num_dos_penal,
                'tribunal_spese' => $req->tribunal_spese,
                'tireur' => $req->tireur,
                'adresse_cheq' => $req->adresse_cheq,
                'cin_cheq' => $req->cin_cheq,
                'procureur_tir' => $req->procureur_tir,
                'benefic' => $req->benefic,
                'procureur_tir_benif' => $req->procureur_tir_benif,
                'charge_suiv' => $req->charge_suiv,
                'banque_tire' => $req->banque_tire,
                'adrs_tire' => $req->adrs_tire,
                'n_compt' => $req->n_compt,
                'montant_glob' => $req->montant_glob,
                'tribnal_compet_cheq' => $req->tribnal_compet_cheq,
                'date_plaint_cheq' => $req->date_plaint_cheq,
                'num_plaint_cheq' => $req->num_plaint_cheq,
                'date_trans_autorite_center_cheq' => $req->date_trans_autorite_center_cheq,
                'DTAC' => $req->DTAC,
                'num_trans_cheq' => $req->num_trans_cheq,
                'sort_plainte_cheq' => $req->sort_plainte_cheq,
                'date_rappel_cheq' => $req->date_rappel_cheq,
                'sort_rappel_cheq' => $req->sort_rappel_cheq,
                'DAI_cheq' => $req->DAI_cheq,
                'date_audience' => $req->date_audience,
                'audiences_suivantes' => $req->audiences_suivantes,
                'jugement' => $req->jugement,
                'date_honorer' => $req->date_honorer,
                'date_non_honor' => $req->date_non_honor,
                'DTP_cheq' => $req->DTP_cheq,
                'DAJPI' => $req->DAJPI,
                'OTCA' => $req->OTCA,
                'Rprt_plc_judic' => $req->Rprt_plc_judic,
                'polic_judic' => $req->polic_judic,
                'chargeDossJurid' => $req->chargeDossJurid,
                'NCT' => $req->NCT,
                'p_j_faits_cheq' => $req->p_j_faits_cheq,
                'entiteDate' => $req->entiteDate,
                'appel_ck' => 0
            ]);
        }
        if (isset($req->phase_appel)) {
            ScenJudicCheq::where('id', $req->id_infos)->update([
                'appel_ck' => 'on',
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
                'ref_st_cheqe' => $req->ref_st_cheqe,
                'num_dos_penal' => $req->num_dos_penal,
                'tribunal_spese' => $req->tribunal_spese,
                'tireur' => $req->tireur,
                'adresse_cheq' => $req->adresse_cheq,
                'cin_cheq' => $req->cin_cheq,
                'procureur_tir' => $req->procureur_tir,
                'benefic' => $req->benefic,
                'procureur_tir_benif' => $req->procureur_tir_benif,
                'charge_suiv' => $req->charge_suiv,
                'banque_tire' => $req->banque_tire,
                'adrs_tire' => $req->adrs_tire,
                'n_compt' => $req->n_compt,
                'montant_glob' => $req->montant_glob,
                'tribnal_compet_cheq' => $req->tribnal_compet_cheq,
                'date_plaint_cheq' => $req->date_plaint_cheq,
                'num_plaint_cheq' => $req->num_plaint_cheq,
                'date_trans_autorite_center_cheq' => $req->date_trans_autorite_center_cheq,
                'DTAC' => $req->DTAC,
                'num_trans_cheq' => $req->num_trans_cheq,
                'sort_plainte_cheq' => $req->sort_plainte_cheq,
                'date_rappel_cheq' => $req->date_rappel_cheq,
                'sort_rappel_cheq' => $req->sort_rappel_cheq,
                'DAI_cheq' => $req->DAI_cheq,
                'date_audience' => $req->date_audience,
                'audiences_suivantes' => $req->audiences_suivantes,
                'jugement' => $req->jugement,
                'DAJPI' => $req->DAJPI,
                'OTCA' => $req->OTCA,
                'Rprt_plc_judic' => $req->Rprt_plc_judic,
                'polic_judic' => $req->polic_judic,
                'chargeDossJurid' => $req->chargeDossJurid,
                'NCT' => $req->NCT,
                'p_j_faits_cheq' => $req->p_j_faits_cheq,
                'entiteDate' => $req->entiteDate,
                'tribunal_competnt_fazeApl_cheque' => $req->tribunal_competnt_fazeApl_cheque,
                'Ndos_fazeApl_cheque' => $req->Ndos_fazeApl_cheque,
                'dt_p_aud_fazeApl_cheque' => $req->dt_p_aud_fazeApl_cheque,
                'aud_suivan_fazeApl_cheque' => $req->aud_suivan_fazeApl_cheque,
                'date_delib_fazeApl_cheque' => $req->date_delib_fazeApl_cheque,
                'dispo_deci_fazeApl_cheque' => $req->dispo_deci_fazeApl_cheque,
                'dt_retrait_jug_fazeApl_cheque' => $req->dt_retrait_jug_fazeApl_cheque,
                'dt_dos_fazeApl_cheque' => $req->dt_dos_fazeApl_cheque,
                'depo_dos_tribunal_fazeApl_cheque' => $req->depo_dos_tribunal_fazeApl_cheque,
            ]);
        }
        if (!isset($req->prem_inst) && !isset($req->phase_appel)) {
            ScenJudicCheq::where('id', $req->id_infos)->update([
                'plainte_ck' => '0',
                'appel_ck' => '0',
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
                'ref_st_cheqe' => $req->ref_st_cheqe,
                'tribunal_spese' => $req->tribunal_spese,
                'num_dos_penal' => $req->num_dos_penal,
                'tireur' => $req->tireur,
                'adresse_cheq' => $req->adresse_cheq,
                'cin_cheq' => $req->cin_cheq,
                'procureur_tir' => $req->procureur_tir,
                'benefic' => $req->benefic,
                'procureur_tir_benif' => $req->procureur_tir_benif,
                'charge_suiv' => $req->charge_suiv,
                'banque_tire' => $req->banque_tire,
                'adrs_tire' => $req->adrs_tire,
                'n_compt' => $req->n_compt,
                'montant_glob' => $req->montant_glob
            ]);
        }
        ScenJudicInfoCheq::where('id', $req->id_infos)->delete();
        for ($t = 0; $t < count($req->cheque_nb); $t++) {
            ScenJudicInfoCheq::create([
                'id_hist_scen' => $req->id_infos,
                'n_cheq' => $req->n_cheq[$t],
                'montant_cheq' => $req->montant_cheq[$t],
                'date_emission' => $req->date_emission[$t],
                'motif' => $req->motif[$t],
            ]);
        }
        $HistoriqueAct = HistoriqueAct::where('id_action', $req->id_infos)
            ->where('id_valid', 1)
            ->where('where', 'scenChequ')
            ->where('what', 'create')
            ->update(['id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0)]);
        // HistoriqueAct::create([
        //     'id_user' => Auth::user()->id,
        //     'what' => 'create',
        //     'where' => 'scenChequ',
        //     'id_action' => $data->id,
        //     'username' => Auth::user()->nom,
        //     'userlastname' => Auth::user()->prenom,
        //     'id_valid' => ((Auth::user()->droit_de_valid_acts == 1)? 1 : '0'),
        // ]);

        $for_id = HistoriqueScenario::where('id', $req->id_scenH)->first();
        $link = route('SceneList', [$for_id->id_doss]);
        return json_encode($link);
    }
    public function delete_cheques($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'scenChequ')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'scenChequ',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        } else {
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'scenChequ',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0),
            ]);
            ScenJudicCheq::where('id', $id)->delete();
        }

        return back();
    }
    public function injonc_payer($id)
    {
        $InjoncPay = Injonction_De_Payer::select('partners.nom_part', 'partners.prenom_part', 'injonction__de__payers.*')
            ->leftjoin('partners', 'partners.id', 'injonction__de__payers.proc_tirer_inj_p')
            ->where('id_hist_scen', $id)
            ->get();
        $avocats = partner::where('type_partn', 'Avocat')->get();
        return view('admin.scens.list_injc_pay')
            ->with('InjoncPay', $InjoncPay)
            ->with('avocats', $avocats)
            ->with('id_hist', $id);
    }
    public function form_injonc_pay($id, Request $req)
    {
        $partner = partner::get();
        return view('admin.scens.form_injoction')
            ->with('id_hist', $id)
            ->with('vald_pieces', $req->vald_pieces)
            ->with('provi', $req->provi)
            ->with('avocat', $req->avocat)
            ->with('date_debut_plant', $req->date_debut_plant)
            ->with('date_fin_plant', $req->date_fin_plant)
            ->with('partner', $partner);
    }
    public function add_injonc_pay_ref(Request $req)
    {
        $injoncDpayarr = [
            'id_hist_scen' => $req->id_hist,
            'injoncDpay' => $req->injoncDpay,
            'ref_inj_p' => $req->ref_inj_p,
            'trib_com_inj_p' => $req->trib_com_inj_p,
            'nom_jug_inj_p' => $req->nom_jug_inj_p,
            'tirer_inj_p' => $req->tirer_inj_p,
            'adrs_tirer_inj_p' => $req->adrs_tirer_inj_p,
            'Cin_tirer_inj_p' => $req->Cin_tirer_inj_p,
            'proc_tirer_inj_p' => $req->proc_tirer_inj_p,
            'benef_inj_p' => $req->benef_inj_p,
            'proc_benf_inj_p' => $req->proc_benf_inj_p,
            'Chrg_inj_p' => $req->Chrg_inj_p,
            'bank_inj_p' => $req->bank_inj_p,
            'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
            'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
            'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'avocat' => $req->avocat,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ];
        if (isset($req->proc_depo_inj_pay)) {
            $injoncDpayarr = [
                'id_hist_scen' => $req->id_hist,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'ref_inj_p' => $req->ref_inj_p,
                'trib_com_inj_p' => $req->trib_com_inj_p,
                'nom_jug_inj_p' => $req->nom_jug_inj_p,
                'tirer_inj_p' => $req->tirer_inj_p,
                'adrs_tirer_inj_p' => $req->adrs_tirer_inj_p,
                'Cin_tirer_inj_p' => $req->Cin_tirer_inj_p,
                'proc_tirer_inj_p' => $req->proc_tirer_inj_p,
                'benef_inj_p' => $req->benef_inj_p,
                'proc_benf_inj_p' => $req->proc_benf_inj_p,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'explication_dep_inj_pay' => $req->explication_dep_inj_pay,
                'proc_depo_inj_pay' => $req->proc_depo_inj_pay,
                'num_doss_inj_pay' => $req->num_doss_inj_pay,
                'date_jug_inj_pay' => $req->date_jug_inj_pay,
                'Accept_demnd_inj_pay' => $req->Accept_demnd_inj_pay,
                'dt_retrt_inj_pay' => $req->dt_retrt_inj_pay,
                'nr_retrt_inj_pay' => $req->nr_retrt_inj_pay,
            ];
        }
        if (isset($req->proc_notif_inj_pay)) {
            $injoncDpayarr = [
                'id_hist_scen' => $req->id_hist,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'ref_inj_p' => $req->ref_inj_p,
                'trib_com_inj_p' => $req->trib_com_inj_p,
                'nom_jug_inj_p' => $req->nom_jug_inj_p,
                'tirer_inj_p' => $req->tirer_inj_p,
                'adrs_tirer_inj_p' => $req->adrs_tirer_inj_p,
                'Cin_tirer_inj_p' => $req->Cin_tirer_inj_p,
                'proc_tirer_inj_p' => $req->proc_tirer_inj_p,
                'benef_inj_p' => $req->benef_inj_p,
                'proc_benf_inj_p' => $req->proc_benf_inj_p,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'proc_depo_inj_pay' => $req->proc_depo_inj_pay,
                'num_doss_inj_pay' => $req->num_doss_inj_pay,
                'date_jug_inj_pay' => $req->date_jug_inj_pay,
                'Accept_demnd_inj_pay' => $req->Accept_demnd_inj_pay,
                'dt_retrt_inj_pay' => $req->dt_retrt_inj_pay,
                'nr_retrt_inj_pay' => $req->nr_retrt_inj_pay,
                'proc_notif_inj_pay' => $req->proc_notif_inj_pay,
                'date_depot_inj_pay' => $req->date_depot_inj_pay,
                'nr_dos_notif_inj_pay' => $req->nr_dos_notif_inj_pay,
                'huiss_designe_inj_pay' => $req->huiss_designe_inj_pay,
                'result_notif_inj_pay' => $req->result_notif_inj_pay,
                'date_app_inj_pay' => $req->date_app_inj_pay,
                'procd_proc_notif' => $req->procd_proc_notif,
            ];
        }
        if (isset($req->proc_exec_inj_pay)) {
            $injoncDpayarr = [
                'id_hist_scen' => $req->id_hist,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'ref_inj_p' => $req->ref_inj_p,
                'trib_com_inj_p' => $req->trib_com_inj_p,
                'nom_jug_inj_p' => $req->nom_jug_inj_p,
                'tirer_inj_p' => $req->tirer_inj_p,
                'adrs_tirer_inj_p' => $req->adrs_tirer_inj_p,
                'Cin_tirer_inj_p' => $req->Cin_tirer_inj_p,
                'proc_tirer_inj_p' => $req->proc_tirer_inj_p,
                'benef_inj_p' => $req->benef_inj_p,
                'proc_benf_inj_p' => $req->proc_benf_inj_p,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'proc_depo_inj_pay' => $req->proc_depo_inj_pay,
                'num_doss_inj_pay' => $req->num_doss_inj_pay,
                'date_jug_inj_pay' => $req->date_jug_inj_pay,
                'Accept_demnd_inj_pay' => $req->Accept_demnd_inj_pay,
                'dt_retrt_inj_pay' => $req->dt_retrt_inj_pay,
                'nr_retrt_inj_pay' => $req->nr_retrt_inj_pay,
                'proc_notif_inj_pay' => $req->proc_notif_inj_pay,
                'date_depot_inj_pay' => $req->date_depot_inj_pay,
                'nr_dos_notif_inj_pay' => $req->nr_dos_notif_inj_pay,
                'huiss_designe_inj_pay' => $req->huiss_designe_inj_pay,
                'result_notif_inj_pay' => $req->result_notif_inj_pay,
                'date_app_inj_pay' => $req->date_app_inj_pay,
                'procd_proc_notif' => $req->procd_proc_notif,
                'proc_exec_inj_pay' => $req->proc_exec_inj_pay,
                'nr_execution_inj_pay' => $req->nr_execution_inj_pay,
                'date_execution_inj_pay' => $req->date_execution_inj_pay,
                'huiss_des_proc_exec_inj_pay' => $req->huiss_des_proc_exec_inj_pay,
                'attes_del_pay_inj_pay' => $req->attes_del_pay_inj_pay,
                'attes_defo_pay_inj_pay' => $req->attes_defo_pay_inj_pay,
                'expiration_inj_pay' => $req->expiration_inj_pay,
                'dt_saisi_proc_exe_inj_p' => $req->dt_saisi_proc_exe_inj_p,
                'type_saisie_inj_p' => $req->type_saisie_inj_p,
                'date_expert_judic_inj_p' => $req->date_expert_judic_inj_p,
                'nom_expert_inj_p' => $req->nom_expert_inj_p,
                'Public_j_a_l' => $req->Public_j_a_l,
                'nom_journal' => $req->nom_journal,
                'vent_encher_publc' => $req->vent_encher_publc,
                'date_vent' => $req->date_vent,
                'result_inj_pay' => $req->result_inj_pay,
            ];
        }
        if (isset($req->proc_appel_inj_pay)) {
            $injoncDpayarr = [
                'id_hist_scen' => $req->id_hist,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'trib_com_inj_p' => $req->trib_com_inj_p,
                'nom_jug_inj_p' => $req->nom_jug_inj_p,
                'tirer_inj_p' => $req->tirer_inj_p,
                'adrs_tirer_inj_p' => $req->adrs_tirer_inj_p,
                'Cin_tirer_inj_p' => $req->Cin_tirer_inj_p,
                'proc_tirer_inj_p' => $req->proc_tirer_inj_p,
                'benef_inj_p' => $req->benef_inj_p,
                'proc_benf_inj_p' => $req->proc_benf_inj_p,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'proc_depo_inj_pay' => $req->proc_depo_inj_pay,
                'num_doss_inj_pay' => $req->num_doss_inj_pay,
                'date_jug_inj_pay' => $req->date_jug_inj_pay,
                'Accept_demnd_inj_pay' => $req->Accept_demnd_inj_pay,
                'dt_retrt_inj_pay' => $req->dt_retrt_inj_pay,
                'nr_retrt_inj_pay' => $req->nr_retrt_inj_pay,
                'proc_notif_inj_pay' => $req->proc_notif_inj_pay,
                'date_depot_inj_pay' => $req->date_depot_inj_pay,
                'nr_dos_notif_inj_pay' => $req->nr_dos_notif_inj_pay,
                'huiss_designe_inj_pay' => $req->huiss_designe_inj_pay,
                'result_notif_inj_pay' => $req->result_notif_inj_pay,
                'date_app_inj_pay' => $req->date_app_inj_pay,
                'procd_proc_notif' => $req->procd_proc_notif,
                'proc_exec_inj_pay' => $req->proc_exec_inj_pay,
                'nr_execution_inj_pay' => $req->nr_execution_inj_pay,
                'date_execution_inj_pay' => $req->date_execution_inj_pay,
                'huiss_des_proc_exec_inj_pay' => $req->huiss_des_proc_exec_inj_pay,
                'attes_del_pay_inj_pay' => $req->attes_del_pay_inj_pay,
                'attes_defo_pay_inj_pay' => $req->attes_defo_pay_inj_pay,
                'expiration_inj_pay' => $req->expiration_inj_pay,
                'dt_saisi_proc_exe_inj_p' => $req->dt_saisi_proc_exe_inj_p,
                'type_saisie_inj_p' => $req->type_saisie_inj_p,
                'date_expert_judic_inj_p' => $req->date_expert_judic_inj_p,
                'nom_expert_inj_p' => $req->nom_expert_inj_p,
                'Public_j_a_l' => $req->Public_j_a_l,
                'nom_journal' => $req->nom_journal,
                'vent_encher_publc' => $req->vent_encher_publc,
                'date_vent' => $req->date_vent,
                'result_inj_pay' => $req->result_inj_pay,
                'proc_appel_inj_pay' => $req->proc_appel_inj_pay,
                'date_depo_proc_appel' => $req->date_depo_proc_appel,
                'num_appel_proc_appel' => $req->num_appel_proc_appel,
                'date_apel_inj_p' => $req->date_apel_inj_p,
                'nom_consult_inj_p' => $req->nom_consult_inj_p,
                'trib_comp_inj_p' => $req->trib_comp_inj_p,
                'note_audience' => $req->note_audience,
            ];
        }

        $injoncDpay = Injonction_De_Payer::create($injoncDpayarr);

        if (isset($req->info_creac_nb_inj_pat)) :
            for ($i = 0; $i < count($req->info_creac_nb_inj_pat); $i++) {
                InfoCreance_inj_pay::create([
                    'id_inj_pays' => $injoncDpay->id,
                    'montant_creanc_inj_pay' => $req->montant_creanc_inj_pay[$i],
                    'n_crance_inj_pay' => $req->n_crance_inj_pay[$i],
                    'date_echc_crenc_inj_pay' => $req->date_echc_crenc_inj_pay[$i],
                    'motif_crenc_inj__pay' => $req->motif_crenc_inj__pay[$i]
                ]);
            }
        endif;

        HistoriqueAct::create([
            'id_user' => Auth::user()->id,
            'what' => 'create',
            'where' => 'scenInjonctionPay',
            'id_action' => $injoncDpay->id,
            'username' => Auth::user()->nom,
            'userlastname' => Auth::user()->prenom,
            'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'),
        ]);

        $for_id = HistoriqueScenario::where('id', $injoncDpay->id_hist_scen)->first();
        if(Auth::user()->droit_de_valid_acts == 1){
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }

        if (!empty($injoncDpay)) {
            $link = route('SceneList', [$for_id->id_doss]);
            return json_encode($link);
        } else {
            return json_encode($injoncDpay);
        }
    }
    public function show_injoction($id)
    {
        $partner = partner::get();
        $Inj_De_Payer = Injonction_De_Payer::where('id', $id)->first();
        $InfoCreance_inj_pay = InfoCreance_inj_pay::where('id_inj_pays', $id)->get();
        return view('admin.scens.show_injoction')
            ->with('partner', $partner)
            ->with('InfoCreance_inj_pay', $InfoCreance_inj_pay)
            ->with('Inj_De_Payer', $Inj_De_Payer);
    }
    public function edit_injonc_pay_ref(Request $req)
    {


        $injoncDpayarr = [
            'id_hist_scen' => $req->id_hist,
            'ref_inj_p' => $req->ref_inj_p,
            'trib_com_inj_p' => $req->trib_com_inj_p,
            'nom_jug_inj_p' => $req->nom_jug_inj_p,
            'tirer_inj_p' => $req->tirer_inj_p,
            'adrs_tirer_inj_p' => $req->adrs_tirer_inj_p,
            'Cin_tirer_inj_p' => $req->Cin_tirer_inj_p,
            'proc_tirer_inj_p' => $req->proc_tirer_inj_p,
            'benef_inj_p' => $req->benef_inj_p,
            'proc_benf_inj_p' => $req->proc_benf_inj_p,
            'Chrg_inj_p' => $req->Chrg_inj_p,
            'bank_inj_p' => $req->bank_inj_p,
            'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
            'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
            'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
            'proc_depo_inj_pay' => 0,
            'proc_exec_inj_pay' => 0,
            'proc_appel_inj_pay' => 0,
            'proc_notif_inj_pay' => 0,
            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'avocat' => $req->avocat,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ];
        if (isset($req->proc_depo_inj_pay)) {
            $injoncDpayarr = [
                'id_hist_scen' => $req->id_hist,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'ref_inj_p' => $req->ref_inj_p,
                'trib_com_inj_p' => $req->trib_com_inj_p,
                'nom_jug_inj_p' => $req->nom_jug_inj_p,
                'tirer_inj_p' => $req->tirer_inj_p,
                'adrs_tirer_inj_p' => $req->adrs_tirer_inj_p,
                'Cin_tirer_inj_p' => $req->Cin_tirer_inj_p,
                'proc_tirer_inj_p' => $req->proc_tirer_inj_p,
                'benef_inj_p' => $req->benef_inj_p,
                'proc_benf_inj_p' => $req->proc_benf_inj_p,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'explication_dep_inj_pay' => $req->explication_dep_inj_pay,

                'proc_depo_inj_pay' => $req->proc_depo_inj_pay,
                'num_doss_inj_pay' => $req->num_doss_inj_pay,
                'date_jug_inj_pay' => $req->date_jug_inj_pay,
                'Accept_demnd_inj_pay' => $req->Accept_demnd_inj_pay,
                'dt_retrt_inj_pay' => $req->dt_retrt_inj_pay,
                'nr_retrt_inj_pay' => $req->nr_retrt_inj_pay,
                'proc_exec_inj_pay' => 0,
                'proc_appel_inj_pay' => 0,
                'proc_notif_inj_pay' => 0
            ];
        }
        if (isset($req->proc_notif_inj_pay)) {
            $injoncDpayarr = [
                'id_hist_scen' => $req->id_hist,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'ref_inj_p' => $req->ref_inj_p,
                'trib_com_inj_p' => $req->trib_com_inj_p,
                'nom_jug_inj_p' => $req->nom_jug_inj_p,
                'tirer_inj_p' => $req->tirer_inj_p,
                'adrs_tirer_inj_p' => $req->adrs_tirer_inj_p,
                'Cin_tirer_inj_p' => $req->Cin_tirer_inj_p,
                'proc_tirer_inj_p' => $req->proc_tirer_inj_p,
                'benef_inj_p' => $req->benef_inj_p,
                'proc_benf_inj_p' => $req->proc_benf_inj_p,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,

                'proc_depo_inj_pay' => $req->proc_depo_inj_pay,
                'num_doss_inj_pay' => $req->num_doss_inj_pay,
                'date_jug_inj_pay' => $req->date_jug_inj_pay,
                'Accept_demnd_inj_pay' => $req->Accept_demnd_inj_pay,
                'dt_retrt_inj_pay' => $req->dt_retrt_inj_pay,
                'nr_retrt_inj_pay' => $req->nr_retrt_inj_pay,
                'proc_notif_inj_pay' => $req->proc_notif_inj_pay,
                'date_depot_inj_pay' => $req->date_depot_inj_pay,
                'nr_dos_notif_inj_pay' => $req->nr_dos_notif_inj_pay,
                'huiss_designe_inj_pay' => $req->huiss_designe_inj_pay,
                'result_notif_inj_pay' => $req->result_notif_inj_pay,
                'date_app_inj_pay' => $req->date_app_inj_pay,
                'procd_proc_notif' => $req->procd_proc_notif,
                'proc_exec_inj_pay' => 0,
                'proc_appel_inj_pay' => 0
            ];
        }
        if (isset($req->proc_appel_inj_pay)) {
            $injoncDpayarr = [
                'id_hist_scen' => $req->id_hist,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'trib_com_inj_p' => $req->trib_com_inj_p,
                'nom_jug_inj_p' => $req->nom_jug_inj_p,
                'tirer_inj_p' => $req->tirer_inj_p,
                'adrs_tirer_inj_p' => $req->adrs_tirer_inj_p,
                'Cin_tirer_inj_p' => $req->Cin_tirer_inj_p,
                'proc_tirer_inj_p' => $req->proc_tirer_inj_p,
                'benef_inj_p' => $req->benef_inj_p,
                'proc_benf_inj_p' => $req->proc_benf_inj_p,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'proc_depo_inj_pay' => $req->proc_depo_inj_pay,
                'num_doss_inj_pay' => $req->num_doss_inj_pay,
                'date_jug_inj_pay' => $req->date_jug_inj_pay,
                'Accept_demnd_inj_pay' => $req->Accept_demnd_inj_pay,
                'dt_retrt_inj_pay' => $req->dt_retrt_inj_pay,
                'nr_retrt_inj_pay' => $req->nr_retrt_inj_pay,
                'proc_notif_inj_pay' => $req->proc_notif_inj_pay,
                'date_depot_inj_pay' => $req->date_depot_inj_pay,
                'nr_dos_notif_inj_pay' => $req->nr_dos_notif_inj_pay,
                'huiss_designe_inj_pay' => $req->huiss_designe_inj_pay,
                'result_notif_inj_pay' => $req->result_notif_inj_pay,
                'date_app_inj_pay' => $req->date_app_inj_pay,
                'procd_proc_notif' => $req->procd_proc_notif,
                'proc_appel_inj_pay' => $req->proc_appel_inj_pay,
                'date_depo_proc_appel' => $req->date_depo_proc_appel,
                'num_appel_proc_appel' => $req->num_appel_proc_appel,
                'date_apel_inj_p' => $req->date_apel_inj_p,
                'nom_consult_inj_p' => $req->nom_consult_inj_p,
                'trib_comp_inj_p' => $req->trib_comp_inj_p,
                'note_audience' => $req->note_audience,
                'proc_exec_inj_pay' => 0
            ];
        }
        if (isset($req->proc_exec_inj_pay)) {
            $injoncDpayarr = [
                'id_hist_scen' => $req->id_hist,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'ref_inj_p' => $req->ref_inj_p,
                'trib_com_inj_p' => $req->trib_com_inj_p,
                'nom_jug_inj_p' => $req->nom_jug_inj_p,
                'tirer_inj_p' => $req->tirer_inj_p,
                'adrs_tirer_inj_p' => $req->adrs_tirer_inj_p,
                'Cin_tirer_inj_p' => $req->Cin_tirer_inj_p,
                'proc_tirer_inj_p' => $req->proc_tirer_inj_p,
                'benef_inj_p' => $req->benef_inj_p,
                'proc_benf_inj_p' => $req->proc_benf_inj_p,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'Chrg_inj_p' => $req->Chrg_inj_p,
                'bank_inj_p' => $req->bank_inj_p,
                'adrs_bnk_inj_p' => $req->adrs_bnk_inj_p,
                'num_cmpt_bnk_inj_p' => $req->num_cmpt_bnk_inj_p,
                'montant_glob_inj_pay' => $req->montant_glob_inj_pay,
                'proc_depo_inj_pay' => $req->proc_depo_inj_pay,
                'num_doss_inj_pay' => $req->num_doss_inj_pay,
                'date_jug_inj_pay' => $req->date_jug_inj_pay,
                'Accept_demnd_inj_pay' => $req->Accept_demnd_inj_pay,
                'dt_retrt_inj_pay' => $req->dt_retrt_inj_pay,
                'nr_retrt_inj_pay' => $req->nr_retrt_inj_pay,
                'proc_notif_inj_pay' => $req->proc_notif_inj_pay,
                'date_depot_inj_pay' => $req->date_depot_inj_pay,
                'nr_dos_notif_inj_pay' => $req->nr_dos_notif_inj_pay,
                'huiss_designe_inj_pay' => $req->huiss_designe_inj_pay,
                'result_notif_inj_pay' => $req->result_notif_inj_pay,
                'date_app_inj_pay' => $req->date_app_inj_pay,
                'procd_proc_notif' => $req->procd_proc_notif,
                'proc_exec_inj_pay' => $req->proc_exec_inj_pay,
                'nr_execution_inj_pay' => $req->nr_execution_inj_pay,
                'date_execution_inj_pay' => $req->date_execution_inj_pay,
                'huiss_des_proc_exec_inj_pay' => $req->huiss_des_proc_exec_inj_pay,
                'attes_del_pay_inj_pay' => $req->attes_del_pay_inj_pay,
                'attes_defo_pay_inj_pay' => $req->attes_defo_pay_inj_pay,
                'expiration_inj_pay' => $req->expiration_inj_pay,
                'dt_saisi_proc_exe_inj_p' => $req->dt_saisi_proc_exe_inj_p,
                'type_saisie_inj_p' => $req->type_saisie_inj_p,
                'date_expert_judic_inj_p' => $req->date_expert_judic_inj_p,
                'nom_expert_inj_p' => $req->nom_expert_inj_p,
                'Public_j_a_l' => $req->Public_j_a_l,
                'nom_journal' => $req->nom_journal,
                'vent_encher_publc' => $req->vent_encher_publc,
                'date_vent' => $req->date_vent,
                'result_inj_pay' => $req->result_inj_pay,
            ];
        }
        $injoncDpay = Injonction_De_Payer::where('id', $req->id_infos)->update($injoncDpayarr);
        InfoCreance_inj_pay::where('id_inj_pays', $req->id_infos)->delete();

        if (isset($req->info_creac_nb_inj_pat)) {
            for ($i = 0; $i < count($req->info_creac_nb_inj_pat); $i++) {
                InfoCreance_inj_pay::create([
                    'id_inj_pays' => $req->id_infos,
                    'montant_creanc_inj_pay' => $req->montant_creanc_inj_pay[$i],
                    'n_crance_inj_pay' => $req->n_crance_inj_pay[$i],
                    'date_echc_crenc_inj_pay' => $req->date_echc_crenc_inj_pay[$i],
                    'motif_crenc_inj__pay' => $req->motif_crenc_inj__pay[$i]
                ]);
            }
        }

        $HistoriqueAct = HistoriqueAct::where('id_action', $req->id_infos)
            ->where('id_valid', 1)
            ->where('where', 'scenInjonctionPay')
            ->where('what', 'create')
            ->update(['id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0)]);

        $for_id = HistoriqueScenario::where('id', $req->id_hist)->first();
        if (!empty($injoncDpay)) {
            $link = route('SceneList', [$for_id->id_doss]);
            return json_encode($link);
        } else {
            return json_encode($for_id);
        }
    }
    public function delete_injoction($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'scenInjonctionPay')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'scenInjonctionPay',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        } else {
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'scenInjonctionPay',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0),
            ]);
            Injonction_De_Payer::where('id', $id)->delete();
        }

        return back();
    }
    public function req_injonc_payer($id, Request $req)
    {
        $ReqInjoncPay = ReqinjoncDpay::select('partners.nom_part', 'partners.prenom_part', 'reqinjonc_dpays.*')
            ->leftjoin('partners', 'partners.id', 'reqinjonc_dpays.huissier_des_req_inj_pay')
            ->where('id_hist_scen', $id)
            ->get();
        $avocats = partner::where('type_partn', 'Avocat')->get();
        return view('admin.scens.listReqInjoction')
            ->with('id_hist', $id)
            ->with('avocats', $avocats)
            ->with('ReqInjoncPay', $ReqInjoncPay);
    }
    public function form_ReqInjonc_pay($id, Request $req)
    {
        $partner = partner::get();
        return view('admin.scens.form_ReqInjonc')
            ->with('id_hist', $id)
            ->with('partner', $partner)

            ->with('vald_pieces', $req->vald_pieces)
            ->with('provi', $req->provi)
            ->with('avocat', $req->avocat)
            ->with('date_debut_plant', $req->date_debut_plant)
            ->with('date_fin_plant', $req->date_fin_plant);
    }
    public function add_ReqInjonc(Request $req)
    {
        $vareqinjoncDpay = [
            'id_hist_scen' => $req->id_hist,
            'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,

            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'avocat' => $req->avocat,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,

        ];
        if (isset($req->les_proc_req_inj_pay)) {
            $vareqinjoncDpay = [
                'id_hist_scen' => $req->id_hist,
                'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ];
        }
        if (isset($req->les_proc_app_req_inj_pay)) {
            $vareqinjoncDpay = [
                'id_hist_scen' => $req->id_hist,
                'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_proc_app_req_inj_pay' => $req->les_proc_app_req_inj_pay,
                'dat_dep_req_inj_pay' => $req->dat_dep_req_inj_pay,
                'num_app_req_inj_pay' => $req->num_app_req_inj_pay,
                'dat_app_req_inj_pay' => $req->dat_app_req_inj_pay,
                'nom_consltnt_req_inj_pay' => $req->nom_consltnt_req_inj_pay,
                'trib_competnt_req_inj_pay' => $req->trib_competnt_req_inj_pay,
                'note_et_audience_req_inj_pay' => $req->note_et_audience_req_inj_pay,
                'dat_dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                'dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ];
        }
        if (isset($req->swt_proc_notif_req_inj_pay)) {
            $vareqinjoncDpay = [
                'id_hist_scen' => $req->id_hist,
                'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_proc_app_req_inj_pay' => $req->les_proc_app_req_inj_pay,
                'dat_dep_req_inj_pay' => $req->dat_dep_req_inj_pay,
                'num_app_req_inj_pay' => $req->num_app_req_inj_pay,
                'dat_app_req_inj_pay' => $req->dat_app_req_inj_pay,
                'nom_consltnt_req_inj_pay' => $req->nom_consltnt_req_inj_pay,
                'trib_competnt_req_inj_pay' => $req->trib_competnt_req_inj_pay,
                'note_et_audience_req_inj_pay' => $req->note_et_audience_req_inj_pay,
                'dat_dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                'dep_doss_au_trib_req_inj_pay' => $req->dep_doss_au_trib_req_inj_pay,
                'swt_proc_notif_req_inj_pay' => $req->swt_proc_notif_req_inj_pay,
                'date_dep_notif_req_inj_pay' => $req->date_dep_notif_req_inj_pay,
                'num_doss_notif_req_inj_pay' => $req->num_doss_notif_req_inj_pay,
                'huissier_des_req_inj_pay' => $req->huissier_des_req_inj_pay,
                'reslt_notif_req_inj_pay' => $req->reslt_notif_req_inj_pay,
                'date_appl_notif_req_inj_pay' => $req->date_appl_notif_req_inj_pay,
                'proces_notif_req_inj_pay' => $req->proces_notif_req_inj_pay,
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ];
        }
        if (isset($req->swt_proc_exe_req_inj_pay)) {
            $vareqinjoncDpay = [
                'id_hist_scen' => $req->id_hist,
                'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_proc_app_req_inj_pay' => $req->les_proc_app_req_inj_pay,
                'dat_dep_req_inj_pay' => $req->dat_dep_req_inj_pay,
                'num_app_req_inj_pay' => $req->num_app_req_inj_pay,
                'dat_app_req_inj_pay' => $req->dat_app_req_inj_pay,
                'nom_consltnt_req_inj_pay' => $req->nom_consltnt_req_inj_pay,
                'trib_competnt_req_inj_pay' => $req->trib_competnt_req_inj_pay,
                'note_et_audience_req_inj_pay' => $req->note_et_audience_req_inj_pay,
                'dat_dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                'dep_doss_au_trib_req_inj_pay' => $req->dep_doss_au_trib_req_inj_pay,
                'swt_proc_notif_req_inj_pay' => $req->swt_proc_notif_req_inj_pay,
                'date_dep_notif_req_inj_pay' => $req->date_dep_notif_req_inj_pay,
                'num_doss_notif_req_inj_pay' => $req->num_doss_notif_req_inj_pay,
                'huissier_des_req_inj_pay' => $req->huissier_des_req_inj_pay,
                'reslt_notif_req_inj_pay' => $req->reslt_notif_req_inj_pay,
                'date_appl_notif_req_inj_pay' => $req->date_appl_notif_req_inj_pay,
                'proces_notif_req_inj_pay' => $req->proces_notif_req_inj_pay,
                'swt_proc_exe_req_inj_pay' => $req->swt_proc_exe_req_inj_pay,
                'num_dos_exe_req_inj_pay' => $req->num_dos_exe_req_inj_pay,
                'date_dos_exe_req_inj_pay' => $req->date_dos_exe_req_inj_pay,
                'huissier_designe_exe_req_inj_pay' => $req->huissier_designe_exe_req_inj_pay,
                'date_exe_req_inj_pay' => $req->date_exe_req_inj_pay,
                'expiration_exe_req_inj_pay' => $req->expiration_exe_req_inj_pay,
                'dat_saisie_req_inj_pay' => $req->dat_saisie_req_inj_pay,
                'type_saisie_req_inj_pay' => $req->type_saisie_req_inj_pay,
                'date_exe_judic_req_inj_pay' => $req->date_exe_judic_req_inj_pay,
                'nom_expert_exe_req_inj_pay' => $req->nom_expert_exe_req_inj_pay,
                'pub_jal_req_inj_pay' => $req->pub_jal_req_inj_pay,
                'nom_journal_req_inj_pay' => $req->nom_journal_req_inj_pay,
                'vent_aux_encher_public_req_inj_pay' => $req->vent_aux_encher_public_req_inj_pay,
                'date_vent_exe_req_inj_pay' => $req->date_vent_exe_req_inj_pay,
                'result_exe_req_inj_pay' => $req->result_exe_req_inj_pay,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ];
        }
        $data = ReqinjoncDpay::create($vareqinjoncDpay);
        $for_id = HistoriqueScenario::where('id', $req->id_hist)->first();
        if(Auth::user()->droit_de_valid_acts == 1){
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }

        HistoriqueAct::create([
            'id_user' => Auth::user()->id,
            'what' => 'create',
            'where' => 'scenReqInjonctionPay', //changabel
            'id_action' => $data->id, //changabel
            'username' => Auth::user()->nom,
            'userlastname' => Auth::user()->prenom,
            'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'),
        ]);

        if (!empty($data)) {
            $link = route('SceneList', [$for_id->id_doss]);
            return json_encode($link);
        } else {
            return json_encode($data);
        }
    }
    public function show_ReqInjoction($id)
    {
        $partner = partner::get();
        $ReqinjoncDpay = ReqinjoncDpay::where('id', $id)->first();

        return view('admin.scens.show_ReqInjoction')
            ->with('partner', $partner)
            ->with('ReqinjoncDpay', $ReqinjoncDpay);
    }
    public function edit_ReqInjonc(Request $req)
    {
        $vareqinjoncDpay = [
            'id_hist_scen' => $req->id_scenH,
            'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
            'les_proc_app_req_inj_pay' => 0,
            'swt_proc_notif_req_inj_pay' => 0,
            'swt_proc_exe_req_inj_pay' => 0,
            'les_proc_req_inj_pay' => 0,
            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'avocat' => $req->avocat,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ];
        if (isset($req->les_proc_req_inj_pay)) {
            $vareqinjoncDpay = [
                'id_hist_scen' => $req->id_scenH,
                'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_proc_app_req_inj_pay' => 0,
                'swt_proc_notif_req_inj_pay' => 0,
                'swt_proc_exe_req_inj_pay' => 0,
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ];
        }
        if (isset($req->les_proc_app_req_inj_pay)) {
            $vareqinjoncDpay = [
                'id_hist_scen' => $req->id_scenH,
                'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_proc_app_req_inj_pay' => $req->les_proc_app_req_inj_pay,
                'dat_dep_req_inj_pay' => $req->dat_dep_req_inj_pay,
                'num_app_req_inj_pay' => $req->num_app_req_inj_pay,
                'dat_app_req_inj_pay' => $req->dat_app_req_inj_pay,
                'nom_consltnt_req_inj_pay' => $req->nom_consltnt_req_inj_pay,
                'trib_competnt_req_inj_pay' => $req->trib_competnt_req_inj_pay,
                'note_et_audience_req_inj_pay' => $req->note_et_audience_req_inj_pay,
                'dat_dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                'dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                'swt_proc_notif_req_inj_pay' => 0,
                'swt_proc_exe_req_inj_pay' => 0,
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ];
        }
        if (isset($req->swt_proc_notif_req_inj_pay)) {
            $vareqinjoncDpay = [
                'id_hist_scen' => $req->id_scenH,
                'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_proc_app_req_inj_pay' => $req->les_proc_app_req_inj_pay,
                'dat_dep_req_inj_pay' => $req->dat_dep_req_inj_pay,
                'num_app_req_inj_pay' => $req->num_app_req_inj_pay,
                'dat_app_req_inj_pay' => $req->dat_app_req_inj_pay,
                'nom_consltnt_req_inj_pay' => $req->nom_consltnt_req_inj_pay,
                'trib_competnt_req_inj_pay' => $req->trib_competnt_req_inj_pay,
                'note_et_audience_req_inj_pay' => $req->note_et_audience_req_inj_pay,
                'dat_dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                'dep_doss_au_trib_req_inj_pay' => $req->dep_doss_au_trib_req_inj_pay,
                'swt_proc_notif_req_inj_pay' => $req->swt_proc_notif_req_inj_pay,
                'date_dep_notif_req_inj_pay' => $req->date_dep_notif_req_inj_pay,
                'num_doss_notif_req_inj_pay' => $req->num_doss_notif_req_inj_pay,
                'huissier_des_req_inj_pay' => $req->huissier_des_req_inj_pay,
                'reslt_notif_req_inj_pay' => $req->reslt_notif_req_inj_pay,
                'date_appl_notif_req_inj_pay' => $req->date_appl_notif_req_inj_pay,
                'proces_notif_req_inj_pay' => $req->proces_notif_req_inj_pay,

                'swt_proc_exe_req_inj_pay' => 0,
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ];
        }
        if (isset($req->swt_proc_exe_req_inj_pay)) {
            $vareqinjoncDpay = [
                'id_hist_scen' => $req->id_scenH,
                'proc_dep_req_inj_pay' => $req->proc_dep_req_inj_pay,
                'les_proc_req_inj_pay' => $req->les_proc_req_inj_pay,
                'les_audins_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_preces_req_inj_pay' => $req->les_audins_req_inj_pay,
                'les_proc_app_req_inj_pay' => $req->les_proc_app_req_inj_pay,
                'dat_dep_req_inj_pay' => $req->dat_dep_req_inj_pay,
                'num_app_req_inj_pay' => $req->num_app_req_inj_pay,
                'dat_app_req_inj_pay' => $req->dat_app_req_inj_pay,
                'nom_consltnt_req_inj_pay' => $req->nom_consltnt_req_inj_pay,
                'trib_competnt_req_inj_pay' => $req->trib_competnt_req_inj_pay,
                'note_et_audience_req_inj_pay' => $req->note_et_audience_req_inj_pay,
                'dat_dep_doss_au_trib_req_inj_pay' => $req->dat_dep_doss_au_trib_req_inj_pay,
                'dep_doss_au_trib_req_inj_pay' => $req->dep_doss_au_trib_req_inj_pay,
                'swt_proc_notif_req_inj_pay' => $req->swt_proc_notif_req_inj_pay,
                'date_dep_notif_req_inj_pay' => $req->date_dep_notif_req_inj_pay,
                'num_doss_notif_req_inj_pay' => $req->num_doss_notif_req_inj_pay,
                'huissier_des_req_inj_pay' => $req->huissier_des_req_inj_pay,
                'reslt_notif_req_inj_pay' => $req->reslt_notif_req_inj_pay,
                'date_appl_notif_req_inj_pay' => $req->date_appl_notif_req_inj_pay,
                'proces_notif_req_inj_pay' => $req->proces_notif_req_inj_pay,
                'swt_proc_exe_req_inj_pay' => $req->swt_proc_exe_req_inj_pay,
                'num_dos_exe_req_inj_pay' => $req->num_dos_exe_req_inj_pay,
                'date_dos_exe_req_inj_pay' => $req->date_dos_exe_req_inj_pay,
                'huissier_designe_exe_req_inj_pay' => $req->huissier_designe_exe_req_inj_pay,
                'date_exe_req_inj_pay' => $req->date_exe_req_inj_pay,
                'expiration_exe_req_inj_pay' => $req->expiration_exe_req_inj_pay,
                'dat_saisie_req_inj_pay' => $req->dat_saisie_req_inj_pay,
                'type_saisie_req_inj_pay' => $req->type_saisie_req_inj_pay,
                'date_exe_judic_req_inj_pay' => $req->date_exe_judic_req_inj_pay,
                'nom_expert_exe_req_inj_pay' => $req->nom_expert_exe_req_inj_pay,
                'pub_jal_req_inj_pay' => $req->pub_jal_req_inj_pay,
                'nom_journal_req_inj_pay' => $req->nom_journal_req_inj_pay,
                'vent_aux_encher_public_req_inj_pay' => $req->vent_aux_encher_public_req_inj_pay,
                'date_vent_exe_req_inj_pay' => $req->date_vent_exe_req_inj_pay,
                'result_exe_req_inj_pay' => $req->result_exe_req_inj_pay,
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ];
        }

        $GG = ReqinjoncDpay::where('id', $req->id_scenH)->update($vareqinjoncDpay);
        $for_id = HistoriqueScenario::where('id', $req->id_hist)->first();

        // activitis
        $HistoriqueAct = HistoriqueAct::where('id_action', $req->id_scenH)
            ->where('id_valid', 1)
            ->where('where', 'scenInjonctionPay')
            ->where('what', 'create')
            ->update(['id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0)]);

        if (!empty($GG)) {
            $link = route('SceneList', [$for_id->id_doss]);
            return json_encode($link);
        } else {
            return json_encode($data);
        }
    }
    public function delete_Reqinjoction($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'scenReqInjonctionPay')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'scenReqInjonctionPay',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        } else {
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'scenReqInjonctionPay',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0),
            ]);
            ReqinjoncDpay::where('id', $id)->delete();
        }
        return back();
    }

    public function citation_list($id)
    {
        $Citation = Citation::where('id_hist_scen', $id)->get();
        $avocats = partner::where('type_partn', 'Avocat')->get();
        return view('admin.scens.list_citaions')
            ->with('Citation', $Citation)
            ->with('avocats', $avocats)
            ->with('id_hist', $id);
    }
    public function citation_form($id, Request $req)
    {
        $partner = partner::get();
        return view('admin.scens.form_citation')
            ->with('id_hist', $id)
            ->with('partner', $partner)
            ->with('vald_pieces', $req->vald_pieces)
            ->with('provi', $req->provi)
            ->with('avocat', $req->avocat)
            ->with('date_debut_plant', $req->date_debut_plant)
            ->with('date_fin_plant', $req->date_fin_plant);
    }
    function historiqueSaveData($what, $where, $id_action, $event, $is_valid = null)
    {   
        if ($event == 'create') {
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => $what,
                'where' => $where, //changabel
                'id_action' => $id_action, //changabel
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'),
            ]);
        }
        if($event == 'update'){
           
            $HistoriqueAct = HistoriqueAct::where('id_action', $id_action)
            ->where('id_valid', 1)
            ->where('where', $where)
            ->where('what', $what)
            ->update(['id_valid' => $is_valid]); 
            return [$id_action];
        }
    }
    public function save_citation_dir(Request $req)
    {
        $audic = [];
        $audic_pha = [];
        $citation = [];

        if (isset($req->cit_phas_app)) {
            $citation = [
                'id_hist_scen' => $req->id_hist,
                'vald_pieces' => $req->vald_pieces,

                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'cit_ref' => (isset($req->cit_ref)) ? $req->cit_ref : '',
                'cit_cod_jud' => (isset($req->cit_cod_jud)) ? $req->cit_cod_jud : '',
                'cit_trib_comp' => (isset($req->cit_trib_comp)) ? $req->cit_trib_comp : '',
                'cit_client' => (isset($req->cit_client)) ? $req->cit_client : '',
                'cit_advers' => (isset($req->cit_advers)) ? $req->cit_advers : '',
                'cit_sujet' => (isset($req->cit_sujet)) ? $req->cit_sujet : '',
                'cit_jugemen' => (isset($req->cit_jugemen)) ? $req->cit_jugemen : '',

                'cit_phas_app' => (isset($req->cit_phas_app)) ? 1 : 0,

                'cit_code_app' => (isset($req->cit_code_app)) ? $req->cit_code_app : '',
                'cit_date_app' => (isset($req->cit_date_app)) ? $req->cit_date_app : '',
                'cit_jugemen_difinit' => (isset($req->cit_jugemen_difinit)) ? $req->cit_jugemen_difinit : ''
            ];
        } else {
            $citation = [
                'id_hist_scen' => $req->id_hist,
                'cit_ref' => (isset($req->cit_ref)) ? $req->cit_ref : '',
                'cit_cod_jud' => (isset($req->cit_cod_jud)) ? $req->cit_cod_jud : '',
                'cit_trib_comp' => (isset($req->cit_trib_comp)) ? $req->cit_trib_comp : '',
                'cit_client' => (isset($req->cit_client)) ? $req->cit_client : '',
                'cit_advers' => (isset($req->cit_advers)) ? $req->cit_advers : '',
                'cit_sujet' => (isset($req->cit_sujet)) ? $req->cit_sujet : '',
                'cit_jugemen' => (isset($req->cit_jugemen)) ? $req->cit_jugemen : '',
                'cit_phas_app' => (isset($req->cit_phas_app)) ? 1 : 0,
                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ];
        }
        $data = Citation::create($citation);
        $Citation_id = $data->id;
        if (isset($req->cit_phas_app)) {
            for ($j = 0; $j < count($req->pha_indeXaudi); $j++) {
                PhaCitAudiences::create([
                    'id_citaton' => $Citation_id,
                    'pha_cit_date_audc' => $req->pha_cit_date_audc[$j],
                    'pha_cit_heur_audc' => $req->pha_cit_heur_audc[$j],
                    'pha_cit_lieu_audc' => $req->pha_cit_lieu_audc[$j],
                    'pha_cit_resume' => $req->pha_cit_resume[$j],
                ]);
            }
            for ($i = 0; $i < count($req->indeXaudi); $i++) {
                CitAudiences::create([
                    'id_citaton' => $Citation_id,
                    'cit_date_audc' => $req->cit_date_audc[$i],
                    'cit_heur_audc' => $req->cit_heur_audc[$i],
                    'cit_lieu_audc' => $req->cit_lieu_audc[$i],
                    'cit_resume' => $req->cit_resume[$i],
                ]);
            }
        } else {
            for ($i = 0; $i < count($req->indeXaudi); $i++) {
                CitAudiences::create([
                    'id_citaton' => $Citation_id,
                    'cit_date_audc' => $req->cit_date_audc[$i],
                    'cit_heur_audc' => $req->cit_heur_audc[$i],
                    'cit_lieu_audc' => $req->cit_lieu_audc[$i],
                    'cit_resume' => $req->cit_resume[$i],
                ]);
            }
        }

        // HistoriqueAct::create([
        //     'id_user' => Auth::user()->id,
        //     'what' => 'create',
        //     'where' => 'citation_direct', //changabel
        //     'id_action' => $Citation_id, //changabel
        //     'username' => Auth::user()->nom,
        //     'userlastname' => Auth::user()->prenom,
        //     'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'),
        // ]);
        $this->historiqueSaveData('create', 'citation_direct', $Citation_id, 'create');

        $for_id = HistoriqueScenario::where('id', $req->id_hist)->first();
        if(Auth::user()->droit_de_valid_acts == 1){
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }
        return json_encode(route('citation_list', [$req->id_hist]));
    }
    public function form_citation_phase_app()
    {
        return json_encode(view('admin.scens.form_citation_phase_app')->render());
    }
    public function delete_citation($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'citation_direct')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'citation_direct',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        } else {
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'citation_direct',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0),
            ]);
            Citation::where('id', $id)->delete();
            CitAudiences::where('id_citaton', $id)->delete();
            PhaCitAudiences::where('id_citaton', $id)->delete();
        }

        return back();
    }
    public function show_citation($id)
    {
        $partner = partner::get();
        $Citation = Citation::where('id', $id)->first();
        $CitAudiences = CitAudiences::where('id_citaton', $id)->get();
        $PhaCitAudiences = PhaCitAudiences::where('id_citaton', $id)->get();
        return view('admin.scens.show_citation')
            ->with('Citation', $Citation)
            ->with('CitAudiences', $CitAudiences)
            ->with('partner', $partner)
            ->with('PhaCitAudiences', $PhaCitAudiences);
    }
    public function edit_citation(Request $req)
    {
        $audic = [];
        $audic_pha = [];
        $citation = [];

        if (isset($req->cit_phas_app)) {
            $citation = [

                'cit_ref' => (isset($req->cit_ref)) ? $req->cit_ref : '',
                'cit_cod_jud' => (isset($req->cit_cod_jud)) ? $req->cit_cod_jud : '',
                'cit_trib_comp' => (isset($req->cit_trib_comp)) ? $req->cit_trib_comp : '',
                'cit_client' => (isset($req->cit_client)) ? $req->cit_client : '',
                'cit_advers' => (isset($req->cit_advers)) ? $req->cit_advers : '',
                'cit_sujet' => (isset($req->cit_sujet)) ? $req->cit_sujet : '',
                'cit_jugemen' => (isset($req->cit_jugemen)) ? $req->cit_jugemen : '',

                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'cit_phas_app' => (isset($req->cit_phas_app)) ? 1 : '0',

                'cit_code_app' => (isset($req->cit_code_app)) ? $req->cit_code_app : '',
                'cit_date_app' => (isset($req->cit_date_app)) ? $req->cit_date_app : '',
                'cit_jugemen_difinit' => (isset($req->cit_jugemen_difinit)) ? $req->cit_jugemen_difinit : ''
            ];
            // return json_encode($citation);
        } else {
            $citation = [

                'provi' => $req->provi,
                'avocat' => $req->avocat,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,

                'cit_ref' => (isset($req->cit_ref)) ? $req->cit_ref : '',
                'cit_cod_jud' => (isset($req->cit_cod_jud)) ? $req->cit_cod_jud : '',
                'cit_trib_comp' => (isset($req->cit_trib_comp)) ? $req->cit_trib_comp : '',
                'cit_client' => (isset($req->cit_client)) ? $req->cit_client : '',
                'cit_advers' => (isset($req->cit_advers)) ? $req->cit_advers : '',
                'cit_sujet' => (isset($req->cit_sujet)) ? $req->cit_sujet : '',
                'cit_jugemen' => (isset($req->cit_jugemen)) ? $req->cit_jugemen : ''
            ];
        }

        Citation::where('id', $req->id_cit)->update($citation);
        $Citation_id = $req->id_cit;

        if (isset($req->cit_phas_app)) {
            PhaCitAudiences::where('id_citaton', $Citation_id)->delete();
            CitAudiences::where('id_citaton', $Citation_id)->delete();
            for ($j = 0; $j < count($req->pha_indeXaudi); $j++) {
                PhaCitAudiences::create([
                    'id_citaton' => $Citation_id,
                    'pha_cit_date_audc' => $req->pha_cit_date_audc[$j],
                    'pha_cit_heur_audc' => $req->pha_cit_heur_audc[$j],
                    'pha_cit_lieu_audc' => $req->pha_cit_lieu_audc[$j],
                    'pha_cit_resume' => $req->pha_cit_resume[$j],
                ]);
            }
            for ($i = 0; $i < count($req->indeXaudi); $i++) {
                CitAudiences::create([
                    'id_citaton' => $Citation_id,
                    'cit_date_audc' => $req->cit_date_audc[$i],
                    'cit_heur_audc' => $req->cit_heur_audc[$i],
                    'cit_lieu_audc' => $req->cit_lieu_audc[$i],
                    'cit_resume' => $req->cit_resume[$i],
                ]);
            }
        } else {
            CitAudiences::where('id_citaton', $Citation_id)->delete();
            for ($i = 0; $i < count($req->indeXaudi); $i++) {
                CitAudiences::create([
                    'id_citaton' => $Citation_id,
                    'cit_date_audc' => $req->cit_date_audc[$i],
                    'cit_heur_audc' => $req->cit_heur_audc[$i],
                    'cit_lieu_audc' => $req->cit_lieu_audc[$i],
                    'cit_resume' => $req->cit_resume[$i],
                ]);
            }
        }

        // activitis
        $this->historiqueSaveData('create', 'citation_direct', $Citation_id, 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : 0));
        return json_encode(route('citation_list', [$req->id_hist]));
    }
    public function cliDossier($id)
    {
        $dossiers = Dossier::select(
            'clients.nom as nomcli',
            'clients.prenom as prenomcli',
            'clients.type_cli',
            'clients.raison_sos',
            'debiteurs.nom as nomdbtr',
            'debiteurs.prenom as prenomdbtr',
            'debiteurs.type_dtbr',
            'dossiers.*'
        )
            ->leftJoin('clients', 'clients.id', 'dossiers.id_client')
            ->leftJoin('debiteurs', 'debiteurs.id', 'dossiers.id_dbtr')
            ->where('dossiers.id_client', $id);

        if (Auth::user()->see_all_doss != 1) {
            $dossiers = $dossiers->where('dossiers.creator_id', Auth::user()->id);
        }
        $dossiers = $dossiers->get();
        return view('dossier.list_dossier')
            ->with('dossiers', $dossiers);
    }
    public function get_list_situ_jurid(Request $req)
    {
        $data = false;
        $type = false;

        if ($req->bilongs == 'cheque_sn_provis_block') {
            $data = ScenJudicCheq::where('id_hist_scen', $req->id)->get();
            $type = $req->bilongs;
        }
        if ($req->bilongs == 'injonction_pay_block') {
            $data = Injonction_De_Payer::where('id_hist_scen', $req->id)->get();
            $type = $req->bilongs;
        }
        if ($req->bilongs == 'req_injonction_pay_block') {
            $data = ReqinjoncDpay::where('id_hist_scen', $req->id)->get();
            $type = $req->bilongs;
        }
        if ($req->bilongs == 'citation_dir_block') {
            $data = Citation::where('id_hist_scen', $req->id)->get();
            $type = $req->bilongs;
        }
        // return json_encode(count($data));

        $view = view('admin.scens.ajax-listScens')
            ->with('data', $data)
            ->with('type', $type)
            ->render();

        return json_encode($view);
    }
    public function get_range_form(Request $req)
    {
        $view = view('admin.scens.ajax-range-form')
            ->with('type', (isset($req->bilongs)) ? $req->bilongs : '')
            ->with('id', (isset($req->id)) ? $req->id : '')
            ->render();

        return json_encode($view);
    }
    public function create_range(Request $req)
    {

        Procedure_range::create(
            [
                'id_histo' => $req->id_histo_from_range,
                'date_1' => $req->date_1,
                'date_2' => $req->date_2,
                'type' => $req->belong,
                'id_doss' => $req->id_doss
            ]
        );

        $data = Procedure_range::where('id_doss', $req->id_doss)->get();

        $view = view('admin.scens.exist_ranges')->with('Procedure_range', $data)->render();
        return json_encode($view);
    }
    public function delete_range(Request $req)
    {
        Procedure_range::where('id', $req->id)->delete();

        $data = Procedure_range::where('id_doss', $req->id_doss)->get();

        $view = view('admin.scens.exist_ranges')->with('Procedure_range', $data)->render();
        return json_encode($view);
    }
    public function update_range(Request $req)
    {
        Procedure_range::where('id', $req->id)->update([
            'date_1' => $req->date_1_,
            'date_2' => $req->date_2_,
        ]);
        $data = Procedure_range::where('id_doss', $req->id_doss)->get();
        $view = view('admin.scens.exist_ranges')->with('Procedure_range', $data)->render();
        return json_encode($view);
    }

    public function duplicat_audi()
    {
        return json_encode(view('admin.scens.audiences_list')->render());
    }
    public function pha_duplicat_audi()
    {
        return json_encode(view('admin.scens.audiences_list_pha')->render());
    }
    public function saisie_fond_commerce_list($id)
    {
        $avocats = partner::where('type_partn', 'Avocat')->get();
        $data = SaisieFondComers::where('id_hist_scen', $id)->get();
        return view('admin.scens.list_fond_comerce')
            ->with('list', $data)
            ->with('avocats', $avocats)
            ->with('id_hist', $id);
    }
    function dossierInfoFonc($id)
    {
        $dossier = $this->dossierInfoSaisie = HistoriqueScenario::select('dossiers.*')->join('dossiers', 'dossiers.id', 'historique_scenarios.id_doss')->where('historique_scenarios.id', $id)->first();
        $this->ClientInfoSaisie = Client::where('id', $dossier->id_client)->first();
        $this->debitersInfoSaisie = Debiteur::where('id', $dossier->id_dbtr)->first();
    }
    public function saisie_fond_commerce_form($id, Request $req)
    {
        $this->dossierInfoFonc($id);
        $partner = partner::get();
        return view('admin.scens.form_fond_comerce')
            ->with('id_hist', $id)
            ->with('dossier', $this->dossierInfoSaisie)
            ->with('client', $this->ClientInfoSaisie)
            ->with('debiteur',  $this->debitersInfoSaisie)
            ->with('partner', $partner)
            ->with('vald_pieces', $req->vald_pieces)
            ->with('provi', $req->provi)
            ->with('avocat', $req->avocat)
            ->with('date_debut_plant', $req->date_debut_plant)
            ->with('date_fin_plant', $req->date_fin_plant);
    }
    public function render_fond_comerc_deroul(Request $req)
    {
        $view = view('admin.scens.fond_comerc_deroulement_audiance')
            ->with('date_result', $req->date_result)
            ->with('result', $req->result)
            ->with('index', $req->index)
            ->render();
        return json_encode($view);
    }
    public function add_fond_commerce_form(Request $req)
    {
        $res = SaisieFondComers::create([
            'id_hist_scen' => $req->id_hist,
            'tribunal_competant' => $req->tribunal_competant,
            'non_juge' => $req->non_juge,
            'ref' => $req->ref,
            'benificiaire' => $req->benificiaire,
            'avoca_benif' => $req->avoca_benif,
            'charger_suivi_binif' => $req->charger_suivi_binif,
            'le_tireur' => $req->le_tireur,
            'address_tireur' => $req->address_tireur,
            'cin_treur' => $req->cin_treur,
            'avocat_tireur' => $req->avocat_tireur,
            'montant_creance' => $req->montant_creance,
            'date_depot' => $req->date_depot,
            'accep_demande' => $req->accep_demande,
            'huissier_designie' => $req->huissier_designie,
            'reslt_notif' => $req->reslt_notif,

            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ]);

        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_sfc' => $res->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            foreach ($req->date_result as $index => $item) {
                $res = SaisieFontComersDeroulAudience::create([
                    'id_fc' => $res->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }
        
        $this->historiqueSaveData('create', 'FondComers', $res->id , 'create');
        $for_id = HistoriqueScenario::where('id', $req->id_hist)->first();
        if(Auth::user()->droit_de_valid_acts == 1){
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }
        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_fond_commerce_list', [$req->id_hist])
        ];
        

        return json_encode($res);
    }
    public function show_fond_commerce_form($id)
    {
        $fond_comrecs = SaisieFondComers::where('id', $id)->first();
        // dd($fond_comrecs);
        $dossier = HistoriqueScenario::select('dossiers.*')
            ->join('dossiers', 'dossiers.id', 'historique_scenarios.id_doss')
            ->where('historique_scenarios.id', $fond_comrecs->id_hist_scen)
            ->first();
        $fond_comrec_infos = SaisieFontComersDeroulAudience::where('id_fc', $id)->get();
        $files = Files::where('id_sfc', $id)->get();
        $File_path = File_path::where('type', 'joints')->first();
        $partner = partner::get();
        return view('admin.scens.saisie_font_commerc')
            ->with('files', $files)
            ->with('partner', $partner)
            ->with('File_path', $File_path)
            ->with('dossier', $dossier)
            ->with('fond_comrec_infos', $fond_comrec_infos)
            ->with('id_hist', $fond_comrecs->id_hist_scen)
            ->with('fond_comrecs', $fond_comrecs);
    }
    public function update_fond_commerce_form(Request $req)
    {
         $res = SaisieFondComers::where('id', $req->id)->update([
            'id_hist_scen' => $req->id_hist,
            'tribunal_competant' => $req->tribunal_competant,
            'non_juge' => $req->non_juge,
            'ref' => $req->ref,
            'benificiaire' => $req->benificiaire,
            'avoca_benif' => $req->avoca_benif,
            'charger_suivi_binif' => $req->charger_suivi_binif,
            'le_tireur' => $req->le_tireur,
            'address_tireur' => $req->address_tireur,
            'cin_treur' => $req->cin_treur,
            'avocat_tireur' => $req->avocat_tireur,
            'montant_creance' => $req->montant_creance,
            'date_depot' => $req->date_depot,
            'accep_demande' => $req->accep_demande,
            'huissier_designie' => $req->huissier_designie,
            'reslt_notif' => $req->reslt_notif,

            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ]);

        if (isset($req->date_result)) {
            SaisieFontComersDeroulAudience::where('id_fc', $req->id)->delete();
            foreach ($req->date_result as $index => $item) {
                $res = SaisieFontComersDeroulAudience::create([
                    'id_fc' => $req->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }

        if (isset($req->file_exc)) {
            Files::where('id_sfc', $req->id)
                ->whereNotIn('file_name', $req->file_exc)
                ->delete();
        } else {
            Files::where('id_sfc', $req->id)
                ->delete();
        }

        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_sfc' => $req->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        $this->historiqueSaveData('create', 'FondComers', $req->id , 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'));
        
        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_fond_commerce_list', [$req->id_hist])
        ];

        return json_encode($res);
    }
    public function delete_font_comerc($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'FondComers')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'FondComers',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        }else{
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'FondComers',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => 1,
            ]);
            SaisieFondComers::where('id', $id)->delete();
            SaisieFontComersDeroulAudience::where('id_fc', $id)->delete();
        }
        
        return back();
    }
    public function saisie_tiers_detonado_list($id)
    {
        $avocats = partner::where('type_partn', 'Avocat')->get();
        $data = SaisieTiersDetenteur::where('id_hist_scen', $id)->get();
        return view('admin.scens.list_saisie_tiers_detonad')
            ->with('avocats', $avocats)
            ->with('list', $data)
            ->with('id_hist', $id);
    }
    public function saisie_tiers_detonad_form($id, Request $req)
    {
        $this->dossierInfoFonc($id);
        $partner = partner::get();
        return view('admin.scens.form_tiers_detonteur')
            ->with('dossier', $this->dossierInfoSaisie)
            ->with('client', $this->ClientInfoSaisie)
            ->with('debiteur',  $this->debitersInfoSaisie)
            ->with('id_hist', $id)
            ->with('partner', $partner)
            ->with('vald_pieces', $req->vald_pieces)
            ->with('provi', $req->provi)
            ->with('avocat', $req->avocat)
            ->with('date_debut_plant', $req->date_debut_plant)
            ->with('date_fin_plant', $req->date_fin_plant);
    }
    public function add_saisie_tiers_detonado(Request $req)
    {
        $res = SaisieTiersDetenteur::create([
            'id_hist_scen' => $req->id_hist,
            'tribunal_competant' => $req->tribunal_competant,
            'non_juge' => $req->non_juge,
            'ref' => $req->ref,
            'benificiaire' => $req->benificiaire,
            'avoca_benif' => $req->avoca_benif,
            'charger_suivi_binif' => $req->charger_suivi_binif,
            'le_tireur' => $req->le_tireur,
            'address_tireur' => $req->address_tireur,
            'cin_treur' => $req->cin_treur,
            'avocat_tireur' => $req->avocat_tireur,
            'montant_creance' => $req->montant_creance,
            'date_depot' => $req->date_depot,
            'accep_demande' => $req->accep_demande,
            'huissier_designie' => $req->huissier_designie,
            'reslt_notif' => $req->reslt_notif,

            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ]);

        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_td' => $res->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            foreach ($req->date_result as $index => $item) {
                $res = SaisieTiersDetenteurDeroulAudience::create([
                    'id_td' => $res->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }
        
        $this->historiqueSaveData('create', 'SaisieTiersDetenteur', $res->id , 'create');
        $for_id = HistoriqueScenario::where('id', $req->id_hist)->first();
        if(Auth::user()->droit_de_valid_acts == 1){
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }
        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_tiers_detonado_list', [$req->id_hist])
        ];

        return json_encode($res);
    }
    public function show_saisie_tiers_detonado($id)
    {
        $tiers_deton = SaisieTiersDetenteur::where('id', $id)->first();
        $dossier = HistoriqueScenario::select('dossiers.*')
            ->join('dossiers', 'dossiers.id', 'historique_scenarios.id_doss')
            ->where('historique_scenarios.id', $tiers_deton->id_hist_scen)
            ->first();
        $tiers_deton_infos = SaisieTiersDetenteurDeroulAudience::where('id_td', $id)->get();
        $files = Files::where('id_td', $id)->get();
        $File_path = File_path::where('type', 'joints')->first();
        $partner = partner::get();
        return view('admin.scens.saisie_tiers_detenteur')
            ->with('id_hist', $tiers_deton->id_hist_scen)
            ->with('tiers_deton', $tiers_deton)
            ->with('dossier', $dossier)
            ->with('tiers_deton_infos', $tiers_deton_infos)
            ->with('files', $files)
            ->with('File_path', $File_path)
            ->with('partner', $partner);
    }
    public function update_saisie_tiers_detonado(Request $req)
    {

        $res = SaisieTiersDetenteur::where('id', $req->id)
            ->update([
                'id_hist_scen' => $req->id_hist,
                'tribunal_competant' => $req->tribunal_competant,
                'non_juge' => $req->non_juge,
                'ref' => $req->ref,
                'benificiaire' => $req->benificiaire,
                'avoca_benif' => $req->avoca_benif,
                'charger_suivi_binif' => $req->charger_suivi_binif,
                'le_tireur' => $req->le_tireur,
                'address_tireur' => $req->address_tireur,
                'cin_treur' => $req->cin_treur,
                'avocat_tireur' => $req->avocat_tireur,
                'montant_creance' => $req->montant_creance,
                'date_depot' => $req->date_depot,
                'accep_demande' => $req->accep_demande,
                'huissier_designie' => $req->huissier_designie,
                'reslt_notif' => $req->reslt_notif,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ]);

        if (isset($req->file_exc)) {
            Files::where('id_td', $req->id)
                ->whereNotIn('file_name', $req->file_exc)
                ->delete();
        } else {
            Files::where('id_td', $req->id)->delete();
        }

        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_td' => $req->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            SaisieTiersDetenteurDeroulAudience::where('id_td', $req->id)->delete();
            foreach ($req->date_result as $index => $item) {
                $res = SaisieTiersDetenteurDeroulAudience::create([
                    'id_td' => $req->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }
        $this->historiqueSaveData('create', 'SaisieTiersDetenteur', $req->id , 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'));
        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_tiers_detonado_list', [$req->id_hist])
        ];

        return json_encode($res);
    }
    public function delete_saisie_tiers_detonado($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'SaisieTiersDetenteur')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'SaisieTiersDetenteur',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        }else{
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'SaisieTiersDetenteur',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => 1,
            ]);
            SaisieTiersDetenteur::where('id', $id)->delete();
            SaisieTiersDetenteurDeroulAudience::where('id_td', $id)->delete();
        }
        return back();
    }
    public function saisie_arret_bancaire_list($id)
    {
        $avocats = partner::where('type_partn', 'Avocat')->get();
        $data = SaisieArretBancaire::where('id_hist_scen', $id)->get();
        return view('admin.scens.list_saisie_arret_bancaire')
            ->with('avocats', $avocats)
            ->with('list', $data)
            ->with('id_hist', $id);
    }
    public function saisie_arret_bancair_form($id, Request $req)
    {
        $this->dossierInfoFonc($id);
        $partner = partner::get();
        return view('admin.scens.form_arret_bancaire')
            ->with('dossier', $this->dossierInfoSaisie)
            ->with('client', $this->ClientInfoSaisie)
            ->with('debiteur',  $this->debitersInfoSaisie)
            ->with('id_hist', $id)
            ->with('partner', $partner)
            ->with('vald_pieces', $req->vald_pieces)
            ->with('provi', $req->provi)
            ->with('avocat', $req->avocat)
            ->with('date_debut_plant', $req->date_debut_plant)
            ->with('date_fin_plant', $req->date_fin_plant);
    }
    public function add_saisie_arret_bancaire(Request $req)
    {
        $res = SaisieArretBancaire::create([
            'id_hist_scen' => $req->id_hist,
            'tribunal_competant' => $req->tribunal_competant,
            'non_juge' => $req->non_juge,
            'ref' => $req->ref,
            'benificiaire' => $req->benificiaire,
            'avoca_benif' => $req->avoca_benif,
            'charger_suivi_binif' => $req->charger_suivi_binif,
            'le_tireur' => $req->le_tireur,
            'address_tireur' => $req->address_tireur,
            'cin_treur' => $req->cin_treur,
            'avocat_tireur' => $req->avocat_tireur,
            'montant_creance' => $req->montant_creance,
            'date_depot' => $req->date_depot,
            'accep_demande' => $req->accep_demande,
            'huissier_designie' => $req->huissier_designie,
            'reslt_notif' => $req->reslt_notif,

            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ]);

        $this->historiqueSaveData('create', 'SaisieArretBancaire', $res->id , 'create');
        $for_id = HistoriqueScenario::where('id', $req->id_hist)->first();
        if(Auth::user()->droit_de_valid_acts == 1){
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }
        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_arrb' => $res->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            foreach ($req->date_result as $index => $item) {
                $res = SaisieArretBancaireDeroulAudience::create([
                    'id_arrb' => $res->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }

        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_arret_bancaire_list', [$req->id_hist])
        ];

        return json_encode($res);
    }
    public function show_saisie_arret_bancaire($id)
    {
        $arret_bancaire = SaisieArretBancaire::where('id', $id)->first();
        $dossier = HistoriqueScenario::select('dossiers.*')
            ->join('dossiers', 'dossiers.id', 'historique_scenarios.id_doss')
            ->where('historique_scenarios.id', $arret_bancaire->id_hist_scen)
            ->first();
        $arret_bancaire_infos = SaisieArretBancaireDeroulAudience::where('id_arrb', $id)->get();
        $files = Files::where('id_arrb', $id)->get();
        $File_path = File_path::where('type', 'joints')->first();
        $partner = partner::where('type_partn', 'Avocat')->get();
        return view('admin.scens.saisie_arret_bancaire')
            ->with('partner', $partner)
            ->with('id_hist', $arret_bancaire->id_hist_scen)
            ->with('arret_bancaire', $arret_bancaire)
            ->with('dossier', $dossier)
            ->with('arret_bancaire_infos', $arret_bancaire_infos)
            ->with('files', $files)
            ->with('File_path', $File_path);
    }
    public function update_saisie_arret_bancaire(Request $req)
    {
        $res = SaisieArretBancaire::where('id', $req->id)
        ->update([
            'id_hist_scen' => $req->id_hist,
            'tribunal_competant' => $req->tribunal_competant,
            'non_juge' => $req->non_juge,
            'ref' => $req->ref,
            'benificiaire' => $req->benificiaire,
            'avoca_benif' => $req->avoca_benif,
            'charger_suivi_binif' => $req->charger_suivi_binif,
            'le_tireur' => $req->le_tireur,
            'address_tireur' => $req->address_tireur,
            'cin_treur' => $req->cin_treur,
            'avocat_tireur' => $req->avocat_tireur,
            'montant_creance' => $req->montant_creance,
            'date_depot' => $req->date_depot,
            'accep_demande' => $req->accep_demande,
            'huissier_designie' => $req->huissier_designie,
            'reslt_notif' => $req->reslt_notif,

            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ]);

        $this->historiqueSaveData('create', 'SaisieArretBancaire', $req->id , 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'));
        
        
        if (isset($req->file_exc)) {
            Files::where('id_arrb', $req->id)
                ->whereNotIn('file_name', $req->file_exc)
                ->delete();
        } else {
            Files::where('id_arrb', $req->id)->delete();
        }

        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_arrb' => $req->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            SaisieArretBancaireDeroulAudience::where('id_arrb', $req->id)->delete();
            foreach ($req->date_result as $index => $item) {
                $res = SaisieArretBancaireDeroulAudience::create([
                    'id_arrb' => $req->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }

        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_arret_bancaire_list', [$req->id_hist])
        ];
        return json_encode($res);
    }
    public function delete_saisie_arret_bancaire($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'SaisieArretBancaire')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'SaisieArretBancaire',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        }else{
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'SaisieArretBancaire',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => 1,
            ]);
            SaisieArretBancaire::where('id', $id)->delete();
            SaisieArretBancaireDeroulAudience::where('id_arrb', $id)->delete();
        }
        
        return back();
    }
    public function saisie_actions_parts_soc_list($id)
    {
        $avocats = partner::where('type_partn', 'Avocat')->get();
        $data = SaisieActionsPartsSoc::where('id_hist_scen', $id)->get();
        return view('admin.scens.list_saisie_action_parts')
            ->with('avocats', $avocats)
            ->with('list', $data)
            ->with('id_hist', $id);
    }
    public function saisie_action_parts_form($id, Request $req)
    {
        $this->dossierInfoFonc($id);
        $partner = partner::get();
        return view('admin.scens.form_actions_parts')
            ->with('dossier', $this->dossierInfoSaisie)
            ->with('client', $this->ClientInfoSaisie)
            ->with('debiteur',  $this->debitersInfoSaisie)
            ->with('partner', $partner)
            ->with('vald_pieces', $req->vald_pieces)
            ->with('provi', $req->provi)
            ->with('avocat', $req->avocat)
            ->with('date_debut_plant', $req->date_debut_plant)
            ->with('date_fin_plant', $req->date_fin_plant)
            ->with('id_hist', $id);
    }
    public function add_saisie_actions_parts(Request $req)
    {
        $res = SaisieActionsPartsSoc::create([
            'id_hist_scen' => $req->id_hist,
            'tribunal_competant' => $req->tribunal_competant,
            'non_juge' => $req->non_juge,
            'ref' => $req->ref,
            'benificiaire' => $req->benificiaire,
            'avoca_benif' => $req->avoca_benif,
            'charger_suivi_binif' => $req->charger_suivi_binif,
            'le_tireur' => $req->le_tireur,
            'address_tireur' => $req->address_tireur,
            'cin_treur' => $req->cin_treur,
            'avocat_tireur' => $req->avocat_tireur,
            'montant_creance' => $req->montant_creance,
            'date_depot' => $req->date_depot,
            'accep_demande' => $req->accep_demande,
            'huissier_designie' => $req->huissier_designie,
            'reslt_notif' => $req->reslt_notif,

            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ]);
        $this->historiqueSaveData('create', 'SaisieActionsPartsSoc', $res->id , 'create');
        $for_id = HistoriqueScenario::where('id', $req->id_hist)->first();
        if(Auth::user()->droit_de_valid_acts == 1){
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }
        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_asp' => $res->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            foreach ($req->date_result as $index => $item) {
                $res = SaisieActionsPartsSocDeroulAudience::create([
                    'id_asp' => $res->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }

        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_actions_parts_soc_list', [$req->id_hist])
        ];

        return json_encode($res);
    }
    public function show_saisie_actions_parts($id)
    {
        $action_parts = SaisieActionsPartsSoc::where('id', $id)->first();
        $dossier = HistoriqueScenario::select('dossiers.*')
            ->join('dossiers', 'dossiers.id', 'historique_scenarios.id_doss')
            ->where('historique_scenarios.id', $action_parts->id_hist_scen)
            ->first();
        $actions_parts_infos = SaisieActionsPartsSocDeroulAudience::where('id_asp', $id)->get();
        $files = Files::where('id_aps', $id)->get();
        $File_path = File_path::where('type', 'joints')->first();
        $partner = partner::where('type_partn', 'Avocat')->get();
        return view('admin.scens.saisie_action_patrs')
            ->with('id_hist', $action_parts->id_hist_scen)
            ->with('action_parts', $action_parts)
            ->with('dossier', $dossier)
            ->with('actions_parts_infos', $actions_parts_infos)
            ->with('files', $files)
            ->with('partner', $partner)
            ->with('File_path', $File_path);
    }
    public function update_saisie_actions_parts(Request $req)
    {
        $res = SaisieActionsPartsSoc::where('id', $req->id)
        ->update([
            'id_hist_scen' => $req->id_hist,
            'tribunal_competant' => $req->tribunal_competant,
            'non_juge' => $req->non_juge,
            'ref' => $req->ref,
            'benificiaire' => $req->benificiaire,
            'avoca_benif' => $req->avoca_benif,
            'charger_suivi_binif' => $req->charger_suivi_binif,
            'le_tireur' => $req->le_tireur,
            'address_tireur' => $req->address_tireur,
            'cin_treur' => $req->cin_treur,
            'avocat_tireur' => $req->avocat_tireur,
            'montant_creance' => $req->montant_creance,
            'date_depot' => $req->date_depot,
            'accep_demande' => $req->accep_demande,
            'huissier_designie' => $req->huissier_designie,
            'reslt_notif' => $req->reslt_notif,

            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ]);
        
        $this->historiqueSaveData('create', 'SaisieActionsPartsSoc', $req->id , 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'));
        
        if (isset($req->file_exc)) {
            Files::where('id_aps', $req->id)
                ->whereNotIn('file_name', $req->file_exc)
                ->delete();
        } else {
            Files::where('id_aps', $req->id)->delete();
        }

        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_aps' => $req->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            SaisieActionsPartsSocDeroulAudience::where('id_asp', $req->id)->delete();
            foreach ($req->date_result as $index => $item) {
                $res = SaisieActionsPartsSocDeroulAudience::create([
                    'id_asp' => $req->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }

        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_actions_parts_soc_list', [$req->id_hist])
        ];
        return json_encode($res);
    }
    public function delete_saisie_actions_parts($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'SaisieActionsPartsSoc')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'SaisieActionsPartsSoc',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        }else{
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'SaisieActionsPartsSoc',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => 1,
            ]);
            SaisieActionsPartsSoc::where('id', $id)->delete();
            SaisieActionsPartsSocDeroulAudience::where('id_asp', $id)->delete();
        }
        return back();
    }
    public function saisie_moblier_list($id)
    {
        $avocats = partner::where('type_partn', 'Avocat')->get();
        $data = SaisieMobiler::where('id_hist_scen', $id)->get();
        return view('admin.scens.list_saisie_mobilier')
            ->with('avocats', $avocats)
            ->with('list', $data)
            ->with('id_hist', $id);
    }
    public function saisie_mobilier_form($id, Request $req)
    {
        $this->dossierInfoFonc($id);
        $partner = partner::get();
        return view('admin.scens.form_saisie_mobilier')
            ->with('dossier', $this->dossierInfoSaisie)
            ->with('client', $this->ClientInfoSaisie)
            ->with('debiteur',  $this->debitersInfoSaisie)
            ->with('partner', $partner)
            ->with('vald_pieces', $req->vald_pieces)
            ->with('provi', $req->provi)
            ->with('avocat', $req->avocat)
            ->with('date_debut_plant', $req->date_debut_plant)
            ->with('date_fin_plant', $req->date_fin_plant)
            ->with('id_hist', $id);
    }
    public function add_saisie_mobilier(Request $req)
    {
        $res = SaisieMobiler::create([
            'id_hist_scen' => $req->id_hist,
            'tribunal_competant' => $req->tribunal_competant,
            'non_juge' => $req->non_juge,
            'ref' => $req->ref,
            'benificiaire' => $req->benificiaire,
            'avoca_benif' => $req->avoca_benif,
            'charger_suivi_binif' => $req->charger_suivi_binif,
            'le_tireur' => $req->le_tireur,
            'address_tireur' => $req->address_tireur,
            'cin_treur' => $req->cin_treur,
            'avocat_tireur' => $req->avocat_tireur,
            'montant_creance' => $req->montant_creance,
            'date_depot' => $req->date_depot,
            'accep_demande' => $req->accep_demande,
            'huissier_designie' => $req->huissier_designie,
            'reslt_notif' => $req->reslt_notif,

            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ]);

        $this->historiqueSaveData('create', 'SaisieMobiler', $res->id , 'create');
        $for_id = HistoriqueScenario::where('id', $req->id_hist)->first();
        if(Auth::user()->droit_de_valid_acts == 1){
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }
        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_smob' => $res->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            foreach ($req->date_result as $index => $item) {
                $res = SaisieMobilerDeroulAudience::create([
                    'id_smod' => $res->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }

        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_moblier_list', [$req->id_hist])
        ];

        return json_encode($res);
    }
    public function show_saisie_mobilier($id)
    {
        $mobilier = SaisieMobiler::where('id', $id)->first();
        $dossier = HistoriqueScenario::select('dossiers.*')
            ->join('dossiers', 'dossiers.id', 'historique_scenarios.id_doss')
            ->where('historique_scenarios.id', $mobilier->id_hist_scen)
            ->first();
        $mobilier_infos = SaisieMobilerDeroulAudience::where('id_smod', $id)->get();
        $files = Files::where('id_smob', $id)->get();
        $File_path = File_path::where('type', 'joints')->first();
        $partner = partner::where('type_partn', 'Avocat')->get();
        return view('admin.scens.saisie_mobilier')
            ->with('id_hist', $mobilier->id_hist_scen)
            ->with('mobilier', $mobilier)
            ->with('dossier', $dossier)
            ->with('mobilier_infos', $mobilier_infos)
            ->with('files', $files)
            ->with('partner', $partner)
            ->with('File_path', $File_path);
    }
    public function update_saisie_mobilier(Request $req)
    {
        $res = SaisieMobiler::where('id', $req->id)
            ->update([
                'id_hist_scen' => $req->id_hist,
                'tribunal_competant' => $req->tribunal_competant,
                'non_juge' => $req->non_juge,
                'ref' => $req->ref,
                'benificiaire' => $req->benificiaire,
                'avoca_benif' => $req->avoca_benif,
                'charger_suivi_binif' => $req->charger_suivi_binif,
                'le_tireur' => $req->le_tireur,
                'address_tireur' => $req->address_tireur,
                'cin_treur' => $req->cin_treur,
                'avocat_tireur' => $req->avocat_tireur,
                'montant_creance' => $req->montant_creance,
                'date_depot' => $req->date_depot,
                'accep_demande' => $req->accep_demande,
                'huissier_designie' => $req->huissier_designie,
                'reslt_notif' => $req->reslt_notif,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ]);

        $this->historiqueSaveData('create', 'SaisieMobiler', $req->id , 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'));

        if (isset($req->file_exc)) {
            Files::where('id_smob', $req->id)
                ->whereNotIn('file_name', $req->file_exc)
                ->delete();
        } else {
            Files::where('id_smob', $req->id)->delete();
        }

        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_smob' => $req->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            SaisieMobilerDeroulAudience::where('id_smod', $req->id)->delete();
            foreach ($req->date_result as $index => $item) {
                $res = SaisieMobilerDeroulAudience::create([
                    'id_smod' => $req->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }

        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_moblier_list', [$req->id_hist])
        ];
        return json_encode($res);
    }
    public function delete_saisie_mobilier($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'SaisieMobiler')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'SaisieMobiler',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        }else{
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'SaisieMobiler',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => 1,
            ]);
            SaisieMobiler::where('id', $id)->delete();
            SaisieMobilerDeroulAudience::where('id_smod', $id)->delete();
        }
        
        return back();

    }
    public function saisie_immoblier_list($id)
    {
        $avocats = partner::where('type_partn', 'Avocat')->get();
        $data = SaisieImobiler::where('id_hist_scen', $id)->get();
        return view('admin.scens.list_saisie_immobilier')
            ->with('list', $data)
            ->with('avocats', $avocats)
            ->with('id_hist', $id);
    }
    public function saisie_immobilier_form($id, Request $req)
    {
        $this->dossierInfoFonc($id);
        $partner = partner::get();
        return view('admin.scens.form_saisie_immobilier')
            ->with('dossier', $this->dossierInfoSaisie)
            ->with('client', $this->ClientInfoSaisie)
            ->with('debiteur',  $this->debitersInfoSaisie)
            ->with('partner', $partner)
            ->with('vald_pieces', $req->vald_pieces)
            ->with('provi', $req->provi)
            ->with('avocat', $req->avocat)
            ->with('date_debut_plant', $req->date_debut_plant)
            ->with('date_fin_plant', $req->date_fin_plant)
            ->with('id_hist', $id);
    }
    public function add_saisie_immobilier(Request $req)
    {
        $res = SaisieImobiler::create([
            'id_hist_scen' => $req->id_hist,
            'tribunal_competant' => $req->tribunal_competant,
            'non_juge' => $req->non_juge,
            'ref' => $req->ref,
            'benificiaire' => $req->benificiaire,
            'avoca_benif' => $req->avoca_benif,
            'charger_suivi_binif' => $req->charger_suivi_binif,
            'le_tireur' => $req->le_tireur,
            'address_tireur' => $req->address_tireur,
            'cin_treur' => $req->cin_treur,
            'avocat_tireur' => $req->avocat_tireur,
            'montant_creance' => $req->montant_creance,
            'date_depot' => $req->date_depot,
            'accep_demande' => $req->accep_demande,
            'huissier_designie' => $req->huissier_designie,
            'reslt_notif' => $req->reslt_notif,

            'vald_pieces' => $req->vald_pieces,
            'provi' => $req->provi,
            'date_debut_plant' => $req->date_debut_plant,
            'date_fin_plant' => $req->date_fin_plant,
        ]);

        $this->historiqueSaveData('create', 'SaisieImobiler', $res->id , 'create');
        $for_id = HistoriqueScenario::where('id', $req->id_hist)->first();
        if(Auth::user()->droit_de_valid_acts == 1){
            Dossier::where('id', $for_id->id_doss)->update(['is_jurid' => 1]);
        }
        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_imob' => $res->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            foreach ($req->date_result as $index => $item) {
                $res = SaisieImmobilerDeroulAudience::create([
                    'id_imob' => $res->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }

        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_immoblier_list', [$req->id_hist])
        ];

        return json_encode($res);
    }
    public function show_saisie_immobilier($id)
    {
        $imobilier = SaisieImobiler::where('id', $id)->first();
        $dossier = HistoriqueScenario::select('dossiers.*')
            ->join('dossiers', 'dossiers.id', 'historique_scenarios.id_doss')
            ->where('historique_scenarios.id', $imobilier->id_hist_scen)
            ->first();
        $imobilier_infos = SaisieImmobilerDeroulAudience::where('id_imob', $id)->get();
        $files = Files::where('id_imob', $id)->get();
        $File_path = File_path::where('type', 'joints')->first();
        $partner = partner::where('type_partn', 'Avocat')->get();
        return view('admin.scens.saisie_immobilier')
            ->with('id_hist', $imobilier->id_hist_scen)
            ->with('imobilier', $imobilier)
            ->with('dossier', $dossier)
            ->with('imobilier_infos', $imobilier_infos)
            ->with('files', $files)
            ->with('partner', $partner)
            ->with('File_path', $File_path);
    }
    public function update_saisie_immobilier(Request $req)
    {
        $res = SaisieImobiler::where('id', $req->id)
            ->update([
                'id_hist_scen' => $req->id_hist,
                'tribunal_competant' => $req->tribunal_competant,
                'non_juge' => $req->non_juge,
                'ref' => $req->ref,
                'benificiaire' => $req->benificiaire,
                'avoca_benif' => $req->avoca_benif,
                'charger_suivi_binif' => $req->charger_suivi_binif,
                'le_tireur' => $req->le_tireur,
                'address_tireur' => $req->address_tireur,
                'cin_treur' => $req->cin_treur,
                'avocat_tireur' => $req->avocat_tireur,
                'montant_creance' => $req->montant_creance,
                'date_depot' => $req->date_depot,
                'accep_demande' => $req->accep_demande,
                'huissier_designie' => $req->huissier_designie,
                'reslt_notif' => $req->reslt_notif,

                'vald_pieces' => $req->vald_pieces,
                'provi' => $req->provi,
                'date_debut_plant' => $req->date_debut_plant,
                'date_fin_plant' => $req->date_fin_plant,
            ]);

        $this->historiqueSaveData('create', 'SaisieImobiler', $req->id , 'update', ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'));

        if (isset($req->file_exc)) {
            Files::where('id_imob', $req->id)
                ->whereNotIn('file_name', $req->file_exc)
                ->delete();
        } else {
            Files::where('id_imob', $req->id)->delete();
        }

        if (isset($req->file_val)) {
            foreach ($req->file_val as $file) {
                $filenameorg = $file->getClientOriginalName();
                $filename = uniqid() . $filenameorg;
                $file->move(public_path('files/joints/'), $filename);
                $data2 = Files::create([
                    'id_imob' => $req->id,
                    'file_name' => $filename,
                    'org_file_name' => $filenameorg
                ]);
            }
        }

        if (isset($req->date_result)) {
            SaisieImmobilerDeroulAudience::where('id_imob', $req->id)->delete();
            foreach ($req->date_result as $index => $item) {
                $res = SaisieImmobilerDeroulAudience::create([
                    'id_imob' => $req->id,
                    'date_result' => $item,
                    'result' => $req->result[$index]
                ]);
            }
        }

        $res = [
            'statue' => 'success',
            'message' => 'les données sont enregistrées avec succès',
            'link' => route('saisie_immoblier_list', [$req->id_hist])
        ];
        return json_encode($res);
    }
    public function delete_saisie_immobilier($id)
    {
        if (Auth::user()->droit_de_valid_acts == 0) {
            $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', 'SaisieImobiler')->where('id_action', $id)->first();
            if (empty($data)) {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => 'delete',
                    'where' => 'SaisieImobiler',
                    'id_action' => $id,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 0,
                ]);
            } else {
                $data->update(['id_valid' => 0]);
            }
        }else{
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => 'delete',
                'where' => 'SaisieImobiler',
                'id_action' => $id,
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => 1,
            ]);

            SaisieImobiler::where('id', $id)->delete();
            SaisieImmobilerDeroulAudience::where('id_imob', $id)->delete();
        }
        
        return back();
    }
}
