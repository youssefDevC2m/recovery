<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\taskManagment;
use App\Dossier;
use App\User;
use App\Files;
use App\File_path;
use Auth;
class TaskController extends Controller
{
    public function GestionTachesList()
    {
        $task=taskManagment::select('task_managments.*','dossiers.ref','users.nom','users.prenom')
        ->leftjoin('dossiers','dossiers.id','task_managments.id_doss')
        ->leftjoin('users','users.id','task_managments.id_agent')
        ->where('users.role','Agent')
        ->get();
        
        return view('admin.taskManager.list')
        ->with('task',$task);
    }

    public function GestionTachesForm()
    {
        $Dossier=Dossier::get();
        $agent=User::where('role','Agent')->get();
        // dd($agent);
        return view('admin.taskManager.taskForm')
        ->with('agent',$agent)
        ->with('Dossier',$Dossier);
    }

    
    public function GestionTachesAdd(Request $req)
    {
        $task=taskManagment::create([
            'id_doss'=>$req->doss,
            'id_agent'=>$req->agent,
            'commande'=>$req->cmnd,
            'date_db'=>$req->date_dbTsk,
            'date_fin'=>$req->date_FinTsk,
            'creator'=>Auth::user()->id,
            'remarque'=>$req->remarque
        ]);
        if(Auth::user()->valid_task == 1 ){
            $task->valid_task = 1;
            $task->save();
           
        }
        if(isset($req->file_val)){
            foreach($req->file_val as $file){
                $filenameorg = $file->getClientOriginalName();
                $filename =uniqid().$filenameorg;
                $file->move(public_path('files/joints/'),$filename);
                $data2= Files::create([
                    'id_task'=>$task->id,
                    'file_name'=>$filename ,
                    'org_file_name'=>$filenameorg
                ]);
            }
        }
        return json_encode($task);
    }

    public function GestionTachesDelete($id)
    {
        taskManagment::where('id',$id)->delete();
        return back();
    }

    public function GestionTachesShow($id)
    {
        $task=taskManagment::where('task_managments.id',$id)->first();
        $Dossier=Dossier::get();
        $agent=User::where('role','Agent')->get();
        $files=Files::where('id_task',$id)->get();
        $File_path=File_path::where('type','joints')->first();
        return view('admin.taskManager.showTask')
        ->with('Dossier',$Dossier)
        ->with('agent',$agent)
        ->with('File_path',$File_path)
        ->with('files',$files)
        ->with('task',$task);
    }

    public function GestionTachesEdit(Request $req)
    {
        $task=taskManagment::where('id',$req->id_task)->update([
            'id_doss'=>$req->doss,
            'id_agent'=>$req->agent,
            'commande'=>$req->cmnd,
            'date_db'=>$req->date_dbTsk,
            'date_fin'=>$req->date_FinTsk,
            'valid_task'=>(isset($req->valid_task))? 1 : 0,
            'remarque'=>$req->remarque
        ]);

        if(isset($req->file_exc)){
            Files::where('id_task',$req->id_task)
            ->whereNotIn('file_name',$req->file_exc)
            ->delete();
        }else{
            Files::where('id_task',$req->id_task)
            ->delete();
        }
        
        
        if(isset($req->file_val)){
            foreach($req->file_val as $file){
                $filenameorg = $file->getClientOriginalName();
                $filename =uniqid().$filenameorg;
                $file->move(public_path('files/joints/'),$filename);
                $data2= Files::create([
                    'id_task'=>$req->id_task,
                    'file_name'=>$filename,
                    'org_file_name'=>$filenameorg 
                ]);
            }
        }

        return json_encode('Done! '.$task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function task_exp_vu(Request $req)
    {
        taskManagment::where('id',$req->id_tsk)->update([
            'admin_exp_vu' => 1
        ]);
        return json_encode('did it');
    }
}
