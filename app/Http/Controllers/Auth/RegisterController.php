<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Client;
use App\tempUser;
use App\File_path;
use App\Files;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    // use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function add_user(Request $req)
    {
        $filename=null;
        if($req->hasFile('pdp')){
            $file=$req->pdp;
            $filename = $file->getClientOriginalName();
            $filenameorg = $file->getClientOriginalName();
            $filename =uniqid().$filename;
            $file->move(public_path('files/users/'),$filename);
        }
       

        if($req->role!='Client'){
            $data = User::create([
                'nom' => $req->nom,
                'prenom'=> $req->prenom,
                'email' => $req->mail,
                'tele'=>$req->tele,
                'address'=>$req->adrs,
                'password' => Hash::make($req->pwd),
                'role'=>$req->role,
                'photo'=>$filename,
                'temp_pwd'=>$req->pwd,

                'acs_tabord'=>((isset($req->acs_tabord))? $req->acs_tabord : 0),

                'acs_user'=>((isset($req->acs_user))? $req->acs_user : 0),
                'give_priv_user'=>((isset($req->give_priv_user))? $req->give_priv_user : 0),
                'droit_de_valid_acts'=>((isset($req->droit_de_valid_acts))? $req->droit_de_valid_acts : 0),

                'acs_cli'=>((isset($req->acs_cli))? $req->acs_cli : 0),
                'add_cli'=>((isset($req->add_cli))? $req->add_cli : 0),
                'edit_cli'=>((isset($req->edit_cli))? $req->edit_cli : 0),
                'delet_cli'=>((isset($req->delet_cli))? $req->delet_cli : 0),
                'list_cli'=>((isset($req->list_cli))? $req->list_cli : 0 ),
                'read_only_cli'=>((isset($req->read_only_cli))? $req->read_only_cli : 0),
                'see_doss_cli'=>((isset($req->see_doss_cli))? $req->see_doss_cli : 0),

                'acs_doss'=>((isset($req->acs_doss))? $req->acs_doss : 0),
                'see_all_doss'=>((isset($req->see_all_doss))? $req->see_all_doss : 0),
                'acs_scens_doss'=>((isset($req->acs_scens_doss))? $req->acs_scens_doss : 0),
                'add_doss'=>((isset($req->add_doss))? $req->add_doss : 0),
                'edit_doss'=>((isset($req->edit_doss))? $req->edit_doss : 0),
                'delet_doss'=>((isset($req->delet_doss))? $req->delet_doss : 0),
                'list_doss'=>((isset($req->list_doss))? $req->list_doss : 0),
                'read_only_doss'=>((isset($req->read_only_doss))? $req->read_only_doss : 0),
                'see_info_cli_doss'=>((isset($req->see_info_cli_doss))? $req->see_info_cli_doss : 0),
                'edit_info_dbitr_doss'=>((isset($req->edit_info_dbitr_doss))? $req->edit_info_dbitr_doss : 0),
                'see_info_dbitr_doss'=>((isset($req->see_info_dbitr_doss))? $req->see_info_dbitr_doss : 0),
                'see_fermetur_doss'=>((isset($req->see_fermetur_doss))? $req->see_fermetur_doss : 0),
                'edit_fermetur_doss'=>((isset($req->edit_fermetur_doss))? $req->edit_fermetur_doss : 0),
                'acs_phas_ami'=>((isset($req->acs_phas_ami))? $req->acs_phas_ami : 0),
                'acs_phas_judis'=>((isset($req->acs_phas_judis))? $req->acs_phas_judis : 0),

                'acs_devis'=>((isset($req->acs_devis))? $req->acs_devis : 0),
                'add_devis'=>((isset($req->add_devis))? $req->add_devis : 0),
                'edit_devis'=>((isset($req->edit_devis))? $req->edit_devis : 0),
                'delet_devis'=>((isset($req->delet_devis))? $req->delet_devis : 0),
                'list_devis'=>((isset($req->list_devis))? $req->list_devis : 0),
                'print_devis'=>((isset($req->print_devis))? $req->print_devis : 0),
                'see_devis'=>((isset($req->see_devis))? $req->see_devis : 0),

                'acs_fact'=>((isset($req->acs_fact))? $req->acs_fact : 0),
                'add_fact'=>((isset($req->add_fact))? $req->add_fact : 0),
                'edit_fact'=>((isset($req->edit_fact))? $req->edit_fact : 0),
                'delet_fact'=>((isset($req->delet_fact))? $req->delet_fact : 0),
                'list_fact'=>((isset($req->list_fact))? $req->list_fact : 0),
                'see_fact'=>((isset($req->see_fact))? $req->see_fact : 0),

                'acs_rglmnt'=>((isset($req->acs_rglmnt))? $req->acs_rglmnt : 0),
                'add_rglmnt'=>((isset($req->add_rglmnt))? $req->add_rglmnt : 0),
                'edit_rglmnt'=>((isset($req->edit_rglmnt))? $req->edit_rglmnt : 0),
                'delet_rglmnt'=>((isset($req->delet_rglmnt))? $req->delet_rglmnt : 0),
                'list_rglmnt'=>((isset($req->list_rglmnt))? $req->list_rglmnt : 0),
                'see_rglmnt'=>((isset($req->see_rglmnt))? $req->see_rglmnt : 0),

                'acs_charge'=>((isset($req->acs_charge))? $req->acs_charge : 0),
                'add_charge'=>((isset($req->add_charge))? $req->add_charge : 0),
                'edit_charge'=>((isset($req->edit_charge))? $req->edit_charge : 0),
                'delet_charge'=>((isset($req->delet_charge))? $req->delet_charge : 0),
                'list_charge'=>((isset($req->list_charge))? $req->list_charge : 0),
                'see_charge'=>((isset($req->see_charge))? $req->see_charge : 0),

                'acs_parten'=>((isset($req->acs_parten))? $req->acs_parten : 0),
                'add_parten'=>((isset($req->add_parten))? $req->add_parten : 0),
                'edit_parten'=>((isset($req->edit_parten))? $req->edit_parten : 0),
                'delet_parten'=>((isset($req->delet_parten))? $req->delet_parten : 0),
                'list_parten'=>((isset($req->list_parten))? $req->list_parten : 0),
                'see_infos_parten'=>((isset($req->see_infos_parten))? $req->see_infos_parten : 0),
                'see_parten'=>((isset($req->see_parten))? $req->see_parten : 0),

                'acs_task'=>((isset($req->acs_task))? $req->acs_task : 0),
                'add_task'=>((isset($req->add_task))? $req->add_task : 0),
                'valid_task'=>((isset($req->valid_task))? $req->valid_task : 0),
                'edit_task'=>((isset($req->edit_task))? $req->edit_task : 0),
                'delet_task'=>((isset($req->delet_task))? $req->delet_task : 0),
                'list_task'=>((isset($req->list_task))? $req->list_task : 0),
                'suivi_task'=>((isset($req->suivi_task))? $req->suivi_task : 0),
                'see_dt_task'=>((isset($req->see_dt_task))? $req->see_dt_task : 0),

            ]);
        }else{
            if($req->id_client!=0){
                $data = User::create([
                    'nom' => $req->nom,
                    'prenom'=> $req->prenom,
                    'email' => $req->mail,
                    'tele'=>$req->tele,
                    'address'=>$req->adrs,
                    'password' => Hash::make($req->pwd),
                    'role'=>$req->role,
                    'photo'=>$filename,
                    'id_client'=>$req->id_client,
                    'temp_pwd'=>$req->pwd,
                ]);
            }else{
                $data = tempUser::create([
                    'nom' => $req->nom,
                    'prenom'=> $req->prenom,
                    'email' => $req->mail,
                    'tele'=>$req->tele,
                    'address'=>$req->adrs,
                    'password' => Hash::make($req->pwd),
                    'role'=>'Client',
                    'photo'=>$filename,
                    'temp_pwd'=>$req->pwd
                ]);
            }
            
        }
        
        return json_encode($data);
    }
    public function edit_user(Request $req){
        
        $filename=null;
        if($req->hasFile('pdp')){
            $file=$req->pdp;
            $filename = $file->getClientOriginalName();
            $filenameorg = $file->getClientOriginalName();
            $filename =uniqid().$filename;
            $file->move(public_path('files/users/'),$filename);
        }
        if(!isset($req->pwd)){
            $res = User::where('id',$req->id_user)->update([
                'nom' => $req->nom,
                'prenom'=> $req->prenom,
                'email' => $req->mail,
                'tele'=>$req->tele,
                'address'=>$req->adrs,
                'photo'=>$filename,
                'role'=>$req->role
            ]);
        }else{
            $res = User::where('id',$req->id_user)->update([
                'nom' => $req->nom,
                'prenom'=> $req->prenom,
                'email' => $req->mail,
                'tele'=>$req->tele,
                'address'=>$req->adrs,
                'password' => Hash::make($req->pwd),
                'role'=>$req->role,
                'photo'=>$filename,
                'temp_pwd'=>$req->pwd  
            ]);
        }

        if(isset($req->acs_cli)){
            User::where('id',$req->id_user)->update([
                'acs_tabord'=>((isset($req->acs_tabord))? $req->acs_tabord : 0),
                
                'acs_user'=>((isset($req->acs_user))? $req->acs_user : ((Auth::user()->id == $req->id_user)? Auth::user()->acs_user : '0' ) ),
                'give_priv_user'=>((isset($req->give_priv_user))? $req->give_priv_user : ((Auth::user()->id == $req->id_user)? Auth::user()->give_priv_user : '0' ) ),
                'droit_de_valid_acts'=>((isset($req->droit_de_valid_acts))? $req->droit_de_valid_acts : ((Auth::user()->id == $req->id_user)? Auth::user()->droit_de_valid_acts : '0' ) ),

                'acs_cli'=>((isset($req->acs_cli))? $req->acs_cli : 0),
                'add_cli'=>((isset($req->add_cli))? $req->add_cli : 0),
                'edit_cli'=>((isset($req->edit_cli))? $req->edit_cli : 0),
                'delet_cli'=>((isset($req->delet_cli))? $req->delet_cli : 0),
                'list_cli'=>((isset($req->list_cli))? $req->list_cli : 0 ),
                'read_only_cli'=>((isset($req->read_only_cli))? $req->read_only_cli : 0),
                'see_doss_cli'=>((isset($req->see_doss_cli))? $req->see_doss_cli : 0),

                'acs_doss'=>((isset($req->acs_doss))? $req->acs_doss : 0),
                'see_all_doss'=>((isset($req->see_all_doss))? $req->see_all_doss : 0),
                'acs_scens_doss'=>((isset($req->acs_scens_doss))? $req->acs_scens_doss : 0),
                'add_doss'=>((isset($req->add_doss))? $req->add_doss : 0),
                'edit_doss'=>((isset($req->edit_doss))? $req->edit_doss : 0),
                'delet_doss'=>((isset($req->delet_doss))? $req->delet_doss : 0),
                'list_doss'=>((isset($req->list_doss))? $req->list_doss : 0),
                'read_only_doss'=>((isset($req->read_only_doss))? $req->read_only_doss : 0),
                'see_info_cli_doss'=>((isset($req->see_info_cli_doss))? $req->see_info_cli_doss : 0),
                'edit_info_dbitr_doss'=>((isset($req->edit_info_dbitr_doss))? $req->edit_info_dbitr_doss : 0),
                'see_info_dbitr_doss'=>((isset($req->see_info_dbitr_doss))? $req->see_info_dbitr_doss : 0),
                'see_fermetur_doss'=>((isset($req->see_fermetur_doss))? $req->see_fermetur_doss : 0),
                'edit_fermetur_doss'=>((isset($req->edit_fermetur_doss))? $req->edit_fermetur_doss : 0),
                'acs_phas_ami'=>((isset($req->acs_phas_ami))? $req->acs_phas_ami : 0),
                'acs_phas_judis'=>((isset($req->acs_phas_judis))? $req->acs_phas_judis : 0),

                'acs_devis'=>((isset($req->acs_devis))? $req->acs_devis : 0),
                'add_devis'=>((isset($req->add_devis))? $req->add_devis : 0),
                'edit_devis'=>((isset($req->edit_devis))? $req->edit_devis : 0),
                'delet_devis'=>((isset($req->delet_devis))? $req->delet_devis : 0),
                'list_devis'=>((isset($req->list_devis))? $req->list_devis : 0),
                'print_devis'=>((isset($req->print_devis))? $req->print_devis : 0),
                'see_devis'=>((isset($req->see_devis))? $req->see_devis : 0),

                'acs_fact'=>((isset($req->acs_fact))? $req->acs_fact : 0),
                'add_fact'=>((isset($req->add_fact))? $req->add_fact : 0),
                'edit_fact'=>((isset($req->edit_fact))? $req->edit_fact : 0),
                'delet_fact'=>((isset($req->delet_fact))? $req->delet_fact : 0),
                'list_fact'=>((isset($req->list_fact))? $req->list_fact : 0),
                'see_fact'=>((isset($req->see_fact))? $req->see_fact : 0),

                'acs_rglmnt'=>((isset($req->acs_rglmnt))? $req->acs_rglmnt : 0),
                'add_rglmnt'=>((isset($req->add_rglmnt))? $req->add_rglmnt : 0),
                'edit_rglmnt'=>((isset($req->edit_rglmnt))? $req->edit_rglmnt : 0),
                'delet_rglmnt'=>((isset($req->delet_rglmnt))? $req->delet_rglmnt : 0),
                'list_rglmnt'=>((isset($req->list_rglmnt))? $req->list_rglmnt : 0),
                'see_rglmnt'=>((isset($req->see_rglmnt))? $req->see_rglmnt : 0),

                'acs_charge'=>((isset($req->acs_charge))? $req->acs_charge : 0),
                'add_charge'=>((isset($req->add_charge))? $req->add_charge : 0),
                'edit_charge'=>((isset($req->edit_charge))? $req->edit_charge : 0),
                'delet_charge'=>((isset($req->delet_charge))? $req->delet_charge : 0),
                'list_charge'=>((isset($req->list_charge))? $req->list_charge : 0),
                'see_charge'=>((isset($req->see_charge))? $req->see_charge : 0),

                'acs_parten'=>((isset($req->acs_parten))? $req->acs_parten : 0),
                'add_parten'=>((isset($req->add_parten))? $req->add_parten : 0),
                'edit_parten'=>((isset($req->edit_parten))? $req->edit_parten : 0),
                'delet_parten'=>((isset($req->delet_parten))? $req->delet_parten : 0),
                'list_parten'=>((isset($req->list_parten))? $req->list_parten : 0),
                'see_infos_parten'=>((isset($req->see_infos_parten))? $req->see_infos_parten : 0),
                'see_parten'=>((isset($req->see_parten))? $req->see_parten : 0),

                'acs_task'=>((isset($req->acs_task))? $req->acs_task : 0),
                'add_task'=>((isset($req->add_task))? $req->add_task : 0),
                'valid_task'=>((isset($req->valid_task))? $req->valid_task : 0),
                'edit_task'=>((isset($req->edit_task))? $req->edit_task : 0),
                'delet_task'=>((isset($req->delet_task))? $req->delet_task : 0),
                'list_task'=>((isset($req->list_task))? $req->list_task : 0),
                'suivi_task'=>((isset($req->suivi_task))? $req->suivi_task : 0),
                'see_dt_task'=>((isset($req->see_dt_task))? $req->see_dt_task : 0),
            ]);
        }

        return json_encode($res);
    }
    public function delete_user($id){
        User::where('id',$id)->delete();
        return back();
    }
    public function register_index(){
        $client=Client::get();
        $imageDef = File_path::where('type','users')->first();
        $defaultImg =$imageDef->file_path.'link.png';
        
        return view('admin.users.register')
        ->with('defaultImg',$defaultImg)
        ->with('client',$client);
    }
    public function users_list(){
        $users=User::select('users.*')->get();
        return view('admin.users.list')->with('users',$users);
    }
    public function edit_userindex($id){
        $user=User::select('users.*')->where('users.id',$id)->first();
        
        return view('admin.users.edit')->with('user',$user);
    }
}
