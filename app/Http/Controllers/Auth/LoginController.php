<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\MessageBag;

use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // public $redirectTo = RouteServiceProvider::HOME;
    // var_dump();die();
    public function index(){
        return  redirect(route('login'));
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // dd();
        $this->middleware('guest')->except('logout');
         
    }
    public function login(Request $request)
    {   
        $input = $request->all();
   
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password'])))
        {
            if (auth()->user()->role == 'Administrateur') {
                return redirect(RouteServiceProvider::HOME);
            }else{
                
                return redirect(RouteServiceProvider::HOMEagent);
            }
        }else{
            
            $errors = new MessageBag(['password' => ['Email and/or password invalid.']]);
            // if unsuccessful, then redirect back to login with the form date
           return redirect()->back()->withErrors($errors)->withInput($request->only('email', 'remember'));
                
        }
          
    }
}
