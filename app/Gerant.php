<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gerant extends Model
{
    protected $fillable=[
        'id_dbtr',
        'nom',
        'prenom',
        'alias',
        'cin',
        'adress_pincipale',
        'ville',
        'tele',
    ];
}
