<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dossier extends Model
{
    //
    protected $fillable=[
        'id_client',
        'id_dbtr',
        'creator_id',
        'represante_legal',
        'tele_represante_legal',
        'contact_represante_legal',
        'date_resep',
        'ref',
        'phase',
        'mt_glob_creance',
        'observation',
        'date_ouverture',
        'date_db_doss',#added from scenario part
        'date_fin_doss',#added from scenario part
        'is_jurid',
        'doss_exp_vu'
    ];

    public function history(){
        return $this->hasMany(HistoriqueScenario::class,'id_doss');
    }
}
