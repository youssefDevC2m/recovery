<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaisieMobilerDeroulAudience extends Model
{
    protected $fillable = [
        'id_smod',
        'date_result',
        'result'
    ];
}
