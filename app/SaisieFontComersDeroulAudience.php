<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaisieFontComersDeroulAudience extends Model
{
    protected $fillable = [
        'id_fc',
        'date_result',
        'result'
    ];
}
