<?php

namespace App\myLib;

use App\HistoriqueAct;
use Auth;
use Illuminate\Support\ServiceProvider;

class myFun extends ServiceProvider
{
    public $id_row;
    
    public static function historiqueSaveData($what, $where, $id_action, $event, $is_valid = null)
    {
        if ($event == 'create') {
            HistoriqueAct::create([
                'id_user' => Auth::user()->id,
                'what' => $what,
                'where' => $where, //changabel
                'id_action' => $id_action, //changabel
                'username' => Auth::user()->nom,
                'userlastname' => Auth::user()->prenom,
                'id_valid' => ((Auth::user()->droit_de_valid_acts == 1) ? 1 : '0'),
            ]);
        }
        if ($event == 'update') {
            $HistoriqueAct = HistoriqueAct::where('id_action', $id_action)
                ->where('id_valid', 1)
                ->where('where', $where)
                ->where('what', $what)
                ->update(['id_valid' => $is_valid]);
            return [$id_action];
        }
        if ($event == 'delete') {
            if (Auth::user()->droit_de_valid_acts == 0) {
                $data = HistoriqueAct::where('what', 'delete')->where('id_user', Auth::user()->id)->where('where', $where)->where('id_action', $id_action)->first();
                if (empty($data)) {
                    HistoriqueAct::create([
                        'id_user' => Auth::user()->id,
                        'what' => $what,
                        'where' => $where,
                        'id_action' => $id_action,
                        'username' => Auth::user()->nom,
                        'userlastname' => Auth::user()->prenom,
                        'id_valid' => 0,
                    ]);
                } else {
                    $data->update(['id_valid' => 0]);
                }
            } else {
                HistoriqueAct::create([
                    'id_user' => Auth::user()->id,
                    'what' => $what,
                    'where' => $where,
                    'id_action' => $id_action,
                    'username' => Auth::user()->nom,
                    'userlastname' => Auth::user()->prenom,
                    'id_valid' => 1,
                ]);
                return 'do_it' ;
            }
        }
    }
    public static function AlertColor($date,$id_row = null){
        if(strtotime($date) <= strtotime(date('Y-m-d'))){
            $style = 'background:#f67c7c';
            $id_row = '.'.$id_row;
        }else{
            $style = '';
        }
        return ['style'=>$style,'id_row'=>$id_row];
    }
}
