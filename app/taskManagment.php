<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class taskManagment extends Model
{
    protected $fillable=[
        'id_agent',
        'id_doss',
        'commande',
        'date_db',
        'date_fin',
        'valid_task',
        'task_ag_vu',
        'creator',
        'admin_exp_vu',
        'remarque'
    ];
}
