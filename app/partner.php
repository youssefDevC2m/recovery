<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class partner extends Model
{
    protected $fillable=[
        'type_partn',
        'agent_id',
        'nom_part',
        'prenom_part',
        'adrs_part',
        'adrs_act_part',
        'tele_part',
        'tele_wts_part',
        'email_part'
    ];
}
