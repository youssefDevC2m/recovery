<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procedure_range extends Model
{
    protected $fillable = [
        'id_histo',
        'date_1',
        'date_2',
        'type',
        'id_doss'
    ];
}
