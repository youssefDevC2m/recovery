<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaisieArretBancaireDeroulAudience extends Model
{
    protected $fillable = [
        'id_arrb',
        'date_result',
        'result'
    ];
}
