<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class scen_courrier extends Model
{
    //
    protected $fillable=[
        'id_hist_scen',
        'date_env',
        'mise_dem_par',
        'comment',
        'id_creator'
    ];
}
