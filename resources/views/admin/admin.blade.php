<!DOCTYPE html>

<html lang="en" class="light">



<head>
    <meta charset="utf-8">
    <link href="{{ asset('dist/images/logo.svg') }}" rel="shortcut icon">

    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    @yield('head')
    <title>@yield('title')</title>
    <!-- BEGIN: CSS Assets-->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}" />
    <script src="https://code.iconify.design/2/2.0.3/iconify.min.js"></script>
    <!-- END: CSS Assets-->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
        crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastify-js/1.11.1/toastify.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css">
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
    <style>
        ::-webkit-scrollbar {
            width: 16px;
            height: 16px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            border-radius: 100vh;
            /* background: #8ab3dc; */
            /* background: rgb(53, 53, 53); */
            background: rgb(221, 221, 221);
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #a7a7a7;
            border-radius: 100vh;
            border: 1px solid #edf2f7;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #919191;

        }

        .current {
            background: rgb(26, 56, 159);
            border-radius: 9px;
            padding: 12px;
            display: inline-block;
            width: 46px;
            border: none;
            font-weight: 600;
            color: white !important;
        }

        a.paginate_button.current {
            color: #ffffff !important;
        }

        .modal {
            height: 100% !important;
        }

        .modal.show>.modal__content {

            position: absolute;
            left: 50%;
            top: 42%;
            transform: translate(-50%, -50%);
            vertical-align: middle;
        }

        .load {
            top: 0%;
            background: rgba(255, 255, 255, 0.3);
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 100;
            /* opacity: 0.3; */
            display: flex;
            align-items: center;
            justify-content: center;
        }
    </style>
    @yield('style')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <script src="{{asset('js/app.js')}}" defer></script> --}}
</head>
<!-- END: Head -->

<body class=" bg-blue-700 intro-y ">

    <!-- BEGIN: Mobile Menu -->
    @include('admin.layout.mobilenav')


    <!-- END: Mobile Menu -->
    <div class="flex">
        <!-- BEGIN: Side Menu -->
        <div class="show_load hidden">
            <div class=" absolute  load">
                <svg width="20" viewBox="0 0 135 140" xmlns="http://www.w3.org/2000/svg" fill="rgb(26, 57, 156)"
                    class="w-8 h-8">
                    <rect y="10" width="15" height="120" rx="6">
                        <animate attributeName="height" begin="0.5s" dur="1s"
                            values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite">
                        </animate>
                        <animate attributeName="y" begin="0.5s" dur="1s"
                            values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear" repeatCount="indefinite">
                        </animate>
                    </rect>
                    <rect x="30" y="10" width="15" height="120" rx="6">
                        <animate attributeName="height" begin="0.25s" dur="1s"
                            values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite">
                        </animate>
                        <animate attributeName="y" begin="0.25s" dur="1s"
                            values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear" repeatCount="indefinite">
                        </animate>
                    </rect>
                    <rect x="60" width="15" height="140" rx="6">
                        <animate attributeName="height" begin="0s" dur="1s"
                            values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite">
                        </animate>
                        <animate attributeName="y" begin="0s" dur="1s"
                            values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear" repeatCount="indefinite">
                        </animate>
                    </rect>
                    <rect x="90" y="10" width="15" height="120" rx="6">
                        <animate attributeName="height" begin="0.25s" dur="1s"
                            values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear" repeatCount="indefinite">
                        </animate>
                        <animate attributeName="y" begin="0.25s" dur="1s"
                            values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear" repeatCount="indefinite">
                        </animate>
                    </rect>
                    <rect x="120" y="10" width="15" height="120" rx="6">
                        <animate attributeName="height" begin="0.5s" dur="1s"
                            values="120;110;100;90;80;70;60;50;40;140;120" calcMode="linear"
                            repeatCount="indefinite"></animate>
                        <animate attributeName="y" begin="0.5s" dur="1s"
                            values="10;15;20;25;30;35;40;45;50;0;10" calcMode="linear" repeatCount="indefinite">
                        </animate>
                    </rect>
                </svg>
            </div>
        </div>

        @include('admin.layout.side')
        <!-- END: Side Menu -->
        <!-- BEGIN: Content -->
        <div class="content">
            @include('admin.layout.nav')
            @yield('content')

        </div>
        <!-- END: Content -->
    </div>

    <!-- BEGIN: JS Assets-->
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    </script>
    @yield('script')

    <!-- END: JS Assets-->
    <script src="{{ asset('dist/js/app.js') }}"></script>
    {{-- <script src="{{asset('dist/js/dataTbl.js')}}"></script> --}}

    <!-- get all parameters -->
    <script>
        $(document).keydown(function(event) {
            if (event.keyCode == 123) { // Prevent F12
                return false;
            } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
                return false;
            } else if (event.ctrlKey && event.keyCode == 85) { // Prevent Ctrl+u        
                return false;
            }
        });
        // $(document).on("contextmenu", function (e) {        
        //     e.preventDefault();
        // });
    </script>

</body>


</html>
