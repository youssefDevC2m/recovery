@extends('admin.admin')
@section('title')
    Tous les activités récentes
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex">
        <a href="" class="">recoveryApp</a>
        <i data-feather="chevron-right" class="breadcrumb__icon"></i>
        <a href="#/" class="breadcrumb--active">Activités récentes</a>
    </div>
@endsection
@section('content')
    <?php $date_hist = ''; ?>
    <div class="box mt-2 p-3">
        <div class="bg-gray-100  p-3 h-screen overflow-y-scroll w-full rounded-md">
            @if (count($HistoAct) > 0)
                <div class="report-timeline relative">
                    @foreach ($HistoAct as $index => $item)
                        @if ($date_hist != date('d/m/Y', strtotime($item->created_at)) && $index > 0)
                            <div class="intro-x text-gray-500 text-xs text-center my-4"> {{ date('d/m/Y', strtotime($item->created_at)) }}</div>
                        @endif
                        <?php $date_hist = date('d/m/Y', strtotime($item->created_at)); ?>

                        <div class="dropdown" data-placement="bottom-start">
                            <div class="dropdown-toggle intro-y relative flex items-center mb-3">
                                <div class="report-timeline__image">
                                    <div class="w-10 h-10 flex-none image-fit rounded-full overflow-hidden">
                                        <img alt="Midone Tailwind HTML Admin Template" src="{{ asset('dist/images/profile-1.jpg') }}">
                                    </div>
                                </div>
                                <div class="box px-5 py-3 ml-4 flex-1 ">
                                    <div class="flex items-center">
                                        <div class="font-medium">{{ $item->username . ' ' . $item->userlastname }}</div>
                                        <div class="text-xs text-gray-500 ml-auto">
                                            {{ $index ==0 ? date('d/m/Y H:m', strtotime($item->created_at)) : date('H:m', strtotime($item->created_at)) }}
                                        </div>
                                    </div>
                                    @include('admin.activites_history.history_titels')
                                </div>
                            </div>
                            <div class="dropdown-box w-48">
                                <div class="dropdown-box__content box dark:bg-dark-1 p-2">
                                    <a href=
                                        "
                                        {{ $item->where =='client' ? Route('showClient', [$item->id_action]) : '' }}
                                        {{ $item->where =='dossier' ? Route('showDossier', [$item->id_action]) : '' }}
                                        {{ $item->where =='NewScen' ? Route('SceneList', [$item->id_action]) : '' }}
                                        {{ $item->where =='scenChequ' ? Route('show_Cheque',[$item->id_action]) : ''}}
                                        {{ $item->where =='scenInjonctionPay' ? Route('show_injoction',[$item->id_action]) : ''}}
                                        {{ $item->where =='scenReqInjonctionPay' ? Route('show_ReqInjoction',[$item->id_action]) : ''}}
                                        {{ $item->where =='citation_direct' ? Route('show_citation',[$item->id_action]) : ''}}
                                        {{ $item->where =='FondComers' ? Route('show_fond_commerce_form',[$item->id_action]) : ''}}
                                        {{ $item->where =='SaisieTiersDetenteur' ? Route('show_saisie_tiers_detonado',[$item->id_action]) : ''}}
                                        {{ $item->where =='SaisieArretBancaire' ? Route('show_saisie_arret_bancaire',[$item->id_action]) : ''}}
                                        {{ $item->where =='SaisieActionsPartsSoc' ? Route('show_saisie_actions_parts',[$item->id_action]) : ''}}
                                        {{ $item->where =='show_saisie_immobilier' ? Route('show_saisie_mobilier',[$item->id_action]) : ''}}
                                        {{ $item->where =='Devis' ? Route('showDevis',[$item->id_action]) : ''}}
                                        {{ $item->where =='Facture' ? Route('showFacture',[$item->id_action]) : ''}}
                                        {{ $item->where =='Reglement' ? Route('showRglement',[$item->id_action]) : ''}}
                                        {{ $item->where =='Charges' ? Route('showCharge',[$item->id_action]) : ''}}
                                        {{ $item->where =='partner' ? Route('showPart',[$item->id_action]) : ''}}
                                        "
                                        class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                                        <i data-feather="eye" class="w-4 h-4 mr-2"></i> Consulter
                                    </a>
                                    <a href="javascript:;" data-toggle="modal"
                                        data-target="#approuv-modal-preview{{ $item->id }}"
                                        class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-theme-9 hover:text-white dark:hover:bg-dark-2 rounded-md">
                                        <i data-feather="thumbs-up" class="w-4 h-4 mr-2"></i> Approuver
                                    </a>
                                    <a href="javascript:;" data-toggle="modal"
                                        data-target="#reject-modal-preview{{ $item->id }}"
                                        class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-theme-6 hover:text-white dark:hover:bg-dark-2 rounded-md">
                                        <i data-feather="thumbs-down" class="w-4 h-4 mr-2"></i> Rejeter
                                    </a>
                                </div>
                            </div>
                        </div>
                        @include('admin.activites_history.approuv_modl')
                        @include('admin.activites_history.reject_modl')
                    @endforeach
                </div>
            @else
                <div class="intro-y box px-5 py-3 ml-4 flex-1 zoom-in text-center">
                    <span class="text-gray-400 text-xl font-semibold ">Aucune activité</span>
                </div>
            @endif
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("body").on('click', '.aprouv', function() {
                $.ajax({
                    url: `{{ route('aprouv_act') }}`,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: $(this).data('id')
                    },
                    success: function(data) {
                        location.reload();
                    },
                    error: function(error) {
                        console.log(error);
                    },
                });
            });
            $("body").on('click', '.reject', function() {
                $.ajax({
                    url: `{{ route('reject_act') }}`,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: $(this).data('id')
                    },
                    success: function(data) {
                        location.reload();
                        // console.log(data);
                    },
                    error: function(error) {
                        console.log(error);
                    },
                });
            });
        });
    </script>
@endsection
