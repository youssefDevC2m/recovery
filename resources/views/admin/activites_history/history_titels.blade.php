<div class="text-gray-600 mt-1 truncate w-64 tooltip"
title="
{{-- this text is related to the (what/where) of any action --}}

    {{-- cli --}}
    {{ $item->what == 'create' && $item->where == 'client' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau client' : '' }}
    {{ $item->what == 'delete' && $item->where == 'client' ? 'Cet utilisateur a supprimer un client' : '' }}
    {{ $item->what == 'create' && $item->where == 'client' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un client" : '' }}
    {{-- cli end --}}

    {{-- dossier --}}
    {{ $item->what == 'create' && $item->where == 'dossier' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau dossier' : '' }}
    {{ $item->what == 'delete' && $item->where == 'dossier' ? 'Cet utilisateur a supprimer un dossier' : '' }}
    {{ $item->what == 'create' && $item->where == 'dossier' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un dossier" : '' }}
    {{-- dossier end --}}

    {{-- scen --}}
    {{ $item->what == 'create' && $item->where == 'NewScen' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau scenario' : '' }}
    {{ $item->what == 'create' && $item->where == 'NewScen' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un scenario" : '' }}
    {{-- end scen --}}

    {{-- scen cheque --}}
    {{ $item->what == 'create' && $item->where == 'scenChequ' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle plainte cheque sans provision' : '' }}
    {{ $item->what == 'delete' && $item->where == 'scenChequ' ? 'Cet utilisateur a supprimer une plainte cheque sans provision' : '' }}
    {{ $item->what == 'create' && $item->where == 'scenChequ' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une plainte cheque sans provision" : '' }}
    {{-- end scen cheque --}}

    {{-- scen injonctionPay --}}
    {{ $item->what == 'create' && $item->where == 'scenInjonctionPay' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle injonction de payer' : '' }}
    {{ $item->what == 'delete' && $item->where == 'scenInjonctionPay' ? 'Cet utilisateur a supprimer une injonction de payer' : '' }}
    {{ $item->what == 'create' && $item->where == 'scenInjonctionPay' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une injonction de payer" : '' }}
    {{-- end scen injonctionPay --}}

    {{-- scen requet injonction --}}
    {{ $item->what == 'create' && $item->where == 'scenReqInjonctionPay' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle requête en injonctions de payer' : '' }}
    {{ $item->what == 'delete' && $item->where == 'scenReqInjonctionPay' ? 'Cet utilisateur a supprimer une requête en injonctions de payer' : '' }}
    {{ $item->what == 'create' && $item->where == 'scenReqInjonctionPay' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une requête en injonctions de payer" : '' }}
    {{-- end scen requet injonction --}}

    {{-- scen citation direct --}}
    {{ $item->what == 'create' && $item->where == 'citation_direct' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle citation directe' : '' }}
    {{ $item->what == 'delete' && $item->where == 'citation_direct' ? 'Cet utilisateur a supprimer une citation directe' : '' }}
    {{ $item->what == 'create' && $item->where == 'citation_direct' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une citation directe" : '' }}
    {{-- end scen citation direct --}}

    {{-- fond commerce --}}
    {{ $item->what == 'create' && $item->where == 'FondComers' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie sur le fond de commerce' : '' }}
    {{ $item->what == 'delete' && $item->where == 'FondComers' ? 'Cet utilisateur a supprimer une saisie sur le fond de commerce' : '' }}
    {{ $item->what == 'create' && $item->where == 'FondComers' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie sur le fond de commerce" : '' }}
    {{-- end fond commerce --}}

    {{-- tiers detenteur --}}
    {{ $item->what == 'create' && $item->where == 'SaisieTiersDetenteur' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie conservatoire tiers détenteur' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieTiersDetenteur' ? 'Cet utilisateur a supprimer une saisie conservatoire tiers détenteur' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieTiersDetenteur' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie conservatoire tiers détenteur" : '' }}
    {{-- end tiers detenteur --}}

    {{-- arret bancaire --}}
    {{ $item->what == 'create' && $item->where == 'SaisieArretBancaire' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie arrêt bancaire' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieArretBancaire' ? 'Cet utilisateur a supprimer une saisie arrêt bancaire' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieArretBancaire' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie arrêt bancaire" : '' }}
    {{-- end arret bancaire --}}

    {{-- action / parts social --}}
    {{ $item->what == 'create' && $item->where == 'SaisieActionsPartsSoc' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie Actions-Parts sociales' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieActionsPartsSoc' ? 'Cet utilisateur a supprimer une saisie Actions-Parts sociales' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieActionsPartsSoc' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie Actions-Parts sociales" : '' }}
    {{-- end action / parts social --}}

    {{-- action / parts social --}}
    {{ $item->what == 'create' && $item->where == 'SaisieActionsPartsSoc' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie Actions-Parts sociales' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieActionsPartsSoc' ? 'Cet utilisateur a supprimer une saisie Actions-Parts sociales' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieActionsPartsSoc' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie Actions-Parts sociales" : '' }}
    {{-- end action / parts social --}}

    {{-- saisie mobilier --}}
    {{ $item->what == 'create' && $item->where == 'SaisieMobiler' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie mobilier' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieMobiler' ? 'Cet utilisateur a supprimer une saisie mobilier' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieMobiler' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie mobilier" : '' }}
    {{-- end saisie mobilier --}}
    
    {{-- saisie immobilier --}}
    {{ $item->what == 'create' && $item->where == 'SaisieImobiler' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie Immobilier' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieImobiler' ? 'Cet utilisateur a supprimer une saisie Immobilier' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieImobiler' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie Immobilier" : '' }}
    {{-- end saisie immobilier --}}

    {{-- devis --}}
    {{ $item->what == 'create' && $item->where == 'Devis' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau devis' : '' }}
    {{ $item->what == 'delete' && $item->where == 'Devis' ? 'Cet utilisateur a supprimer un devis' : '' }}
    {{ $item->what == 'create' && $item->where == 'Devis' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un devis" : '' }}
    {{-- end devis --}}

    {{-- Facture --}}
    {{ $item->what == 'create' && $item->where == 'Facture' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle facture' : '' }}
    {{ $item->what == 'delete' && $item->where == 'Facture' ? 'Cet utilisateur a supprimer une facture' : '' }}
    {{ $item->what == 'create' && $item->where == 'Facture' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une facture" : '' }}
    {{-- end Facture --}}
    
    {{-- reglement --}}
    {{ $item->what == 'create' && $item->where == 'Reglement' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau reglement' : '' }}
    {{ $item->what == 'delete' && $item->where == 'Reglement' ? 'Cet utilisateur a supprimer un reglement' : '' }}
    {{ $item->what == 'create' && $item->where == 'Reglement' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un reglement" : '' }}
    {{-- end reglement --}}
    
    {{-- charge --}}
    {{ $item->what == 'create' && $item->where == 'Charges' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle charge' : '' }}
    {{ $item->what == 'delete' && $item->where == 'Charges' ? 'Cet utilisateur a supprimer une charge' : '' }}
    {{ $item->what == 'create' && $item->where == 'Charges' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une charge" : '' }}
    {{-- end charge --}}

    {{-- partner --}}
    {{ $item->what == 'create' && $item->where == 'partner' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau partenaire' : '' }}
    {{ $item->what == 'delete' && $item->where == 'partner' ? 'Cet utilisateur a supprimer un partenaire' : '' }}
    {{ $item->what == 'create' && $item->where == 'partner' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un partenaire" : '' }}
    {{-- end partner --}}

    {{-- gestion tach --}}
    {{ $item->what == 'create' && $item->where == 'partner' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau partenaire' : '' }}
    {{ $item->what == 'delete' && $item->where == 'partner' ? 'Cet utilisateur a supprimer un partenaire' : '' }}
    {{ $item->what == 'create' && $item->where == 'partner' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un partenaire" : '' }}
    {{-- end gestion tach --}}
"
>
    {{-- this text is related to the (what/where) of any action --}}

    {{-- cli --}}
    {{ $item->what == 'create' && $item->where == 'client' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau client' : '' }}
    {{ $item->what == 'delete' && $item->where == 'client' ? 'Cet utilisateur a supprimer un client' : '' }}
    {{ $item->what == 'create' && $item->where == 'client' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un client" : '' }}
    {{-- cli end --}}

    {{-- dossier --}}
    {{ $item->what == 'create' && $item->where == 'dossier' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau dossier' : '' }}
    {{ $item->what == 'delete' && $item->where == 'dossier' ? 'Cet utilisateur a supprimer un dossier' : '' }}
    {{ $item->what == 'create' && $item->where == 'dossier' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un dossier" : '' }}
    {{-- dossier end --}}

    {{-- scen --}}
    {{ $item->what == 'create' && $item->where == 'NewScen' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau scenario' : '' }}
    {{ $item->what == 'create' && $item->where == 'NewScen' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un scenario" : '' }}
    {{-- end scen --}}

    {{-- scen cheque --}}
    {{ $item->what == 'create' && $item->where == 'scenChequ' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle plainte cheque sans provision' : '' }}
    {{ $item->what == 'delete' && $item->where == 'scenChequ' ? 'Cet utilisateur a supprimer une plainte cheque sans provision' : '' }}
    {{ $item->what == 'create' && $item->where == 'scenChequ' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une plainte cheque sans provision" : '' }}
    {{-- end scen cheque --}}

    {{-- scen injonctionPay --}}
    {{ $item->what == 'create' && $item->where == 'scenInjonctionPay' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle injonction de payer' : '' }}
    {{ $item->what == 'delete' && $item->where == 'scenInjonctionPay' ? 'Cet utilisateur a supprimer une injonction de payer' : '' }}
    {{ $item->what == 'create' && $item->where == 'scenInjonctionPay' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une injonction de payer" : '' }}
    {{-- end scen injonctionPay --}}

    {{-- scen requet injonction --}}
    {{ $item->what == 'create' && $item->where == 'scenReqInjonctionPay' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle requête en injonctions de payer' : '' }}
    {{ $item->what == 'delete' && $item->where == 'scenReqInjonctionPay' ? 'Cet utilisateur a supprimer une requête en injonctions de payer' : '' }}
    {{ $item->what == 'create' && $item->where == 'scenReqInjonctionPay' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une requête en injonctions de payer" : '' }}
    {{-- end scen requet injonction --}}

    {{-- scen citation direct --}}
    {{ $item->what == 'create' && $item->where == 'citation_direct' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle citation directe' : '' }}
    {{ $item->what == 'delete' && $item->where == 'citation_direct' ? 'Cet utilisateur a supprimer une citation directe' : '' }}
    {{ $item->what == 'create' && $item->where == 'citation_direct' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une citation directe" : '' }}
    {{-- end scen citation direct --}}

    {{-- fond commerce --}}
    {{ $item->what == 'create' && $item->where == 'FondComers' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie sur le fond de commerce' : '' }}
    {{ $item->what == 'delete' && $item->where == 'FondComers' ? 'Cet utilisateur a supprimer une saisie sur le fond de commerce' : '' }}
    {{ $item->what == 'create' && $item->where == 'FondComers' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie sur le fond de commerce" : '' }}
    {{-- end fond commerce --}}

    {{-- tiers detenteur --}}
    {{ $item->what == 'create' && $item->where == 'SaisieTiersDetenteur' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie conservatoire tiers détenteur' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieTiersDetenteur' ? 'Cet utilisateur a supprimer une saisie conservatoire tiers détenteur' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieTiersDetenteur' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie conservatoire tiers détenteur" : '' }}
    {{-- end tiers detenteur --}}

    {{-- arret bancaire --}}
    {{ $item->what == 'create' && $item->where == 'SaisieArretBancaire' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie arrêt bancaire' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieArretBancaire' ? 'Cet utilisateur a supprimer une saisie arrêt bancaire' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieArretBancaire' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie arrêt bancaire" : '' }}
    {{-- end arret bancaire --}}

    {{-- action / parts social --}}
    {{ $item->what == 'create' && $item->where == 'SaisieActionsPartsSoc' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie Actions-Parts sociales' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieActionsPartsSoc' ? 'Cet utilisateur a supprimer une saisie Actions-Parts sociales' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieActionsPartsSoc' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie Actions-Parts sociales" : '' }}
    {{-- end action / parts social --}}

    {{-- action / parts social --}}
    {{ $item->what == 'create' && $item->where == 'SaisieActionsPartsSoc' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie Actions-Parts sociales' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieActionsPartsSoc' ? 'Cet utilisateur a supprimer une saisie Actions-Parts sociales' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieActionsPartsSoc' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie Actions-Parts sociales" : '' }}
    {{-- end action / parts social --}}

    {{-- saisie mobilier --}}
    {{ $item->what == 'create' && $item->where == 'SaisieMobiler' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie mobilier' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieMobiler' ? 'Cet utilisateur a supprimer une saisie mobilier' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieMobiler' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie mobilier" : '' }}
    {{-- end saisie mobilier --}}
    
    {{-- saisie immobilier --}}
    {{ $item->what == 'create' && $item->where == 'SaisieImobiler' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle saisie Immobilier' : '' }}
    {{ $item->what == 'delete' && $item->where == 'SaisieImobiler' ? 'Cet utilisateur a supprimer une saisie Immobilier' : '' }}
    {{ $item->what == 'create' && $item->where == 'SaisieImobiler' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une saisie Immobilier" : '' }}
    {{-- end saisie immobilier --}}

    {{-- devis --}}
    {{ $item->what == 'create' && $item->where == 'Devis' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau devis' : '' }}
    {{ $item->what == 'delete' && $item->where == 'Devis' ? 'Cet utilisateur a supprimer un devis' : '' }}
    {{ $item->what == 'create' && $item->where == 'Devis' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un devis" : '' }}
    {{-- end devis --}}

    {{-- Facture --}}
    {{ $item->what == 'create' && $item->where == 'Facture' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle facture' : '' }}
    {{ $item->what == 'delete' && $item->where == 'Facture' ? 'Cet utilisateur a supprimer une facture' : '' }}
    {{ $item->what == 'create' && $item->where == 'Facture' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une facture" : '' }}
    {{-- end Facture --}}
    
    {{-- reglement --}}
    {{ $item->what == 'create' && $item->where == 'Reglement' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau reglement' : '' }}
    {{ $item->what == 'delete' && $item->where == 'Reglement' ? 'Cet utilisateur a supprimer un reglement' : '' }}
    {{ $item->what == 'create' && $item->where == 'Reglement' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un reglement" : '' }}
    {{-- end reglement --}}
    
    {{-- charge --}}
    {{ $item->what == 'create' && $item->where == 'Charges' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté une nouvelle charge' : '' }}
    {{ $item->what == 'delete' && $item->where == 'Charges' ? 'Cet utilisateur a supprimer une charge' : '' }}
    {{ $item->what == 'create' && $item->where == 'Charges' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'une charge" : '' }}
    {{-- end charge --}}

    {{-- partner --}}
    {{ $item->what == 'create' && $item->where == 'partner' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau partenaire' : '' }}
    {{ $item->what == 'delete' && $item->where == 'partner' ? 'Cet utilisateur a supprimer un partenaire' : '' }}
    {{ $item->what == 'create' && $item->where == 'partner' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un partenaire" : '' }}
    {{-- end partner --}}

    {{-- gestion tach --}}
    {{ $item->what == 'create' && $item->where == 'partner' && $item->created_at == $item->updated_at ? 'Cet utilisateur a ajouté un nouveau partenaire' : '' }}
    {{ $item->what == 'delete' && $item->where == 'partner' ? 'Cet utilisateur a supprimer un partenaire' : '' }}
    {{ $item->what == 'create' && $item->where == 'partner' && $item->created_at != $item->updated_at ? "Cet utilisateur à modifier les données d'un partenaire" : '' }}
    {{-- end gestion tach --}}

</div>
