<div class="modal" id="reject-modal-preview{{ $item->id }}">
    <div class="modal__content">
        <div class="p-5 text-center">
            <i data-feather="thumbs-down" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
            <div class="text-3xl mt-5">Voulez vous rejeter cette action?</div>
            {{-- <div class="text-gray-600 mt-2">Do you really want to delete these records? This process cannot be undone.</div> --}}
        </div>
        <div class="px-5 pb-8 text-center"> 
            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 dark:border-dark-5 dark:text-gray-300 mr-1">Annuler</button>
            <button data-id='{{ $item->id }}' data-action="{{ $item->id_action }}" type="button" class="button w-24 bg-theme-6 text-white reject">Rejeter</button>
        </div>
    </div>
</div>