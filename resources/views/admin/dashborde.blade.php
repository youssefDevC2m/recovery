@extends('admin.admin')

@section('title')
    Tableaux de borde
@endsection

@section('menu-title')
    <style>
        .report-box__indicator {
            padding-right: 0.50rem !important;
        }
    </style>
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">sysLocation</a> <i
            data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Tableau de
            bord</a> </div>
@endsection

@section('content')
    @if (Auth::user()->acs_tabord == 1)
        <div class="grid grid-cols-12">
            <!-- BEGIN: General Report -->
            <div class="{{(Auth::user()->droit_de_valid_acts == 1)? 'lg:col-span-6 ' : '' }} col-span-12 mt-8">
                <div class="intro-y flex items-center h-10">
                    <h2 class="text-lg font-medium truncate mr-5">
                        Rapport général
                    </h2>
                    <a href="{{ url('/home/admin') }}" class="ml-auto flex text-theme-1 dark:text-theme-10"> <i
                            data-feather="refresh-ccw" class="w-4 h-4 mr-3"></i> Recharger les données </a>
                </div>
                <div class="">
                    <div class="grid grid-cols-1 gap-4 mt-5">
                        
                        @if (Auth::user()->acs_cli == 1)
                            <div class="intro-y w-full">
                                <div class="report-box zoom-in ">
                                    <div class="box p-5">
                                        <div class="flex">
                                            <span class="">
                                                <i data-feather="users" class="text-theme-9 font-semibold"></i>
                                            </span>
                                            <div class="ml-auto">
                                                <div class="report-box__indicator bg-theme-9  cursor-pointer">
                                                    <a href="javascript:;" data-theme="light" class="tooltip"
                                                        title="nombre des clients">{{ $Client }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-3xl font-bold leading-8 mt-6">Clients</div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if (Auth::user()->acs_user == 1)
                            <div class="intro-y">
                                <div class="report-box zoom-in">
                                    <div class="box p-5">
                                        <div class="flex">
                                            <span class="">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                                    stroke-linecap="round" stroke-linejoin="round"
                                                    class="feather feather-user report-box__icon text-theme-3">
                                                    <polygon points="1 6 1 22 8 18 16 22 23 18 23 2 16 6 8 2 1 6"></polygon>
                                                    <line x1="8" y1="2" x2="8" y2="18"></line>
                                                    <line x1="16" y1="6" x2="16" y2="22"></line>
                                                </svg>
                                            </span>
                                            <div class="ml-auto">
                                                <div class="report-box__indicator bg-blue-500  cursor-pointer text-center ">
                                                    <a href="javascript:;" data-theme="light" class="tooltip"
                                                        title="nombre des utilisateurs">{{ $User }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-3xl font-bold leading-8 mt-6">Utilisateurs </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if (Auth::user()->acs_doss == 1)
                            <div class="intro-y">
                                <div class="report-box zoom-in">
                                    <div class="box p-5">
                                        <div class="flex">
                                            <span class="relative">
                                                {{-- <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user report-box__icon text-theme-3"><polygon points="1 6 1 22 8 18 16 22 23 18 23 2 16 6 8 2 1 6"></polygon><line x1="8" y1="2" x2="8" y2="18"></line><line x1="16" y1="6" x2="16" y2="22"></line></svg> --}}
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                                    stroke-linecap="round" stroke-linejoin="round"
                                                    class="feather feather-folder report-box__icon text-yellow-500">
                                                    <path
                                                        d="M22 19a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h5l2 3h9a2 2 0 0 1 2 2z">
                                                    </path>
                                                </svg>
                                            </span>
                                            <div class="ml-auto ">
                                                <div class="report-box__indicator bg-yellow-500  cursor-pointer text-center ">
                                                    <a href="javascript:;" data-theme="light" class="tooltip"
                                                        title="nombre des dossiers">{{ $Dossier }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-3xl font-bold leading-8 mt-6">Dossiers</div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="grid lg:grid-cols-2 grid-cols-1 gap-4 mt-5">
                        @if (Auth::user()->acs_charge == 1)
                            <div class="intro-y">
                                <div class="report-box zoom-in">
                                    <div class="box p-5">
                                        <div class="flex">
                                            <span class="relative">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                                    stroke-linecap="round" stroke-linejoin="round"
                                                    class="feather feather-file-text report-box__icon text-red-500">
                                                    <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z">
                                                    </path>
                                                    <polyline points="14 2 14 8 20 8"></polyline>
                                                    <line x1="16" y1="13" x2="8" y2="13">
                                                    </line>
                                                    <line x1="16" y1="17" x2="8" y2="17">
                                                    </line>
                                                    <polyline points="10 9 9 9 8 9"></polyline>
                                                </svg>
                                            </span>
                                            <div class="ml-auto ">
                                                <div class="report-box__indicator bg-red-500  cursor-pointer text-center ">
                                                    <a href="javascript:;" data-theme="light" class="tooltip"
                                                        title="nombre des dossiers">{{ $Charges }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-3xl font-bold leading-8 mt-6">Charges</div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if (Auth::user()->acs_devis == 1)
                            <div class="intro-y">
                                <div class="report-box zoom-in">
                                    <div class="box p-5">
                                        <div class="flex">
                                            <span class="relative">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                    stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                                                    class="feather feather-file-text report-box__icon text-green-500">
                                                    <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z">
                                                    </path>
                                                    <polyline points="14 2 14 8 20 8"></polyline>
                                                    <line x1="16" y1="13" x2="8" y2="13">
                                                    </line>
                                                    <line x1="16" y1="17" x2="8" y2="17">
                                                    </line>
                                                    <polyline points="10 9 9 9 8 9"></polyline>
                                                </svg>
                                            </span>
                                            <div class="ml-auto ">
                                                <div class="report-box__indicator bg-theme-9  cursor-pointer text-center ">
                                                    <a href="javascript:;" data-theme="light" class="tooltip"
                                                        title="nombre des dossiers">{{ $Devis }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-3xl font-bold leading-8 mt-6 w-32  ">
                                            <label for="">Devis</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
            {{-- action Historique --}}
            @if (Auth::user()->droit_de_valid_acts == 1)
                <div class="lg:col-span-6 sm:my-5 col-span-12 lg:mt-8 m-5">
                    <div class="intro-y flex items-center h-10">
                        <h2 class="text-lg font-medium truncate ">
                            Activités récentes
                        </h2>
                        <a href="{{ route('admin_see_all_acts') }}"
                            class="ml-auto text-theme-1 dark:text-theme-10 truncate">Voir tout</a>
                        {{-- <div class="">
                            <div class=" intro-x flex items-center h-10">
                                <h2 class="text-lg font-medium truncate mr-5">
                                    Activités récentes
                                </h2>
                                
                            </div>

                            
                        </div> --}}
                        <?php $date_hist = ''; ?>
                    </div>
                    @if (count($HistoAct) > 0)
                        <div class="report-timeline pt-5 relative overflow-y-scroll p-3" style="height: 60vh">
                            @foreach ($HistoAct as $index => $item)
                                @if ($date_hist != date('d/m/Y', strtotime($item->created_at)) && $index > 0)
                                    <div class="intro-x text-gray-500 text-xs text-center my-4">
                                        {{ date('d/m/Y', strtotime($item->created_at)) }}</div>
                                @endif
                                <?php $date_hist = date('d/m/Y', strtotime($item->created_at)); ?>
                                <div class="intro-y relative flex items-center mb-3">
                                    <div class="report-timeline__image">
                                        <div class="w-10 h-10 flex-none image-fit rounded-full overflow-hidden">
                                            <img alt="Midone Tailwind HTML Admin Template"
                                                src="dist/images/profile-1.jpg">
                                        </div>
                                    </div>
                                    <div class="box px-5 py-3 ml-4 flex-1 zoom-in">
                                        <div class="flex items-center">
                                            <div class="font-medium">
                                                {{ $item->username . ' ' . $item->userlastname }}
                                            </div>
                                            <div class="text-xs text-gray-500 ml-auto">
                                                {{ $index == 0 ? date('d/m/Y H:m', strtotime($item->created_at)) : date('H:m', strtotime($item->created_at)) }}
                                            </div>
                                        </div>
                                        @include('admin.activites_history.history_titels')
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="intro-y box px-5 py-3 mt-5 ml-4 flex-1 zoom-in text-center">
                            <span class="text-gray-400 text-xl font-semibold ">Aucune activité</span>
                        </div>
                    @endif
                </div>
            @endif
            {{-- end action Historique --}}
            <!-- END: General Report -->
        </div>
    @endif

    <script>
        $('.dbord').attr('class', 'side-menu side-menu--active');
    </script>
@endsection
