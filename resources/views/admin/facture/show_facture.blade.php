@extends('admin.admin')

@section('title')
    Modification facture
@endsection

@section('menu-title')
    <style>
        .dropdown-optgroup {

            height: 128px;



            overflow-y: auto;

        }
    </style>

    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i
            data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Modification
            facture</a> </div>
@endsection

@section('content')

    <div class="mt-5 box font-semiblod">

        <form action="" id="form_data">

            <input type="hidden" name="id_facture" value="{{ $facture->id }}">

            <div class="p-5">



                <div class="grid lg:grid-cols-5 md:grid-cols-2 sm:grid-cols-1 gap-2">

                    <div>

                        <label for="">Client</label>

                        <select name="client" class="input w-full border mt-2">

                            <option value="0">Sélectionner un client</option>
                            @if (isset($client->id))
                                <option value="{{ $client->id }}" selected>
                                    {{ $client->type_cli == 'perso' ? $client->nom . ' ' . $client->prenom : $client->raison_sos }}
                                </option>
                            @endif

                        </select>

                    </div>

                    <div>

                        <label for="">Dossier</label>

                        <select name="dossier[]" data-placeholder="Selectionner les dossiers"
                            class="input w-full  mt-2 tail-select" data-tail-select="tail-1" multiple>



                            @foreach ($alldoss as $item)
                                <option value="{{ $item->id }}" {{ in_array($item->id, $dossier) ? 'selected' : '' }}>
                                    {{ $item->ref }}</option>
                            @endforeach



                        </select>

                    </div>

                    <div>

                        <label for="">Type de facture</label>

                        <input type="text" name="type_facture" value="{{ $facture->type_facture }}"
                            placeholder="Entrer le type de facture" class="input w-full border mt-2">

                    </div>

                    <div>

                        <label for="">Date facture</label>

                        <input type="date" name="date_facture"
                            value="{{ date('Y-m-d', strtotime($facture->date_facture)) }}"
                            class="input w-full border mt-2">

                    </div>

                    <div>

                        <label for="">Âge de facture</label>

                        <input type="text" value="<?php
                        $dossCont = new App\Http\Controllers\dossierController();
                        $diff = $dossCont->dateDifference(date('Y-m-d'), date('Y-m-d', strtotime($facture->date_facture)) ?? 0);
                        echo $diff;
                        
                        ?>" class="input w-full border mt-2">

                    </div>

                </div>



                <div class="w-full mt-2">

                    <label for="">Remarque</label>

                    <textarea name="remarque" placeholder="Remarque..." class="input w-full h-32 border mt-2">{{ $facture->remarque }}</textarea>

                </div>

                <div class="py-3">

                    <div class="border-2 border-dashed dark:border-dark-5 rounded-md pt-4">

                        <div class="flex flex-wrap px-4 file_block">

                            @if (count($files) > 0)
                                @foreach ($files as $item)
                                    <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2">

                                        <a href="{{ asset($File_path->file_path . $item->file_name) }}" target="_blank">

                                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">

                                                <span class="w-3/5 file__icon file__icon--file mx-auto">

                                                    <div class="file__icon__file-name"></div>

                                                </span>

                                                <span
                                                    class="block font-medium mt-4 text-center truncate">{{ $item->org_file_name }}</span>

                                            </div>

                                        </a>

                                        <input type="text" name="" id="">

                                        <div title=""
                                            class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file_base "
                                            data-id="kill_file_base{{ $item->id }}">

                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                                stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-x w-4 h-4">
                                                <line x1="18" y1="6" x2="6" y2="18"></line>
                                                <line x1="6" y1="6" x2="18" y2="18"></line>
                                            </svg>

                                        </div>

                                    </div>

                                    <input type="hidden" name="file_exc[]" value="{{ $item->file_name }}"
                                        class="kill_file_base{{ $item->id }}">
                                @endforeach
                            @endif

                        </div>

                        <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">

                            <button
                                class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600">

                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                    stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2">
                                    <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                    <polyline points="14 2 14 8 20 8"></polyline>
                                    <line x1="16" y1="13" x2="8" y2="13"></line>
                                    <line x1="16" y1="17" x2="8" y2="17"></line>
                                    <polyline points="10 9 9 9 8 9"></polyline>
                                </svg>

                                <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span>

                            </button>

                            <input type="file"
                                class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0"
                                name="file_val[]" data-stp="not">

                        </div>

                    </div>

                </div>

                <div class="px-3 py-2 box intro-x mt-2">

                    <div class="w-full my-3">

                        <div class=" text-center">

                            <label for="" class="text-2xl text-gray-700 ">Produits</label>

                        </div>

                        <div class="my-2 text-center">

                            <a href="javascript:;" data-toggle="modal" data-target="#header-footer-modal-preview"
                                class=" button w-auto mr-1 mb-2 border border-gray-600 hover:bg-theme-9 hover:text-white hover:border-white text-gray-700 dark:bg-dark-5 dark:text-gray-300">Ajouter
                                Un Produit</a>

                        </div>

                        {{-- ///////////////// --}}

                        <div class="modal" id="header-footer-modal-preview">

                            <div class="modal__content">

                                <div
                                    class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200 dark:border-dark-5">

                                    <h2 class="font-medium text-base mr-auto">Details</h2>

                                </div>

                                <div class="w-full mt-2 p-5">

                                    <div class="mt-2">

                                        <label>Désignation</label>

                                        <input type="text" class="input w-full border mt-2 flex-1 nomProd"
                                            placeholder="Entrez une désignation">

                                    </div>

                                </div>

                                <div class="p-5 grid grid-cols-2 gap-2">

                                    <div class="mt-2">

                                        <label>Montant</label>

                                        <input type="number" class="input w-full border mt-2 flex-1 prix"
                                            placeholder="Entrez le montant">

                                    </div>

                                    <div class="mt-2">

                                        <label>Remise %</label>

                                        <input type="number" class="input w-full border mt-2 flex-1 remis"
                                            placeholder="Remise %">

                                    </div>

                                </div>



                                <div class="px-5 py-3 text-right border-t border-gray-200 dark:border-dark-5">

                                    <button data-dismiss="modal" type="button"
                                        class=" button w-20 border text-gray-700 dark:border-dark-5 dark:text-gray-300 mr-1">Annuler</button>

                                    <button data-dismiss="modal" type="button"
                                        class="button w-20 bg-theme-1 text-white Add_prod">Ajouter</button>

                                </div>

                            </div>

                        </div>

                        <div class="dt_prod_form"></div>

                        {{-- /////////////// --}}

                        <div class="overflow-x-scroll">

                            <table class="table mt-2  ">

                                <thead>

                                    <tr class="bg-gray-700  dark:bg-dark-1 text-white">

                                        <th class="whitespace-no-wrap">Désignation</th>

                                        <th class="whitespace-no-wrap">montant</th>

                                        <th class="whitespace-no-wrap">Remise %</th>

                                        <th class="whitespace-no-wrap text-center">Action</th>

                                    </tr>

                                </thead>

                                <tbody class="product">

                                    @foreach ($produit as $item)
                                        <tr id="line{{ $item->id }}">

                                            <td id="col-nomProd-l-{{ $item->id }}"
                                                class="border-b dark:border-dark-5">

                                                {{ $item->designation }}

                                                <input type="hidden" name="designation[]"
                                                    value="{{ $item->designation }}">

                                                <input type="hidden" name="prod_rows[]" value="1">

                                            </td>

                                            <td id="col-prix-l-{{ $item->id }}" class="border-b dark:border-dark-5">

                                                {{ $item->montant }}

                                                <input type="hidden" name="montant[]" value="{{ $item->montant }}">

                                            </td>

                                            <td id="col-remise-l-{{ $item->id }}"
                                                class="border-b dark:border-dark-5">

                                                {{ $item->remise }}

                                                <input type="hidden" name="remise[]" value="{{ $item->remise }}">

                                            </td>

                                            <td class="border-b dark:border-dark-5 grid justify-items-center">

                                                <div class="flex flex-row">

                                                    <a data-delete="line{{ $item->id }}"
                                                        class="rm-line button px-2 mr-1 mb-2 bg-theme-6 text-white">

                                                        <span class="w-5 h-5 flex items-center justify-center">

                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                height="24" viewBox="0 0 24 24" fill="none"
                                                                stroke="currentColor" stroke-width="2.5"
                                                                stroke-linecap="round" stroke-linejoin="round"
                                                                class="feather feather-trash w-4 h-4">
                                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                                <path
                                                                    d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                                                </path>
                                                            </svg>

                                                        </span>

                                                    </a>

                                                    <a href="javascript:;" data-toggle="modal"
                                                        data-target="#header-footer-modal-edition{{ $item->id }}"
                                                        data-num={{ $item->id }}
                                                        class="button px-2 mr-1 mb-2 bg-theme-9 text-white">

                                                        <span class="w-5 h-5 flex items-center justify-center">

                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                height="24" viewBox="0 0 24 24" fill="none"
                                                                stroke="currentColor" stroke-width="2.5"
                                                                stroke-linecap="round" stroke-linejoin="round"
                                                                class="feather feather-edit w-4 h-4">
                                                                <path
                                                                    d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7">
                                                                </path>
                                                                <path
                                                                    d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z">
                                                                </path>
                                                            </svg></span>

                                                    </a>

                                                </div>

                                            </td>

                                        </tr>

                                        {{-- ------------------------------------------------------ --}}

                                        <div class="modal" id="header-footer-modal-edition{{ $item->id }}">

                                            <div class="modal__content">

                                                <div
                                                    class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200 dark:border-dark-5">

                                                    <h2 class="font-medium text-base mr-auto">Details</h2>

                                                </div>

                                                <div class="w-full mt-2 p-5">

                                                    <div class="mt-2">



                                                        <label>Désignation</label>

                                                        <input type="text"
                                                            class="input w-full border mt-2 flex-1 nomProd_edited_v"
                                                            value="{{ $item->designation }}"
                                                            placeholder="Entrez une désignation">

                                                    </div>

                                                </div>

                                                <div class="p-5 grid grid-cols-2 gap-2">

                                                    <div class="mt-2">

                                                        <label>Montant</label>

                                                        <input type="text"
                                                            class="input w-full border mt-2 flex-1 prix_edited_v"
                                                            value="{{ $item->montant }}"
                                                            placeholder="Entrez le montant">

                                                    </div>

                                                    <div class="mt-2">

                                                        <label>Remise %</label>

                                                        <input type="text"
                                                            class="input w-full border mt-2 flex-1 remis_edited_v"
                                                            value="{{ $item->remise }}" placeholder="Remise %">

                                                    </div>

                                                </div>



                                                <div
                                                    class="px-5 py-3 text-right border-t border-gray-200 dark:border-dark-5">

                                                    <button data-dismiss="modal" type="button"
                                                        class=" button w-20 border text-gray-700 dark:border-dark-5 dark:text-gray-300 mr-1">Annuler</button>

                                                    <button data-dismiss="modal" type="button"
                                                        data-numb={{ $item->id }}
                                                        class="button w-20 bg-theme-1 text-white edit_prod">Modifier</button>

                                                </div>

                                            </div>

                                        </div>

                                        {{-- ------------------------------------------------------ --}}
                                    @endforeach

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>


                @if (Auth::user()->edit_fact == 1)
                    <div class="grid justify-items-center">

                        <div>

                            <button class="button bg-theme-1 text-white save">

                                Enregistrer

                            </button>

                        </div>

                    </div>
                @endif


            </div>

        </form>



    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function() {



            var rows = $('.edit_prod').data('numb');

            $('body').on('click', '.Add_prod', function() {

                event.preventDefault();



                rows++;

                $('.product').append(`

                <tr id="line${rows}">

                    <td id="col-nomProd-l-${rows}" class="border-b dark:border-dark-5">

                        ${$('.nomProd').val()}

                        <input type="hidden" name="designation[]" value="${$('.nomProd').val()}">

                        <input type="hidden" name="prod_rows[]" value="1">

                    </td>

                    <td id="col-prix-l-${rows}" class="border-b dark:border-dark-5">

                        ${$('.prix').val()}

                        <input type="hidden" name="montant[]" value="${$('.prix').val()}">

                    </td>

                    <td id="col-remise-l-${rows}" class="border-b dark:border-dark-5">

                        ${$('.remis').val()}

                        <input type="hidden" name="remise[]" value="${$('.remis').val()}">

                    </td>

                    <td class="border-b dark:border-dark-5 grid justify-items-center">

                        <div class="flex flex-row">

                            <a data-delete="line${rows}" class="rm-line button px-2 mr-1 mb-2 bg-theme-6 text-white"> 

                                <span class="w-5 h-5 flex items-center justify-center"> 

                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"  viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash w-4 h-4"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>

                                </span> 

                            </a>

                            <a href="javascript:;" data-toggle="modal" data-target="#header-footer-modal-edition${rows}" data-num=${rows} class="button px-2 mr-1 mb-2 bg-theme-9 text-white"> 

                                <span class="w-5 h-5 flex items-center justify-center"> 

                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit w-4 h-4"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></span> 

                            </a>

                        </div>

                    </td>

                </tr>

                `);



                $('.dt_prod_form').append(`

                <div class="modal" id="header-footer-modal-edition${rows}">

                    <div class="modal__content">

                        <div class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200 dark:border-dark-5">

                            <h2 class="font-medium text-base mr-auto">Details</h2> 

                        </div>

                        <div class="w-full mt-2 p-5">

                            <div class="mt-2"> 

                                

                                <label>Désignation</label> 

                                <input type="text" class="input w-full border mt-2 flex-1 nomProd_edited_v" value="${$('.nomProd').val()}" placeholder="Entrez une désignation"> 

                            </div>

                        </div>

                        <div class="p-5 grid grid-cols-2 gap-2">

                            <div class="mt-2"> 

                                <label>Montant</label> 

                                <input type="number" class="input w-full border mt-2 flex-1 prix_edited_v" value="${$('.prix').val()}" placeholder="Entrez le montant"> 

                            </div>

                            <div class="mt-2"> 

                                <label>Remise %</label> 

                                <input type="number" class="input w-full border mt-2 flex-1 remis_edited_v" value="${$('.remis').val()}" placeholder="Remise %"> 

                            </div>

                        </div>

                        

                        <div class="px-5 py-3 text-right border-t border-gray-200 dark:border-dark-5"> 

                            <button data-dismiss="modal" type="button" class=" button w-20 border text-gray-700 dark:border-dark-5 dark:text-gray-300 mr-1">Annuler</button> 

                            <button data-dismiss="modal" type="button" data-numb=${rows} class="button w-20 bg-theme-1 text-white edit_prod">Modifier</button> 

                        </div>

                    </div>

                </div>

                `);

            });

            $('body').on('click', '.edit_prod', function() {

                var row = $(this).data('numb');

                var nomProd_edited_v = $(this).parent().parent().find('.nomProd_edited_v').val();

                var prix_edited_v = $(this).parent().parent().find('.prix_edited_v').val();

                var remis_edited_v = $(this).parent().parent().find('.remis_edited_v').val();



                $(`#col-nomProd-l-${row}`).html(

                    `

                ${nomProd_edited_v}

                    <input type="hidden" name="designation[]" value="${nomProd_edited_v}">

                    <input type="hidden" name="prod_rows[]" value="1">

                    

                `

                );



                $(`#col-prix-l-${row}`).html(

                    `

                    ${prix_edited_v}

                    <input type="hidden" name="montant[]" value="${prix_edited_v}">

                `

                );



                $(`#col-remise-l-${row}`).html(

                    `

                    ${remis_edited_v}

                    <input type="hidden" name="remise[]" value="${remis_edited_v}">

                `

                );

            });

            $("body").on('click', '.rm-line', function() {

                $(`#${$(this).data('delete')}`).remove();

            });

        });
    </script>

    <script>
        $('.save').click(function() {

            event.preventDefault();

            var form_data = new FormData(document.getElementById('form_data'));

            $.ajax({

                url: '{{ route('updateFacture') }}',

                type: 'post',

                dataType: 'json',

                data: form_data,

                processData: false,

                contentType: false,

                cache: false,

                success: function(data) {

                    // console.log(data);

                    location.replace("{{ route('listFacture') }}");

                },

                error: function(error) {

                    console.log(error);

                },

            });

        });
    </script>

    <script>
        var kill_nb = 0;

        $('body').on('change', '.this_file', function(input) {

            var file = $(this).get(0).files[0];

            if (file) {

                var reader = new FileReader();

                $(this).parent().parent().find('.file_block').append(

                    `

                        <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">

                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">

                                <span class="w-3/5 file__icon file__icon--file mx-auto">

                                    <div class="file__icon__file-name"></div>

                                </span>

                                <span class="block font-medium mt-4 text-center truncate">` + file.name +
                    `</span>

                            </div>

                            <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file` +
                    kill_nb + `"> 

                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 

                            </div>

                        </div>

                        `

                );



                $(this).parent().find('button').hide();

                $(this).parent().append(
                    `

                        <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 

                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>

                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 

                        </button>

                        <input type="file" name="file_val[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file` +
                    kill_nb + `">

                    `);

                kill_nb++;

            }

        });

        $("body").on('click tap', '.kill_file,.kill_file_base', function() {

            $('.' + $(this).data('id')).attr('name', '');

            $(this).parent().toggle();



        });
    </script>
    <script>
        $("body").ready(function() {
            $('.facture').addClass('side-menu--active');
        });
    </script>
@endsection
