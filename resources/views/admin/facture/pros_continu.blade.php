@extends('admin.admin')
@section('title')
Liaison facture/dossier
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Liaison facture/dossier</a> </div>
@endsection
@section('content')
<div class="w-full h-full intro-x">
    <form action="" id="form_data">
        <input type="hidden" name="id_fact" value="{{$id_fact}}">
        <div class="p-5 text-5xl font-bold">
            <span>Sélectionner les dossiers </span>
        </div>
        <div class="w-full grid justify-items-center place-items-center">
            <div class="box font-semiblod {{((count($dossier) >= 3)? 'h-64 overflow-y-scroll' : '')}} w-9/12 font-semibold ">
                @php($i = 0)
                @foreach ($dossier as $item)
                    <div class="p-5 m-2 {{((count($dossier)-1 != $i)? 'border-b-2' : '')}}">
                        <div class="flex items-center text-gray-700 dark:text-gray-500 mt-2"> 
                            <input type="checkbox" value="{{$item->id_doss}}" name="dossier[]" class="input border border-gary-500 mr-2 " id="{{$item->id_doss.$item->ref.$item->id_doss}}"> 
                            <label class="cursor-pointer select-none" for="{{$item->id_doss.$item->ref.$item->id_doss}}">Dossier :{{$item->ref}}</label> 
                            <label class="ml-5 cursor-pointer select-none" for="{{$item->id_doss.$item->ref.$item->id_doss}}">Débiteur :{{($item->type_dtbr=='perso')? $item->nom.' '.$item->prenom :  $item->nom}}</label> 
                        </div>
                    </div>  
                    @php($i++)
                @endforeach
            </div> 
        </div>
        <div class="grid justify-items-center mt-2">
            <div>
                <button class="button bg-theme-1 text-white save">
                    Enregistrer
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $('body').on('click','.save',function(){
                event.preventDefault();
                var form_data = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:`{{route('factuerlaststep')}}`,
                    data : form_data,
                    dataType:'json',
                    type : 'post',
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        location.replace(`{{route('listFacture')}}`);
                        // console.log(data);
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });
        });
    </script>
    <script>
        $("body").ready(function(){
            $('.facture').addClass('side-menu--active');
        });
    </script>
@endsection