<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        @page{
            margin: 10px;
        }
    </style>
</head>
<body style="width: 100%; height: 100%; border:3px dashed rgb(0, 14, 116); border-radius: 6px;">
    {{-- facture head --}}
    <center>
        <div>
            <img src="data:image/png;base64,{{$data['logo']}}" style="width: 20%; margin-top: 10px;">
        </div> 
        <h1>Centre Juridique Droit et Devoir</h1>  
        <h3><i>FACTURE N°</i><span style="color: rgba(109, 109, 109, 0.837)"> {{$data['id_fact']}}</span></h3> 
    </center>
    <div style="margin-top:20px; ">
        <table style="width: 100%;">
            <tbody>
                <tr>
                    <td align="">
                        <div style="margin-left: 20px ;font-size: 1.25rem;  line-height: 1.75rem;  ">
                          {{-- <b>Référence Dossier</b> <span style="border:2px solid black; padding:0.3rem">{{$data['devis']->ref}}</span>  --}}
                            <b>Date :</b> <span style="padding:0.3rem">
                                {{date('d/m/Y H:i')}}
                                {{-- {{date('d/m/Y',strtotime($data['date_facture']))}} --}}
                            </span>
                        </div>
                    </td>
                    <td align="right">
                        <div style="margin-right: 20px ;font-size: 1.25rem;  line-height: 1.75rem; ">
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    {{-- end facture head --}}
    <div style=" padding-left: 20px; padding-right: 20px;">
        <table style="width: 100%;">
            <thead>
                <tr>
                    <th align="left" style="font-size: 1.25rem; line-height: 1.75rem; height: 30px;">
                        Désignation
                    </th>

                    <th align="right" style="font-size: 1.25rem; line-height: 1.75rem; height: 30px;">
                        Montant
                    </th>

                </tr>
                
            </thead>
            <tbody>
                @php($mt_total=0)
                @foreach ($data['dt'] as $item)
                <tr style="font-size: 1.25rem; color: rgb(94, 94, 94);  line-height: 1.75rem; height: 30px;">
                    <td align="">
                        <div>
                           <b>{{$item->designation}}<span style="color: rgb(0, 0, 0);">{{(!empty($item->remise))?  ' Avec '.$item->remise.'% de remise' : '' }}</span></b> 
                        </div>
                    </td>
                    <td align="right">
                        <div style="">
                            @php($mt_total+=($item->montant - ((!empty($item->remise))? $item->montant * ($item->remise/100) : 0 )))
                            {{number_format($item->montant - ((!empty($item->remise))? $item->montant * ($item->remise/100) : 0 ), 2, ',', ' ')}} Mad
                        </div>
                    </td>
                </tr>  
                @endforeach
                <tr style="font-size: 1.25rem;  line-height: 1.75rem; height: 1000px;">
                    <td align="">
                        <div>
                           <b>Total :</b> 
                        </div>
                    </td>
                    <td align="right">
                        <div style="color: rgb(94, 94, 94);  line-height: 1.75rem; height: 30px;">
                            {{number_format($mt_total, 2, ',', ' ')}} Mad
                        </div>
                    </td>
                </tr>
                <tr style="">
                    {{-- <td style="width: 80%" align="">
                        <div style="">
                            Arreté le présent devis à la somme de : <b>{{ucfirst($data['nb']->ConvNumberLetter($mt_total,0,0))}} dhs</b>
                        </div>
                    </td>
                    <td align="">
                        
                    </td>
                     --}}
                </tr>
            </tbody>
            <div style="width: 100%  ">
                <div style="font-size: 1.25rem;  line-height: 1.75rem;">
                    Arreté le présent devis à la somme de : <b>{{ucfirst($data['nb']->ConvNumberLetter($mt_total,0,0))}} dhs</b>
                </div>
            </div>
        </table>
    </div>
</body>
</html>