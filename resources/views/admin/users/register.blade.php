@extends('admin.admin')
@section('title')
Créer des utilisateurs
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Créer des utilisateurs</a> </div>
<style>
    img {
        object-fit: cover;
        object-position: center center;
    }    
</style>

@endsection
@section('content')

<form method="post"  action="" id="form_data" onchange="previewFile(this);" enctype="multipart/form-data">
    <div class="intro-y box px-5 py-8 mt-5">

        <center>
            <div href="javascript:;" data-theme="light" class="tooltip button inline-block bg-gradient-to-b from-gray-100 to-gray-400 text-white hover:shadow-lg" title="Cliquez pour ajouter une photo !">
                <img class="rounded-full w-32 h-32 photo" src="{{asset($defaultImg)}}" alt="">
                <input type="file" class="hidden pdp" name="pdp">
            </div>
        </center>
        <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2 mt-2">
            <div>
                <label for="">Nom :</label>
                <input type="text" placeholder="Entrer le nom" name="nom" class="input border w-full mt-2">
            </div>
            <div>
                <label for="">Prenom :</label>
                <input type="text"placeholder="Entrer le prenom"  name="prenom" class="input border w-full mt-2">
            </div>
            <div>
                <label for="">Email : <i class="text-gray-400">( Identifiant )</i></label>
                <input type="email" placeholder="Entrer le email" name="mail" class="input border w-full mt-2">
            </div>
            <div>
                <label for="">Téléphone :</label>
                <input type="text"placeholder="Entrer le numero de téléphone"  name="tele" class="input border w-full mt-2">
            </div>
        </div>
        <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2 mt-2 role_b">
            <div>
                <label for="">Address :</label>
                <input type="text"placeholder="Entrer une address"  name="adrs" class="input border w-full mt-2">
            </div>
            <div>
                <label for="">Role :</label>
                <select  id="" class="input border w-full cat mt-2 role" name="role">
                    <option value="0">Sélectionner un rôle</option>
                    <option value="Administrateur">Administrateur</option>
                    <option value="Agent">Agent</option>
                    <option value="Client">Client</option>
                </select>
            </div>
            <div  class="clients_b hidden">
                <label for="">Clients :</label>
                <select  id="" class="input border w-full cat mt-2 id_client" name="id_client">
                    <option value="0">Ajouter un nouveau client</option>
                    @foreach ($client as $item)
                    <option value="{{$item->id}}">{{($item->type_cli=='perso')? $item->nom.' '.$item->prenom : $item->raison_sos }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2 mt-2">
            <div>
                <label for="">Mot de passe : </label>
                <input type="password" placeholder="Mot de passe "  class="input border w-full mt-2 password" autocomplete="false">
            </div>
            <div>
                <label for="">Confirmer mot de passe :</label>
                <input type="password"  placeholder="Confirmer mot de passe" name="pwd" class="input border w-full mt-2 confirmation" autocomplete="false">
                <div class="hidden alert_pwd ">
                    <div class="intro-y  rounded-md mt-2 flex items-center px-2 py-2 mb-2 bg-theme-31 text-theme-6"> <i data-feather="alert-octagon" class="w-6 h-6 mr-2"></i> Awesome alert with icon </div>
                </div>
            </div>
        </div>
    </div>
    <div class="privs" style=" {{(Auth::user()->give_priv_user == 1)? 'display: none;' :'' }} ">
        <div class="intro-y box my-3 grid justify-items-center " >
            <div class="text-center  text-2xl font-semibold py-1  w-64">
                <span>Privilèges</span>
            </div>
        </div>
        <div>
            @include('admin.users.previlegs')
        </div>    
    </div>
    <div class="-intro-y  px-5 py-4 mt-5">
        <center>
            <button type="button" class="save button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-save w-4 h-4 mr-2"><path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path><polyline points="17 21 17 13 7 13 7 21"></polyline><polyline points="7 3 7 8 15 8"></polyline></svg>
                Enregistrer 
            </button>
        </center>
    </div>
</form>

@endsection
@section('script')
    <script>
    function previewFile(input){
        var file = $("input[type=file]").get(0).files[0];

        if(file){
            var reader = new FileReader();

            reader.onload = function(){
                $(".photo").attr("src", reader.result);
            }

            reader.readAsDataURL(file);
            // alert(file);
        }
    }
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // $('.users').attr('class','side-menu side-menu--active users');
        $('input').addClass('border border-gray-500');
        $('label').addClass('font-semibold');
        $('select').addClass('border border-gray-500');
        $('textarea').addClass('border border-gray-500');
        $('.users').attr('class','side-menu side-menu--active users');
        $('.role').change(function(){
            if($(this).val()=='Client'){
                $('.role_b').attr('class','grid lg:grid-cols-3 sm:grid-cols-1 gap-2 mt-2 role_b');
                $('.clients_b').show();
            }else{
                $('.role_b').attr('class','grid lg:grid-cols-2 sm:grid-cols-1 gap-2 mt-2 role_b');
                $('.clients_b').hide();
                $('.clients').val("");
            }
        });
        $('.photo').click(function(){
            $('.pdp').trigger('click');
        });
        
        $('.save').click(function(){
            event.preventDefault();
          
            var formData =new FormData(document.getElementById("form_data"));
            if($('.confirmation').val()!=null && $('.password').val()==$('.confirmation').val()){
                $('.confirmation').removeClass(" border-red-500");
                $('.alert_pwd').hide();
                $.ajax({
                    url:"{{ route('add_user') }}",
                    type:'post',
                    dataType:'json',
                    data:formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        if($('.role').val()=='Client'){
                            if($(".id_client").val()==0){
                                console.log($('.client').val());
                                url="{{route('clientUser',['id'])}}";
                                url=url.replace('id',data.id);
                                window.location.replace(url);
                                
                            }else{
                                // window.location.replace("{{route('users_list')}}");
                                Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Les données sont enregistrées',
                                showConfirmButton: true,
                                }).then((result) =>{
                                    if( result.isConfirmed )
                                    window.location.replace("{{route('users_list')}}");
                                });
                            }
                        }else{
                            // window.location.replace("{{route('users_list')}}");
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Les données sont enregistrées',
                                showConfirmButton: true,
                            }).then((result) =>{
                                if( result.isConfirmed )
                                window.location.replace("{{route('users_list')}}");
                            });
                        }
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
                // if($('.role').val()=='Client'){
                    
                // }else{
                //     $.ajax({
                //         url:"{{ route('add_user') }}",
                //         type:'post',
                //         dataType:'json',
                //         data:formData,
                //         processData: false,
                //         contentType: false,
                //         cache: false,
                //         success:function(data){
                //             console.log(data);
                            
                //         },
                //         error:function(error){
                //             console.log(error);
                //         },
                //     });
                // }
                    
            }
            if($('.confirmation').val()==null || $('.password').val()!=$('.confirmation').val()){

                $('.confirmation').addClass("border border-red-500");

                $('.alert_pwd').show();
            }
            
        });
        $('body').on('change','.cat',function(){
            if($(this).val() == 'Administrateur'){
                $('.privs').show();
            }else{
                $('.privs').hide();
            }
        });
    });
    </script>
@endsection