@extends('admin.admin')
@section('title')
    Liste des utilisateurs
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i
            data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Liste des
            utilisateurs</a> </div>
    <style>
        img {
            object-fit: cover;
            object-position: center center;
        }
    </style>
@endsection
@section('content')
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-8">
        <a href="{{ route('register_index') }}" class="button text-white bg-theme-1 shadow-md mr-2">Ajouter un nouveau
            utilisateur</a>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="hidden md:block mx-auto text-gray-600"></div>
    </div>
    <div class="intro-y box px-5 py-8 mt-5">
        <div class="intro-y col-span-12 xl:overflow-x-scroll">
            <table class="table table-report -mt-2 border-none" style="border-style: none !important;" id="table_id">
                <thead style="border-style: none !important;">
                    <tr class="shadow-lg">
                        <th class="whitespace-no-wrap text-center " style="border-style: none !important;">Photo</th>
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Nom</th>
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Prenom</th>
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Email</th>
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $item)
                        <tr class="shadow-md rounded-lg intro-y ">
                            <td class="w-16" align="center">
                                <div class="rounded-full p-3" style="width: 5.4rem;">
                                    {{-- <div class="text-center font-semibold">
                                {{$item->id}} <span class="font-semibold ">{{($item->id==Auth::user()->id)?'Connectée':''}}</span>
                            </div> --}}
                                    <div class="relative ">
                                        <img class="shadow-lg hover:shadow-xl  rounded-full w-16 h-16 photo"
                                            src="{{ !empty($item->photo) ? asset(App\File_path::where('type', 'users')->first()->file_path . $item->photo) : asset(App\File_path::where('type', 'users')->first()->file_path . 'Link.png') }}"
                                            alt="">
                                        <div
                                            class="{{ $item->id == Auth::user()->id ? 'block' : 'hidden' }} w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white">
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="w-40">
                                <div class="">
                                    <div class="text-center">
                                        {{ $item->nom }}
                                    </div>
                                </div>
                            </td>
                            <td class="w-40">
                                <div class="">
                                    <div class="text-center">
                                        {{ $item->prenom }}
                                    </div>
                                </div>
                            </td>
                            <td class="w-40">
                                <div class="">
                                    <div class="text-center">
                                        {{ $item->email }}
                                    </div>
                                </div>
                            </td>
                            <td class="table-report__action w-56">
                                <div class="flex justify-center items-center">
                                    <a class="flex items-center mr-3" href="{{ route('edit_userindex', [$item->id]) }}"> <i
                                            data-feather="check-square" class="w-4 h-4 mr-1"></i> Modifier </a>
                                    @if (Auth::user()->id != $item->id)
                                        <a class="flex items-center text-theme-6" href="javascript:;" data-toggle="modal"
                                            data-target="#delete-confirmation-modal{{ $item->id }}"> <i
                                                data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete </a>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <!-- BEGIN: Delete Confirmation Modal -->
                        <div class="modal" id="delete-confirmation-modal{{ $item->id }}">
                            <div class="modal__content">
                                <div class="p-5 text-center">
                                    <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                                    <div class="text-3xl mt-5">Are you sure?</div>
                                    <div class="text-gray-600 mt-2">Do you really want to delete these records? This process
                                        cannot be undone.</div>
                                </div>
                                <div class="px-5 pb-8 text-center">
                                    <button type="button" data-dismiss="modal"
                                        class="button w-24 border text-gray-700 mr-1">Annuler</button>
                                    <a class="button w-24 bg-theme-6 text-white"
                                        href="{{ route('delete_user', [$item->id]) }}">Supprimer</a>
                                </div>
                            </div>
                        </div>
                        <!-- END: Delete Confirmation Modal -->
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('dist/js/dataTbl.js') }}"></script>
@endsection
