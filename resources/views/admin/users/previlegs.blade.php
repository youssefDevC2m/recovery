<div class="grid grid-cols-2 gap-2">
    
    <div class="">
        <div class="text-center text-2xl font-semibold">
            <span>Privilèges utilisateurs</span>
        </div>
        @if (!isset($user->id) && Auth::user()->acs_user == 1 || Auth::user()->acs_user == 1 && $user->id != Auth::user()->id )
        <div class=" mb-3">

            <div class="intro-y box px-5 py-8 mt-5  ">
                <div class="">
                    <label for="" class="text-lg">Accès aux utilisateurs </label>
                    <div class="flex flex-col sm:flex-row mt-2">
                        <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                            <input type="radio" class="input border mr-2" id="acs_user0" name="acs_user" value="0" {{(isset($user->acs_user) && $user->acs_user==0)? 'checked' : ''}} {{(!isset($user->acs_user))? 'checked' : ''}}>
                            <label class="cursor-pointer select-none" for="acs_user0" >Non</label>
                        </div>
                        <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                            <input type="radio" class="input border mr-2" id="acs_user1" name="acs_user" value="1" {{(isset($user->acs_user) && $user->acs_user==1)? 'checked' : ''}}>
                            <label class="cursor-pointer select-none" for="acs_user1">Oui</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
        <div class=" mb-3">

            <div class="intro-y box px-5 py-8 mt-5  ">
                <div class="">
                    <label for="" class="text-lg">Accès aux privilèges </label>
                    <div class="flex flex-col sm:flex-row mt-2">
                        <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                            <input type="radio" class="input border mr-2" id="give_priv_user0" name="give_priv_user" value="0" {{(isset($user->give_priv_user) && $user->give_priv_user==0)? 'checked' : ''}} {{(!isset($user->give_priv_user))? 'checked' : ''}}>
                            <label class="cursor-pointer select-none" for="give_priv_user0" >Non</label>
                        </div>
                        <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                            <input type="radio" class="input border mr-2" id="give_priv_user1" name="give_priv_user" value="1" {{(isset($user->give_priv_user) && $user->give_priv_user==1)? 'checked' : ''}}>
                            <label class="cursor-pointer select-none" for="give_priv_user1">Oui</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endif
        <div class=" mb-3">

            <div class="intro-y box px-5 py-8 mt-5  ">
                <div class="">
                    <label for="" class="text-lg">Validation des actions </label>
                    <div class="flex flex-col sm:flex-row mt-2">
                        <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                            <input type="radio" class="input border mr-2" id="droit_de_valid_acts0" name="droit_de_valid_acts" value="0" {{(isset($user->droit_de_valid_acts) && $user->droit_de_valid_acts==0)? 'checked' : ''}} {{(!isset($user->droit_de_valid_acts))? 'checked' : ''}}>
                            <label class="cursor-pointer select-none" for="droit_de_valid_acts0" >Non</label>
                        </div>
                        <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                            <input type="radio" class="input border mr-2" id="droit_de_valid_acts1" name="droit_de_valid_acts" value="1" {{(isset($user->droit_de_valid_acts) && $user->droit_de_valid_acts==1)? 'checked' : ''}}>
                            <label class="cursor-pointer select-none" for="droit_de_valid_acts1">Oui</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>     
    </div>    
    
    <div>
        <div class="text-center text-2xl font-semibold">
            <span>Privilèges tableau de bord</span>
        </div>

        <div class=" mb-3">

            <div class="intro-y box px-5 py-8 mt-5  ">
                <div class="">
                    <label for="" class="text-lg">Accès au tableau de bord </label>
                    <div class="flex flex-col sm:flex-row mt-2">
                        <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                            <input type="radio" class="input border mr-2" id="acs_tabord0" name="acs_tabord" value="0" {{(isset($user->acs_tabord) && $user->acs_tabord==0)? 'checked' : ''}} {{(!isset($user->acs_tabord))? 'checked' : ''}}>
                            <label class="cursor-pointer select-none" for="acs_tabord0" >Non</label>
                        </div>
                        <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                            <input type="radio" class="input border mr-2" id="acs_tabord1" name="acs_tabord" value="1" {{(isset($user->acs_tabord) && $user->acs_tabord==1)? 'checked' : ''}}>
                            <label class="cursor-pointer select-none" for="acs_tabord1">Oui</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>    
    </div>
</div>




<div class="text-center text-2xl font-semibold">
    <span>Privilèges utilisateurs sur client</span>
</div>
<div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Accès aux clients</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_cli0"  name="acs_cli" value="0" {{(isset($user->acs_cli) && $user->acs_cli==0)? 'checked' : ''}} {{(!isset($user->acs_cli))? 'checked' : ''}} >
                    <label class="cursor-pointer select-none" for="acs_cli0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_cli1" name="acs_cli" value="1" {{(isset($user->acs_cli) && $user->acs_cli==1)? 'checked' : ''}} >
                    <label class="cursor-pointer select-none" for="acs_cli1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Ajouter des clients</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="add_cli0" name="add_cli" value="0" {{(isset($user->add_cli) && $user->add_cli==0)? 'checked' : ''}} {{(!isset($user->add_cli))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_cli0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="add_cli1" name="add_cli" value="1" {{(isset($user->add_cli) && $user->add_cli==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_cli1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Modifier les clients</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="edit_cli0" name="edit_cli" value="0"  {{(isset($user->edit_cli) && $user->edit_cli==0)? 'checked' : ''}} {{(!isset($user->edit_cli))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_cli0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="edit_cli1" name="edit_cli" value="1"  {{(isset($user->edit_cli) && $user->edit_cli==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_cli1">Oui</label>
                </div>
            </div>
        </div>
    </div>
    
    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Supprimer les clients</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="delet_cli0" name="delet_cli" value="0" {{(isset($user->delet_cli) && $user->delet_cli==0)? 'checked' : ''}} {{(!isset($user->delet_cli))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_cli0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="delet_cli1" name="delet_cli" value="1" {{(isset($user->delet_cli) && $user->delet_cli==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_cli1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Voir la liste des clients</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="list_cli0" name="list_cli" value="0" {{(isset($user->list_cli) && $user->list_cli==0)? 'checked' : ''}} {{(!isset($user->list_cli))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_cli0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="list_cli1" name="list_cli" value="1" {{(isset($user->list_cli) && $user->list_cli==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_cli1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Lecture seulement client</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="read_only_cli0" name="read_only_cli" value="0" {{(isset($user->read_only_cli) && $user->read_only_cli==0)? 'checked' : ''}} {{(!isset($user->read_only_cli))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="read_only_cli0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="read_only_cli1" name="read_only_cli" value="1" {{(isset($user->read_only_cli) && $user->read_only_cli==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="read_only_cli1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 tooltip" title="Voir la liste des dossiers client">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Voir la liste des dossiers client</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_doss_cli0" name="see_doss_cli" value="0" {{(isset($user->see_doss_cli) && $user->see_doss_cli==0)? 'checked' : ''}} {{(!isset($user->see_doss_cli))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_doss_cli0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_doss1" name="see_doss_cli" value="1" {{(isset($user->see_doss_cli) && $user->see_doss_cli==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_doss_cli1">Oui</label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="text-center text-2xl font-semibold my-3">
    <span>Privilèges utilisateurs sur dossiers</span>
</div>
<div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Accès aux dossiers</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_doss0" name="acs_doss" value="0" {{(isset($user->acs_doss) && $user->acs_doss==0)? 'checked' : ''}} {{(!isset($user->acs_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_doss1" name="acs_doss" value="1" {{(isset($user->acs_doss) && $user->acs_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Voir tous les dossiers</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_all_doss0" name="see_all_doss" value="0" {{(isset($user->see_all_doss) && $user->see_all_doss==0)? 'checked' : ''}} {{(!isset($user->see_all_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_all_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_all_doss1" name="see_all_doss" value="1" {{(isset($user->see_all_doss) && $user->see_all_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_all_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 tooltip" title="Accéder aux scènes de dossier">
        <div>
            <div class="truncate lg:w-40">
                <label for="" class="text-lg">Accéder aux scènes de dossier</label>
            </div>
            
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_scens_doss0" name="acs_scens_doss" value="0" {{(isset($user->acs_scens_doss) && $user->acs_scens_doss==0)? 'checked' : ''}} {{(!isset($user->acs_scens_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_scens_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_scens_doss1" name="acs_scens_doss" value="1" {{(isset($user->acs_scens_doss) && $user->acs_scens_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_scens_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Ajouter dossiers</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="add_doss0" name="add_doss" value="0" {{(isset($user->add_doss) && $user->add_doss==0)? 'checked' : ''}} {{(!isset($user->add_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="add_doss1" name="add_doss" value="1" {{(isset($user->add_doss) && $user->add_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Modifier dossiers</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="edit_doss0" name="edit_doss" value="0" {{(isset($user->edit_doss) && $user->edit_doss==0)? 'checked' : ''}} {{(!isset($user->edit_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="edit_doss1" name="edit_doss" value="1" {{(isset($user->edit_doss) && $user->edit_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Supprimer dossiers</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="delet_doss0" name="delet_doss" value="0" {{(isset($user->delet_doss) && $user->delet_doss==0)? 'checked' : ''}} {{(!isset($user->delet_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="delet_doss1" name="delet_doss" value="1" {{(isset($user->delet_doss) && $user->delet_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 ">
        <div>
            <label for="" class="text-lg">Liste des dossiers</label>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="list_doss0" name="list_doss" value="0" {{(isset($user->list_doss) && $user->list_doss==0)? 'checked' : ''}} {{(!isset($user->list_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="list_doss1" name="list_doss" value="1" {{(isset($user->list_doss) && $user->list_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 tooltip" title="Lecture seulement dossiers">
        <div>
            <div class="lg:w-40 truncate">
                <label for="" class="text-lg">Lecture seulement dossiers</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="read_only_doss0" name="read_only_doss" value="0" {{(isset($user->read_only_doss) && $user->read_only_doss==0)? 'checked' : ''}} {{(!isset($user->read_only_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="read_only_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="read_only_doss1" name="read_only_doss" value="1" {{(isset($user->read_only_doss) && $user->read_only_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="read_only_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 tooltip" title="Voir les clients liés aux dossiers">
        <div>
            <div class="lg:w-40 truncate">
                <label for="" class="text-lg">Voir les clients liés aux dossiers</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_info_cli_doss0" name="see_info_cli_doss" value="0" {{(isset($user->see_info_cli_doss) && $user->see_info_cli_doss==0)? 'checked' : ''}} {{(!isset($user->see_info_cli_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_info_cli_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_info_cli_doss1" name="see_info_cli_doss" value="1" {{(isset($user->see_info_cli_doss) && $user->see_info_cli_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_info_cli_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 tooltip" title="Modifier les infos des débiteur liés aux dossiers">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Modifier les infos des débiteur liés aux dossiers</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="edit_info_dbitr_doss0" name="edit_info_dbitr_doss" value="0" {{(isset($user->edit_info_dbitr_doss) && $user->edit_info_dbitr_doss==0)? 'checked' : ''}} {{(!isset($user->edit_info_dbitr_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_info_dbitr_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="edit_info_dbitr_doss1" name="edit_info_dbitr_doss" value="1" {{(isset($user->edit_info_dbitr_doss) && $user->edit_info_dbitr_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_info_dbitr_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 tooltip" title="Voir les infos des débiteur liés aux dossiers">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Voir les infos des débiteur liés aux dossiers</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_info_dbitr_doss0" name="see_info_dbitr_doss" value="0" {{(isset($user->see_info_dbitr_doss) && $user->see_info_dbitr_doss==0)? 'checked' : ''}} {{(!isset($user->see_info_dbitr_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_info_dbitr_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_info_dbitr_doss1" name="see_info_dbitr_doss" value="1" {{(isset($user->see_info_dbitr_doss) && $user->see_info_dbitr_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_info_dbitr_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 tooltip" title="Accéder au fermeture dossier">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Accéder au fermeture dossier</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_fermetur_doss0" name="see_fermetur_doss" value="0" {{(isset($user->see_fermetur_doss) && $user->see_fermetur_doss==0)? 'checked' : ''}} {{(!isset($user->see_fermetur_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_fermetur_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_fermetur_doss1" name="see_fermetur_doss" value="1" {{(isset($user->see_fermetur_doss) && $user->see_fermetur_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_fermetur_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5 tooltip" title="Accéder au fermeture dossier"> 
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Modifier au fermeture dossier</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="edit_fermetur_doss0" name="edit_fermetur_doss" value="0" {{(isset($user->edit_fermetur_doss) && $user->edit_fermetur_doss==0)? 'checked' : ''}} {{(!isset($user->edit_fermetur_doss))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_fermetur_doss0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="edit_fermetur_doss1" name="edit_fermetur_doss" value="1" {{(isset($user->edit_fermetur_doss) && $user->edit_fermetur_doss==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_fermetur_doss1">Oui</label>
                </div>
            </div>
        </div>
    </div>
    <div class="intro-y box px-5 py-8 mt-5" > 
        <div>
            <div class="w-80 truncate tooltip" title="Accès à la phase amiable">
                <label for="" class="text-lg">Accès à la phase amiable </label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_phas_ami0" name="acs_phas_ami" value="0" {{(isset($user->acs_phas_ami) && $user->acs_phas_ami==0)? 'checked' : ''}} {{(!isset($user->acs_phas_ami))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_phas_ami0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_phas_ami1" name="acs_phas_ami" value="1" {{(isset($user->acs_phas_ami) && $user->acs_phas_ami==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_phas_ami1">Oui</label>
                </div>
            </div>
        </div>
    </div>
    <div class="intro-y box px-5 py-8 mt-5" > 
        <div>
            <div class="w-80 truncate tooltip" title="Accès à la phase judiciaire" >
                <label for="" class="text-lg">Accès à la phase judiciaire </label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_phas_judis0" name="acs_phas_judis" value="0" {{(isset($user->acs_phas_judis) && $user->acs_phas_judis==0)? 'checked' : ''}} {{(!isset($user->acs_phas_judis))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_phas_judis0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_phas_judis1" name="acs_phas_judis" value="1" {{(isset($user->acs_phas_judis) && $user->acs_phas_judis==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_phas_judis1">Oui</label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="text-center text-2xl font-semibold my-3">
    <span>Privilèges utilisateurs sur devis</span>
</div>
<div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">
    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Accéder au devis</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_devis0" name="acs_devis" value="0" {{(isset($user->acs_devis) && $user->acs_devis==0)? 'checked' : ''}} {{(!isset($user->acs_devis))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_devis0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_devis1" name="acs_devis" value="1" {{(isset($user->acs_devis) && $user->acs_devis==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_devis1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Ajouter les devis</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="add_devis0" name="add_devis" value="0" {{(isset($user->add_devis) && $user->add_devis==0)? 'checked' : ''}} {{(!isset($user->add_devis))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_devis0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="add_devis1" name="add_devis" value="1" {{(isset($user->add_devis) && $user->add_devis==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_devis1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Modifier les devis</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="edit_devis0" name="edit_devis" value="0" {{(isset($user->edit_devis) && $user->edit_devis==0)? 'checked' : ''}} {{(!isset($user->edit_devis))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_devis0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="edit_devis1" name="edit_devis" value="1" {{(isset($user->edit_devis) && $user->edit_devis==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_devis1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Supprimer les devis</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="delet_devis0" name="delet_devis" value="0" {{(isset($user->delet_devis) && $user->delet_devis==0)? 'checked' : ''}} {{(!isset($user->delet_devis))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_devis0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="delet_devis1" name="delet_devis" value="1" {{(isset($user->delet_devis) && $user->delet_devis==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_devis1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Liste des devis</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="list_devis0" name="list_devis" value="0" {{(isset($user->list_devis) && $user->list_devis==0)? 'checked' : ''}} {{(!isset($user->list_devis))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_devis0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="list_devis1" name="list_devis" value="1" {{(isset($user->list_devis) && $user->list_devis==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_devis1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Imprimer le devis</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="print_devis0" name="print_devis" value="0" {{(isset($user->print_devis) && $user->print_devis==0)? 'checked' : ''}} {{(!isset($user->print_devis))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="print_devis0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="print_devis1" name="print_devis" value="1" {{(isset($user->print_devis) && $user->print_devis==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="print_devis1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Lecture seulement devis</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_devis0" name="see_devis" value="0" {{(isset($user->see_devis) && $user->see_devis==0)? 'checked' : ''}} {{(!isset($user->see_devis))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_devis0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_devis1" name="see_devis" value="1" {{(isset($user->see_devis) && $user->see_devis==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_devis1">Oui</label>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="text-center text-2xl font-semibold my-3">
    <span>Privilèges utilisateurs sur facture</span>
</div>
<div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Accéder au facture</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_fact0" name="acs_fact" value="0" {{(isset($user->acs_fact) && $user->acs_fact==0)? 'checked' : ''}} {{(!isset($user->acs_fact))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_fact0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_fact1" name="acs_fact" value="1" {{(isset($user->acs_fact) && $user->acs_fact==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_fact1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Ajouter facture</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="add_fact0" name="add_fact" value="0" {{(isset($user->add_fact) && $user->add_fact==0)? 'checked' : ''}} {{(!isset($user->add_fact))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_fact0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="add_fact1" name="add_fact" value="1" {{(isset($user->add_fact) && $user->add_fact==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_fact1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Modifier facture</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="edit_fact0" name="edit_fact" value="0" {{(isset($user->edit_fact) && $user->edit_fact==0)? 'checked' : ''}} {{(!isset($user->edit_fact))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_fact0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="edit_fact1" name="edit_fact" value="1" {{(isset($user->edit_fact) && $user->edit_fact==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_fact1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Supprimer factures</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="delet_fact0" name="delet_fact" value="0" {{(isset($user->delet_fact) && $user->delet_fact==0)? 'checked' : ''}} {{(!isset($user->delet_fact))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_fact0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="delet_fact1" name="delet_fact" value="1" {{(isset($user->delet_fact) && $user->delet_fact==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_fact1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Voir la liste des factures</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="list_fact0" name="list_fact" value="0" {{(isset($user->list_fact) && $user->list_fact==0)? 'checked' : ''}} {{(!isset($user->list_fact))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_fact0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="list_fact1" name="list_fact" value="1" {{(isset($user->list_fact) && $user->list_fact==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_fact1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Lecture seulement factures</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_fact0" name="see_fact" value="0" {{(isset($user->see_fact) && $user->see_fact==0)? 'checked' : ''}} {{(!isset($user->see_fact))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_fact0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_fact1" name="see_fact" value="1" {{(isset($user->see_fact) && $user->see_fact==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_fact1">Oui</label>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="text-center text-2xl font-semibold my-3">
    <span>Privilèges utilisateurs sur règlements</span>
</div>
<div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Accéder aux règlements</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_rglmnt0" name="acs_rglmnt" value="0" {{(isset($user->acs_rglmnt) && $user->acs_rglmnt==0)? 'checked' : ''}} {{(!isset($user->acs_rglmnt))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_rglmnt0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_rglmnt1" name="acs_rglmnt" value="1" {{(isset($user->acs_rglmnt) && $user->acs_rglmnt==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_rglmnt1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Ajouter règlements</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="add_rglmnt0" name="add_rglmnt" value="0" {{(isset($user->add_rglmnt) && $user->add_rglmnt==0)? 'checked' : ''}} {{(!isset($user->add_rglmnt))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_rglmnt0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="add_rglmnt1" name="add_rglmnt" value="1" {{(isset($user->add_rglmnt) && $user->add_rglmnt==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_rglmnt1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Modifier règlements</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="edit_rglmnt0" name="edit_rglmnt" value="0" {{(isset($user->edit_rglmnt) && $user->edit_rglmnt==0)? 'checked' : ''}} {{(!isset($user->edit_rglmnt))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_rglmnt0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="edit_rglmnt1" name="edit_rglmnt" value="1" {{(isset($user->edit_rglmnt) && $user->edit_rglmnt==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_rglmnt1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Supprimer règlements</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="delet_rglmnt0" name="delet_rglmnt" value="0" {{(isset($user->delet_rglmnt) && $user->delet_rglmnt==0)? 'checked' : ''}} {{(!isset($user->delet_rglmnt))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_rglmnt0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="delet_rglmnt1" name="delet_rglmnt" value="1" {{(isset($user->delet_rglmnt) && $user->delet_rglmnt==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_rglmnt1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Voir la liste des règlements</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="list_rglmnt0" name="list_rglmnt" value="0" {{(isset($user->list_rglmnt) && $user->list_rglmnt==0)? 'checked' : ''}} {{(!isset($user->list_rglmnt))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_rglmnt0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="list_rglmnt1" name="list_rglmnt" value="1" {{(isset($user->list_rglmnt) && $user->list_rglmnt==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_rglmnt1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Lecture seulement règlements</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_rglmnt0" name="see_rglmnt" value="0" {{(isset($user->see_rglmnt) && $user->see_rglmnt==0)? 'checked' : ''}} {{(!isset($user->see_rglmnt))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_rglmnt0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_rglmnt" name="see_rglmnt" value="1" {{(isset($user->see_rglmnt) && $user->see_rglmnt==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_rglmnt">Oui</label>
                </div>
            </div>
        </div>
    </div>

</div>


<div class="text-center text-2xl font-semibold my-3">
    <span>Privilèges utilisateurs sur charges</span>
</div>
<div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Accéder aux charges</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_charge0" name="acs_charge" value="0" {{(isset($user->acs_charge) && $user->acs_charge==0)? 'checked' : ''}} {{(!isset($user->acs_charge))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_charge0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_charge1" name="acs_charge" value="1" {{(isset($user->acs_charge) && $user->acs_charge==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_charge1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Ajouter charges</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="add_charge0" name="add_charge" value="0" {{(isset($user->add_charge) && $user->add_charge==0)? 'checked' : ''}} {{(!isset($user->add_charge))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_charge0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="add_charge1" name="add_charge" value="1" {{(isset($user->add_charge) && $user->add_charge==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_charge1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Modifier charges</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="edit_charge0" name="edit_charge" value="0" {{(isset($user->edit_charge) && $user->edit_charge==0)? 'checked' : ''}} {{(!isset($user->edit_charge))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_charge0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="edit_charge1" name="edit_charge" value="1" {{(isset($user->edit_charge) && $user->edit_charge==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_charge1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Supprimer charges</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="delet_charge0" name="delet_charge" value="0" {{(isset($user->delet_charge) && $user->delet_charge==0)? 'checked' : ''}} {{(!isset($user->delet_charge))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_charge0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="delet_charge1" name="delet_charge" value="1" {{(isset($user->delet_charge) && $user->delet_charge==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_charge1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Voir la liste des charges</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="list_charge0" name="list_charge" value="0" {{(isset($user->list_charge) && $user->list_charge==0)? 'checked' : ''}} {{(!isset($user->list_charge))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_charge0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="list_charge1" name="list_charge" value="1" {{(isset($user->list_charge) && $user->list_charge==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_charge1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Lecture seulement charges</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_charge0" name="see_charge" value="0" {{(isset($user->see_charge) && $user->see_charge==0)? 'checked' : ''}} {{(!isset($user->acs_charge))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_charge0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_charge1" name="see_charge" value="1" {{(isset($user->see_charge) && $user->see_charge==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_charge1">Oui</label>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="text-center text-2xl font-semibold my-3">
    <span>Privilèges utilisateurs sur partenaires</span>
</div>
<div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Accéder aux partenaires</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_parten0" name="acs_parten" value="0" {{(isset($user->acs_parten) && $user->acs_parten==0)? 'checked' : ''}} {{(!isset($user->acs_parten))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_parten0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_parten1" name="acs_parten" value="1" {{(isset($user->acs_parten) && $user->acs_parten==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_parten1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Ajouter partenaires</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="add_parten0" name="add_parten" value="0" {{(isset($user->add_parten) && $user->add_parten==0)? 'checked' : ''}} {{(!isset($user->add_parten))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_parten0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="add_parten1" name="add_parten" value="1" {{(isset($user->add_parten) && $user->add_parten==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_parten1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Modifier partenaires</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="edit_parten0" name="edit_parten" value="0" {{(isset($user->edit_parten) && $user->edit_parten==0)? 'checked' : ''}} {{(!isset($user->edit_parten))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_parten0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="edit_parten1" name="edit_parten" value="1" {{(isset($user->edit_parten) && $user->edit_parten==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_parten1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Supprimer partenaires</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="delet_parten0" name="delet_parten" value="0" {{(isset($user->delet_parten) && $user->delet_parten==0)? 'checked' : ''}} {{(!isset($user->delet_parten))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_parten0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="delet_parten1" name="delet_parten" value="1" {{(isset($user->delet_parten) && $user->delet_parten==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_parten1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Voir la liste des partenaires</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="list_parten0" name="list_parten" value="0" {{(isset($user->list_parten) && $user->list_parten==0)? 'checked' : ''}} {{(!isset($user->list_parten))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_parten0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="list_parten1" name="list_parten" value="1" {{(isset($user->list_parten) && $user->list_parten==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_parten1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Lecture seulement partenaires</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_parten0" name="see_parten" value="0" {{(isset($user->see_parten) && $user->see_parten==0)? 'checked' : ''}} {{(!isset($user->see_parten))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_parten0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_parten1" name="see_parten" value="1" {{(isset($user->see_parten) && $user->see_parten==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_parten1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate tooltip" title="Voir la liste des dossiers partenaires">
                <label for="" class="text-lg ">Voir la liste des dossiers partenaires</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_infos_parten0" name="see_infos_parten" value="0" {{(isset($user->see_infos_parten) && $user->see_infos_parten==0)? 'checked' : ''}} {{(!isset($user->see_infos_parten))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_infos_parten0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_infos_parten1" name="see_infos_parten" value="1" {{(isset($user->see_infos_parten) && $user->see_infos_parten==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_infos_parten1">Oui</label>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="text-center text-2xl font-semibold my-3">
    <span>Privilèges utilisateurs sur gestionnaire des tâches</span>
</div>
<div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Accéder aux gestionnaire des tâches</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="acs_task0" name="acs_task" value="0" {{(isset($user->acs_task) && $user->acs_task==0)? 'checked' : ''}} {{(!isset($user->acs_task))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_task0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="acs_task1" name="acs_task" value="1" {{(isset($user->acs_task) && $user->acs_task==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="acs_task1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Ajouter tâches</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="add_task0" name="add_task" value="0" {{(isset($user->add_task) && $user->add_task==0)? 'checked' : ''}} {{(!isset($user->add_task))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_task0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="add_task1" name="add_task" value="1" {{(isset($user->add_task) && $user->add_task==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="add_task1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Modifier tâches</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="edit_task0" name="edit_task" value="0" {{(isset($user->edit_task) && $user->edit_task==0)? 'checked' : ''}} {{(!isset($user->edit_task))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_task0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="edit_task1" name="edit_task" value="1" {{(isset($user->edit_task) && $user->edit_task==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="edit_task1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Supprimer tâches</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="delet_task0" name="delet_task" value="0" {{(isset($user->delet_task) && $user->delet_task==0)? 'checked' : ''}} {{(!isset($user->delet_task))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_task0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="delet_task1" name="delet_task" value="1" {{(isset($user->delet_task) && $user->delet_task==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="delet_task1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Liste des tâches</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="list_task0" name="list_task" value="0" {{(isset($user->list_task) && $user->list_task==0)? 'checked' : ''}} {{(!isset($user->list_task))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_task0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="list_task1" name="list_task" value="1" {{(isset($user->list_task) && $user->list_task==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="list_task1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Suivie tâches</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="suivi_task0" name="suivi_task" value="0" {{(isset($user->suivi_task) && $user->suivi_task==0)? 'checked' : ''}} {{(!isset($user->suivi_task))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="suivi_task0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="suivi_task1" name="suivi_task" value="1" {{(isset($user->suivi_task) && $user->suivi_task==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="suivi_task1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate tooltip" title="Lecture seulement tâches">
                <label for="" class="text-lg">Lecture seulement tâches</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="see_dt_task0" name="see_dt_task" value="0" {{(isset($user->see_dt_task) && $user->see_dt_task==0)? 'checked' : ''}} {{(!isset($user->see_dt_task))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="see_dt_task0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="see_dt_task1" name="see_dt_task" value="1" {{(isset($user->see_dt_task) && $user->see_dt_task==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="suivi_task1">Oui</label>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div>
            <div class="w-80 truncate">
                <label for="" class="text-lg">Valider  tâches</label>
            </div>
            <div class="flex flex-col sm:flex-row mt-2">
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2">
                    <input type="radio" class="input border mr-2" id="valid_task0" name="valid_task" value="0" {{(isset($user->valid_task) && $user->valid_task==0)? 'checked' : ''}} {{(!isset($user->valid_task))? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="valid_task0" >Non</label>
                </div>
                <div class="flex items-center text-gray-700 dark:text-gray-500 mr-2 mt-2 sm:mt-0">
                    <input type="radio" class="input border mr-2" id="valid_task1" name="valid_task" value="1" {{(isset($user->valid_task ) && $user->valid_task ==1)? 'checked' : ''}}>
                    <label class="cursor-pointer select-none" for="valid_task1">Oui</label>
                </div>
            </div>
        </div>
    </div>

</div>


