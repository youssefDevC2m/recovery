@extends('admin.admin')
@section('title')
Liste des partenaires
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Liste des partenaires</a> </div>
@endsection
@section('content')
@if (Auth::user()->add_parten == 1)
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-8">
        <a href="{{route('partForm')}}" class="button text-white bg-theme-1 shadow-md mr-2">Ajouter des partenaires</a>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="hidden md:block mx-auto text-gray-600"></div>
    </div>    
@endif

<div class="intro-y box px-5 py-8 mt-5">
    <div class="intro-y col-span-12 overflow-y-scroll xl:overflow-x-scroll" style="height: 30rem;">
        <table class="table table-report -mt-2 border-none" style="border-style: none !important;" id="table_id">
            <thead style="border-style: none !important;">
                <tr class="shadow-lg">
                    <th class="whitespace-no-wrap text-center " style="border-style: none !important;"></th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Nom-prenom</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Téléphone </th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Adresse</th>
                    @if (Auth::user()->edit_parten == 1 || Auth::user()->delet_parten == 1 || Auth::user()->see_infos_parten == 1 || Auth::user()->see_parten == 1)
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Actions</th>
                    @endif
                    
                </tr>
            </thead>
            <tbody>
                @foreach ($partner as $item)
                <?php
                    $create = App\HistoriqueAct::where('id_action',$item->id)
                    ->where('where','partner')
                    ->where('what','create')
                    ->orderby('id','desc')
                    ->first();
                    
                    $delete = App\HistoriqueAct::where('id_action',$item->id)
                    ->where('where','partner')
                    ->where('what','delete')
                    ->orderby('id','desc')
                    ->first();
                    
                    if(isset($create->id) && $create->id_user != Auth::user()->id && $create->id_valid <= 0 ){
                        continue;
                    }
                    if(isset($delete->id) && $delete->id_user == Auth::user()->id && $delete->id_valid == 0 ){
                        continue;
                    }
                ?>
                <tr class="shadow-md rounded-lg intro-y " >
                    <td class="w-16" align="center">
                        <div class="rounded-full p-3"  style="width: 5.4rem;">
                            <div class="text-center font-semibold">
                               {{$item->type_partn}}
                            </div> 
                        </div> 
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{$item->nom_part}} {{$item->prenom_part}}
                            </div>
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{$item->tele_part}}
                            </div>
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{$item->adrs_part }}
                            </div>
                        </div>
                    </td>
                    @if (Auth::user()->edit_parten == 1 || Auth::user()->delet_parten == 1 || Auth::user()->see_infos_parten == 1 || Auth::user()->see_parten == 1)
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                @if (Auth::user()->edit_parten == 1)
                                    <a class="flex items-center" href="{{route('showPart',[$item->id])}}"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-square w-4 h-4 mr-1"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                        Modifier 
                                    </a>    
                                @endif
                                @if (Auth::user()->see_parten == 1)
                                    <a class="flex items-center" href="{{route('showPart',[$item->id])}}"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-square w-4 h-4 mr-1"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                        Aperçu 
                                    </a>    
                                @endif
                                @if (Auth::user()->delet_parten == 1)
                                    <a class="flex items-center text-theme-6 mx-3" href="javascript:;" data-toggle="modal" data-target="#delete-confirmation-modal{{$item->id}}"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 w-4 h-4 mr-1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                        Delete 
                                    </a>    
                                @endif
                                @if (Auth::user()->see_infos_parten == 1)
                                    <div class="dropdown "> 
                                        <button class="dropdown-toggle button px-2 mr-1 mb-2 bg-theme-33 text-white m-2"><i data-feather="plus"></i></button>
                                        <div class="dropdown-box h-16 w-40">
                                            <div class="dropdown-box__content box dark:bg-dark-1 p-2"> 
                                                <a href="{{route('part_work',[$item->id])}}" class="block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md flex items-center">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-alert-circle w-4 h-4 mr-1"><circle cx="12" cy="12" r="10"></circle>
                                                        <line x1="12" y1="8" x2="12" y2="12"></line>
                                                        <line x1="12" y1="16" x2="12.01" y2="16"></line>
                                                    </svg>
                                                    info partenaire
                                                </a> 
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </td>
                    @endif
                </tr>

                <!-- BEGIN: Delete Confirmation Modal -->
                    <div class="modal" id="delete-confirmation-modal{{$item->id}}">
                        <div class="modal__content">
                            <div class="p-5 text-center">
                                <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
                                <div class="text-3xl mt-5">Suppression !</div>
                                <div class="text-gray-600 mt-2">Voulez-vous vraiment supprimer ces enregistrements ? Il n'y pas de retour en arriere.</div>
                            </div>
                            <div class="px-5 pb-8 text-center">
                                <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Annuler</button>
                                <a class="button w-24 bg-theme-6 text-white" href="{{route('deletePart',[$item->id])}}">Supprimer</a>
                            </div>

                        </div>
                    </div>
                <!-- END: Delete Confirmation Modal -->
                @endforeach
            </tbody>
        </table>
    </div>
</div>   
@endsection
@section('script')
    <script src="{{asset('dist/js/dataTbl.js')}}"></script>
    <script>
        $("body").ready(function(){
            $('.partners').addClass('side-menu--active');
        });
    </script>
@endsection