@extends('admin.admin')
@section('title')
Ajouter des Partenaires
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ajouter des Partenaires</a> </div>
@endsection
@section('content')
<form action="{{route('creatPart')}}" method="get" id="form_data" enctype="multipart/form-data">
    <div class="flex flex-row ">
        <div class="intro-x box w-64 mt-5 font-semibold mr-2">
            <div class="p-5">
                <label for="type_partn">Type de partenaire</label>
                <select name="type_partn" id="type_partn" required class="input type_partn w-full border border-gray-500 mt-2">
                    <option value="0">Sélectionnez un type</option>
                    <option value="Avocat">Avocat</option>
                    <option value="Huissier">Huissier</option>
                    <option value="Collaborateur">Collaborateur</option>
                </select>
            </div>
        </div>
        <div class="intro-x box w-64 mt-5 font-semibold agents hidden">
            <div class="p-5">
                <label for="type_partn">Collaborateurs</label>
                <select name="agent_id" id="list_agents" required class="input list_agents w-full border border-gray-500 mt-2 ">
                    <option value="0" selected>Sélectionnez un Collaborateur</option>
                    @foreach ($agents as $item)
                       <option value="{{$item->id}}">{{$item->nom}} {{$item->prenom}}</option> 
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="-intro-x box w-full mt-5 font-semibold p-3">
        <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Nom</label>
                <input type="text" name="nom_part" required placeholder="Entre le nom" class="nom_part input w-full border border-gray-500 mt-2">
                
            </div>
            <div class="mt-2">
                <label for="">Prenom</label>
                <input type="text" name="prenom_part" required placeholder="Entre le prenom" class="prenom_part input w-full border border-gray-500 mt-2">
            </div>
        </div>
        <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Adresse</label>
                <input type="text" name="adrs_part"  placeholder="Entre la adresse" class="adrs_part input w-full border border-gray-500 mt-2">
            </div>
            <div class="mt-2">
                <label for="">Adresse cabinet</label>
                <input type="text" name="adrs_act_part"  placeholder="Entre la adresse cabinet" class="adrs_act_part input w-full border border-gray-500 mt-2">
            </div>
        </div>
        <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Téléphone</label>
                <input type="tel" name="tele_part"  placeholder="Entre le téléphone" class="tele_part input w-full border border-gray-500 mt-2">
            </div>
            <div class="mt-2">
                <label for="">Téléphone <span style="color: rgb(118, 247, 118)">Whatsapp</span></label>
                <input type="tel" name="tele_wts_part"  placeholder="Entre le Whatsapp" class="tele_wts_part input w-full border border-gray-500 mt-2">
            </div>
            <div class="mt-2">
                <label for="">E-mail</label>
                <input type="email" name="email_part"  placeholder="Entre le email" class="email_part input w-full border border-gray-500 mt-2">
            </div>
        </div>
        <div class="grid justify-items-center m-3">
            <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white"> 
                <i data-feather="activity" class="w-4 h-4 mr-2"></i>Enregistrer  
            </button>
        </div>
    </div>
</form>
    
@endsection
@section('script')
    <script src="{{asset('dist/js/dataTbl.js')}}"></script>
    <script>
        $("body").ready(function(){
            $('.partners').addClass('side-menu--active');
            $('.type_partn').change(function(){
                if($(this).val()=='Collaborateur'){
                    $('.agents').show();
                    $(this).parent().parent().parent().parent().find('input').prop('readonly',false);
                    $(this).parent().parent().parent().parent().find('input').val("");
                }else{
                    $('.agents').hide();
                    $('.list_agents').val(0);
                    $(this).parent().parent().parent().parent().find('input').val("");
                }
            });
            $('body').on('change','.list_agents',function(){
                var agents = $(this).val();
                $(this).parent().parent().parent().parent().find('input').prop('readonly',false);
              
                $.ajax({
                    url:`{{route('getAgents')}}`,
                    data:{agents:agents},
                    dataType:'json',
                    type:'post',
                    success:function(data){
                        $('.nom_part').val(data.nom);
                        $('.nom_part').prop('readonly',((data.nom!=null)? true : false));
                        $('.prenom_part').val(data.prenom);
                        $('.prenom_part').prop('readonly',((data.prenom!=null)? true : false));
                        $('.tele_part').val(data.tele);
                        $('.tele_part').prop('readonly',((data.tele!=null)? true : false));
                        $('.adrs_part').val(data.address);
                        $('.adrs_part').prop('readonly',((data.address!=null)? true : false));
                        $('.email_part').val(data.email);
                        $('.email_part').prop('readonly',((data.email!=null)? true : false));
                        // console.log('tel'+data.tele);
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });
        });
    </script>
@endsection