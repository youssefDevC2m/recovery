@extends('admin.admin')
@section('title')
Modifier Partenaire
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Modifier Partenaire</a> </div>
@endsection
@section('content')
<form action="{{route('editPart')}}" method="get" id="form_data" enctype="multipart/form-data">
    <input type="hidden" name="id_part" value="{{$partner->id}}">
    <div class="intro-x box w-64 mt-5 font-semibold ">
        <div class="p-5">
            <label for="type_partn">Type de partenaire</label>
            <select name="type_partn" id="type_partn" required class="input type_partn w-full border border-gray-500 mt-2">
                <option value="0">Sélectionnez un type</option>
                <option value="Avocat" {{($partner->type_partn=='Avocat')? 'selected' : ''}}>Avocat</option>
                <option value="Huissier" {{($partner->type_partn=='Huissier')? 'selected' : ''}}>Huissier</option>
                <option value="Collaborateur" {{($partner->type_partn=='Collaborateur')? 'selected' : ''}}>Collaborateur</option>
            </select>
        </div>
    </div>
    <div class="-intro-x box w-full mt-5 font-semibold p-3">
        <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Nom</label>
                <input type="text" name="nom_part" required value="{{$partner->nom_part}}" placeholder="Entre le nom" class="input w-full border border-gray-500 mt-2">
            </div>
            <div class="mt-2">
                <label for="">Prenom</label>
                <input type="text" name="prenom_part" required value="{{$partner->prenom_part}}"  placeholder="Entre le prenom" class="input w-full border border-gray-500 mt-2">
            </div>
        </div>
        <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Adresse</label>
                <input type="text" name="adrs_part" value="{{$partner->adrs_part}}" placeholder="Entre la adresse" class="input w-full border border-gray-500 mt-2">
            </div>
            <div class="mt-2">
                <label for="">Adresse cabinet</label>
                <input type="text" name="adrs_act_part" value="{{$partner->adrs_act_part}}"  placeholder="Entre la adresse cabinet" class="input w-full border border-gray-500 mt-2">
            </div>
        </div>
        <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Téléphone</label>
                <input type="tel" name="tele_part" value="{{$partner->tele_part}}" placeholder="Entre le téléphone" class="input w-full border border-gray-500 mt-2">
            </div>
            <div class="mt-2">
                <label for="">Téléphone <span style="color: rgb(118, 247, 118)">Whatsapp</span></label>
                <input type="tel" name="tele_wts_part" value="{{$partner->tele_wts_part}}"  placeholder="Entre le Whatsapp" class="input w-full border border-gray-500 mt-2">
            </div>
            <div class="mt-2">
                <label for="">E-mail</label>
                <input type="email" name="email_part" value="{{$partner->email_part}}" placeholder="Entre le email" class="input w-full border border-gray-500 mt-2">
            </div>
        </div>
        @if (Auth::user()->edit_parten == 1)
            <div class="grid justify-items-center m-3">
                <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white"> 
                    <i data-feather="activity" class="w-4 h-4 mr-2"></i>Enregistrer  
                </button>
            </div>    
        @endif
        
    </div>
</form>
    
@endsection
@section('script')
    <script src="{{asset('dist/js/dataTbl.js')}}"></script>
    <script>
        $("body").ready(function(){
            $('.partners').addClass('side-menu--active');
        });
    </script>
@endsection