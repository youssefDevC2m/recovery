@extends('admin.admin')
@section('title')
Détails du partenaire
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Détails du partenaire</a> </div>
@endsection
@section('content')
<form action="" id="form_data" enctype="multipart/form-data">
    <input type="hidden" name="id_part" value="{{$id_part}}">
    <input type="hidden" name="id_dt" value="{{$id_dt}}">
    <div class="intro-x box w-full mt-5 font-semibold p-3">
        <div class="">
            <div class="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 gap-2">
                <div class="">
                    <label for="">Dossier</label>
                    <select name="doss" class="input doss w-full border border-gray-500 mt-2">
                        <option value="0">Sélectionnez un dossier</option>        
                        @foreach ($Dossier as $item)
                            <option value="{{$item->id}}" {{($partDet->doss==$item->id)? 'selected' : ''}}>{{$item->ref}}</option>        
                        @endforeach
                    </select>
                </div>
                <div class="">
                    <label for="">Débiteur</label>
                    <select name="dbteur" class="input dbteur w-full border border-gray-500 mt-2">
                        <option value="{{$partDet->id_dbtr}}">{{($partDet->type_dtbr=='preso')? $partDet->nom.' '.$partDet->prenom : $partDet->nom}}</option>        
                    </select>
                </div>
                <div class="">
                    <label for="">Ville débiteur</label>
                    <input type="text" name="vill_debtr" value="{{$partDet->vill_debtr}}" placeholder="Ville débiteur" class="input vill_debtr w-full border border-gray-500 mt-2">
                </div>
            </div>
            <div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2 mt-3">
                <div class="">
                    <label for="">Montant créance</label>
                    <input type="number" name="mt_creance" min=0 value="{{$partDet->mt_creance}}" placeholder="Montant créance" class="input w-full border border-gray-500 mt-2">
                </div>
                <div class="">
                    <label for="">Nature créance </label>
                    <select id=""name="nature_creance" class="input w-full border border-gray-500 mt-2">
                        <option value="Chèque" {{($partDet->nature_creance=='Chèque')? 'selected' : ''}}>Chèque</option>
                        <option value="Effect" {{($partDet->nature_creance=='Effect')? 'selected' : ''}}>Effect</option>
                        <option value="Facture" {{($partDet->nature_creance=='Facture')? 'selected' : ''}}>Facture</option>
                        <option value="Autre" {{($partDet->nature_creance=='Autre')? 'selected' : ''}}>Autre</option>
                    </select>
                </div>
                <div class="">
                    <label for="">Motif créance</label>
                    <select name="motif_creance" class="input w-full border border-gray-500 mt-2">
                        <option value="Signature non conforme" {{($partDet->motif_creance=='Signature non conforme')? 'selected' : ''}}>Signature non conforme</option>
                        <option value="Opposition pour perte ou vol" {{($partDet->motif_creance=='Opposition pour perte ou vol')? 'selected' : ''}}>Opposition pour perte ou vol</option>
                        <option value="Endos irrégulier" {{($partDet->motif_creance=='Endos irrégulier')? 'selected' : ''}} >Endos irrégulier</option>
                        <option value="Non conformité de la somme en lettre et en chiffre" {{($partDet->motif_creance=='Non conformité de la somme en lettre et en chiffre')? 'selected' : ''}} >Non conformité de la somme en lettre et en chiffre</option>
                        <option value="Décédé" {{($partDet->motif_creance=='Décédé')? 'selected' : ''}}>Décédé</option>
                        <option value="Prescrit" {{($partDet->motif_creance=='Prescrit')? 'selected' : ''}}>Prescrit</option>
                        <option value="Compte clôturé" {{($partDet->motif_creance=='Compte clôturé')? 'selected' : ''}}>Compte clôturé</option>
                        <option value="Opposition de paiement" {{($partDet->motif_creance=='Opposition de paiement')? 'selected' : ''}}>Opposition de paiement</option>
                        <option value="Autres" {{($partDet->motif_creance=='Autres')? 'selected' : ''}}>Autres</option>
                    </select>
                </div>
                
                <div>
                    <label for="">date de transfert</label>
                    <input type="date" name="date_trans" value="{{date('Y-m-d',strtotime($partDet->date_trans))}}" class="input w-full border border-gray-500 mt-2">
                </div>
            </div>
        </div>
    </div> 
    <div class="-intro-x box w-full mt-5 font-semibold p-3">
        <div class="w-full">
            <label for="">Letter transfert</label>
            <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                <div class="flex flex-wrap px-4 file_block" >
                    @if (count($files)>0)
                        @foreach ($files as $item)
                            <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2">
                                <a href="{{asset($File_path->file_path.$item->file_name)}}" target="_blank">
                                    <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                        <span class="w-3/5 file__icon file__icon--file mx-auto">
                                            <div class="file__icon__file-name"></div>
                                        </span>
                                        <span class="block font-medium mt-4 text-center truncate">{{$item->org_file_name}}</span>
                                    </div>
                                </a>
                                <input type="text" name="" id="">
                                <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file_base " data-id="kill_file_base{{$item->id}}"> 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                                </div>
                            </div>
                            <input type="hidden" name="file_exc[]" value="{{$item->file_name}}" class="kill_file_base{{$item->id}}">
                        @endforeach
                    @endif
                </div>
                <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                    <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                        <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                    </button>
                    <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0" name="file_val[]" data-stp="not">
                </div>
            </div>
            <textarea name="remarque" class="input w-full h-32 border border-gray-500 mt-2" placeholder="Remarque...">{{$partDet->remarque}}</textarea>
        </div>
    </div>
    <div class="grid justify-items-center mt-5">
        <button class="save button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white"> 
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-activity w-4 h-4 mr-2"><polyline points="22 12 18 12 15 21 9 3 6 12 2 12"></polyline></svg>
            Enregistrer  
        </button>
    </div>
</form>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('body').on('click','.save',function(){
                event.preventDefault();
                var formData = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:`{{route('editDetail')}}`,
                    type:'post',
                    dataType:'json',
                    data:formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        // console.log(data);
                        window.location.replace(`{{route('part_work',[$id_part])}}`);
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });
            $('body').on('change','.doss',function(){
                var that= $(this);
                $.ajax({
                    url:`{{route('GetDebit')}}`,
                    type:'post',
                    dataType:'json',
                    data:{id_dos:that.val()},
                    success:function(data){
                        if(that.val()=='0'){
                            $('.dbteur').html(`<option value="0">Aucun débiteur</option>`);
                        }else{
                            $('.dbteur').html(`
                                <option value="${data.id}">${(data.type_dtbr=='perso')? data.nom+' '+data.prenom : data.nom }</option>
                            `);
                            $('.vill_debtr').val(data.ville_adress_prv);
                        }
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });
        });
    </script>
    <script>
        $('document').ready(function(){
            var kill_nb=0;
            $('body').on('change','.this_file',function(input){
                var file = $(this).get(0).files[0];
                if(file){
                    var reader = new FileReader();
                    $(this).parent().parent().find('.file_block').append(
                        `
                        <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">
                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                <span class="w-3/5 file__icon file__icon--file mx-auto">
                                    <div class="file__icon__file-name"></div>
                                </span>
                                <span class="block font-medium mt-4 text-center truncate">`+file.name+`</span>
                            </div>
                            <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file`+kill_nb+`"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                            </div>
                        </div>
                        `
                    );
                    $(this).parent().find('button').hide();
                    $(this).parent().append(`
                        <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                        </button>
                        <input type="file" name="file_val[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file`+kill_nb+`">
                    `);
                    kill_nb++;
                }
            });
            $("body").on('click tap','.kill_file,.kill_file_base',function(){
                $('.'+$(this).data('id')).attr('name' ,'');
                $(this).parent().toggle();
            });
        });
    </script>
@endsection

