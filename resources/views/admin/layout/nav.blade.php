<div class="top-bar">
    @yield('menu-title')
    <!-- BEGIN: Search -->

    <div class="intro-x relative mr-3 sm:mr-6">
        {{-- <div class="search">
            <input type="text" class="search__input w-full input placeholder-theme-13 findg" placeholder="Rcherche...">
            <i data-feather="search" class="search__icon dark:text-gray-300"></i> 
        </div> --}}
        <div class="search-result rounded-lg shadow-lg w-full">
            <div class="search-result__content">
                <div class="flex">
                    <div class="flex-1"></div>
                    <div class="flex-1"></div>
                    <div class="flex-1"></div>
                    <div class="flex-1"></div>
                    <div class="flex-1"></div>
                    <div class="flex-1">
                        <div class="w-8 h-8 bg-red-300 text-red-500 flex items-center justify-center rounded-full stop">
                            <i class="w-4 h-4 items-end" data-feather="x-circle"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Search -->
    @if (Auth::user()->role == 'Administrateur')
            <?php
            $curr = date('Y-m-d');
            
            $cheks = App\DetailDossier::select('dossiers.id', 'detail_dossiers.id as id_dt', 'detail_dossiers.type_pay', 'detail_dossiers.motif', 'detail_dossiers.date_ancent')
                ->join('dossiers', 'dossiers.id', 'detail_dossiers.id_doss')
                ->where('type_pay', 'Chèque')
                ->where('date_ancent', '<=', date('Y-m-d', strtotime('-48 months', strtotime($curr))))
                ->whereNull('detail_dossiers.vue_alert_anc')
                ->limit(3)
                ->orderBy('detail_dossiers.created_at', 'desc');

            if (Auth::user()->see_all_doss == 0) {
                $cheks = $cheks->where('dossiers.creator_id', Auth::user()->id);
            }

            $cheks = $cheks->get();
            
            $fact = App\DetailDossier::select('dossiers.id', 'detail_dossiers.id as id_dt', 'detail_dossiers.type_pay', 'detail_dossiers.motif', 'detail_dossiers.date_ancent')
                ->join('dossiers', 'dossiers.id', 'detail_dossiers.id_doss')
                ->where('type_pay', 'Effet')
                ->where('date_ancent', '<=', date('Y-m-d', strtotime('-36 months', strtotime($curr))))
                ->whereNull('detail_dossiers.vue_alert_anc')
                ->limit(3)
                ->orderBy('detail_dossiers.created_at', 'desc');

            if (Auth::user()->see_all_doss == 0) {
                $fact = $fact->where('dossiers.creator_id', Auth::user()->id);
            }

            $fact = $fact->get();
            
            $effect = App\DetailDossier::select('dossiers.id', 'detail_dossiers.id as id_dt', 'detail_dossiers.type_pay', 'detail_dossiers.motif', 'detail_dossiers.date_ancent')
                ->join('dossiers', 'dossiers.id', 'detail_dossiers.id_doss')
                ->where('type_pay', 'Effet')
                ->where('date_ancent', '<=', date('Y-m-d', strtotime('-36 months', strtotime($curr))))
                ->whereNull('detail_dossiers.vue_alert_anc')
                ->limit(3)
                ->orderBy('detail_dossiers.created_at', 'desc');

            if (Auth::user()->see_all_doss == 0) {
                $effect = $effect->where('dossiers.creator_id', Auth::user()->id);
            }

            $effect = $effect->get();

            $Autre = App\DetailDossier::select('dossiers.id', 'detail_dossiers.id as id_dt', 'detail_dossiers.type_pay', 'detail_dossiers.motif', 'detail_dossiers.date_ancent')
                ->join('dossiers', 'dossiers.id', 'detail_dossiers.id_doss')
                ->where('type_pay', 'Autre')
                ->where('date_ancent', '<=', date('Y-m-d', strtotime('-36 months', strtotime($curr))))
                ->whereNull('detail_dossiers.vue_alert_anc')
                ->limit(3)
                ->orderBy('detail_dossiers.created_at', 'desc');

            if (Auth::user()->see_all_doss == 0) {
                $Autre = $Autre->where('dossiers.creator_id', Auth::user()->id);
            }

            $Autre = $Autre->get();
            
            $ex_doss = App\Dossier::where('date_fin_doss', '<=', date('Y-m-d H:i:s'))
                ->where('doss_exp_vu', 0)
                ->where('phase', '<>', 'Clôturée')
                ->take(5);
            
            if (Auth::user()->see_all_doss == 0) {
                $ex_doss = $ex_doss->where('dossiers.creator_id', Auth::user()->id);
            }

            $ex_doss = $ex_doss->get();
            
            $tasks = \App\taskManagment::select('task_managments.*', 'dossiers.ref')
                ->join('dossiers', 'dossiers.id', 'task_managments.id_doss')
                // ->where('creator',Auth::user()->id)
                ->where('task_managments.date_fin', '<=', date('Y-m-d H:i:s'))
                ->whereNull('task_managments.admin_exp_vu')
                ->where('task_managments.valid_task', 1)
                ->take(5);
            
            if (Auth::user()->see_all_doss == 0) {
                $tasks = $tasks->where('dossiers.creator_id', Auth::user()->id);
            }

            $tasks = $tasks->get();
            
            ?>
            <div class="intro-x dropdown mr-auto sm:mr-6">
                <div
                    class="dropdown-toggle notification notif_bull cursor-pointer {{ count($cheks) + count($fact) + count($effect) + count($ex_doss) + count($Autre) > 0 ? 'notification--bullet' : '' }} ">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                        stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                        class="feather feather-bell notification__icon dark:text-gray-300">
                        <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                        <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                    </svg>
                </div>
                <div class="notification-content pt-2 dropdown-box" id="_b3vrq130n" data-popper-placement="bottom-end"
                    style="position: absolute; inset: 0px auto auto 0px; transform: translate(-330px, 20px);">
                    <div class="notification-content__box dropdown-box__content box dark:bg-dark-6">
                        <div class="notification-content__title">Notifications</div>
                        <div class="depased_block {{ count($cheks) + count($fact) + count($effect) + count($ex_doss)  + count($Autre) == 0 ? 'h-16' : 'h-64 overflow-y-scroll p-3' }} ">
                            @if (count($cheks) + count($fact) + count($effect) + count($Autre)== 0)
                                <center>
                                    <span class="font-semibold text-gray-500">Aucune notification</span>
                                </center>
                            @endif

                            @foreach ($cheks as $item)
                            
                                <div class="depased rounded-lg lg:shadow-lg md:shadow-lg p-2">
                                    <div class="cursor-pointer relative flex items-center my-2">
                                        <a id='#\' href="#\" >
                                            <div class="ml-2 overflow-hidden">
                                                <div class="flex items-center">
                                                    <a href="{{ route('showDossier', $item->id) }}" data-id="{{ $item->id_dt }}"  class="font-medium truncate mr-5 vu_anc">
                                                        {{ $item->motif }}
                                                    </a> 
                                                    <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">
                                                        {{ $item->type_pay }}
                                                    </div>
                                                </div>
                                                <div class="w-full truncate text-gray-600">
                                                    <span class="font-medium">Date d'expiration :</span> {{ date('d-m-Y', strtotime($item->date_ancent)) }}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach

                            @foreach ($fact as $item)
                                <div class="depased rounded-lg lg:shadow-lg md:shadow-lg p-2">
                                    <div class="cursor-pointer relative flex items-center my-2">
                                        <a href="#\">
                                            <div class="ml-2 overflow-hidden">
                                                <div class="flex items-center">
                                                    <a href="{{ route('showDossier', $item->id) }}" data-id="{{ $item->id_dt }}"
                                                        class="font-medium truncate mr-5 vu_anc">
                                                        {{ $item->motif }}
                                                    </a>
                                                    <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">
                                                        {{ $item->type_pay }}
                                                    </div>
                                                </div>
                                                <div class="w-full truncate text-gray-600">
                                                    <span class="font-medium">Date d'expiration :</span>
                                                    {{ date('d-m-Y', strtotime($item->date_ancent)) }}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach

                            @foreach ($effect as $item)
                                <div class="depased rounded-lg lg:shadow-lg md:shadow-lg p-2">
                                    <div class="cursor-pointer relative flex items-center my-2">
                                        <a id='#\'href="#\">
                                            <div class="ml-2 overflow-hidden">
                                                <div class="flex items-center">
                                                    <a href="{{ route('showDossier', $item->id) }}" data-id="{{ $item->id_dt }}" class="font-medium truncate mr-5 vu_anc">
                                                        {{ $item->motif }}
                                                    </a> 
                                                    <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">
                                                        {{ $item->type_pay }}
                                                    </div>
                                                </div>
                                                <div class="w-full truncate text-gray-600">
                                                    <span class="font-medium">Date d'expiration  :</span> {{ date('d-m-Y', strtotime($item->date_ancent)) }}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach

                            @foreach ($Autre as $item)
                                <div class="depased rounded-lg lg:shadow-lg md:shadow-lg p-2">
                                    <div class="cursor-pointer relative flex items-center my-2">
                                        <a id='#\'href="#\">
                                            <div class="ml-2 overflow-hidden">
                                                <div class="flex items-center">
                                                    <a href="{{ route('showDossier', $item->id) }}" data-id="{{ $item->id_dt }}" class="font-medium truncate mr-5 vu_anc">
                                                        {{ $item->motif }}
                                                    </a> 
                                                    <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">
                                                        {{ $item->type_pay }}
                                                    </div>
                                                </div>
                                                <div class="w-full truncate text-gray-600">
                                                    <span class="font-medium">Date d'expiration  :</span> {{ date('d-m-Y', strtotime($item->date_ancent)) }}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach

                            @foreach ($ex_doss as $item)
                                <div class="depased rounded-lg lg:shadow-lg md:shadow-lg p-2 ">
                                    <div class="cursor-pointer relative flex items-center my-2">
                                        <a id='#\' href="#\" >
                                            <div class="ml-2 overflow-hidden">
                                                <div class="flex items-center">
                                                    <a href="{{ route('showDossier', $item->id) }}"   class="font-medium truncate mr-5 vu_anc">
                                                        {{ $item->ref }}
                                                    </a> 
                                                    <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">
                                                        Ce dossier est expiré
                                                    </div>
                                                </div>
                                                <div class="w-full truncate text-gray-600">
                                                    <span class="font-medium">Date d'expiration dossier :</span> {{ date('d-m-Y', strtotime($item->date_fin_doss)) }}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <div class="depased rounded-lg lg:shadow-lg md:shadow-lg p-2">
                            <a href="{{ route('list_alert_anc') }}"
                                class="-intro-y w-full block text-center hover:bg-theme-9 hover:text-white rounded-md py-3 border border-dotted border-theme-15 dark:border-dark-5 text-theme-16 dark:text-gray-600">Plus
                                d'infos
                            </a>
                        </div>
                    </div>
                </div>
            </div>
    @endif

    <div class="intro-x dropdown mr-auto sm:mr-6 {{ count($tasks) == 0 ? 'hidden' : '' }} ">
        <div
            class="dropdown-toggle notification notif_bull cursor-pointer {{ count($tasks) > 0 ? 'notification--bullet' : '' }} ">
            <svg data-theme="light" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                class="feather feather-bell notification__icon text-red-500 tooltip" title="tâches expirées">
                <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
            </svg>
        </div>
        <div class="notification-content pt-2 dropdown-box" id="_b3vrq130n" data-popper-placement="bottom-end"
            style="position: absolute; inset: 0px auto auto 0px; transform: translate(-330px, 20px);">
            <div class="notification-content__box dropdown-box__content box dark:bg-dark-6">
                <div class="notification-content__title">Tâches expirées</div>
                <div class="my-3 grid justify-items-center {{ count($tasks) > 0 ? 'hidden' : '' }}">
                    <div>
                        <span>Aucune notification</span>
                    </div>
                </div>
                <div class="depased_block h-64 overflow-y-scroll ">
                    @foreach ($tasks as $item)
                        <div class="depased rounded-lg lg:shadow-lg md:shadow-lg show_task" data-id="{{ $item->id }}"
                            data-link="{{ route('Messenger.admin', [$item->id]) }}">
                            <div class="cursor-pointer relative flex items-center my-2 p-3">
                                <a id='#\' href="{{ route('Messenger.admin', [$item->id]) }}" >
                                    <div class="ml-2 overflow-hidden">
                                        <div class="flex items-center">
                                            <a href="3\"   class="font-medium truncate mr-5">
                                                <span>Dossier : {{ $item->ref }}</span>
                                            </a> 
                                            <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">
                                                {{ $item->commande }}
                                            </div>
                                        </div>
                                        <div class="w-full truncate text-gray-700">
                                            <span class="font-medium ">Date début :</span> <span class="text-gray-600">{{ date('d-m-Y H:i', strtotime($item->date_db)) }}</span> 
                                            <br>
                                            <span class="font-medium ">Date fin :</span><span class="text-gray-600">{{ date('d-m-Y H:i', strtotime($item->date_fin)) }}</span> 
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                    
            </div>
        </div>
    </div>
    
    <!-- BEGIN: Account Menu -->
    <div class="intro-x dropdown w-8 h-8 hidden xl:block lg:block md:block sm:block relative">
        <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg bg-blue-400 image-fit zoom-in">
            <img class="rounded-full w-16 h-16" src="{{ !empty(Auth::user()->photo) ? asset(App\File_path::where('type', 'users')->first()->file_path . Auth::user()->photo) : asset(App\File_path::where('type', 'users')->first()->file_path . 'link.jpg') }}" alt="">

        </div>
        <div class="dropdown-box w-56 bg-theme-38 rounded-lg ">
            <div class="dropdown-box__content border-b-2 border-gray-600 dark:bg-dark-6 text-white mx-2">
                <div class="p-1 py-2 dark:border-dark-3">
                    <div class="font-medium ">
                        <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdown">
                            <span class="dropdown-item grid justify-items-right">
                                <div class="text-lg">
                                    {{ Auth::user()->nom }} {{ Auth::user()->prenom }}
                                </div>
                                <span class="text-gray-400">{{ Auth::user()->email }}</span>
                            </span>
                        </div>
                    </div>
                    {{-- <input type="hidden" id="id_auth" value="{{Auth::user()->nom}}" > --}}
                    
                    {{-- <div class="text-xs text-theme-41 dark:text-gray-600"></div> --}}
                </div>
            </div>
            {{-- {{ route('logout')}} --}}
            <div class="dropdown-box__content text-white rounded-lg hover:bg-blue-700 mx-2">
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <div class="p-1 dark:border-dark-3 ">
                        <div class="font-medium">
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <span class="dropdown-item">{{ __('Logout') }}</span>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="border-b-2 border-gray-600 mx-2 my-2"></div>
            <div class="dropdown-box__content text-white rounded-lg hover:bg-blue-700 mx-2 mb-2">
                <a href="{{ route('edit_userindex', [Auth::user()->id]) }}">
                    <div class="p-1 dark:border-dark-3 ">
                        <div class="font-medium">
                            <div class="dropdown-menu dropdown-menu-right"
                                aria-labelledby="navbarDropdown">
                                <span class="dropdown-item">profile</span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
                        {{--  --}}
        </div>
        <div class="block w-3 h-3 bg-theme-9 absolute right-0 top-0 rounded-full border-2 border-white">
        </div>

    </div>
            <!-- END: Account Menu -->
</div>
<script src="{{ asset('dist/js/app.js') }}"></script>
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('body').on('click', '.vu_anc', function() {
            event.preventDefault();
            var thiss = $(this);
            $.ajax({
                url: "{{ route('vu_anc') }}",
                data: {
                    id_dt: thiss.data('id')
                },
                dataType: 'json',
                type: 'post',
                success: function(data) {
                    window.location.replace(thiss.attr('href'));
                    // console.log(data);
                },
                error: function(error) {
                    console.log(error);
                },
            });
        });
        $('body').on('click', '.show_task', function() {
            event.preventDefault();
            var thiss = $(this);
            $.ajax({
                url: "{{ route('task_exp_vu') }}",
                data: {
                    id_tsk: thiss.data('id')
                },
                dataType: 'json',
                type: 'post',
                success: function(data) {
                    window.location.replace(thiss.data('link'));
                    // console.log(data);
                },
                error: function(error) {
                    console.log(error);
                },
            });
        });

        if ("{{ Auth::user()->nom }}" == null) {
            window.location.replace("{{ route('out') }}");
        }
    });
</script>
