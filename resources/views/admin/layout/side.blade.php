<nav class="side-nav">
    <a href="" class="intro-x flex items-center pl-5 pt-4">
        <img alt="Midone Tailwind HTML Admin Template" class="w-6" src="{{ asset('dist/images/logo.svg') }}">
        <span class="hidden xl:block text-gray-300 text-lg ml-3"> recovery<span class="font-medium text-white">App</span>
        </span>
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul class="">
        <li>
            <a href="{{ route('home') }}" id="bord" class="dbord side-menu side-menu">
                <div class="side-menu__icon"> <i data-feather="monitor"></i> </div>
                <div class="side-menu__title"> Tableau de bord </div>
            </a>
        </li>
        @if (Auth::user()->acs_cli == 1)
            @if (Auth::user()->add_cli == 1 || Auth::user()->list_cli == 1)
                <li class="">
                    <a href="javascript:;" id="client" class="client side-menu side-menu">
                        <div class="side-menu__icon"><i data-feather="users"></i> </div>
                        <div class="side-menu__title">Clients<i data-feather="chevron-down"
                                class="side-menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        @if (Auth::user()->add_cli == 1)
                            <li>
                                <a href="{{ route('indexClient') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="user-plus"></i> </div>
                                    <div class="side-menu__title"> Ajouter un client</div>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->list_cli == 1)
                            <li>
                                <a href="{{ route('listClient') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                                    <div class="side-menu__title">Liste des client </div>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif

        @if (Auth::user()->acs_doss == 1)
            @if (Auth::user()->add_doss == 1 || Auth::user()->list_doss == 1)
                <li class="">
                    <a href="javascript:;" id="dossier" class="dossier side-menu side-menu">
                        <div class="side-menu__icon"><i data-feather="folder"></i> </div>
                        <div class="side-menu__title">Dossier<i data-feather="chevron-down"
                                class="side-menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        @if (Auth::user()->add_doss == 1)
                            <li>
                                <a href="{{ route('ouvertureDoss') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="folder-plus"></i> </div>
                                    <div class="side-menu__title"> Ouverture dossier</div>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->list_doss == 1)
                            <li>
                                <a href="{{ route('listDossier') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                                    <div class="side-menu__title"> Liste des dossiers</div>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif

        @if (Auth::user()->acs_devis == 1)
            @if (Auth::user()->add_devis == 1 || Auth::user()->list_devis == 1)
                <li class="">
                    <a href="javascript:;" id="devis" class="devis side-menu side-menu">
                        <div class="side-menu__icon"><i data-feather="folder"></i> </div>
                        <div class="side-menu__title">Devis<i data-feather="chevron-down"
                                class="side-menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        @if (Auth::user()->add_devis == 1)
                            <li>
                                <a href="{{ route('addDevis') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="folder-plus"></i> </div>
                                    <div class="side-menu__title"> Ajouter un devis</div>
                                </a>
                            </li>
                        @endif

                        @if (Auth::user()->list_devis == 1)
                            <li>
                                <a href="{{ route('listDevis') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                                    <div class="side-menu__title"> Liste des devis</div>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif

        @if (Auth::user()->acs_fact == 1)
            @if (Auth::user()->add_fact == 1 || Auth::user()->list_fact == 1)
                <li class="">
                    <a href="javascript:;" id="facture" class="facture side-menu side-menu">
                        <div class="side-menu__icon"><i data-feather="folder"></i> </div>
                        <div class="side-menu__title">Facture<i data-feather="chevron-down"
                                class="side-menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        @if (Auth::user()->add_fact == 1)
                            <li>
                                <a href="{{ route('addFacture') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="folder-plus"></i> </div>
                                    <div class="side-menu__title"> Ajouter une facture</div>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->list_fact == 1)
                            <li>
                                <a href="{{ route('listFacture') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                                    <div class="side-menu__title"> Liste des factures</div>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif
        @if (Auth::user()->acs_rglmnt == 1)
            @if (Auth::user()->add_rglmnt == 1 || Auth::user()->list_rglmnt == 1)
                <li class="">
                    <a href="javascript:;" id="reglement" class="reglement side-menu side-menu">
                        <div class="side-menu__icon"><i data-feather="folder"></i> </div>
                        <div class="side-menu__title">Règlements<i data-feather="chevron-down"
                                class="side-menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        @if (Auth::user()->add_rglmnt == 1)
                            <li>
                                <a href="{{ route('addReglementp') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="folder-plus"></i> </div>
                                    <div class="side-menu__title"> Ajouter Un Règlement</div>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->list_rglmnt == 1)
                            <li>
                                <a href="{{ route('listReglementp') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                                    <div class="side-menu__title"> Liste Des Règlements</div>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif
        @if (Auth::user()->acs_charge == 1)
            @if (Auth::user()->add_charge == 1 || Auth::user()->list_charge == 1)
                <li class="">
                    <a href="javascript:;" id="charge" class="charge side-menu side-menu">
                        <div class="side-menu__icon"><i data-feather="briefcase"></i> </div>
                        <div class="side-menu__title">Charges<i data-feather="chevron-down"
                                class="side-menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        @if (Auth::user()->add_charge == 1)
                            <li>
                                <a href="{{ route('addIndex') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="plus-square"></i> </div>
                                    <div class="side-menu__title"> Ajouter Les Charges</div>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->list_charge == 1)
                            <li>
                                <a href="{{ route('listCharge') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                                    <div class="side-menu__title"> Liste Des Charges</div>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif

        @if (Auth::user()->acs_parten == 1)
            @if (Auth::user()->add_parten == 1 || Auth::user()->list_parten == 1)
                <li class="">
                    <a href="javascript:;" id="partners" class="partners side-menu side-menu">
                        <div class="side-menu__icon"><i data-feather="users"></i> </div>
                        <div class="side-menu__title">Les Partenaires<i data-feather="chevron-down"
                                class="side-menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        @if (Auth::user()->add_parten == 1)
                            <li>
                                <a href="{{ route('partForm') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="user-plus"></i> </div>
                                    <div class="side-menu__title"> Ajouter Les Partenaires</div>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->list_parten == 1)
                            <li>
                                <a href="{{ route('listForm') }}" id="bord" class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                                    <div class="side-menu__title"> Liste Des Partenaires</div>
                                </a>
                            </li>
                        @endif

                    </ul>
                </li>
            @endif
        @endif
        @if (Auth::user()->acs_task == 1)
            @if (Auth::user()->add_task == 1 || Auth::user()->list_task == 1)
                <li class="">
                    <a href="javascript:;" id="taskManager" class="taskManager side-menu side-menu">
                        <div class="side-menu__icon"><i data-feather="layers"></i> </div>
                        <div class="side-menu__title">Gestionnaire des tâches<i data-feather="chevron-down"
                                class="side-menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        @if (Auth::user()->add_task == 1)
                            <li>
                                <a href="{{ route('GestionTachesForm') }}" id="bord"
                                    class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="message-square"></i> </div>
                                    <div class="side-menu__title"> Ajouter une tâche</div>
                                </a>
                            </li>
                        @endif
                        @if (Auth::user()->list_task == 1)
                            <li>
                                <a href="{{ route('GestionTachesList') }}" id="bord"
                                    class="side-menu side-menu">
                                    <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                                    <div class="side-menu__title">Liste des tâches </div>
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif
        <li>
            <a href="{{route('bib_model_index')}}" id="bord" class="side-menu side-menu bib_model">
                <div class="side-menu__icon"> <i data-feather="settings"></i> </div>
                <div class="side-menu__title"> Bibliothèque de modèles </div>
            </a>
        </li>
        @if (Auth::user()->acs_user == 1)
            <li class="">
                <a href="javascript:;" id="parametre" class="users side-menu side-menu">
                    <div class="side-menu__icon"><i data-feather="user"></i> </div>
                    <div class="side-menu__title">Utilisateurs<i data-feather="chevron-down"
                            class="side-menu__sub-icon"></i> </div>
                </a>
                <ul class="">
                    <li>
                        <a href="{{ route('register_index') }}" id="bord" class="side-menu side-menu">
                            <div class="side-menu__icon"> <i data-feather="user-plus"></i> </div>
                            <div class="side-menu__title"> Ajouter un utilisateur</div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('users_list') }}" id="bord" class="side-menu side-menu">
                            <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                            <div class="side-menu__title">Liste des utilisateurs </div>
                        </a>
                    </li>
                </ul>
            </li>
        @endif


        {{-- <li>
            <a href="{{route('indexSettings')}}" id="bord" class="side-menu side-menu param">
                <div class="side-menu__icon"> <i data-feather="settings"></i> </div>
                <div class="side-menu__title"> Paramètres </div>
            </a>
        </li> --}}
    </ul>
</nav>
