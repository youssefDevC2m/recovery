@extends('admin.admin')
@section('title')
Ajouter des règlements
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ajouter des règlements</a> </div>
@endsection
@section('content')
<form method="post" action="" id="form_data" enctype="multipart/form-data">
    <div class="intro-y p-5 box mt-3 font-semibold w-64">
        <label for="" class="text-2xl text-gray-600">Règlement</label>
        <select name="distin" class="distin input  border border-gray-500 rounded-md w-full mt-2">
            <option value="cli">Client</option>
            <option value="debit">Debiteur</option>
            <option value="avoc">Avocat</option>
        </select>
    </div>
    <div class="intro-y p-5 box mt-3 font-semibold">
        <div>
            <label for="" class="text-2xl text-gray-600">Règlement infos</label>
        </div>
        <div class="grid lg:grid-cols-2 grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Dossier</label>
                <select name="id_doss" class="input doss border border-gray-500 rounded-md w-full">
                    <option value="0">Sélectionner le dossier</option>
                    @foreach ($folds as $item)
                        <option value="{{$item->id}}">{{$item->ref}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mt-2 cli_blok tooltip" title="Sélectionnez le dossier pour remplir la liste des clients" data-theme="light">
                <label for="" >Client</label>
                <select name="id_cli" class="id_cli input border border-gray-500 rounded-md w-full">
                    <option value="0">Sélectionner le client</option>
                </select>
            </div>
            <div class="mt-2 debit_blok tooltip hidden" title="Sélectionnez le dossier pour remplir la liste des débiteurs" data-theme="light">
                <label for="" >Débiteur</label>
                <select name="id_debit" class="id_debit input border border-gray-500 rounded-md w-full" disabled>
                    <option value="0">Sélectionner le Débiteur</option>
                </select>
            </div>
        </div>
        <div class="grid lg:grid-cols-3 grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="" >Mode Règlement</label>
                <select name="mode_regl"  class="input border border-gray-500 rounded-md w-full">
                    <option value="Espèces">Espèces</option>
                    <option value="Cheque">Cheque</option>
                    <option value="Virement">Virement</option>
                    <option value="Carte bancaire">Carte bancaire</option>
                    <option value="Effect">Effect</option>
                    <option value="Autre">Autre</option>
                </select>
            </div>
            <div class="mt-2 regl_block">
                <label for="" >Type Règlement</label>
                <select name="type_regl" class="type_regl input border border-gray-500 rounded-md w-full">
                    <option value="Partial">Partial</option>
                    <option value="Solder">Solder</option>
                </select>
            </div>
            <div class="mt-2">
                <label for="" >Montant</label>
                <input type="number" min=0 step="0.01" name="montant_reg" class="input border border-gray-500 w-full" placeholder="Entrer le montant de règlement">
            </div>
            <div class="mt-2">
                <label for="" class="debit_encess_lab">Date échéance</label>
                <input type="date" name="date_echeance" class="input border border-gray-500 w-full" value="{{date('Y-m-d')}}">
            </div>
            <div class="mt-2">
                <label for="" >Affectation</label>
                <input type="text" name="affectation" class="input border border-gray-500 w-full" placeholder="Affectation...">
            </div>
            <div class="mt-2">
                <label for="" >Nature de règlement</label>
                <select name="nature_reglement" data-search="true"  class="nature_reglement input border border-gray-500 rounded-md w-full">
                    <option value="honoraire de clôture dossier">Honoraire de clôture dossier</option>
                    <option value="provision">Provision</option>
                    <option value="frais d'overture">frais d'overture</option>
                </select>
            </div>
        </div>
    </div>
    <div class="intro-y p-5 box mt-3 font-semibold">
        <div class="">
            <div class="mt-2">
                <label for="">Remarque</label>
                <textarea name="remarque" class="input border border-gray-500 h-32 w-full" placeholder="Remarque..."></textarea>
            </div>
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Pièces jointes</label>
            <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                <div class="flex flex-wrap px-4 file_block" >
                    
                </div>
                <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                    <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                        <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                    </button>
                    <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0" name="file_val[]" data-stp="not">
                </div>
            </div>
        </div>
    </div>
    <div class="my-5 grid justify-items-center">
        <button class="save button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white"> 
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-save w-4 h-4 mr-2"><path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path><polyline points="17 21 17 13 7 13 7 21"></polyline><polyline points="7 3 7 8 15 8"></polyline></svg>
            Enregistrer 
        </button>
    </div>
</form>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.save').click(function(){
            event.preventDefault();
            var form_data= new FormData(document.getElementById('form_data'));
            
            $.ajax({
                url:'{{route("storeRegle")}}',
                type:'post',
                dataType:'json',
                data:form_data,
                processData: false,
                contentType: false,
                cache: false,
                success:function(data){
                    // console.log(data);
                    location.replace("{{route('listReglementp')}}");
                },
                error:function(error){
                    console.log(error);
                },
            });

        });
        $('body').on('change','.doss',function(){
            if($('.doss').val()!=0){
                $.ajax({
                    url:"{{route('get_cli_reg')}}",
                    type:'post',
                    dataType:'json',
                    data:{id_doss:$(this).val(),distin:$('.distin').val()},
                    success:function(data){
                        if($('.distin').val()=='cli' ){
                            console.log();
                            $('.id_cli').html(`
                                <option value="${data.id}">${(data.type_cli=='entre')? data.raison_sos : data.nom+' '+data.prenom }</option>
                            `);
                        }
                        if($('.distin').val()=='debit' || $('.distin').val()=='avoc'){
                            $('.id_debit').html(`
                                <option value="${data.id}">${(data.type_dtbr=='entre')? data.nom : data.nom+' '+data.prenom }</option>
                            `);
                        }
                    },error:function(error){
                        console.log(error);
                    },
                });
            }
            
        });
    });
</script>
<script>
    $(document).ready(function(){
        var kill_nb=0;
        $('body').on('change','.this_file',function(input){
            var file = $(this).get(0).files[0];
            if(file){
                var reader = new FileReader();
                $(this).parent().parent().find('.file_block').append(
                    `
                    <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">
                        <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                            <span class="w-3/5 file__icon file__icon--file mx-auto">
                                <div class="file__icon__file-name"></div>
                            </span>
                            <span class="block font-medium mt-4 text-center truncate">`+file.name+`</span>
                        </div>
                        <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file`+kill_nb+`"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                        </div>
                    </div>
                    `
                );
                $(this).parent().find('button').hide();
                $(this).parent().append(`
                    <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                        <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                    </button>
                    <input type="file" name="file_val[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file`+kill_nb+`">
                `);
                kill_nb++;
            }
        });
        $("body").on('click tap','.kill_file,.kill_file_base',function(){
            $('.'+$(this).data('id')).attr('name' ,'');
            $(this).parent().toggle();
            
        });
    });
</script>
<script>
    $("body").ready(function(){
        $('body').on('change','.distin',function(){
            if($(this).val()=='debit'){
                $('.cli_blok').hide();
                $('.id_cli').prop('disabled',true);
                $('.debit_encess_lab').text("Date encaissement");

                $('.debit_blok').show();
                $('.id_debit').prop('disabled',false);
                $('.doss').val("0");

                $('.regl_block').show();
                $('.type_regl').prop('disabled',false);

                $('.nature_reglement').html(`
                    <option value="Honoraire de médiation">Honoraire de médiation</option>
                    <option value="Règlement créance">Règlement créance</option>
                `);
            }
            if($(this).val()=='cli'){
                $('.cli_blok').show();
                $('.id_cli').prop('disabled',false);
                $('.debit_encess_lab').text("Date échéance");

                $('.debit_blok').hide();
                $('.id_debit').prop('disabled',true);
                $('.doss').val("0");

                $('.regl_block').show();
                $('.type_regl').prop('disabled',false);

                $('.nature_reglement').html(`
                    <option value="honoraire de clôture dossier">Honoraire de clôture dossier</option>
                    <option value="provision">Provision</option>
                    <option value="frais d'overture">frais d'overture</option>
                `);
            }

            if($(this).val()=='avoc'){
                $('.cli_blok').hide();
                $('.id_cli').prop('disabled',true);
                $('.debit_encess_lab').text("Date échéance");

                $('.debit_blok').show();
                $('.id_debit').prop('disabled',false);
                $('.doss').val("0");

                $('.regl_block').hide();
                $('.type_regl').prop('disabled',true);

                $('.nature_reglement').html(`
                    <option value="Honoraire de médiation">Honoraire de médiation</option>
                    <option value="Règlement créance">Règlement créance</option>
                `);
            }
        });
        $('.reglement').addClass('side-menu--active');
    });
</script>
@endsection