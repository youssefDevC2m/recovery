@extends('admin.admin')
@section('title')
Modifier le règlement
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Modifier le règlement</a> </div>
@endsection
@section('content')
<form method="post" action="" id="form_data" enctype="multipart/form-data">
    <input type="hidden" name="id_reg" value="{{$regle->id}}">
    
    <div class="intro-y p-5 box mt-3 font-semibold w-64">
        <label for="" class="text-2xl text-gray-600">Règlement</label>
        <select name="" disabled class=" input  border border-gray-500 rounded-md w-full mt-2">
            <option value="cli" {{($regle->distin=='cli')? 'selected' : ''}}>Client</option>
            <option value="debit" {{($regle->distin=='debit')? 'selected' : ''}}>Debiteur</option>
            <option value="avoc" {{($regle->distin=='avoc')? 'selected' : ''}}>Avocat</option>
        </select>
        <input type="hidden" name="distin" class="distin" value="{{$regle->distin}}">
    </div>
    <div class="intro-y p-5 box mt-3 font-semibold">
        <div>
            <label for="" class="text-2xl text-gray-600">Règlement infos</label>
        </div>
        <div class="grid lg:grid-cols-2 grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Dossier</label>
                <select name="id_doss" class="input doss border border-gray-500 rounded-md w-full">
                    <option value="0">Sélectionner le dossier</option>
                    @foreach ($folds as $item)
                        <option value="{{$item->id}}" {{($regle->id_doss==$item->id)? 'selected' : ''}}>{{$item->ref}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mt-2 cli_blok {{($regle->distin=='cli')? 'block' : 'hidden'}} tooltip" title="Sélectionnez le dossier pour remplir la liste des clients" data-theme="light">
                <label for="" >Client</label>
                <select name="id_cli" {{($regle->distin!='cli')? 'disbled' : ''}} class="id_cli input border border-gray-500 rounded-md w-full">
                    
                    <option value="{{$regle->id_cli}}" selected>{{($regle->type_cli='entre')? $regle->raison_sos : $regle->nom.' '.$regle->prenom }}</option>
                </select>
            </div>

            <div class="mt-2 debit_blok {{($regle->distin=='debit' || $regle->distin=='avoc')? 'block' : 'hidden'}} tooltip" title="Sélectionnez le dossier pour remplir la liste des débiteurs" data-theme="light">
                <label for="" >Débiteur</label>
                <select name="id_debit" {{($regle->distin=='debit' || $regle->distin=='avoc')? '' : 'disbled'}} class="id_debit input border border-gray-500 rounded-md w-full">
                    
                    <option value="{{$regle->id_debit}}" selected>{{($regle->type_dtbr='entre')? $regle->nom : $regle->nom.' '.$regle->prenom }}</option>
                </select>
            </div>
            
        </div>
        <div class="grid lg:grid-cols-3 grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="" >Mode Règlement</label>
                <select name="mode_regl" data-search="true"  class="input border border-gray-500 rounded-md w-full">
                    <option value="Espèces" {{($regle->mode_regl=='Espèces')? 'selected' :''}}>Espèces</option>
                    <option value="Cheque" {{($regle->mode_regl=='Cheque')? 'selected' :''}}>Cheque</option>
                    <option value="Virement" {{($regle->mode_regl=='Virement')? 'selected' :''}}>Virement</option>
                    <option value="Effect" {{($regle->mode_regl=='Effect')? 'selected' :''}}>Effect</option>
                    <option value="Autre" {{($regle->mode_regl=='Autre')? 'selected' :''}}>Autre</option>
                </select>
            </div> 
            <div class="mt-2 {{($regle->distin=='avoc')? 'hidden' : ''}}" >
                <label for="" >Type Règlement</label>
                <select name="type_regl" data-search="true" {{($regle->distin=='avoc')? 'disabled' : ''}} class=" input border border-gray-500 rounded-md w-full">
                    <option value="Partial"  {{($regle->type_regl=='Partial')? 'selected' :''}}>Partial</option>
                    <option value="Solder" {{($regle->type_regl=='Solder')? 'selected' :''}}>Solder</option>
                </select>
            </div>
            <div class="mt-2">
                <label for="" >Montant</label>
                <input type="number" min=0 step="0.01" value="{{$regle->montant_reg}}" name="montant_reg" class="input border border-gray-500 w-full" placeholder="Entrer le montant de règlement">
            </div>
            <div class="mt-2">
                <label for="" >{{($regle->distin=='debit')? "Date encaissement" : 'Date échéance'}} </label>
                <input type="date" name="date_echeance" value="{{$regle->date_echeance}}" class="input border border-gray-500 w-full" value="{{date('Y-m-d')}}">
            </div>
            <div class="mt-2" >
                <label for="" >Affectation</label>
                <input type="text" name="affectation" value="{{$regle->affectation}}" class="input border border-gray-500 w-full" placeholder="Affectation...">
            </div>
            <div class="mt-2">
                <label for="" >Nature de règlement</label>
                <select name="nature_reglement" data-search="true"  class=" input border border-gray-500 rounded-md w-full">
                    <option value="honoraire de clôture dossier" {{($regle->nature_reglement=='honoraire de clôture dossier')? 'selected' :''}}>Honoraire de clôture dossier</option>
                    <option value="provision" {{($regle->nature_reglement=='provision')? 'selected' :''}}>Provision</option>
                    <option value="frais d'overture" {{($regle->nature_reglement=="frais d'overture")? 'selected' :''}}>frais d'overture</option>
                </select>
            </div>
        </div>
    </div>
    <div class="intro-y p-5 box mt-3 font-semibold">
        <div class="">
            <div class="mt-2">
                <label for="">Remarque</label>
                <textarea name="remarque" class="input border border-gray-500 h-32 w-full" placeholder="Remarque...">{{$regle->remarque}}</textarea>
            </div>
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Pièces jointes</label>
            <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                <div class="flex flex-wrap px-4 file_block" >
                    @if (count($files)>0)
                        @foreach ($files as $item)
                            <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2">
                                <a href="{{asset($File_path->file_path.$item->file_name)}}" target="_blank">
                                    <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                        <span class="w-3/5 file__icon file__icon--file mx-auto">
                                            <div class="file__icon__file-name"></div>
                                        </span>
                                        <span class="block font-medium mt-4 text-center truncate">{{$item->org_file_name}}</span>
                                    </div>
                                </a>
                                <input type="text" name="" id="">
                                <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file_base " data-id="kill_file_base{{$item->id}}"> 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                                </div>
                            </div>
                            <input type="hidden" name="file_exc[]" value="{{$item->file_name}}" class="kill_file_base{{$item->id}}">
                        @endforeach
                    @endif
                </div>
                <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                    <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                        <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                    </button>
                    <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0" name="file_val[]" data-stp="not">
                </div>
            </div>
        </div>
    </div>
    @if (Auth::user()->edit_rglmnt == 1)
        <div class="my-5 grid justify-items-center">
            <button class="save button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-save w-4 h-4 mr-2"><path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path><polyline points="17 21 17 13 7 13 7 21"></polyline><polyline points="7 3 7 8 15 8"></polyline></svg>
                Enregistrer 
            </button>
        </div>    
    @endif
    
</form>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.save').click(function(){
            event.preventDefault();
            var form_data= new FormData(document.getElementById('form_data'));
            
            $.ajax({
                url:'{{route("updateRegle")}}',
                type:'post',
                dataType:'json',
                data:form_data,
                processData: false,
                contentType: false,
                cache: false,
                success:function(data){
                    // console.log(data);
                    location.replace("{{route('listReglementp')}}");
                },
                error:function(error){
                    console.log(error);
                },
            });

        });
        $('body').on('change','.doss',function(){
            if($('.doss').val()!=0){
                $.ajax({
                    url:"{{route('get_cli_reg')}}",
                    type:'post',
                    dataType:'json',
                    data:{id_doss:$(this).val(),distin:$('.distin').val()},
                    success:function(data){
                        if($('.distin').val()=='cli' ){
                            console.log();
                            $('.id_cli').html(`
                                <option value="${data.id}">${(data.type_cli=='entre')? data.raison_sos : data.nom+' '+data.prenom }</option>
                            `);
                        }
                        if($('.distin').val()=='debit' || $('.distin').val()=='avoc'){
                            $('.id_debit').html(`
                                <option value="${data.id}">${(data.type_dtbr=='entre')? data.nom : data.nom+' '+data.prenom }</option>
                            `);
                        }
                    },error:function(error){
                        console.log(error);
                    },
                });
            }
            
        });
    });
</script>
<script>
    $(document).ready(function(){
        var kill_nb=0;
        $('body').on('change','.this_file',function(input){
            var file = $(this).get(0).files[0];
            if(file){
                var reader = new FileReader();
                $(this).parent().parent().find('.file_block').append(
                    `
                    <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">
                        <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                            <span class="w-3/5 file__icon file__icon--file mx-auto">
                                <div class="file__icon__file-name"></div>
                            </span>
                            <span class="block font-medium mt-4 text-center truncate">`+file.name+`</span>
                        </div>
                        <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file`+kill_nb+`"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                        </div>
                    </div>
                    `
                );
               
                $(this).parent().find('button').hide();
                $(this).parent().append(`
                    <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                        <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                    </button>
                    <input type="file" name="file_val[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file`+kill_nb+`">
                `);
                kill_nb++;
            }
        });
        $("body").on('click tap','.kill_file,.kill_file_base',function(){
            $('.'+$(this).data('id')).attr('name' ,'');
            $(this).parent().toggle();
            
        });
    });
</script>
<script>
    $("body").ready(function(){
        $('.reglement').addClass('side-menu--active');
    });
</script>
@endsection