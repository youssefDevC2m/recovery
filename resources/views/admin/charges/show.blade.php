@extends('admin.admin')
@section('title')
Ajouter des charges
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ajouter des charges</a> </div>
@endsection
@section('content')
<form method="post" action="" id="form_data" enctype="multipart/form-data" class="font-semibold">
    <input type="hidden" name="id_charge" value="{{$charges->id}}">
    <div class="intro-y p-5 box mt-3 font-semibold w-64">
        <label for="" class="text-2xl text-gray-600">Charge</label>
        <select name="owner_type" class="input border border-gray-500 rounded-md w-full mt-2" disabled>
            <option value="Avocat" {{($charges->owner_type=='Avocat')? 'selected' : ''}}>Avocat</option>
            <option value="Expert judiciaire" {{($charges->owner_type=='Expert judiciaire')? 'selected' : ''}}>Expert judiciaire</option>
            <option value="Huissier justice" {{($charges->owner_type=='Huissier justice')? 'selected' : ''}}>Huissier justice</option>
            <option value="Médiateur" {{($charges->owner_type=='Médiateur')? 'selected' : ''}}>Médiateur</option>
        </select>
        <input type="hidden" name="owner_type" value="{{$charges->owner_type}}">
    </div>
    <div class="intro-y p-5 box mt-3 font-semibold">
        <div class="grid lg:grid-cols-3 md:grid-cols-1 gap-2 ">
            <div class="mt-2">
                <label for="">Dossier</label>
                <select name="id_doss"  data-search="true" class="tail-select border border-gray-500 rounded-md w-full">
                    @foreach ($dosses as $item)
                        <option value="{{$item->id}}" {{($charges->id_doss==$item->id)? 'selected' : ''}}>{{$item->ref}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mt-2">
                <label for="">Mode reglement</label>
                <select name="mode_reg"  class="input border border-gray-500 rounded-md w-full">
                    <option value="Espèces" {{($charges->mode_reg=='Espèces')? 'selected' : ''}}>Espèces</option>
                    <option value="Cheque" {{($charges->mode_reg=='Cheque')? 'selected' : ''}}>Cheque</option>
                    <option value="Virement" {{($charges->mode_reg=='Virement')? 'selected' : ''}}>Virement</option>
                    {{-- <option value="Carte bancaire" {{($charges->mode_reg=='Carte bancaire')? 'selected' : ''}}>Carte bancaire</option> --}}
                    <option value="Effect" {{($charges->mode_reg=='Effect')? 'selected' : ''}}>Effect</option>
                    <option value="Autre" {{($charges->mode_reg=='Autre')? 'selected' : ''}}>Autre</option>
                </select>
            </div>
            
            <div class="mt-2">
                <label for="">Montant</label>
                <input type="number" step="0.01" min="0" value="{{$charges->montant}}" name="montant" class="input border border-gray-500 w-full" placeholder="Entrez un montant">
            </div>
        </div>
        <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Nature reglement</label>
                <input type="text" name="nature_reg" value="{{$charges->nature_reg}}" class="input border border-gray-500 w-full" placeholder="Entrez la nature de reglement">
            </div>
            <div class="mt-2">
                <label for="">Affectation</label>
                <select name="affect" class="input border border-gray-500 w-full">
                    <option value="Compte Client" {{($charges->affect=='Compte Client')? 'selected' : ''}}>Compte Client</option>
                    <option value="Compte Cabinet" {{($charges->affect=='Compte Cabinet')? 'selected' : ''}}>Compte Cabinet</option>
                    <option value="Compte Personnel" {{($charges->affect=='Compte Personnel')? 'selected' : ''}}>Compte Personnel</option>
                    <option value="Caisse" {{($charges->affect=='Caisse')? 'selected' : ''}}>Caisse</option>
                </select>
            </div>
            
        </div>
    </div>
    <div class="intro-y p-5 box mt-3 font-semibold">
        <div>
            <label for="">Remarque</label>
            <textarea name="remarque" class="input border border-gray-500 w-full h-64" placeholder="Remarque...">{{$charges->remarque}}</textarea>
        </div>
    </div>
    @if (Auth::user()->edit_charge == 1)
        <div class="my-5 grid justify-items-center">
            <button class="save button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-save w-4 h-4 mr-2"><path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path><polyline points="17 21 17 13 7 13 7 21"></polyline><polyline points="7 3 7 8 15 8"></polyline></svg>
                Enregistrer 
            </button>
        </div>    
    @endif
    
</form>
@endsection
@section('script')
    <script>
        $("body").ready(function(){ 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("body").on('click','.save',function(){
                event.preventDefault();
                var form_data = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:"{{route('updateCharge')}}",
                    type:'post',
                    dataType:'json',
                    data:form_data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        Swal.fire({
                            // position: 'center',
                            icon: 'success',
                            title: 'Les données sont enregistrées',
                            showConfirmButton: false,
                            timer: 1500
                        }).then((result) => {
                            location.replace("{{route('listCharge')}}");
                        });
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });
        });
    </script>
    <script>
        $("body").ready(function(){
            $('.charge').addClass('side-menu--active');
        });
    </script>
@endsection

