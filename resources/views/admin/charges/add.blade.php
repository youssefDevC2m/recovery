@extends('admin.admin')
@section('title')
Ajouter des charges
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ajouter des charges</a> </div>
@endsection
@section('content')
<form method="post" action="" id="form_data" enctype="multipart/form-data" class="font-semibold">
    <div class="intro-y p-5 box mt-3 font-semibold w-64">
        <label for="" class="text-2xl text-gray-600">Charge</label>
        <select name="owner_type" class="input border border-gray-500 rounded-md w-full mt-2">
            <option value="Avocat">Avocat</option>
            <option value="Expert judiciaire">Expert judiciaire</option>
            <option value="Huissier justice">Huissier justice</option>
            <option value="Médiateur">Médiateur</option>
        </select>
    </div>
    <div class="intro-y p-5 box mt-3 font-semibold">
        <div class="grid lg:grid-cols-3 md:grid-cols-1 gap-2 ">
            
            <div class="mt-2">
                <label for="">Dossier</label>
                <select name="id_doss"  data-search="true" class="tail-select border border-gray-500 rounded-md w-full">
                    @foreach ($dosses as $item)
                        <option value="{{$item->id}}">{{$item->ref}}</option>
                    @endforeach
                </select>
            </div>

            <div class="mt-2">
                <label for="">Mode reglement</label>
                <select name="mode_reg"  class="input border border-gray-500 rounded-md w-full">
                    <option value="Espèces">Espèces</option>
                    <option value="Cheque">Cheque</option>
                    <option value="Virement">Virement</option>
                    {{-- <option value="Carte bancaire">Carte bancaire</option> --}}
                    <option value="Effect">Effect</option>
                    <option value="Autre">Autre</option>
                </select>
            </div>
            
            <div class="mt-2">
                <label for="">Montant</label>
                <input type="number" step="0.01" min="0" name="montant" class="input border border-gray-500 w-full" placeholder="Entrez un montant">
            </div>
        </div>
        <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Nature reglement</label>
                <input type="text" name="nature_reg" class="input border border-gray-500 w-full" placeholder="Entrez la nature de reglement">
            </div>
            <div class="mt-2">
                <label for="">Affectation</label>
                <select name="affect" class="input border border-gray-500 w-full">
                    <option value="Compte Client">Compte Client</option>
                    <option value="Compte Cabinet">Compte Cabinet</option>
                    <option value="Caisse">Caisse</option>
                </select>
            </div>
        </div>
    </div>
    <div class="intro-y p-5 box mt-3 font-semibold">
        <div>
            <label for="">Remarque</label>
            <textarea name="remarque" class="input border border-gray-500 w-full h-64" placeholder="Remarque..."></textarea>
        </div>
    </div>
    <div class="my-5 grid justify-items-center">
        <button class="save button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white"> 
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-save w-4 h-4 mr-2"><path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path><polyline points="17 21 17 13 7 13 7 21"></polyline><polyline points="7 3 7 8 15 8"></polyline></svg>
            Enregistrer 
        </button>
    </div>
</form>
@endsection
@section('script')
    <script>
        $("body").ready(function(){ 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("body").on('click','.save',function(){
                event.preventDefault();
                var form_data = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:"{{route('storeCharge')}}",
                    type:'post',
                    dataType:'json',
                    data:form_data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        // console.log(data);
                        Swal.fire({
                            // position: 'center',
                            icon: 'success',
                            title: 'Les données sont enregistrées',
                            showConfirmButton: false,
                            timer: 1500
                        }).then((result) => {
                            location.replace("{{route('listCharge')}}");
                        });
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });
        });
    </script>
    <script>
        $("body").ready(function(){
            $('.charge').addClass('side-menu--active');
        });
    </script>
@endsection

