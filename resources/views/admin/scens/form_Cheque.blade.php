@extends('admin.admin')
@section('title')
    Plaintes Chèques Sans Provision
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i
            data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ajouter des
            plaintes Chèques Sans Provision</a> </div>
@endsection
@section('content')
    <form action="" id="form_data">
        <input type="hidden" name="id_hist" value="{{ $id_hist }}">
        <div class="intro-x w-full box my-2">
            <div class="p-5 grid grid-cols-12 gap-4 row-gap-3 font-semibold">
                <div class="col-span-12 sm:col-span-6">
                    <label>Validation pièces</label>
                    <div class="flex flex-row">
                        <div class="">
                            <label for="">Oui</label>
                            <input required type="radio" name="vald_pieces"
                                class="input w-full border border-gray-400 mt-2 flex-1" {{($vald_pieces==1)? 'checked' : ''}} value="1"
                                style="transform: translate(0px, 4px);">
                        </div>
                        <div class="ml-3">
                            <label for="">Non</label>
                            <input required type="radio" name="vald_pieces"
                                class="input w-full border border-gray-400 mt-2 flex-1" {{($vald_pieces==0)? 'checked' : ''}} value="0"
                                style="transform: translate(0px, 4px);">
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6"> </div>
                <div class="col-span-12 sm:col-span-6">
                    <label>Provision</label>
                    <input required type="text" class="input w-full border border-gray-400 mt-2 flex-1"  name="provi" value="{{$provi}}"
                        placeholder="Provision">
                </div>

                <div class="col-span-12 sm:col-span-6">
                    <label>Avocat</label>
                    <select name="avocat" class="input w-full border border-gray-400 mt-2 flex-1" required>
                        <option value="">Sélectionnez un avocat</option>
                        @foreach ($partner->where('type_partn','Avocat') as $item)
                            <option value="{{ $item->id }}" {{($avocat==$item->id)? 'selected' : ''}}>{{ $item->prenom_part . ' ' . $item->nom_part }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-span-12 sm:col-span-6">
                    <label>Date début</label>
                    <input required type="date" name="date_debut_plant" class="input w-full border mt-2 flex-1"
                        value="{{ date('Y-m-d',strtotime($date_debut_plant)) }}">
                </div>

                <div class="col-span-12 sm:col-span-6">
                    <label>Date fin</label>
                    <input required type="date" name="date_fin_plant" class="input w-full border mt-2 flex-1"
                        value="{{ date('Y-m-d',strtotime($date_fin_plant)) }}">
                </div>
            </div>
        </div>
        <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold conatains_judic_chek">
            <div class="accordion__pane active">
                <a href="javascript:;"
                    class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg">
                    <span class="text-xl">
                        PLAINTE CHEQUE SANS PROVISION
                    </span>
                </a>
                <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
                    <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <label for="">Référence</label>
                            <input type="text" name="ref_st_cheqe" class="input w-full border border-gray-500 mt-2"
                                placeholder="Entrez un référence">
                        </div>
                        <div class="mt-2">
                            <label for="">Tribunal spécialisé</label>
                            <input type="text" name="tribunal_spese" class="input w-full border border-gray-500 mt-2"
                                placeholder="Entrez tribunal spécialisé">
                        </div>
                    </div>
                    <div class="my-2">
                        <label for="" class="text-lg ">les partie</label>
                    </div>
                    <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                        <div class="">
                            <div class="mt-2">
                                <label for="">Le Tireur</label>
                                <input type="text" name="tireur" class="input w-full border border-gray-500 mt-2"
                                    placeholder="Entrez le tireur">
                            </div>
                            <div class="mt-2">
                                <label for="">Adresse</label>
                                <input type="text" name="adresse_cheq" class="input w-full border border-gray-500 mt-2"
                                    placeholder="Entrez une adresse">
                            </div>
                            <div class="mt-2">
                                <label for="">C.i.n</label>
                                <input type="text" name="cin_cheq" class="input w-full border border-gray-500 mt-2"
                                    placeholder="Entrez le cin de tireur">
                            </div>
                            <div class="mt-2">
                                <label for="">le mandataire</label>
                                <select name="procureur_tir" class="input w-full border border-gray-500 mt-2">
                                    <option value='0'>Sélectionnez un mandataire</option>
                                    @foreach ($partner as $item)
                                        <option value='{{ $item->id }}'>{{ $item->nom_part }} {{ $item->prenom_part }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="">
                            <div class="mt-2">
                                <label for="">Bénéficiaire</label>
                                <input type="text" name="benefic" class="input w-full border border-gray-500 mt-2"
                                    placeholder="Entrez le bénéficiaire">
                            </div>
                            <div class="mt-2">
                                <label for="">le mandataire</label>
                                <input type="text" name="procureur_tir_benif"
                                    class="input w-full border border-gray-500 mt-2"
                                    placeholder="Entrez le procureur de bénéficiaire">
                            </div>
                            <div class="mt-2">
                                <label for="">Chargé De Suivi</label>
                                <input type="text" name="charge_suiv" class="input w-full border border-gray-500 mt-2"
                                    placeholder="Entrez Le Chargé De Suivi">
                            </div>
                        </div>
                    </div>
                    <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <label for="">La Banque</label>
                            <input type="text" class="input w-full border border-gray-500 mt-2"
                                placeholder="Entrez la banque" name="banque_tire">
                        </div>
                        <div class="mt-2">
                            <label for="">Adresse</label>
                            <input type="text" class="input w-full border border-gray-500 mt-2"
                                placeholder="Entrez l'adresse" name="adrs_tire">
                        </div>
                        <div class="mt-2">
                            <label for="">Numéro du compte</label>
                            <input type="text" class="input w-full border border-gray-500 mt-2"
                                placeholder="Entrez le numéro de compte" name="n_compt">
                        </div>
                    </div>
                    <div class="my-5">
                        <label for="" class="text-lg">Informations sur chèque</label>
                    </div>
                    <div class="cheqs">
                        <div class="grid lg:grid-cols-4 md:grid-cols-1 sm:grid-cols-1 gap-1 border-b pb-3 ">
                            <input type="hidden" name="cheque_nb[]" value="1">
                            <div class="mt-2">
                                <label for="">Montant du chèque</label>
                                <input type="number" class="input w-full border border-gray-500 mt-2" min="0"
                                    step="0.5" name="montant_cheq[]" placeholder="Entrez le montant du chèque">
                            </div>
                            <div class="mt-2">
                                <label for="">Numéro du chèque</label>
                                <input type="number" class="input w-full border border-gray-500 mt-2" min="0"
                                    step="0.5" name="n_cheq[]" placeholder="Saisissez le numéro du chèque">
                            </div>
                            <div class="mt-2">
                                <label for="">Date D’émission </label>
                                <input type="date" class="input w-full border border-gray-500 mt-2"
                                    name="date_emission[]" value="{{ date('Y-m-d') }}">
                            </div>
                            <div class="mt-2">
                                <label for="">Motif</label>
                                <input type="text" class="input w-full border border-gray-500 mt-2" name="motif[]"
                                    placeholder="Entrez le motif">
                            </div>
                        </div>
                    </div>

                    <div class="grid justify-items-center my-3">
                        <div class="flex flex-row hover:text-theme-33 add_cheq">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-plus-circle mx-auto">
                                <circle cx="12" cy="12" r="10"></circle>
                                <line x1="12" y1="8" x2="12" y2="16"></line>
                                <line x1="8" y1="12" x2="16" y2="12"></line>
                            </svg>
                            <span class="" style="margin-top: 0.1rem">Plus</span>
                        </div>
                    </div>
                    <div class="mt-2 grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
                        <div>
                            <label for="">Montant Global </label>
                            <input type="number" name="montant_glob" step="0.1" min="0"
                                class="input w-full border border-gray-500 mt-2 " placeholder=" Montant Global"
                                id="">
                        </div>
                    </div>
                    <div class="grid lg:justify-items-end lg:mt-0 mt-3 my-2">
                        <div class="flex flex-row">
                            <label class="text-gray-600">La Phase De Première Instance </label>
                            <input type="checkbox" class="input input--switch border ml-2 prem_inst" name="prem_inst">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid justify-items-center">
            <div class="flex lg:flex-row md:flex-row flex-col ">
                <button class="button w-24 mr-1 mb-2 bg-theme-1 text-white mt-4 save">Enregistrer</button>
            </div>
        </div>
    </form>
@endsection
@section('script')
    <script src="{{ asset('dist/js/param.js') }}"></script>
    {{-- //////////////////////////////////////////////// --}}
    <script>
        $(document).ready(function() {
            $("body").on('click', '.save', function() {
                event.preventDefault();
                var form_data = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url: "{{ route('add_cheques_ref') }}",
                    data: form_data,
                    dataType: 'json',
                    type: 'post',
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data) {
                        Swal.fire(
                            'Validé!',
                            'les détails sont enregistrés avec succès !',
                            'success'
                        ).then((result) => {
                            window.location.replace(data);
                        });

                        // console.log(data);
                    },
                    error: function(error) {
                        console.log(error);
                    },
                });
            });
        });
    </script>
    {{-- //////////////////////////////////////////////// --}}
    <script>
        $(document).ready(function() {
            $('body').on('change', '.prem_inst', function() {
                if ($(this).is(":checked")) {
                    $('.conatains_judic_chek').append(prem_insthtml);
                    $('.prem_insthtml').show('slow');
                } else {
                    $('.prem_insthtml').slideUp(300);
                    $('.prem_insthtml').html("");

                    $('.phase_appelhtml').slideUp(300);
                    $('.phase_appelhtml').html("");
                }
            });

            $('body').on('change', '.phase_appel', function() {
                if ($(this).is(":checked")) {
                    $('.conatains_judic_chek').append(phase_appelhtml);
                    $('.phase_appelhtml').show('slow');
                } else {
                    $('.phase_appelhtml').slideUp(300);
                    $('.phase_appelhtml').html('');

                    $('.proc_appelhtml').slideUp(300);
                    $('.proc_appelhtml').html("");
                }
            });

            $('body').on('click', '.add_cheq', function() {
                $('.cheqs').append(cheqs);
            });

            $("body").on('click', '.kill_cheq', function() {
                $(this).parent().parent().parent().slideUp(300);
                $(this).parent().parent().parent().html("");
            });
        });
    </script>
@endsection
