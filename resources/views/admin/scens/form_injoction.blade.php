@extends('admin.admin')
@section('title')
Ajouter Des Injonctions De Payer
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ajouter Des Injonctions De Payer</a> </div>
@endsection
@section('content')
<form action="" id="form_data">
    <input type="hidden" name="id_hist" value="{{$id_hist}}">
    <div class="intro-x w-full box my-2">
        <div class="p-5 grid grid-cols-12 gap-4 row-gap-3 font-semibold">
            <div class="col-span-12 sm:col-span-6">
                <label>Validation pièces</label>
                <div class="flex flex-row">
                    <div class="">
                        <label for="">Oui</label>
                        <input required type="radio" name="vald_pieces"
                            class="input w-full border border-gray-400 mt-2 flex-1" {{($vald_pieces==1)? 'checked' : ''}} value="1"
                            style="transform: translate(0px, 4px);">
                    </div>
                    <div class="ml-3">
                        <label for="">Non</label>
                        <input required type="radio" name="vald_pieces"
                            class="input w-full border border-gray-400 mt-2 flex-1" {{($vald_pieces==0)? 'checked' : ''}} value="0"
                            style="transform: translate(0px, 4px);">
                    </div>
                </div>
            </div>
            <div class="col-span-12 sm:col-span-6"> </div>
            <div class="col-span-12 sm:col-span-6">
                <label>Provision</label>
                <input required type="text" class="input w-full border border-gray-400 mt-2 flex-1"  name="provi" value="{{$provi}}"
                    placeholder="Provision">
            </div>

            <div class="col-span-12 sm:col-span-6">
                <label>Avocat</label>
                <select name="avocat" class="input w-full border border-gray-400 mt-2 flex-1" required>
                    <option value="">Sélectionnez un avocat</option>
                    @foreach ($partner->where('type_partn','Avocat') as $item)
                        <option value="{{ $item->id }}" {{($avocat==$item->id)? 'selected' : ''}}>{{ $item->prenom_part . ' ' . $item->nom_part }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-span-12 sm:col-span-6">
                <label>Date début</label>
                <input required type="date" name="date_debut_plant" class="input w-full border mt-2 flex-1"
                    value="{{ date('Y-m-d',strtotime($date_debut_plant)) }}">
            </div>

            <div class="col-span-12 sm:col-span-6">
                <label>Date fin</label>
                <input required type="date" name="date_fin_plant" class="input w-full border mt-2 flex-1"
                    value="{{ date('Y-m-d',strtotime($date_fin_plant)) }}">
            </div>
        </div>
    </div>
    <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold injoncDpayHtml">
        <div class="accordion__pane active"> 
            <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
                <span class="text-xl">
                    INJONCTION DE PAYER
                </span>    
            </a>
            <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
                <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2">
                    <div class="mt-2">
                        <label for="">Référence</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="ref_inj_p" placeholder="Entrez Le Référence">
                    </div>
                    <div class="mt-2">
                        <label for="">Tribunal Compétente</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="trib_com_inj_p" placeholder="Entrez Le Tribunal Compétente">
                    </div>
                    <div class="mt-2">
                        <label for="">Le Nom De Juge</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="nom_jug_inj_p" placeholder="Entrez Le Nom De Juge">
                    </div>
                </div>
                <div class="my-3">
                    <label for="" class="text-lg ">Les Parties</label>
                </div>
                <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                    <div>
                        <div class="mt-2">
                            <label for="">Le Tireur</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="tirer_inj_p" placeholder="Entrez Le Tireur">
                        </div>
                        <div class="mt-2">
                            <label for="">L’adresse</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="adrs_tirer_inj_p" placeholder="Entrez Le L’adresse">
                        </div>
                        <div class="mt-2">
                            <label for="">C.i.n</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="Cin_tirer_inj_p" placeholder="Entrez le cin">
                        </div>
                        <div class="mt-2">
                            <label for="">le mandataire</label>
                            <select name="proc_tirer_inj_p" class="input w-full border border-gray-500 mt-2">
                                <option value='0'>Sélectionnez un mandataire</option>
                                @foreach($partner as $item)
                                    <option value='{{$item->id}}'>{{$item->nom_part}} {{$item->prenom_part}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div>
                        <div class="mt-2">
                            <label for="">Le Bénéficiaire</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="benef_inj_p" placeholder="Entrez Le Bénéficiaire">
                        </div>
                        <div class="mt-2">
                            <label for="">le mandataire</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="proc_benf_inj_p" placeholder="Entrez le mandataire">
                        </div>
                        <div class="mt-2">
                            <label for="">Chargé De Suivi</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="Chrg_inj_p"  placeholder="Entrez Chargé De Suivi">
                        </div>
                    </div>
                </div>
                <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2">
                    <div class="mt-2">
                        <label for="">La Banque</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="bank_inj_p" placeholder="Entrez La Banque">
                    </div>
                    <div class="mt-2">
                        <label for="">L’adresse</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="adrs_bnk_inj_p" placeholder="Entrez L’adresse">
                    </div>
                    <div class="mt-2">
                        <label for="">Numéro De Compte</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="num_cmpt_bnk_inj_p" placeholder="Entrez Le Numéro De Compte">
                    </div>
                </div>
                <div class="my-2">
                    <label for="" class="text-lg">Informations sur la Créance</label>
                </div>
                <div class="info_creace">
                    <div class="grid lg:grid-cols-4 md:grid-cols-1 sm:grid-cols-1 gap-1 border-b pb-3 ">
                        <input type="hidden" name="info_creac_nb_inj_pat[]" value="1">
                        <div class="mt-2">
                            <label for="">Montant De La Créance</label>
                            <input type="number" class="input w-full border border-gray-500 mt-2"  min="0" step="0.5" name="montant_creanc_inj_pay[]"  value=""  placeholder="Entrez le montant du chèque">
                        </div>
                        <div class="mt-2">
                            <label for="">Numéro</label>
                            <input type="text" class="input w-full border border-gray-500 mt-2"  min="0" step="0.5" name="n_crance_inj_pay[]" value="" placeholder="Saisissez le numéro du chèque">
                        </div>
                        <div class="mt-2">
                            <label for="">Date D’Echéance</label>
                            <input type="date" class="input w-full border border-gray-500 mt-2" name="date_echc_crenc_inj_pay[]" value="{{date('Y-m-d')}}">
                        </div>
                        <div class="mt-2">
                            <label for="">Motif</label>
                            <input type="text" class="input w-full border border-gray-500 mt-2" name="motif_crenc_inj__pay[]" placeholder="Entrez le motif" value="">
                        </div>
                    </div>
                </div>
                <div class="grid justify-items-center my-3">
                    <div class="flex flex-row hover:text-theme-33 add_creanc_info">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle mx-auto"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                        <span class="" style="margin-top: 0.1rem">Plus</span>
                    </div>
                </div>
                <div class="grid lg:grid-cols-2 grid-cols-1 gap-2 mt-2">
                    <div class="mt-2">
                        <label for="" class="text-lg">Montant Global</label>
                        <input type="number" min='0' step="0.01" class="input w-full border border-gray-500 mt-2" min="0" step="0.01" name="montant_glob_inj_pay" placeholder="Montant Global">
                    </div>
                </div>
                <div class="grid lg:justify-items-end lg:mt-0 mt-3 my-2">
                    <div class="flex flex-row">
                        <label class="text-gray-600">Procédure De Dépôt</label>
                        <input type="checkbox" class="input input--switch border ml-2 proc_depo_inj_pay" name="proc_depo_inj_pay"> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="grid justify-items-center">
        <div class="flex lg:flex-row md:flex-row flex-col ">
            <button class="button w-24 mr-1 mb-2 bg-theme-1 text-white mt-4 save">Enregistrer</button> 
        </div>
    </div>
</form>
    
@endsection
@section('script')
    <script>
        var huissier=`
        <select name="huiss_designe_inj_pay" class="input w-full border border-gray-500 ">
            <option value='0'>Sélectionnez un mandataire</option>
            @foreach($partner as $item)
                <option value='{{$item->id}}'>{{$item->nom_part}} {{$item->prenom_part}}</option>
            @endforeach
        </select>
        `;
        var proc_exec_huissierr=`
        <select name="huiss_des_proc_exec_inj_pay" class="input w-full border border-gray-500 ">
            <option value='0'>Sélectionnez un mandataire</option>
            @foreach($partner as $item)
                <option value='{{$item->id}}'>{{$item->nom_part}} {{$item->prenom_part}}</option>
            @endforeach
        </select>
        `;
    </script>
    <script src="{{asset('dist/js/param.js')}}"></script>
    <script>
        $(document).ready(function(){

            $("body").on('click','.save',function(){
                event.preventDefault();
                var form_data=new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:"{{route('add_injonc_pay_ref')}}",
                    data:form_data,
                    dataType:'json',
                    type:'post',
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        Swal.fire(
                        'Validé!',
                        'les détails sont enregistrés avec succès !',
                        'success'
                        ).then((result) =>{
                            window.location.replace(data);
                            // console.log(data);
                        });
                       
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });

            $('body').on('change','.proc_depo_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.injoncDpayHtml').append(proc_dephtml);
                    $('.proc_dephtml').show('slow');
                }else{
                    $('.proc_dephtml').slideUp(300);
                    $('.proc_dephtml').html("");

                    $('.proc_notifhtml').slideUp(300);
                    $('.proc_notifhtml').html("");

                    $('.proc_exehtml').slideUp(300);
                    $('.proc_exehtml').html("");
                    
                    $('.proc_appel_inj_pay').slideUp(300);
                    $('.proc_appel_inj_pay').html("");

                    $('.proc_appelhtml').slideUp(300);
                    $('.proc_appelhtml').html("");
                }
            });

            $('body').on('change','.proc_notif_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.injoncDpayHtml').append(proc_notifhtml);
                    $('.proc_notifhtml').show('slow');
                }else{
                    $('.proc_notifhtml').slideUp(300);
                    $('.proc_notifhtml').html("");
                    
                    $('.proc_exehtml').slideUp(300);
                    $('.proc_exehtml').html("");
                    
                    $('.proc_appel_inj_pay').slideUp(300);
                    $('.proc_appel_inj_pay').html("");

                    $('.proc_appelhtml').slideUp(300);
                    $('.proc_appelhtml').html("");
                }
            });

            $('body').on('change','.proc_appel_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.injoncDpayHtml').append(proc_appelhtml);
                    $('.proc_appelhtml').show('slow');
                }else{
                    $('.proc_appelhtml').slideUp(300);
                    $('.proc_appelhtml').html("");

                    $('.proc_exehtml').slideUp(300);
                    $('.proc_exehtml').html("");
                }
            });

            $('body').on('change','.proc_exec_inj_pay',function(){
                if($(this).is(":checked")){
                    
                    $('.injoncDpayHtml').append(proc_exehtml);
                    $('.proc_exehtml').show('slow');
                    
                }else{
                    $('.proc_exehtml').slideUp(300);
                    $('.proc_exehtml').html("");

                }
            });

            $('body').on('click','.add_creanc_info',function(){
                $('.info_creace').append(infocreacehtml);
            });

        });
    </script>
@endsection
