@extends('admin.admin')
@section('title')
    Modifier L'injonction De Payer
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Modifier L'injonction De Payer</a> </div>
@endsection
@section('content')
<form action="" id="form_data">
<input type="hidden" name="id_infos" value="{{$Inj_De_Payer->id}}">
<input type="hidden" name="id_hist" value="{{$Inj_De_Payer->id_hist_scen}}">
<div class="intro-x w-full box my-2">
    <div class="p-5 grid grid-cols-12 gap-4 row-gap-3 font-semibold">
        <div class="col-span-12 sm:col-span-6">
            <label>Validation pièces</label>
            <div class="flex flex-row">
                <div class="">
                    <label for="">Oui</label>
                    <input required type="radio" name="vald_pieces"
                        class="input w-full border border-gray-400 mt-2 flex-1"
                        {{ $Inj_De_Payer->vald_pieces == 1 ? 'checked' : '' }} value="1"
                        style="transform: translate(0px, 4px);">
                </div>
                <div class="ml-3">
                    <label for="">Non</label>
                    <input required type="radio" name="vald_pieces"
                        class="input w-full border border-gray-400 mt-2 flex-1"
                        {{ $Inj_De_Payer->vald_pieces == 0 ? 'checked' : '' }} value="0"
                        style="transform: translate(0px, 4px);">
                </div>
            </div>
        </div>
        <div class="col-span-12 sm:col-span-6"> </div>
        <div class="col-span-12 sm:col-span-6">
            <label>Provision</label>
            <input required type="text" class="input w-full border border-gray-400 mt-2 flex-1" name="provi"
                value="{{ $Inj_De_Payer->provi }}" placeholder="Provision">
        </div>

        <div class="col-span-12 sm:col-span-6">
            <label>Avocat</label>
            <select name="avocat" class="input w-full border border-gray-400 mt-2 flex-1" required>
                <option value="">Sélectionnez un avocat</option>
                @foreach ($partner->where('type_partn', 'Avocat') as $item)
                    <option value="{{ $item->id }}" {{ $Inj_De_Payer->avocat == $item->id ? 'selected' : '' }}>
                        {{ $item->prenom_part . ' ' . $item->nom_part }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-span-12 sm:col-span-6">
            <label>Date début</label>
            <input required type="date" name="date_debut_plant" class="input w-full border mt-2 flex-1"
                value="{{ date('Y-m-d', strtotime($Inj_De_Payer->date_debut_plant)) }}">
        </div>

        <div class="col-span-12 sm:col-span-6">
            <label>Date fin</label>
            <input required type="date" name="date_fin_plant" class="input w-full border mt-2 flex-1"
                value="{{ date('Y-m-d', strtotime($Inj_De_Payer->date_fin_plant)) }}">
        </div>
    </div>
</div>
<div class="accordion intro-y box px-5 py-8 mt-5 font-semibold injoncDpayHtml">
    <div class="accordion__pane active"> 
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
            <span class="text-xl">
                INJONCTION DE PAYER
            </span>    
        </a>
        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
            <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Référence</label>
                    <input type="text" class="input w-full mt-2 border border-gray-500" name="ref_inj_p" value="{{$Inj_De_Payer->ref_inj_p}}" placeholder="Entrez Le Référence">
                </div>
                <div class="mt-2">
                    <label for="">Tribunal Compétente</label>
                    <input type="text" class="input w-full mt-2 border border-gray-500" name="trib_com_inj_p" value="{{$Inj_De_Payer->trib_com_inj_p}}" placeholder="Entrez Le Tribunal Compétente">
                </div>
                <div class="mt-2">
                    <label for="">Le Nom De Juge</label>
                    <input type="text" class="input w-full mt-2 border border-gray-500" name="nom_jug_inj_p" value="{{$Inj_De_Payer->nom_jug_inj_p}}" placeholder="Entrez Le Nom De Juge">
                </div>
            </div>
            <div class="my-3">
                <label for="" class="text-lg ">Les Parties</label>
            </div>
            <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                <div>
                    <div class="mt-2">
                        <label for="">Le Tireur</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="tirer_inj_p" value="{{$Inj_De_Payer->tirer_inj_p}}" placeholder="Entrez Le Tireur">
                    </div>
                    <div class="mt-2">
                        <label for="">L’adresse</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="adrs_tirer_inj_p" value="{{$Inj_De_Payer->adrs_tirer_inj_p}}" placeholder="Entrez Le L’adresse">
                    </div>
                    <div class="mt-2">
                        <label for="">C.i.n</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="Cin_tirer_inj_p" value="{{$Inj_De_Payer->Cin_tirer_inj_p}}" placeholder="Entrez le cin">
                    </div>
                    <div class="mt-2">
                        <label for="">le mandataire</label>
                        <select name="proc_tirer_inj_p" class="input w-full border border-gray-500 mt-2">
                            <option value='0'>Sélectionnez un mandataire</option>
                            @foreach($partner as $item)
                                <option value='{{$item->id}}' {{($Inj_De_Payer->proc_tirer_inj_p==$item->id)? 'selected' : ''}}>{{$item->nom_part}} {{$item->prenom_part}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <div class="mt-2">
                        <label for="">Le Bénéficiaire</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="benef_inj_p" value="{{$Inj_De_Payer->benef_inj_p}}" placeholder="Entrez Le Bénéficiaire">
                    </div>
                    <div class="mt-2">
                        <label for="">le mandataire</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="proc_benf_inj_p" value="{{$Inj_De_Payer->proc_benf_inj_p}}" placeholder="Entrez le mandataire">
                    </div>
                    <div class="mt-2">
                        <label for="">Chargé De Suivi</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="Chrg_inj_p" value="{{$Inj_De_Payer->Chrg_inj_p}}" placeholder="Entrez Chargé De Suivi">
                    </div>
                </div>
            </div>
            <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">La Banque</label>
                    <input type="text" class="input w-full mt-2 border border-gray-500" name="bank_inj_p" value="{{$Inj_De_Payer->bank_inj_p}}" placeholder="Entrez La Banque">
                </div>
                <div class="mt-2">
                    <label for="">L’adresse</label>
                    <input type="text" class="input w-full mt-2 border border-gray-500" name="adrs_bnk_inj_p" value="{{$Inj_De_Payer->adrs_bnk_inj_p}}" placeholder="Entrez L’adresse">
                </div>
                <div class="mt-2">
                    <label for="">Numéro De Compte</label>
                    <input type="text" class="input w-full mt-2 border border-gray-500" name="num_cmpt_bnk_inj_p" value="{{$Inj_De_Payer->num_cmpt_bnk_inj_p}}" placeholder="Entrez Le Numéro De Compte">
                </div>
            </div>
            <div class="my-2">
                <label for="" class="text-lg">Informations sur la Créance</label>
            </div>
            <div class="info_creace">
                @if (count($InfoCreance_inj_pay)>0)
                    @foreach ($InfoCreance_inj_pay as $iteInfoCreance)
                        <div class="grid lg:grid-cols-4 md:grid-cols-1 sm:grid-cols-1 gap-1 border-b pb-3 ">
                            <input type="hidden" name="info_creac_nb_inj_pat[]" value="1">
                            <div class="mt-2">
                                <label for="">Montant De La Créance</label>
                                <input type="number" class="input w-full border border-gray-500 mt-2"  min="0" step="0.5" name="montant_creanc_inj_pay[]"  value="{{$iteInfoCreance->montant_creanc_inj_pay}}"  placeholder="Entrez le montant du chèque">
                            </div>
                            <div class="mt-2">
                                <label for="">Numéro</label>
                                <input type="text" class="input w-full border border-gray-500 mt-2"  min="0" step="0.5" name="n_crance_inj_pay[]" value="{{$iteInfoCreance->n_crance_inj_pay}}" placeholder="Saisissez le numéro du chèque">
                            </div>
                            <div class="mt-2">
                                <label for="">Date D’Echéance</label>
                                <input type="date" class="input w-full border border-gray-500 mt-2" name="date_echc_crenc_inj_pay[]" value="{{date('Y-m-d',strtotime($iteInfoCreance->date_echc_crenc_inj_pay))}}">
                            </div>
                            <div class="mt-2">
                                <label for="">Motif</label>
                                <input type="text" class="input w-full border border-gray-500 mt-2" name="motif_crenc_inj__pay[]" value="{{$iteInfoCreance->motif_crenc_inj__pay}}" placeholder="Entrez le motif" >
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="grid lg:grid-cols-4 md:grid-cols-1 sm:grid-cols-1 gap-1 border-b pb-3 ">
                        <input type="hidden" name="info_creac_nb_inj_pat[]" value="1">
                        <div class="mt-2">
                            <label for="">Montant De La Créance</label>
                            <input type="number" class="input w-full border border-gray-500 mt-2"  min="0" step="0.5" name="montant_creanc_inj_pay[]"  value=""  placeholder="Entrez le montant du chèque">
                        </div>
                        <div class="mt-2">
                            <label for="">Numéro</label>
                            <input type="text" class="input w-full border border-gray-500 mt-2"  min="0" step="0.5" name="n_crance_inj_pay[]" value="" placeholder="Saisissez le numéro du chèque">
                        </div>
                        <div class="mt-2">
                            <label for="">Date D’Echéance</label>
                            <input type="date" class="input w-full border border-gray-500 mt-2" name="date_echc_crenc_inj_pay[]" value="{{date('Y-m-d')}}">
                        </div>
                        <div class="mt-2">
                            <label for="">Motif</label>
                            <input type="text" class="input w-full border border-gray-500 mt-2" name="motif_crenc_inj__pay[]" placeholder="Entrez le motif" value="">
                        </div>
                    </div>
                @endif
            </div>
            <div class="grid justify-items-center my-3">
                <div class="flex flex-row hover:text-theme-33 add_creanc_info">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle mx-auto"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                    <span class="" style="margin-top: 0.1rem">Plus</span>
                </div>
            </div>
            <div class="grid lg:grid-cols-2 grid-cols-1 gap-2 mt-2">
                <div class="mt-2">
                    <label for="" class="text-lg">Montant Global</label>
                    <input type="number" min='0' step="0.01" class="input w-full border border-gray-500 mt-2" min="0" step="0.01" name="montant_glob_inj_pay" value="{{$Inj_De_Payer->montant_glob_inj_pay}}" placeholder="Montant Global">
                </div>
            </div>
            <div class="grid lg:justify-items-end lg:mt-0 mt-3 my-2">
                <div class="flex flex-row">
                    <label class="text-gray-600">Procédure De Dépôt</label>
                    <input type="checkbox" class="input input--switch border ml-2 proc_depo_inj_pay" name="proc_depo_inj_pay" {{($Inj_De_Payer->proc_depo_inj_pay=='on')? 'checked' : ''}}> 
                </div>
            </div>
        </div>
    </div>
    @if ($Inj_De_Payer->proc_depo_inj_pay=="on")
    <div class="accordion__pane proc_dephtml" > 
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
            <span class="text-xl">
                PROCÉDURE DE DÉPÔT
            </span>    
        </a>
        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
            <div class="grid lg:grid-cols-2 grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Numéro Du Dossier</label>
                    <input type="text" name="num_doss_inj_pay" value="{{$Inj_De_Payer->num_doss_inj_pay}}" class="input w-full border border-gray-500 mt-2" placeholder="Numéro Du Dossier">
                </div>
                <div class="mt-2">
                    <label for="">Date De Jugement</label>
                    <input type="date" name="date_jug_inj_pay" value="{{date('Y-m-d',strtotime($Inj_De_Payer->date_jug_inj_pay))}}" class="input w-full border border-gray-500 mt-2" value="` + curr + `">
                </div>
            </div>
            <div class="mt-2">
                <label for="">Acceptation De La Demande</label>
                <div class="my-2 grid justify-items-center">
                    <div class="grid lg:grid-cols-2 sm:grid-cols-1">
                        <div class="mx-2 flex flex-row">
                            <label for="">Demande accepté</label>
                            <input type="radio" name="Accept_demnd_inj_pay" {{($Inj_De_Payer->Accept_demnd_inj_pay=='Demande accepté')? 'checked' : ''}} value="Demande accepté" class="input border border-gray-500 ml-1" style="margin-top: 0.1rem;">
                        </div>
                        <div class="mx-2 flex flex-row">
                            <label for="">Refus De La Demande</label>
                            <input type="radio" name="Accept_demnd_inj_pay" {{($Inj_De_Payer->Accept_demnd_inj_pay=='Refus De La Demande')? 'checked' : ''}} value="Refus De La Demande" class="input border border-gray-500 ml-1" style="margin-top: 0.1rem;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Date De Retrait D’injonction</label>
                    <input type="date" name="dt_retrt_inj_pay" value="{{date('Y-m-d',strtotime($Inj_De_Payer->dt_retrt_inj_pay))}}" class="input w-full border border-gray-500" >
                </div>
                <div class="mt-2">
                    <label for="">Numéro De Retrait D’injonction</label>
                    <input type="text" name="nr_retrt_inj_pay" value="{{$Inj_De_Payer->nr_retrt_inj_pay}}" class="input w-full border border-gray-500" placeholder="Numéro De Retrait D’injonction">
                </div>
            </div>
            <div class="mt-2">
                <label for="">Explication</label>
                <textarea name="explication_dep_inj_pay"  class="input w-full border border-gray-500 h-32" placeholder="Explication...">{{$Inj_De_Payer->explication_dep_inj_pay}}</textarea>
            </div>
            <div class="grid lg:justify-items-end  my-3">
                <div class="flex flex-row">
                    <label class="text-gray-600">PROCÉDURES DE NOTIFICATION</label>
                    <input type="checkbox" class="input input--switch border ml-2 proc_notif_inj_pay" name="proc_notif_inj_pay" {{($Inj_De_Payer->proc_notif_inj_pay=="on")? 'checked' : ''}}> 
                </div>
            </div>
        </div>
    </div>
    @endif
    @if ($Inj_De_Payer->proc_notif_inj_pay=="on")
    <div class="accordion__pane proc_notifhtml"> 
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
            <span class="text-xl">
                PROCÉDURES DE NOTIFICATION
            </span>    
        </a>
        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
            <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Date De Dépôt</label>
                    <input type="date" name="date_depot_inj_pay" value="{{date('Y-m-d',strtotime($Inj_De_Payer->date_depot_inj_pay))}}" class="input w-full border border-gray-500" >
                </div>
                <div class="mt-2">
                    <label for="">Numéro Du Dossier De Notification</label>
                    <input type="numbre" name="nr_dos_notif_inj_pay" value="{{$Inj_De_Payer->nr_dos_notif_inj_pay}}" class="input w-full border border-gray-500" placeholder="Numéro Du Dossier De Notification">
                </div>
            </div>
            <div class="grid lg:grid-cols-3 md:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Huissier Désigné</label>
                    <select name="huiss_designe_inj_pay" class="input w-full border border-gray-500 ">
                        <option value='0'>Sélectionnez un mandataire</option>
                        @foreach($partner as $item)
                            <option value='{{$item->id}}' {{($Inj_De_Payer->huiss_designe_inj_pay==$item->id)? 'selected' : ''}}>{{$item->nom_part}} {{$item->prenom_part}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mt-2">
                    <label for="">Résultat De La Notification</label>
                    <input type="text" name="result_notif_inj_pay" value="{{$Inj_De_Payer->result_notif_inj_pay}}" class="input w-full border border-gray-500" placeholder="Numéro Du Dossier De Notification">
                </div>
                <div class="mt-2">
                    <label for="">Date D’appel</label>
                    <input type="date" name="date_app_inj_pay" value="{{date('Y-m-d',strtotime($Inj_De_Payer->date_app_inj_pay))}}" class="input w-full border border-gray-500" >
                </div>
            </div>
            <div class="mt-2">
                <label for="">Procédure</label>
                <textarea name="procd_proc_notif"  class="input w-full border border-gray-500 h-32" placeholder="Procédure...">{{$Inj_De_Payer->procd_proc_notif}}</textarea>
            </div>
            <div class="grid lg:justify-items-end lg:mt-0 mt-3 my-2">
                <div class="flex flex-row">
                    <label class="text-gray-600">Procédures D’appel</label>
                    <input type="checkbox" class="input input--switch border ml-2 proc_appel_inj_pay" {{($Inj_De_Payer->proc_appel_inj_pay=='on')? 'checked' : ''}} name="proc_appel_inj_pay"> 
                </div>
            </div>
        </div>
    </div>
    @endif
    @if ($Inj_De_Payer->proc_appel_inj_pay=='on')
    <div class="accordion__pane proc_appelhtml"> 
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
            <span class="text-xl">
                PROCÉDURES D’APPEL
            </span>    
        </a>
        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
            <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Date De dépôt</label>    
                    <input type="date" name="date_depo_proc_appel" value="{{date('Y-m-d',strtotime($Inj_De_Payer->date_depo_proc_appel))}}" class="input w-full border border-gray-500">
                </div>
                <div class="mt-2">
                    <label for="">Numéro De D'appel</label>    
                    <input type="text" name="num_appel_proc_appel" value="{{$Inj_De_Payer->num_appel_proc_appel}}" class="input w-full border border-gray-500" placeholder="Numéro De D'appel">
                </div>
                <div class="mt-2">
                    <label for="">Date D’appel</label>   
                    <input type="date" name="date_apel_inj_p" value="{{date('Y-m-d',strtotime($Inj_De_Payer->date_apel_inj_p))}}" class="input w-full border border-gray-500" placeholder="Numéro De D'appel">
                </div>
                <div class="mt-2">
                    <label for="">Nom Du Consultant</label>
                    <input type="text" name="nom_consult_inj_p" value="{{$Inj_De_Payer->nom_consult_inj_p}}" class="input w-full border border-gray-500" placeholder="Numéro De D'appel">
                </div>
                <div class="mt-2">
                    <label for="">Tribunal Compétente</label>
                    <input type="text" name="trib_comp_inj_p" value="{{$Inj_De_Payer->trib_comp_inj_p}}" class="input w-full border border-gray-500" placeholder="Tribunal Compétente">
                </div>
            </div>
            <div class="mt-2">
                <label for="">Note Et Audiences</label>
                <textarea name="note_audience" class="input w-full border border-gray-500 h-32" placeholder="Note Et Audiences...">{{$Inj_De_Payer->note_audience}}</textarea>
            </div>
            <div class="grid lg:justify-items-end my-3">
                <div class="flex flex-row">
                    <label class="text-gray-600">Procédures D’exécution</label>
                    <input type="checkbox" class="input input--switch border ml-2 proc_exec_inj_pay" name="proc_exec_inj_pay" {{(isset($Inj_De_Payer->proc_exec_inj_pay))? 'checked' : ''}}> 
                </div>
            </div>
        </div>
    </div>
    @endif
    @if ($Inj_De_Payer->proc_exec_inj_pay=="on")
    <div class="accordion__pane proc_exehtml"> 
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
            <span class="text-xl">
                PROCÉDURES D’EXÉCUTION
            </span>    
        </a>
        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
            <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Numéro D’exécution</label>
                    <input type="text" name="nr_execution_inj_pay" value="{{$Inj_De_Payer->nr_execution_inj_pay}}" class="input w-full border border-gray-500" placeholder="Numéro D’exécution">
                </div>
                <div class="mt-2">
                    <label for="">Date D’exécution</label>
                    <input type="date" name="date_execution_inj_pay" value="{{date('Y-m-d',strtotime($Inj_De_Payer->date_execution_inj_pay))}}" class="input w-full border border-gray-500">
                </div>
                <div class="mt-2">
                    <label for="">Huissier Désigné</label>
                    <select name="huiss_des_proc_exec_inj_pay" class="input w-full border border-gray-500 ">
                        <option value='0'>Sélectionnez un mandataire</option>
                        @foreach($partner as $item)
                            <option value='{{$item->id}}' {{($Inj_De_Payer->huiss_des_proc_exec_inj_pay==$item->id)? 'selected' : ''}}>{{$item->nom_part}} {{$item->prenom_part}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mt-2">
                    <label for="">Attestation De Délais de Paiement</label>
                    <input type="text" name="attes_del_pay_inj_pay" value="{{$Inj_De_Payer->attes_del_pay_inj_pay}}" class="input w-full border border-gray-500" placeholder="Attestation De Délais de Paiement">
                </div>
                <div class="mt-2">
                    <label for="">Attestation De Défaut de Paiement</label>
                    <input type="text" name="attes_defo_pay_inj_pay" value="{{$Inj_De_Payer->attes_defo_pay_inj_pay}}" class="input w-full border border-gray-500" placeholder="Attestation De Défaut de Paiement">
                </div>
                <div class="mt-2">
                    <label for="">Expiration</label>
                    <input type="text" name="expiration_inj_pay" value="{{$Inj_De_Payer->expiration_inj_pay}}" class="input w-full border border-gray-500" placeholder="Expiration">
                </div>
                <div class="mt-2">
                    <label for="">Date De La Saisie</label>
                    <input type="date" name="dt_saisi_proc_exe_inj_p" value="{{date('Y-m-d',strtotime($Inj_De_Payer->dt_saisi_proc_exe_inj_p))}}"  class="input w-full border border-gray-500" >
                </div>
                <div class="mt-2">
                    <label for="">Type De La Saisie</label>
                    <input type="text" name="type_saisie_inj_p" value="{{$Inj_De_Payer->type_saisie_inj_p}}" class="input w-full border border-gray-500" placeholder="Type De La Saisie">
                </div>
                <div class="mt-2">
                    <label for="">Date D’expertise Judiciaire</label>
                    <input type="date" name="date_expert_judic_inj_p" value="{{date('Y-m-d',strtotime($Inj_De_Payer->date_expert_judic_inj_p))}}" class="input w-full border border-gray-500" >
                </div>
                <div class="mt-2">
                    <label for="">Nom D’expert</label>
                    <input type="text" name="nom_expert_inj_p" value="{{$Inj_De_Payer->nom_expert_inj_p}}" class="input w-full border border-gray-500" placeholder="Nom D’expert">
                </div>
                <div class="mt-2">
                    <label for="">Publication Dans J.A.L</label>
                    <input type="text" name="Public_j_a_l" value="{{$Inj_De_Payer->Public_j_a_l}}" class="input w-full border border-gray-500" placeholder="Publication Dans J.A.L">
                </div>
                <div class="mt-2">
                    <label for="">Nom Du Journal</label>
                    <input type="text" name="nom_journal" value="{{$Inj_De_Payer->nom_journal}}" class="input w-full border border-gray-500" placeholder="Nom Du Journal">
                </div>
                <div class="mt-2">
                    <label for="">Ventes Aux Enchères Publique</label>
                    <input type="text" name="vent_encher_publc" value="{{$Inj_De_Payer->vent_encher_publc}}"  class="input w-full border border-gray-500" placeholder="Ventes Aux Enchères Publique">
                </div>
                <div class="mt-2">
                    <label for="">La Date De La Vente</label>
                    <input type="date" name="date_vent" value="{{date('Y-m-d',strtotime($Inj_De_Payer->date_vent))}}" class="input w-full border border-gray-500">
                </div>
            </div>
            <div class="mt-2">
                <label for="">Résultat</label>
                <textarea name="result_inj_pay" class="input w-full border border-gray-500 h-32" placeholder="Résultat...">{{$Inj_De_Payer->result_inj_pay}}</textarea>
            </div>
            
        </div>
    </div>
    @endif
    
</div>    
<div class="grid justify-items-center">
    <div class="flex lg:flex-row md:flex-row flex-col ">
        <button class="button w-24 mr-1 mb-2 bg-theme-1 text-white mt-4 save">Enregistrer</button> 
    </div>
</div>
</form>

@endsection
@section('script')
    <script src="{{asset('dist/js/param.js')}}"></script>
    <script>
        $(document).ready(function(){

            $("body").on('click','.save',function(){
                event.preventDefault();
                var form_data=new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:"{{route('edit_injonc_pay_ref')}}",
                    data:form_data,
                    dataType:'json',
                    type:'post',
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        Swal.fire(
                        'Validé!',
                        'les détails sont enregistrés avec succès !',
                        'success'
                        ).then((result) =>{
                            window.location.replace(data);
                           
                        });
                        console.log(data);
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });

            $('body').on('change','.proc_depo_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.injoncDpayHtml').append(proc_dephtml);
                    $('.proc_dephtml').show('slow');
                }else{
                    $('.proc_dephtml').slideUp(300);
                    $('.proc_dephtml').html("");

                    $('.proc_notifhtml').slideUp(300);
                    $('.proc_notifhtml').html("");

                    $('.proc_exehtml').slideUp(300);
                    $('.proc_exehtml').html("");
                    
                    $('.proc_appel_inj_pay').slideUp(300);
                    $('.proc_appel_inj_pay').html("");

                    $('.proc_appelhtml').slideUp(300);
                    $('.proc_appelhtml').html("");
                }
            });

            $('body').on('change','.proc_notif_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.injoncDpayHtml').append(proc_notifhtml);
                    $('.proc_notifhtml').show('slow');
                }else{
                    $('.proc_notifhtml').slideUp(300);
                    $('.proc_notifhtml').html("");
                    
                    $('.proc_exehtml').slideUp(300);
                    $('.proc_exehtml').html("");
                    
                    $('.proc_appel_inj_pay').slideUp(300);
                    $('.proc_appel_inj_pay').html("");

                    $('.proc_appelhtml').slideUp(300);
                    $('.proc_appelhtml').html("");
                }
            });

            $('body').on('change','.proc_appel_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.injoncDpayHtml').append(proc_appelhtml);
                    $('.proc_appelhtml').show('slow');
                }else{
                    $('.proc_appelhtml').slideUp(300);
                    $('.proc_appelhtml').html("");

                    $('.proc_exehtml').slideUp(300);
                    $('.proc_exehtml').html("");
                }
            });

            $('body').on('change','.proc_exec_inj_pay',function(){
                if($(this).is(":checked")){
                    
                    $('.injoncDpayHtml').append(proc_exehtml);
                    $('.proc_exehtml').show('slow');
                    
                }else{
                    $('.proc_exehtml').slideUp(300);
                    $('.proc_exehtml').html("");

                }
            });

            $('body').on('click','.add_creanc_info',function(){
                $('.info_creace').append(infocreacehtml);
            });

        });
    </script>
@endsection
