<div class="accordion__pane border-b border-gray-200 dark:border-dark-5 "> 
    <div class="grid grid-cols-2  hover:bg-gray-400 rounded-lg ">
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2" >
            <span class="text-xl">
                Solvabilite du débiteur
            </span>    
        </a>
        <div class="grid justify-items-end flex items-center">
            <div title="" class=" w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 mr-2  kill_sitatJudic"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
            </div>   
        </div>
    </div>
    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
        <div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">
            <input type="hidden" name="nbsolva[]" value="1">
            <div class="mt-2">
                <label for="">Biens Personnels</label>
                <input type="text" name="bien_personnels[]" value="{{$item->bien_personnels}}" class="input w-full border border-gray-500" placeholder="Biens Personnels">
            </div>
            <div class="mt-2">
                <label for="">T.D</label>
                <input type="text" name="td[]" value="{{$item->td}}" class="input w-full border border-gray-500" placeholder="T.D">
            </div>
            <div class="mt-2">
                <label for="">Héritage</label>
                <input type="text" name="hertage[]" value="{{$item->hertage}}"  class="input w-full border border-gray-500" placeholder="Héritage">
            </div>
            <div class="mt-2">
                <label for="">Participation</label>
                <input type="text" name="participation[]" value="{{$item->participation}}" class="input w-full border border-gray-500" placeholder="Participation">
            </div>
        </div>
        <div class="mt-2">
            <label for="">Commentaires</label>
            <textarea name="comment_solv_deb[]" class="input w-full border border-gray-500 h-32" placeholder="Commentaires...">{{$item->comment_solv_deb}}</textarea>
        </div>
    </div>
</div>