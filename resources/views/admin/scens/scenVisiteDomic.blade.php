<div class="accordion__pane border-b border-gray-200 dark:border-dark-5 "> 
    <div class="grid grid-cols-2  hover:bg-gray-400 rounded-lg ">
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2" >
            <span class="text-xl">
                Viste domiciliaire
            </span>    
        </a>
        <div class="grid justify-items-end flex items-center">
            <div title="" class=" w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 mr-2  kill_visit"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
            </div>   
        </div>
    </div>
    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
        <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-1">
            <div class="mt-2">
                <label for="">Domicile</label>
                <select name="domicile[]" class="input w-full border mt-2">
                    <option value="0">sélectionner une résidence</option>
                    <option value="personnel" {{($item->domicile=='personnel')? 'selected' : ''}}>Personnelle</option>
                    <option value="pro" {{($item->domicile=='pro')? 'selected' : ''}}>Professionnelle</option>
                    <option value="proche" {{($item->domicile=='proche')? 'selected' : ''}}>Proche</option>
                </select>
            </div>
            <div class="mt-2">
                <label for="">Adresse</label>
                <input type="text" class="input w-full border mt-2" name="adress[]" value="{{$item->adress_pro}}{{$item->adress_proche}}{{$item->adress_perso}}" placeholder="Entrer une adresse">
            </div>
        </div>
        <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-1">
            <div class="mt-2">
                <label for="">Date et heure</label>
                <input type="datetime-local" name="date_H_visite[]" value="{{date('Y-m-d\TH:i',strtotime($item->date_H_visite))}}" class="input w-full border mt-2" value="" >
            </div>
            <div class="mt-2">
                <label for="">Visiteur</label>
                <input type="text" name="visiteur[]" class="input w-full border mt-2" value="{{$item->date_H_visite}}" placeholder="Entrer le visiteur">
            </div>
        </div>
        <div class="mt-2 w-full">
            <label for="">Promesse</label>
            <textarea name="promess[]" class="input w-full border mt-2 h-32 w-full" placeholder="Promesses...">{{$item->promess}}</textarea>
        </div>
        <div class="mt-2 w-full">
            <label for="">Commentaires</label>
            <textarea name="comment[]" class="input w-full border mt-2 h-32 w-full" placeholder="Commentaires...">{{$item->comment}}</textarea>
        </div>
    </div>
</div>