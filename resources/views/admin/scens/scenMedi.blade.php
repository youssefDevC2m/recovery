<div class="accordion__pane border-b border-gray-200 dark:border-dark-5 "> 
    <div class="grid grid-cols-2  hover:bg-gray-400 rounded-lg ">
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2" >
            <span class="text-xl">
                Médiations
            </span>    
        </a>
        <div class="grid justify-items-end flex items-center">
            <div title="" class=" w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 mr-2  kill_medi"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
            </div>   
        </div>
    </div>
    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
        <div class="grid lg:grid-cols-2 grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Lieu</label>
                <select name="lieu_med[]" class="input border border-gray-500 w-full">
                    <option value="0">Sélectionnez un lieu</option>
                    <option value="Au Cabinet" {{($item->lieu_med=='Au Cabinet')? 'selected' :''}}>Au Cabinet</option>
                    <option value="Chez le Débiteur(bureau)" {{($item->lieu_med=='Chez le Débiteur(bureau)')? 'selected' :''}}>Chez le Débiteur(bureau)</option>
                    <option value="Chez le Client(bureau)" {{($item->lieu_med=='Chez le Client(bureau)')? 'selected' :''}}>Chez le Client(bureau)</option>
                </select>
            </div>
        </div>
        <div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Date et heure</label>
                <input type="datetime-local" name="date_med[]" class="input border border-gray-500 w-full" value="{{date('Y-m-d\TH:i',strtotime($item->date_med))}}">
            </div>
            <div class="mt-2">
                <label for="">Adresse de médiation</label>
                <input type="text" name="adress_med[]" class="input border border-gray-500 w-full" placeholder="Adresse de médiation" value="{{$item->adress_med}}">
            </div>
            <div class="mt-2">
                <label for="">Médiateur</label>
                <select name="mediateur[]" class="input border border-gray-500 w-full">
                    <option value="0">Sélectionnez un médiateur</option>
                        @foreach ($agent as $item1)
                            <option value="{{$item1->id}}" {{($item->mediateur== $item1->id)? 'selected' :''}}>{{$item1->nom}} {{$item1->prenom}}</option>  
                        @endforeach
                </select>
            </div>
            <div class="mt-2">
                <label for="">Débiteur / Mandataire</label>
                <input type="text" name="debiteur_mandataire[]" class="input border border-gray-500 w-full" value="{{$debiteur->nom}} {{$debiteur->prenom}}">
            </div>
        </div>
        <div class="mt-2">
            <label for="">Promesse</label>
            <textarea name="promesse_medi[]" class="input border border-gray-500 w-full h-32" placeholder="promesses...">{{$item->promesse}}</textarea>
        </div>
        <div class="mt-2">
            <label for="">Commentaires</label>
            <textarea name="comment_medi[]" class="input border border-gray-500 w-full h-32" placeholder="Commentaires...">{{$item->comment}}</textarea>
        </div>
    </div>
</div>