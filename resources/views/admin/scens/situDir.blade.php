@if (isset($SituationDirect))
<div class="accordion intro-y box px-5 py-8 mt-5 font-semibold SituationDirectHtml" >
    <div class="accordion__pane "> 
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
            <span class="text-xl">
                Citation Direct
            </span>    
        </a>
        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
            <div class="mt-1">
                <textarea name="situ_direc_rmarq" class="input w-full border border-gray-500 h-32" placeholder="Remarque...">{{$SituationDirect->remarque}}</textarea>
            </div>
            <div class="mt-3">
                <div class="p-8">
                    <label for="" class="font-semibold">Pièces jointes</label>
                    <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                        <div class="flex flex-wrap px-4 file_block" >
                            @if (count($SituationDirectFiles)>0)
                                @foreach ($SituationDirectFiles as $item)
                                    <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2">
                                        <a href="{{asset($File_path->file_path.$item->file_name)}}" target="_blank">
                                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                                <span class="w-3/5 file__icon file__icon--file mx-auto">
                                                    <div class="file__icon__file-name"></div>
                                                </span>
                                                <span class="block font-medium mt-4 text-center truncate">{{$item->org_file_name}}</span>
                                            </div>
                                        </a>
                                        <input type="text" name="" id="">
                                        <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file_base " data-id="kill_file_base{{$item->id}}"> 
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                                        </div>
                                    </div>
                                    <input type="hidden" name="file_exc[]" value="{{$item->file_name}}" class="kill_file_base{{$item->id}}">
                                @endforeach
                            @endif
                        </div>
                        <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                            <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                            </button>
                            <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0" name="file_val[]" data-stp="not">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
@endif