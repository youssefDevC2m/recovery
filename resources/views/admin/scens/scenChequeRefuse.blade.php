@extends('admin.admin')
@section('title')
PLAINTE CHEQUE SANS PROVISION
@endsection
<div class="accordion intro-y box px-5 py-8 mt-5 font-semibold conatains_judic_chek">
    <div class="accordion__pane active"> 
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
            <span class="text-xl">
                PLAINTE CHEQUE SANS PROVISION
            </span>    
        </a>
        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
            <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Référence</label>
                    <input type="text" name="ref_st_cheqe" value="{{$ScenJudicCheq->ref_st_cheqe}}" class="input w-full border border-gray-500 mt-2" placeholder="Entrez un référence">
                </div>
                <div class="mt-2">
                    <label for="">Tribunal spécialisé</label>
                    <input type="text" name="tribunal_spese" value="{{$ScenJudicCheq->tribunal_spese}}" class="input w-full border border-gray-500 mt-2" placeholder="Entrez tribunal spécialisé">
                </div>
            </div>
            <div class="my-2">
                <label for="" class="text-lg ">les partie</label>
            </div>
            <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="">
                    <div class="mt-2">
                        <label for="">Le Tireur</label>
                        <input type="text" name="tireur" value="{{$ScenJudicCheq->tireur}}" class="input w-full border border-gray-500 mt-2" placeholder="Entrez le tireur">
                    </div>
                    <div class="mt-2">
                        <label for="">Adresse</label>
                        <input type="text" name="adresse_cheq" value="{{$ScenJudicCheq->adresse_cheq}}" class="input w-full border border-gray-500 mt-2" placeholder="Entrez une adresse">
                    </div>
                    <div class="mt-2">
                        <label for="">C.i.n</label>
                        <input type="text" name="cin_cheq" value="{{$ScenJudicCheq->cin_cheq}}" class="input w-full border border-gray-500 mt-2" placeholder="Entrez le cin de tireur">
                    </div>
                    <div class="mt-2">
                        <label for="">le mandataire</label>
                        <select name="procureur_tir" class="input w-full border border-gray-500 mt-2">
                            <option value='0'>Sélectionnez un mandataire</option>
                            @foreach($partner as $item)
                                <option value='{{$item->id}}' {{($ScenJudicCheq->procureur_tir==$item->id)? 'selected' : ''}}>{{$item->nom_part}} {{$item->prenom_part}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="">
                    <div class="mt-2">
                        <label for="">Bénéficiaire</label>
                        <input type="text" name="benefic" value="{{$ScenJudicCheq->benefic}}" class="input w-full border border-gray-500 mt-2" placeholder="Entrez le bénéficiaire">
                    </div>
                    <div class="mt-2">
                        <label for="">le mandataire</label>
                        <input type="text" name="procureur_tir_benif" value="{{$ScenJudicCheq->procureur_tir_benif}}" class="input w-full border border-gray-500 mt-2" placeholder="Entrez le procureur de bénéficiaire">
                    </div>
                    <div class="mt-2">
                        <label for="">Chargé De Suivi</label>
                        <input type="text" name="charge_suiv" value="{{$ScenJudicCheq->charge_suiv}}" class="input w-full border border-gray-500 mt-2" placeholder="Entrez Le Chargé De Suivi">
                    </div>
                </div>
            </div>
            <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">La Banque</label>
                    <input type="text" class="input w-full border border-gray-500 mt-2" placeholder="Entrez la banque" value="{{$ScenJudicCheq->banque_tire}}" name="banque_tire" >
                </div>
                <div class="mt-2">
                    <label for="">Adresse</label>
                    <input type="text" class="input w-full border border-gray-500 mt-2" placeholder="Entrez l'adresse" value="{{$ScenJudicCheq->adrs_tire}}" name="adrs_tire" >
                </div>
                <div class="mt-2">
                    <label for="">Numéro du compte</label>
                    <input type="text" class="input w-full border border-gray-500 mt-2" placeholder="Entrez le numéro de compte"  value="{{$ScenJudicCheq->n_compt}}"  name="n_compt" >
                </div>
            </div>
            <div class="my-5">
                <label for="" class="text-lg">Informations sur chèque</label>
            </div>
            <div class="cheqs">
                @foreach ($infoCheq as $item)
                    <div class="grid lg:grid-cols-4 md:grid-cols-1 sm:grid-cols-1 gap-1 border-b pb-3 ">
                        <input type="hidden" name="cheque_nb[]" value="1">
                        <div class="mt-2">
                            <label for="">Montant du chèque</label>
                            <div class="flex flex-row">
                                <input type="number" class="input w-full border border-gray-500 mt-2"  min="0" step="0.5" name="montant_cheq[]"  value="{{$item->montant_cheq}}"  placeholder="Entrez le montant du chèque">
                                <svg style="margin-top: 1rem;" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle mx-auto ml-2 block lg:hidden kill_cheq hover:text-red-500"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                            </div>
                        </div>
                        <div class="mt-2">
                            <label for="">Numéro du chèque</label>
                            <input type="number" class="input w-full border border-gray-500 mt-2"  min="0" step="0.5" name="n_cheq[]" value="{{$item->n_cheq}}" placeholder="Saisissez le numéro du chèque">
                        </div>
                        <div class="mt-2">
                            <label for="">Date D’émission </label>
                            <input type="date" class="input w-full border border-gray-500 mt-2" name="date_emission[]" value="{{date('Y-m-d',strtotime($item->date_emission))}}">
                        </div>
                        <div class="mt-2">
                            <label for="">Motif</label>
                            <div class="flex flex-row">
                                <input type="text" class="input w-full border border-gray-500 mt-2" name="motif[]" placeholder="Entrez le motif" value="{{$item->motif}}">
                                <svg style="margin-top: 1rem;" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle mx-auto ml-2 hidden lg:block kill_cheq hover:text-red-500"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
                            </div>
                        </div>
                    </div>   
                @endforeach
            </div>
            <div class="grid justify-items-center my-3">
                <div class="flex flex-row hover:text-theme-33 add_cheq">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle mx-auto"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                    <span class="" style="margin-top: 0.1rem">Plus</span>
                </div>
            </div>
            <div class="mt-2 grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div>
                    <label for="">Montant Global </label>
                    <input type="number" name="montant_glob" step="0.1" min="0" class="input w-full border border-gray-500 mt-2 " placeholder="Montant Global" value="{{$ScenJudicCheq->montant_glob}}">
                </div>
            </div>
            <div class="grid lg:justify-items-end lg:mt-0 mt-3 my-2">
                <div class="flex flex-row">
                    <label class="text-gray-600">La Phase De Première Instance </label>
                    <input type="checkbox" class="input input--switch border ml-2 prem_inst" name="prem_inst" {{($ScenJudicCheq->plainte_ck=='on')? 'checked':''}}> 
                </div>
            </div>
        </div>    
    </div>
    @if ($ScenJudicCheq->plainte_ck=='on')
    <div class="accordion__pane  prem_insthtml border-t"> 
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
            <span class="text-xl">
                LA PHASE DE PREMIÈRE INSTANCE
            </span>    
        </a>
        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
            <div class="grid justify-items-center my-3">
                <label for="" class="text-lg">Procédure De La Plainte</label>
            </div>
            <div class="mt-2">
                <label for="">Tribunal compétent</label>
                <input type="text" class="input w-full mt-2 border border-gray-500" name="tribnal_compet_cheq" value="{{$ScenJudicCheq->tribnal_compet_cheq}}" placeholder="Tribunal compétent">
            </div>
            <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Date De La Plainte</label>
                    <input type="date" class="input w-full mt-2 border border-gray-500" name="date_plaint_cheq" value="{{date('Y-m-d',strtotime($ScenJudicCheq->date_plaint_cheq))}}">
                </div>
                <div class="mt-2">
                    <label for="">Numéro De La Plainte</label>
                    <input type="text" class="input w-full mt-2 border border-gray-500" name="num_plaint_cheq" value="{{$ScenJudicCheq->num_plaint_cheq}}" placeholder="Numéro De La Plainte">
                </div>
                <div class="mt-2">
                    <label for="">Date De Transfère à L’autorité Central</label>
                    <input type="date" class="input w-full mt-2 border border-gray-500" name="date_trans_autorite_center_cheq" value="{{date('Y-m-d',strtotime($ScenJudicCheq->date_trans_autorite_center_cheq))}}">
                </div>
                <div class="mt-2">
                    <label for="">Numéro De Transfère</label>
                    <input type="text" class="input w-full mt-2 border border-gray-500" name="num_trans_cheq" value="{{$ScenJudicCheq->num_trans_cheq}}" placeholder="Numéro De Transfère">
                </div>
            </div>
            <div class="mt-2 grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div>
                    <label for="">Date De Transfère à L’autorité Compétente</label>
                    <input type="date" class="input w-full mt-2 border border-gray-500" name="DTAC" value="{{date('Y-m-d',strtotime($ScenJudicCheq->DTAC))}}"> 
                </div>
            </div>
            <div class="mt-2">
                <label for="">Le Sort De La Plainte</label>
                <textarea name="sort_plainte_cheq" class="input h-32 w-full mt-2 border border-gray-500" placeholder="Le Sort De La Plainte...">{{$ScenJudicCheq->sort_plainte_cheq}}</textarea>
            </div>
            <div class="mt-2 grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div>
                    <label for="">Date De Rappel</label>
                    <input type="date" class="input w-full mt-2 border border-gray-500" name="date_rappel_cheq" value="{{date('Y-m-d',strtotime($ScenJudicCheq->date_rappel_cheq))}}"> 
                </div>
            </div>
            <div class="mt-2">
                <label for="">Le Sort De Rappel</label>
                <textarea name="sort_rappel_cheq" class="input h-32 w-full mt-2 border border-gray-500" placeholder="Le Sort De Rappel...">{{$ScenJudicCheq->sort_rappel_cheq}}</textarea>
            </div>
            <div class="grid justify-items-center my-3">
                <label for="" class="text-lg">L’assignation Et L’interrogation</label>
            </div>
            
            <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Date De L’assignation Et L’interrogation</label>
                    <input type="date" class="input  w-full mt-2 border border-gray-500" name="DAI_cheq" value="{{date('Y-m-d',strtotime($ScenJudicCheq->DAI_cheq))}}">
                </div>
                <div class="mt-2">
                    <label for="">Date De Transfère Au Parquet</label>
                    <input type="date" class="input  w-full mt-2 border border-gray-500" name="DTP_cheq" value="{{date('Y-m-d',strtotime($ScenJudicCheq->DTP_cheq))}}">
                </div>
                <div class="">
                    <label for="">Date D’honoré</label>
                    <input type="date" class="input  w-full mt-2 border border-gray-500" name="date_honorer" value="{{date('Y-m-d',strtotime($ScenJudicCheq->date_honorer))}}">
                </div>
                <div class="">
                    <label for="">Date De Non Honoré</label>
                    <input type="date" class="input  w-full mt-2 border border-gray-500" name="date_non_honor" value="{{date('Y-m-d',strtotime($ScenJudicCheq->date_non_honor))}}">
                </div>
            </div> 
            <div class="grid justify-items-center my-3">
                <label for="" class="text-lg">Les Audiences Et Le Jugement </label>
            </div>
            <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Numéro Du Dossier Pénale</label>
                    <input type="text" class="input w-full mt-2 border border-gray-500" name="num_dos_penal" value="{{$ScenJudicCheq->num_dos_penal}}" placeholder="Numéro Du Dossier Pénale">
                </div>
                <div class="mt-2">
                    <label for="">Date D’audience</label>
                    <input type="date" class="input w-full mt-2 border border-gray-500" name="date_audience" value="{{date('Y-m-d',strtotime($ScenJudicCheq->date_audience))}}">
                </div>
            </div>
            <div class="mt-2">
                <label for="">Les Audiences Suivantes</label>
                <textarea name="audiences_suivantes" class="input w-full mt-2 h-32 border border-gray-500" placeholder="Les Audiences Suivantes">{{$ScenJudicCheq->audiences_suivantes}}</textarea>
            </div>
            <div class="mt-2">
                <label for="">Le Jugement</label>
                <textarea name="jugement" class="input w-full mt-2 h-32 border border-gray-500" placeholder="Le Jugement">{{$ScenJudicCheq->jugement}}</textarea>
            </div>
            <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Date D’appel De jugement De Première Instance</label>
                    <input type="date" name="DAJPI" value="{{date('Y-m-d',strtotime($ScenJudicCheq->DAJPI))}}" class="input w-full mt-2 border border-gray-500" >
                </div>
                <div class="mt-2">
                    <label for="">Ordre De Transfère Au Cour D’appel</label>
                    <input type="text" name="OTCA" value="{{$ScenJudicCheq->OTCA}}" class="input w-full mt-2 border border-gray-500" placeholder="Ordre De Transfère Au Cour D’appel">
                </div>
            </div>
            <div class="grid justify-items-center my-3">
                <label for="" class="text-lg">Les Faits</label>
            </div>
            <div class="mt-2">
                <label for="">Débiteur :(Voir Le Procès Verbal De La Police Judiciaire)</label>
                <textarea name="Rprt_plc_judic" class="input w-full mt-2 h-32 border border-gray-500" placeholder="Procès Verbal De La Police Judiciaire">{{$ScenJudicCheq->Rprt_plc_judic}}</textarea>
            </div>
            <div class="mt-2">
                <label for="">Police Judiciaire</label>
                <input type="text" name="polic_judic" value="{{$ScenJudicCheq->polic_judic}}" class="input w-full mt-2 border border-gray-500" placeholder="Police Judiciaire">
            </div>
            <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2 mt-2">
                <div class="mt-2">
                    <label for="">Chargé Du Dossier</label>
                    <input type="text" name="chargeDossJurid" value="{{$ScenJudicCheq->chargeDossJurid}}" class="input w-full mt-2 border border-gray-500" placeholder="Police Judiciaire" >
                </div>
                <div class="mt-2">
                    <label for="">Numéro De Contact De Téléphone</label>
                    <input type="text" name="NCT" value="{{$ScenJudicCheq->NCT}}" class="input w-full mt-2 border border-gray-500" placeholder="Numéro De Contact De Téléphone" >
                </div>
            </div>
            <div class="mt-2">
                <textarea name="p_j_faits_cheq" class="input w-full mt-2 h-32 border border-gray-500" placeholder="Observation">{{$ScenJudicCheq->p_j_faits_cheq}}</textarea>
            </div>
            <div class="grid justify-items-center my-3">
                <label for="" class="text-lg">Citations</label>
            </div>
            <div class="mt-2">
                <label for="">Entité Et Date</label>
                <textarea name="entiteDate" class="input w-full mt-2 h-32 border border-gray-500" placeholder="Entité Et Date">{{$ScenJudicCheq->entiteDate}}</textarea>
            </div>
            <div class="grid lg:justify-items-end lg:mt-0 mt-3 my-2">
                <div class="flex flex-row">
                    <label class="text-gray-600">La Phase D’appel</label>
                    <input type="checkbox" class="input input--switch border ml-2 phase_appel" name="phase_appel" {{($ScenJudicCheq->appel_ck=='on')? 'checked' : ''}}> 
                </div>
            </div>
        </div>
    </div> 
    @endif
    @if ($ScenJudicCheq->appel_ck=='on')
    <div class="accordion__pane phase_appelhtml"> 
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
            <span class="text-xl">
                LA PHASE D’APPEL
            </span>    
        </a>
        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
            <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Tribunal Compétent</label>
                    <input type="text" name="tribunal_competnt_fazeApl_cheque" value="{{$ScenJudicCheq->tribunal_competnt_fazeApl_cheque}}" class="input w-full border border-gray-500 mt-2" placeholder="Tribunal Compétent">
                </div>
                <div class="mt-2">
                    <label for="">Numéro Du Dossier D’appel</label>
                    <input type="text" name="Ndos_fazeApl_cheque" value="{{$ScenJudicCheq->Ndos_fazeApl_cheque}}" class="input w-full border border-gray-500 mt-2" placeholder="Numéro Du Dossier D’appel">
                </div>
                <div class="mt-2">
                    <label for="">Date Première Audience</label>
                    <input type="date" name="dt_p_aud_fazeApl_cheque" value="{{date('Y-m-d',strtotime($ScenJudicCheq->dt_p_aud_fazeApl_cheque))}}" class="input w-full border border-gray-500 mt-2" placeholder="Date Première Audience" >
                </div>
            </div>
            <div class="mt-2">
                <label for="">Les Audiences Suivantes</label>
                <textarea name="aud_suivan_fazeApl_cheque" class="input w-full border border-gray-500 mt-2 h-32" placeholder="Les Audiences Suivantes">{{$ScenJudicCheq->aud_suivan_fazeApl_cheque}}</textarea>
            </div>
            <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Date Délibération</label>
                    <input type="date" name="date_delib_fazeApl_cheque" value="{{date('Y-m-d',strtotime($ScenJudicCheq->date_delib_fazeApl_cheque))}}" class="input w-full border border-gray-500 mt-2" value="` + curr + `">
                </div>
            </div>
            <div class="mt-2">
                <label for="">Dispositif Du Décision</label>
                <textarea name="dispo_deci_fazeApl_cheque" class="input w-full border border-gray-500 mt-2 h-32" placeholder="Dispositif Du Décision">{{$ScenJudicCheq->dispo_deci_fazeApl_cheque}}</textarea>
            </div>
            <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Date De Retrait De Jugement</label>
                    <input type="date" name="dt_retrait_jug_fazeApl_cheque" class="input w-full border border-gray-500 mt-2"  value="{{date('Y-m-d',strtotime($ScenJudicCheq->dt_retrait_jug_fazeApl_cheque))}}">
                </div>
                <div class="mt-2">
                    <label for="">Date Du Dossier</label>
                    <input type="date" name="dt_dos_fazeApl_cheque" class="input w-full border border-gray-500 mt-2" value="{{date('Y-m-d',strtotime($ScenJudicCheq->dt_dos_fazeApl_cheque))}}">
                </div>
                <div class="mt-2">
                    <label for="">Dépôt Du Dossier Au Tribunal</label>
                    <input type="text" name="depo_dos_tribunal_fazeApl_cheque" class="input w-full border border-gray-500 mt-2" placeholder="Dépôt Du Dossier Au Tribunal" value="{{$ScenJudicCheq->depo_dos_tribunal_fazeApl_cheque}}">
                </div>
            </div>
        </div>
    </div>
    @endif
    
</div>