@extends('admin.admin')
@section('title')
arrêt bancaire
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i
            data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ajouter une saisie arrêt bancaire</a> </div>
@endsection
@section('content')
    @include('admin.scens.saisie_form')
@endsection
@section('script')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            var i = 0;
            $('body').on('click', '.add_deroul', function() {

                event.preventDefault();

                $.ajax({
                    url: `{{ route('render_fond_comerc_deroul') }}`,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        date_result: $('.date_result').val(),
                        result: $('.result').val(),
                        index: i,
                    },
                    success: function(data) {
                        // console.log(data);
                        $('.result_list').append(data);
                        i++;
                    },
                    error: function(error) {
                        console.log(error);
                    },
                });
            });
            $('body').on('click', '.delete', function() {
                id = `#row${$(this).data('index')}`
                $(this).parent().remove();
                $(id).remove();
            });
            $("body").on('click', '.edit', function() {
                var index = $(this).data('index');
                var date = $(this).data('date');
                var result = $(this).data('result');
                $('.date_result').val(date);
                $('.result').val(result);
                $('.edit_deroul').data('id', `row${index}`);
                $('.add_deroul').hide('normal');
                $('.edit_deroul').show('normal');
            });
            $('body').on('click', '.edit_deroul', function() {
                event.preventDefault();
                var id_container = `#${$(this).data('id')}`;
                var new_date = $('.date_result').val();
                var new_result = $('.result').val();
                var date = new Date(new_date);
                // -==============================================================================================-//
                $(id_container).find('.date_deroul_aud').text(date.toLocaleDateString(
                    'fr-FR')); //change date text
                $(id_container).find('.result_aud').text(new_result); //change result text
                $(id_container).find('.date_result_val').val(new_date); //change date values
                $(id_container).find('.result_val').val(new_result); //change result values
                // -==============================================================================================-//
                $(this).hide('normal');
                $('.add_deroul').show('normal');
            });
            $("body").on('click', '.save', function() {
                event.preventDefault();

                formdata = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url: `{{ route('add_saisie_arret_bancaire') }}`,
                    dataType: 'json',
                    type: 'post',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data) {
                        console.log(data);
                        if (data.statue == 'success') {
                            swal('Validé', data.message, data.statue)
                            .then(()=>{
                                window.location.replace(data.link);
                            });
                        }
                    },
                    error: function(error) {
                        swal('Erreur', '', 'error');
                        console.log(error);
                    },
                });
            });
            var kill_nb = 0;

            $('body').on('change', '.this_file', function(input) {
                var file = $(this).get(0).files[0];
                // alert(file);
                if (file) {
                    var reader = new FileReader();
                    $(this).parent().parent().find('.file_block').append(
                        `
                        <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">
                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                <span class="w-3/5 file__icon file__icon--file mx-auto">
                                    <div class="file__icon__file-name"></div>
                                </span>
                                <span class="block font-medium mt-4 text-center truncate">` + file.name +
                        `</span>
                            </div>
                            <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file` +
                        kill_nb + `"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                            </div>
                        </div>
                        `
                    );

                    $(this).parent().find('button').hide();
                    $(this).parent().append(
                        `
                        <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                        </button>
                        <input type="file" name="file_val[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file` +
                        kill_nb + `">
                    `);
                    kill_nb++;
                }
            });

            $("body").on('click tap', '.kill_file,.kill_file_base', function() {
                $('.' + $(this).data('id')).attr('name', '');
                $(this).parent().toggle();

            });
        });
    </script>
@endsection
