@extends('admin.admin')
@section('title')
    Liste des plantes Chèques Sans Provision
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i
            data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Liste des
            plantes Chèques Sans Provision</a> </div>
@endsection
@section('content')
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-8">
        {{-- <a href="{{route('form_Cheque',[$id_hist])}}" class="button text-white bg-theme-1 shadow-md mr-2">Ajouter une plainte</a> --}}
        <div class="text-center">
            <a href="javascript:;" data-toggle="modal" data-target="#header-footer-modal-preview"
                class="button inline-block bg-theme-1 text-white">Ajouter une plaintel</a>
        </div>
        <div class="modal" id="header-footer-modal-preview">
            <div class="modal__content">
                <form action="{{ route('form_Cheque', [$id_hist]) }}">
                    @csrf
                    <div class="flex items-center px-5 py-5 sm:py-3 border-b border-gray-200 dark:border-dark-5">
                        <h2 class="font-medium text-base mr-auto">Broadcast Message</h2>
                    </div>
                    <div class="p-5 grid grid-cols-12 gap-4 row-gap-3 font-semibold">
                        <div class="col-span-12 sm:col-span-6">
                            <label>Validation pièces</label>
                            <div class="flex flex-row">
                                <div class="">
                                    <label for="">Oui</label>
                                    <input required type="radio" name="vald_pieces"
                                        class="input w-full border border-gray-400 mt-2 flex-1" value="1"
                                        style="transform: translate(5px, 4px);">
                                </div>
                                <div class="ml-3">
                                    <label for="">Non</label>
                                    <input required type="radio" name="vald_pieces"
                                        class="input w-full border border-gray-400 mt-2 flex-1" value="0"
                                        style="transform: translate(5px, 4px);">
                                </div>
                            </div>
                        </div>
                        <div class="col-span-12 sm:col-span-6"> </div>
                        <div class="col-span-12 sm:col-span-6">
                            <label>Provision</label>
                            <input required type="text" class="input w-full border border-gray-400 mt-2 flex-1"
                                name="provi" placeholder="Provision">
                        </div>

                        <div class="col-span-12 sm:col-span-6">
                            <label>Avocat</label>
                            <select name="avocat" class="input w-full border border-gray-400 mt-2 flex-1" required>
                                <option value="">Sélectionnez un avocat</option>
                                @foreach ($avocats as $item)
                                    <option value="{{ $item->id }}">{{ $item->prenom_part . ' ' . $item->nom_part }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-span-12 sm:col-span-6">
                            <label>Date début</label>
                            <input required type="date" name="date_debut_plant" class="input w-full border mt-2 flex-1"
                                value="{{ date('Y-m-d') }}">
                        </div>

                        <div class="col-span-12 sm:col-span-6">
                            <label>Date fin</label>
                            <input required type="date" name="date_fin_plant" class="input w-full border mt-2 flex-1"
                                value="{{ date('Y-m-d') }}">
                        </div>

                    </div>
                    <div class="px-5 py-3 text-right border-t border-gray-200 dark:border-dark-5">
                        <button type="button" data-dismiss="modal"
                            class="button w-20 border text-gray-700 dark:border-dark-5 dark:text-gray-300 mr-1">Annuler</button>
                        <button type="submit" class="button bg-theme-1 text-white">Continuez</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="hidden md:block mx-auto text-gray-600"></div>
    </div>

    <div class="intro-y box px-5 py-8 mt-5">
        <div class="intro-y col-span-12 xl:overflow-x-scroll">
            <table class="table table-report -mt-2 border-none" style="border-style: none !important;" id="table_id">
                <thead style="border-style: none !important;">
                    <tr class="shadow-lg">
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">id</th>
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Situation</th>
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Mandataire</th>
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Date de création
                        </th>
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <?php
                        $res = myFun::AlertColor($item->date_fin_plant,$item->id);
                        $create = App\HistoriqueAct::where('id_action', $item->id)
                            ->where('where', 'scenChequ')
                            ->where('what', 'create')
                            ->orderby('id', 'desc')
                            ->first();
                        
                        $delete = App\HistoriqueAct::where('id_action', $item->id)
                            ->where('where', 'scenChequ')
                            ->where('what', 'delete')
                            ->orderby('id', 'desc')
                            ->first();
                        
                        if (isset($create->id) && $create->id_user != Auth::user()->id && $create->id_valid <= 0) {
                            continue;
                        }
                        
                        if (isset($delete->id) && $delete->id_user == Auth::user()->id && $delete->id_valid == 0) {
                            continue;
                        }
                        ?>
                        @if (isset($bg_alert) && $doss == $item->id)
                            <?php continue; ?>
                        @endif
                        <tr class="shadow-md rounded-lg intro-y ">
                            <td style="{{$res['style']}}" class="w-40">
                                <div class="">
                                    <div class="text-center">
                                        {{ $res['id_row'] }}
                                    </div>
                                </div>
                            </td>
                            <td style="{{$res['style']}}" class="w-40">
                                <div class="">
                                    <div class="text-center">
                                        Plainte Cheque Sans Provision
                                    </div>
                                </div>
                            </td>

                            <td style="{{$res['style']}}" class="w-40">
                                <div class="text-center">
                                    <div class="bg-theme-33 text-white rounded-lg">
                                        {{ $item->nom_part }} {{ $item->prenom_part }}
                                    </div>
                                </div>
                            </td>
                            <td style="{{$res['style']}}" class="w-40">
                                <div class="">
                                    <div class="text-center">
                                        {{ date('m-d-Y', strtotime($item->created_at)) }}
                                    </div>
                                </div>
                            </td>
                            <td style="{{$res['style']}}" class="table-report__action w-56">
                                <div class="flex justify-center items-center">
                                    <a class="flex items-center my-4" href="{{ route('show_Cheque', [$item->id]) }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                            stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-check-square w-4 h-4 mr-1">
                                            <polyline points="9 11 12 14 22 4"></polyline>
                                            <path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path>
                                        </svg>
                                        Modifier
                                    </a>
                                    <a class="flex items-center text-theme-6 mx-3 my-4" href="javascript:;"
                                        data-toggle="modal" data-target="#delete-confirmation-modal{{ $item->id }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                            stroke-linecap="round" stroke-linejoin="round"
                                            class="feather feather-trash-2 w-4 h-4 mr-1">
                                            <polyline points="3 6 5 6 21 6"></polyline>
                                            <path
                                                d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                            </path>
                                            <line x1="10" y1="11" x2="10" y2="17"></line>
                                            <line x1="14" y1="11" x2="14" y2="17"></line>
                                        </svg>
                                        Supprimer
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <!-- BEGIN: Delete Confirmation Modal -->
                        <div class="modal" id="delete-confirmation-modal{{ $item->id }}">
                            <div class="modal__content">
                                <div class="p-5 text-center">
                                    <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                                    <div class="text-3xl mt-5">Suppression !</div>
                                    <div class="text-gray-600 mt-2">Voulez-vous vraiment supprimer ces enregistrements ? Il
                                        n'y pas de retour en arriere.</div>
                                </div>
                                <div class="px-5 pb-8 text-center">
                                    <button type="button" data-dismiss="modal"
                                        class="button w-24 border text-gray-700 mr-1">Annuler</button>
                                    <a class="button w-24 bg-theme-6 text-white"
                                        href="{{ route('delete_cheques', [$item->id]) }}">Supprimer</a>
                                </div>

                            </div>
                        </div>
                        <!-- END: Delete Confirmation Modal -->
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('dist/js/dataTbl.js') }}"></script>
    <script>
        $("body").ready(function() {
            $('.dossier').addClass('side-menu--active');
        });
    </script>
@endsection
