<div class="w-full mt-5">
    <div class="grid grid-cols-1 lg:grid-cols-2 gap-2">
        @foreach ($Procedure_range as $item)
            <form action="" class="form_data">
                <div class="grid-cols-1 box p-4 intro-y">
                    <div class="mb-3">
                        <label class="text-lg text-gray-600 font-semibold">
                            @if (!empty($item->type))
                                <?php
                                    $ref_st_cheqe = ((isset(\App\ScenJudicCheq::where('id', $item->id_histo)->first()->ref_st_cheqe))? \App\ScenJudicCheq::where('id', $item->id_histo)->first()->ref_st_cheqe :'');
                                    $ref_inj_p = ((isset(\App\Injonction_De_Payer::where('id', $item->id_histo)->first()->ref_inj_p))? \App\Injonction_De_Payer::where('id', $item->id_histo)->first()->ref_inj_p :'');
                                    $created_at= ((isset(\App\ReqinjoncDpay::where('id', $item->id_histo)->first()->created_at))? date('Y-m-d', strtotime(\App\ReqinjoncDpay::where('id', $item->id_histo)->first()->created_at)) : '');
                                ?>
                                {{ $item->type == 'cheque_sn_provis_block' ? 'Cheque Sans Provision : Réf ' .$ref_st_cheqe  : '' }}
                                {{ $item->type == 'injonction_pay_block' ? 'Cheque Sans Provision : Réf ' . $ref_inj_p : '' }}
                                {{ $item->type == 'req_injonction_pay_block' ? 'Requete en Injonction De Payer : ' .$created_at  : '' }}
                                {{ $item->type == 'citation_dir_block' ? 'Citation directe  : ' . date('Y-m-d', strtotime($item->created_at)) : '' }}
                                {{ $item->type == 'ses_conserv_bien_imob_block' ? 'Saisie conservatoire de bien immobilier : ' . date('Y-m-d', strtotime($item->created_at)) : '' }}
                                {{ $item->type == 'ses_arret_bank_block' ? 'Saisie arrêt bancaire : ' . date('Y-m-d', strtotime($item->created_at)) : '' }}
                                {{ $item->type == 'plant_scam_block' ? 'Plainte d’escroquerie : ' . date('Y-m-d', strtotime($item->created_at)) : '' }}
                                {{ $item->type == 'ses_conerv_fon_com_block' ? 'Saisie conservatoire sur le fond de commerce : ' . date('Y-m-d', strtotime($item->created_at)) : '' }}
                                {{ $item->type == 'ses_conserv_bien_mob_block' ? 'Saisie conservatoire de bien mobilier : ' . date('Y-m-d', strtotime($item->created_at)) : '' }}
                                {{ $item->type == 'ses_arret_bank_entr_main_tier_block' ? 'Saisie entre les mains des tiers : ' . date('Y-m-d', strtotime($item->created_at)) : '' }}
                            @endif

                        </label>
                    </div>
                    <input type="hidden" value="{{ !empty($item->type) ? $item->type : '' }}" class="belong_"
                        name="belong_">
                    <input type="hidden" value="{{ $item->id }}" class="id_rang_" name="id_rang_">
                    <div class="">
                        <label class="font-semibold" for="">Date début</label>
                        <input type="date" value="{{ date('Y-m-d', strtotime($item->date_1)) }}"
                            class="input w-full border-2 date_1_{{$item->id}}" name="date_1_{{$item->id}}">
                    </div>
                    <div class="">
                        <label class="font-semibold" for="">Date fin</label>
                        <input type="date" value="{{ date('Y-m-d', strtotime($item->date_2)) }}"
                            class="input w-full border-2 date_2_{{$item->id}}" name="date_2_{{$item->id}}">
                    </div>
                    <div class="">
                        <label class="font-semibold" for="">Différence</label>
                        <input type="text" readonly value="<?php
                        $dossCont = new App\Http\Controllers\dossierController();
                        $diff = $dossCont->dateDifference(date('Y-m-d', strtotime($item->date_1)), date('Y-m-d', strtotime($item->date_2)));
                        echo $diff;
                        ?>" class="input w-full border-2">
                    </div>

                    <div class="grid justify-items-center mt-3 dropdown ml-auto sm:ml-3 tooltip"
                        title="Modifier/Supprimer" data-theme="light" style="position: relative;">
                        <div class="dropdown w-5 box hover:shadow-lg">
                            <a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-500 ">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                    stroke-linecap="round" stroke-linejoin="round"
                                    class="feather feather-more-horizontal mx-auto">
                                    <circle cx="12" cy="12" r="1"></circle>
                                    <circle cx="19" cy="12" r="1"></circle>
                                    <circle cx="5" cy="12" r="1"></circle>
                                </svg>
                            </a>
                            <div class="dropdown-box w-40">
                                <div class="dropdown-box__content box dark:bg-dark-1 p-2 text-center shadow-lg">
                                    <a data-dismiss="dropdown" data-id_range='{{ $item->id }}'
                                        
                                        class="block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md font-semibold update_range cursor-pointer">Modifier</a>
                                    <a class="block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md font-semibold text-theme-6 delete_range cursor-pointer"
                                        data-id_range='{{ $item->id }}' data-dismiss="dropdown">Supprimer</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        @endforeach
    </div>
</div>
