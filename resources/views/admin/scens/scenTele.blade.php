<div class="accordion__pane border-b border-gray-200 dark:border-dark-5 "> 
    <div class="grid grid-cols-2  hover:bg-gray-400 rounded-lg ">
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2" >
            <span class="text-xl">
                Contact téléphonique
            </span>    
        </a>
        <div class="grid justify-items-end flex items-center">
            <div title="" class=" w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 mr-2  kill_tele"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
            </div>   
        </div>
    </div>
    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
        <div class="">
            <div class="">
                <div class="grid lg:grid-cols-3 md:grid-cols-1 gap-2">
                    <div class="mt-2">
                        <label for="">Type Tél</label>
                        <select name="type_tele[]" class="input w-full border mt-2">
                            <option value="0" >Sélectionner un type</option>
                            <option value="GSM Personnel" {{($item->type_tele=="GSM Personnel")? 'selected' : ''}}>GSM Personnel</option>
                            <option value="Tél. Domicile" {{($item->type_tele=="Tél. Domicile")? 'selected' : ''}}>Tél. Domicile</option>
                            <option value="Tél. Bureau" {{($item->type_tele=="Tél. Bureau")? 'selected' : ''}}>Tél. Bureau</option>
                            <option value="Tél. Proche" {{($item->type_tele=="Tél. Proche")? 'selected' : ''}}>Tél. Proche</option>
                        </select>
                    </div>
                    <div class="mt-2">
                        <label for="">Numero de téléphone</label>
                        <input type="text" name="n_tele[]" class="input w-full border mt-2" value="{{($item->n_tele)? $item->n_tele : ''}}" placeholder="Entrer un numero">
                    </div>
                    <div class="mt-2">
                        <label for="">Description sur le numéro</label>
                        <input type="text" name="desc_num_tel[]" class="input w-full border mt-2" value="{{($item->desc_num_tel)? $item->desc_num_tel : ''}}" placeholder="Entrer un description">
                    </div>
                </div>
                <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
                    <div class="mt-2">
                        <label for="">Nom de chargé</label>
                        <input type="text" name="nom_charge[]" class="input w-full border mt-2" value="{{($item->nom_charge)? $item->nom_charge : ''}}" placeholder="Entrer le nom de chargé" >
                    </div>
                    <div class="mt-2">
                        <label for="">Date et heure de contact</label>
                        <input type="datetime-local" name="date_H_contact[]" class="input w-full border mt-2" value="{{($item->date_H_contact)?date('Y-m-d\TH:i',strtotime($item->date_H_contact)) : ''}}">
                    </div>
                </div>
                <div class="mt-2">
                    <label for="">les promesses de paiement</label>
                    <textarea name="promess_pay[]" class="input w-full mt-2 border h-64" >{{$item->promess_pay}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>