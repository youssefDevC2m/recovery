<div class="accordion intro-y box px-5 py-8 mt-5 font-semibold ">
    @foreach ($SenConvo as $item)
        <div class="accordion__pane injoncDpayHtml"> 
            <div class="grid grid-cols-2  hover:bg-gray-400 rounded-lg ">
                <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
                    <span class="text-xl">
                        Convocation
                    </span>    
                </a>
                <div class="grid justify-items-end flex items-center">
                    <div title="" class=" w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 mr-2  kill_convo"> 
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                    </div>   
                </div>
            </div>
            <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
                <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                    <div class="mt-2">
                        <label for="">Remise par</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500" name="remise_par[]" value="{{$item->remise_par}}" placeholder="Remise par">
                    </div>
                    <div class="mt-2">
                        <label for="">Date et heure</label>
                        <input type="datetime-local" class="input w-full mt-2 border border-gray-500" name="date_H_conv[]" value="{{date('Y-m-d\TH:i',strtotime($item->date_H_conv))}}">
                    </div>
                </div>
                <div class="">
                    <div class="my-2">
                        <label for="">Au:</label>
                    </div>
                    <div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2 mt-2 justify-items-center">
                        <div class="flex flex-row">
                            <label for="">Débiteur</label>
                            <input type="checkbox" name="obtenu_par[]" value="Débiteur" {{($item->obtenu_par=='Débiteur')? 'checked' : ''}} class="obtenu_par input w-full mt-2 border border-gray-500 ml-1" style="margin-top: 0.2rem;">
                        </div>
                        <div class="flex flex-row">
                            <label for="">Proche</label>
                            <input type="checkbox" name="obtenu_par[]"  value="Proche" {{($item->obtenu_par=='Proche')? 'checked' : ''}} class="obtenu_par input w-full mt-2 border border-gray-500 ml-1" style="margin-top: 0.2rem;">
                        </div>
                        <div class="flex flex-row">
                            <label for="">Employeur</label>
                            <input type="checkbox" name="obtenu_par[]" value="Employeur" {{($item->obtenu_par=='Employeur')? 'checked' : ''}} class="obtenu_par input w-full mt-2 border border-gray-500 ml-1" style="margin-top: 0.2rem;">
                        </div>
                        <div class="flex flex-row">
                            <label for="">Autres</label>
                            <input type="checkbox" name="obtenu_par[]" value="Autres" {{($item->obtenu_par=='Autres')? 'checked' : ''}} class="obtenu_par input w-full mt-2 border border-gray-500 ml-1" style="margin-top: 0.2rem;">
                        </div>
                    </div>
                </div>
                <div class="mt-2">
                    <label for="">Commentaire</label>
                    <textarea name="comment_convo[]" class="input w-full mt-2 border border-gray-500 h-32" placeholder="Commentaire...">{{$item->comment}}</textarea>
                </div>
            </div>
        </div>
    @endforeach
    <div class="added_convo"></div>
</div>