<form action="" id="data_Form">
    <div class="w-full mt-5 p-4 box intro-x">
        <div class="mb-3">
            <label class="text-lg text-gray-500 font-semibold">
                Plage De Procédure
                @if (!empty($type))
                    {{ $type == 'cheque_sn_provis_block' ? ': Cheque Sans Provision : Réf ' . \App\ScenJudicCheq::where('id', $id)->first()->ref_st_cheqe : '' }}
                    {{ $type == 'injonction_pay_block' ? ': Cheque Sans Provision : Réf ' . \App\Injonction_De_Payer::where('id', $id)->first()->ref_inj_p : '' }}
                    {{ $type == 'req_injonction_pay_block' ? 'Requete en Injonction De Payer de ' . date('Y-m-d',strtotime(\App\ReqinjoncDpay::where('id', $id)->first()->created_at))  : '' }}
                    {{ $type == 'citation_dir_block' ? 'Citation directe Réf'. \App\Citation::where('id', $id)->first()->cit_ref : '' }}
                    {{ $type == 'ses_conserv_bien_imob_block' ? 'Saisie conservatoire de bien immobilier' : '' }}
                    {{ $type == 'ses_arret_bank_block' ? 'Saisie arrêt bancaire' : '' }}
                    {{ $type == 'plant_scam_block' ? 'Plainte d’escroquerie' : '' }}
                    {{ $type == 'ses_conerv_fon_com_block' ? 'Saisie conservatoire sur le fond de commerce' : '' }}
                    {{ $type == 'ses_conserv_bien_mob_block' ? 'Saisie conservatoire de bien mobilier' : '' }}
                    {{ $type == 'ses_arret_bank_entr_main_tier_block' ? 'Saisie entre les mains des tiers' : '' }}
                @endif
            </label>
        </div>
        <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
            <input type="hidden" value="{{ !empty($type) ? $type : '' }}" class="belong" name="belong">
            <input type="hidden" value="{{$id}}" name="id_histo_from_range" class="id_histo_from_range">
            <div class="">
                <label for="">Date début</label>
                <input type="date" value="{{ date('Y-m-d') }}" class="input w-full border-2 date_1" name="date_1">
            </div>
            <div class="">
                <label for="">Date fin</label>
                <input type="date" value="{{ date('Y-m-d') }}" class="input w-full border-2 date_2" name="date_2">
            </div>
        </div>
        <div class="my-3">
            <center>
                <button class="button bg-theme-33 text-white valid_rang">Valider</button>
            </center>
        </div>
    </div>
</form>

