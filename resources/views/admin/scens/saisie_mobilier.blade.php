@extends('admin.admin')
@section('title')
    Saisie mobilier
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i
            data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Liste saisies mobiliers</a> </div>
@endsection
@section('content')
    <div class="">
        <form action="" id="form_data">
            <input type="hidden" name="id_hist" value="{{ $id_hist }}">
            <input type="hidden" name="id" value="{{ $mobilier->id }}">

            <div class="intro-x w-full box my-2">
                <div class="p-5 grid grid-cols-12 gap-4 row-gap-3 font-semibold">
                    <div class="col-span-12 sm:col-span-6">
                        <label>Validation pièces</label>
                        <div class="flex flex-row">
                            <div class="">
                                <label for="">Oui</label>
                                <input required type="radio" name="vald_pieces"
                                    class="input w-full border border-gray-400 mt-2 flex-1"
                                    {{ $mobilier->vald_pieces == 1 ? 'checked' : '' }} value="1"
                                    style="transform: translate(0px, 4px);">
                            </div>
                            <div class="ml-3">
                                <label for="">Non</label>
                                <input required type="radio" name="vald_pieces"
                                    class="input w-full border border-gray-400 mt-2 flex-1"
                                    {{ $mobilier->vald_pieces == 0 ? 'checked' : '' }} value="0"
                                    style="transform: translate(0px, 4px);">
                            </div>
                        </div>
                    </div>
                    <div class="col-span-12 sm:col-span-6"> </div>
                    <div class="col-span-12 sm:col-span-6">
                        <label>Provision</label>
                        <input required type="text" class="input w-full border border-gray-400 mt-2 flex-1"
                            name="provi" value="{{ $mobilier->provi }}" placeholder="Provision">
                    </div>

                    <div class="col-span-12 sm:col-span-6">
                        <label>Date début</label>
                        <input required type="date" name="date_debut_plant" class="input w-full border mt-2 flex-1"
                            value="{{ date('Y-m-d', strtotime($mobilier->date_debut_plant)) }}">
                    </div>

                    <div class="col-span-12 sm:col-span-6">
                        <label>Date fin</label>
                        <input required type="date" name="date_fin_plant" class="input w-full border mt-2 flex-1"
                            value="{{ date('Y-m-d', strtotime($mobilier->date_fin_plant)) }}">
                    </div>
                </div>
            </div>

            <div class="intro-x box w-full  mt-3">
                <div class="grid md:grid-cols-1 sm:grid-cols-1 lg:grid-cols-3 gap-2 p-3">
                    <div>
                        <label for="" class="font-semibold">Référence</label>
                        <input type="text" class="input w-full border" value="{{ $mobilier->ref }}" name="ref"
                            placeholder="Référence">
                    </div>
                    <div>
                        <label for="" class="font-semibold">Tribunal Compétente</label>
                        <input type="text" class="input w-full border" value="{{ $mobilier->tribunal_competant }}"
                            name="tribunal_competant" placeholder="Tribunal Compétente">
                    </div>
                    <div>
                        <label for="" class="font-semibold">Le Nom De Juge</label>
                        <input type="text" class="input w-full border" value="{{ $mobilier->non_juge }}"
                            name="non_juge" placeholder="Le Nom De Juge">
                    </div>
                </div>
            </div>
            <div class="my-2">
                <h1 class="text-lg font-bold">Les parties:</h1>
            </div>
            <div class="intro-x p-3 box ">
                <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
                    <div class="w-full">
                        <div class="my-2">
                            <label for="" class="font-semibold">Le Tireur</label>
                            <input type="text" class="input w-full border" name="le_tireur"
                                value="{{ $mobilier->le_tireur }}" placeholder="Le Tireur">
                        </div>
                        <div>
                            <label for="" class="">L’adresse</label>
                            <input type="text" class="input w-full border" name="address_tireur"
                                value="{{ $mobilier->address_tireur }}" placeholder="L’adresse">
                        </div>
                        <div>
                            <label for="" class="">Cin</label>
                            <input type="text" class="input w-full border" name="cin_treur"
                                value="{{ $mobilier->cin_treur }}" placeholder="Numéro carte d'identité">
                        </div>
                        <div class="my-2">
                            <label for="" class="">L’Avocat </label>
                            <input type="text" class="input w-full border" name="avocat_tireur"
                                value="{{ $mobilier->avocat_tireur }}" placeholder="L’Avocat De Tireur">
                        </div>
                    </div>
                    <div class="w-full">
                        <div class="my-2">
                            <label for="" class="font-semibold">Le Bénéficiaire</label>
                            <input type="text" class="input w-full border" value="{{ $mobilier->benificiaire }}"
                                name="benificiaire" placeholder="Le Bénéficiaire">
                        </div>
                        <div class="col-span-12 sm:col-span-6">
                            <label>Avocat</label>
                            <select name="avoca_benif" class="input w-full border border-gray-400 mt-2 flex-1">
                                <option value="">Sélectionnez un avocat</option>
                                @foreach ($partner as $item)
                                    <option value="{{ $item->id }}" {{ $mobilier->avoca_benif == $item->id ? 'selected' : '' }}> {{ $item->prenom_part . ' ' . $item->nom_part }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="grid lg:grid-cols-3 lg:grid-cols-1 lg:grid-cols-1">
                    <div class="col-span-1">
                        <label for="" class="font-semibold">Montant Global De la Créance</label>
                        <input type="number" min="0" step="0.1" class="input w-full border"
                            value="{{ $mobilier->montant_creance }}" name="montant_creance"
                            placeholder="Montant Global De la Créance">
                    </div>
                </div>
            </div>
            <div class="my-2">
                <h1 class="text-lg font-bold">Procédure De Dépôt:</h1>
            </div>
            <div class="intro-x p-3 box">
                <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
                    <div class="my-2">
                        <label for="" class="font-semibold">Numéro Du Dossier</label>
                        <input type="text" class="input w-full border" value="{{ $dossier->ref }}"
                            placeholder="Numéro Du Dossier" disabled>
                    </div>
                    <div class="my-2">
                        <label for="" class="font-semibold">Date De Dépôt</label>
                        <input type="date" class="input w-full border" name="date_depot"
                            value="{{ date('Y-m-d', strtotime($mobilier->date_depot)) }}">
                    </div>
                    <div class="my-2">
                        <label for="" class="font-semibold">Huissier Désigné</label>
                        <input type="text" class="input w-full border" name="huissier_designie"
                            value="{{ $mobilier->huissier_designie }}" placeholder="Huissier Désigné">
                    </div>
                    <div class="grid lg:grid-cols-6 md:grid-cols-1 sm:grid-cols-1 gap-2 my-2 place-items-center">
                        <div class="col-span-3">
                            <label for="" class="font-semibold">Acceptation De La Demande :</label>
                            <input type="radio" class="input w-full border border-gray-500" name="accep_demande"
                                {{ $mobilier->accep_demande == 1 ? 'checked' : '' }} value="1"
                                style="transform: translateY(3px);">
                        </div>
                        <div class="col-span-3">
                            <label for="" class="font-semibold">Refus De La Demande :</label>
                            <input type="radio" class="input w-full border border-gray-500"
                                {{ $mobilier->accep_demande == 0 ? 'checked' : '' }} name="accep_demande"
                                value="0" style="transform: translateY(3px);">
                        </div>
                    </div>
                    <div class="my-2 col-span-2">
                        <label for="" class="font-semibold">Résultat De La Notification</label>
                        <textarea name="reslt_notif" class="input w-full border" placeholder="Résultat De La Notification...">{{ $mobilier->reslt_notif }}</textarea>
                    </div>
                </div>
            </div>
            <div class="my-2">
                <h1 class="text-lg font-bold">Déroulement d’audience :</h1>
            </div>
            <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                <div class="intro-x p-3 box">
                    <div class="my-2 lg:w-2/4">
                        <label for="" class="font-semibold">Date</label>
                        <input type="date" class="input w-full border date_result" name=""
                            value="{{ date('Y-m-d') }}">
                    </div>
                    <div class="my-2">
                        <label for="" class="font-semibold">Résultat</label>
                        <textarea name="" class="input w-full border result" placeholder="Résultat..."></textarea>
                    </div>
                    <center>
                        <div>
                            <button class="button w-24 mr-1 mb-2 bg-theme-33 text-white mt-4 add_deroul">Ajouter</button>
                            <button class="button w-24 mr-1 mb-2 bg-theme-9 text-white mt-4 edit_deroul hidden"
                                data-id=''>Modifier</button>
                        </div>
                    </center>
                </div>
                <div class="overflow-y-scroll overflow-x-none h-64 ">
                    <div class="result_list p-2 overflow-x-none">
                        <?php $count = 0; ?>
                        @foreach ($mobilier_infos as $index => $item)
                            <?php
                            $date_result = $item->date_result;
                            $result = $item->result;
                            ?>
                            @include('admin.scens.fond_comerc_deroulement_audiance')
                            <?php $count = $index + 1; ?>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="box mt-3">
                <div class="p-8">
                    <label for="" class="font-semibold">Pièces jointes</label>
                    <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                        <div class="flex flex-wrap px-4 file_block">
                            @if (count($files) > 0)
                                @foreach ($files as $item)
                                    <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2">
                                        <a href="{{ asset($File_path->file_path . $item->file_name) }}" target="_blank">
                                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                                <span class="w-3/5 file__icon file__icon--file mx-auto">
                                                    <div class="file__icon__file-name"></div>
                                                </span>
                                                <span
                                                    class="block font-medium mt-4 text-center truncate">{{ $item->org_file_name }}</span>
                                            </div>
                                        </a>
                                        <input type="text" name="" id="">
                                        <div title=""
                                            class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file_base "
                                            data-id="kill_file_base{{ $item->id }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                                                class="feather feather-x w-4 h-4">
                                                <line x1="18" y1="6" x2="6" y2="18">
                                                </line>
                                                <line x1="6" y1="6" x2="18" y2="18">
                                                </line>
                                            </svg>
                                        </div>
                                    </div>
                                    <input type="hidden" name="file_exc[]" value="{{ $item->file_name }}"
                                        class="kill_file_base{{ $item->id }}">
                                @endforeach
                            @endif
                        </div>
                        <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                            <button
                                class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                    stroke-linecap="round" stroke-linejoin="round"
                                    class="feather feather-file-text w-4 h-4 mr-2">
                                    <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                    <polyline points="14 2 14 8 20 8"></polyline>
                                    <line x1="16" y1="13" x2="8" y2="13"></line>
                                    <line x1="16" y1="17" x2="8" y2="17"></line>
                                    <polyline points="10 9 9 9 8 9"></polyline>
                                </svg>
                                <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span>
                            </button>
                            <input type="file"
                                class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0"
                                name="file_val[]" data-stp="not">
                        </div>
                    </div>
                </div>
            </div>
            <div class="my-3">
                <center><button class=" button w-24 mr-1 mb-2 bg-theme-1 text-white my-2 save">Enregistrer</button>
                </center>
            </div>
        </form>
    </div>
@endsection
@section('script')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            var i = `{{ $count }}`;
            $('body').on('click', '.add_deroul', function() {
                event.preventDefault();
                $.ajax({
                    url: `{{ route('render_fond_comerc_deroul') }}`,
                    dataType: 'json',
                    type: 'post',
                    data: {
                        date_result: $('.date_result').val(),
                        result: $('.result').val(),
                        index: i,
                    },
                    success: function(data) {
                        $('.result_list').append(data);
                        i++;
                    },
                    error: function(error) {
                        console.log(error);
                    },
                });
            });
            $('body').on('click', '.delete', function() {
                id = `#row${$(this).data('index')}`
                $(this).parent().remove();
                $(id).remove();
            });
            $("body").on('click', '.edit', function() {
                var index = $(this).data('index');
                var date = $(this).data('date');
                var result = $(this).data('result');
                $('.date_result').val(date);
                $('.result').val(result);
                $('.edit_deroul').data('id', `row${index}`);
                $('.add_deroul').hide('normal');
                $('.edit_deroul').show('normal');
            });
            $('body').on('click', '.edit_deroul', function() {
                event.preventDefault();
                var id_container = `#${$(this).data('id')}`;
                var new_date = $('.date_result').val();
                var new_result = $('.result').val();
                var date = new Date(new_date);
                // -==============================================================================================-//
                $(id_container).find('.date_deroul_aud').text(date.toLocaleDateString(
                    'fr-FR')); //change date text
                $(id_container).find('.result_aud').text(new_result); //change result text
                $(id_container).find('.date_result_val').val(new_date); //change date values
                $(id_container).find('.result_val').val(new_result); //change result values
                // -==============================================================================================-//
                $(this).hide('normal');
                $('.add_deroul').show('normal');
            });
            $("body").on('click', '.save', function() {
                event.preventDefault();

                formdata = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url: `{{ route('update_saisie_mobilier') }}`,
                    dataType: 'json',
                    type: 'post',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data) {
                        console.log(data);
                        if (data.statue == 'success') {
                            swal('Validé', data.message, data.statue)
                                .then(() => {
                                    window.location.replace(data.link);
                                });
                        }
                    },
                    error: function(error) {
                        swal('Erreur', '', 'error');
                        console.log(error);
                    },
                });
            });
            var kill_nb = 0;
            $('body').on('change', '.this_file', function(input) {
                var file = $(this).get(0).files[0];
                if (file) {
                    var reader = new FileReader();
                    $(this).parent().parent().find('.file_block').append(
                        `
                        <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">
                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                <span class="w-3/5 file__icon file__icon--file mx-auto">
                                    <div class="file__icon__file-name"></div>
                                </span>
                                <span class="block font-medium mt-4 text-center truncate">` + file.name +
                        `</span>
                            </div>
                            <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file` +
                        kill_nb + `"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                            </div>
                        </div>
                        `
                    );

                    $(this).parent().find('button').hide();
                    $(this).parent().append(
                        `
                        <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                        </button>
                        <input type="file" name="file_val[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file` +
                        kill_nb + `">
                    `);
                    kill_nb++;
                }
            });
            $("body").on('click tap', '.kill_file,.kill_file_base', function() {
                $('.' + $(this).data('id')).attr('name', '');
                $(this).parent().toggle();
            });
        });
    </script>
@endsection
