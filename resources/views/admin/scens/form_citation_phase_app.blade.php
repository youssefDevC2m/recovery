<div class="accordion__pane phase_appel">
    <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg">
        <span class="text-xl">
            Phase D’appel
        </span>
    </a>

    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
        <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-2">
            <div class="my-3">
                <label for="">Code D’appel</label>
                <input type="text" name="cit_code_app" class="input w-full mt-2 border border-gray-500"
                    placeholder="Code D’appel">
            </div>
            <div class="my-3">
                <label for="">Date D’appel</label>
                <input type="date" name="cit_date_app" class="input w-full mt-2 border border-gray-500"
                    placeholder="Date D’appel" value="{{ date('Y-m-d') }}">
            </div>
        </div>
        <div class="w-full text-center">
            <h1 class="text-lg my-2">Le Jugement</h1>
        </div>

        <div class="w-full pha_audiences_list">
            <div class="shadow-lg my-3 p-5 box">
                <input type="hidden" name="pha_indeXaudi[]" value="1">
                <div class="grid grid-cols-2">
                    <div>
                        <h1 class="text-lg">Les Audiences</h1>
                    </div>
                    <div class="grid justify-items-end">
                        <div class="tooltip pha_duplicat_audi" title="Ajouter">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-plus-circle mx-auto hover:text-theme-9">
                                <circle cx="12" cy="12" r="10"></circle>
                                <line x1="12" y1="8" x2="12" y2="16"></line>
                                <line x1="8" y1="12" x2="16" y2="12"></line>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-clos-1 gap-2">
                    <div class="mt-2">
                        <label class="ml-2" for="">Date</label>
                        <input type="date" class="input w-full mt-2 border border-gray-500"
                            name="pha_cit_date_audc[]" value="{{ date('Y-m-d') }}">
                    </div>
                    <div class="mt-2">
                        <label class="ml-2" for="">L’heure</label>
                        <input type="time" class="input w-full mt-2 border border-gray-500"
                            name="pha_cit_heur_audc[]" value="{{ date('H:i') }}">
                    </div>
                    <div class="mt-2">
                        <label class="ml-2" for="">Lieu</label>
                        <input type="text" class="input w-full mt-2 border border-gray-500"
                            name="pha_cit_lieu_audc[]" placeholder="lieu">
                    </div>
                </div>

                <div class="w-full">
                    <div class="mt-2">
                        <label class="ml-2" for="">Résumé</label>
                        <textarea name="pha_cit_resume[]" class="input w-full mt-2 border border-gray-500" placeholder="Résumé ..."
                            style="
                            height: 100px;
                        "></textarea>
                    </div>
                </div>

            </div>
        </div>

        <div class="w-full">
            <label for="">Le Jugement définitive</label>
            <textarea name="cit_jugemen_difinit" class="input w-full mt-2 border border-gray-500" placeholder="Le Jugement ..."
                style="height: 100px;"></textarea>
        </div>

    </div>

</div>
