<div class="accordion__pane border-b border-gray-200 dark:border-dark-5 "> 
    <div class="grid grid-cols-2  hover:bg-gray-400 rounded-lg ">
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2" >
            <span class="text-xl">
                Envoi de courrier
            </span>    
        </a>
        <div class="grid justify-items-end flex items-center">
            <div title="" class=" w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 mr-2  kill_courr"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
            </div>   
        </div>
    </div>
    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
        <div class="">
            <div class="">
                <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
                    <div class="mt-2">
                        <label for="">Date d'envoi</label>
                        <input type="datetime-local" name="date_env[]" class="input w-full border mt-2" value="{{date('Y-m-d\TH:i',strtotime($item->date_env))}}" >
                    </div>
                </div>
                <div class="my-3">
                    <label for="" class="my-2 text-xl">Mise en demeure par</label>
                    <div class="grid justify-items-center">
                        <div class="flex lg:flex-row sm:flex-col">
                            <div class="mt-2 mr-2 flex flex-row">
                                <label for="Normale{{$item->id}}">Normale</label>
                                <input id="Normale{{$item->id}}" value="Normale" {{($item->mise_dem_par=='Normale')? 'checked' :"" }} name="mise_dem_par[]" type="checkbox" class="input mise_dem_par border border-gray-600 border-2  ml-2 " style="margin-top: 0.1rem;">
                            </div>
                            <div class="mt-2 mr-2 flex flex-row">
                                <label for="lr{{$item->id}}">L.R</label>
                                <input id="lr{{$item->id}}" value="lr" {{($item->mise_dem_par=='lr')? 'checked' :"" }} name="mise_dem_par[]" type="checkbox" class="input mise_dem_par border border-gray-600 border-2  ml-2" style="margin-top: 0.1rem;">
                            </div>
                            <div class="mt-2 mr-2 flex flex-row">
                                <label for="lar{{$item->id}}">L.A.R</label>
                                <input id="lar{{$item->id}}" value="lar" {{($item->mise_dem_par=='lar')? 'checked' :"" }} name="mise_dem_par[]" type="checkbox" class="input mise_dem_par border border-gray-600 border-2  ml-2" style="margin-top: 0.1rem;">
                            </div>
                            <div class="mt-2 mr-2 flex flex-row">
                                <label for="huissier{{$item->id}}">Huissier</label>
                                <input id="huissier{{$item->id}}" value="huissier" {{($item->mise_dem_par=='huissier')? 'checked' :"" }} name="mise_dem_par[]" type="checkbox" class="input mise_dem_par border border-gray-600 border-2  ml-2" style="margin-top: 0.1rem;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2">
                    <label for="">Commentaires</label>
                    <textarea name="comment[]" class="input w-full mt-2 border h-64" >{{$item->comment}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>  