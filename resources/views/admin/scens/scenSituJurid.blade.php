<div class="accordion__pane border-b border-gray-200 dark:border-dark-5 "> 
    <input type="hidden" value="1" name="stj_nb[]">
    <div class="grid grid-cols-2  hover:bg-gray-400 rounded-lg ">
        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2" >
            <span class="text-xl">
                Situation Judiciaire
            </span>    
        </a>
        <div class="grid justify-items-end flex items-center">
            <div title="" class=" w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 mr-2  kill_stj"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
            </div>   
        </div>
    </div>
    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
        <div class="mt-2">
        <label for="">Débiteur particulier :</label>
            <div class="grid  justify-items-center">
                <div class="flex flex-row ">
                    <div class="mt-2 mr-3 ">
                        <label for="">P.V</label>
                        <input type="checkbox" name="type_situation_judic[]" class="type_situation_judic input ml-1 border border-gray-500" value="pv" {{($item->type_situation_judic=='pv')? 'checked':''}}>
                    </div>
                    <div class="mt-2 mr-3">
                        <label for="">P.R</label>
                        <input type="checkbox" name="type_situation_judic[]" class="type_situation_judic input ml-1 border border-gray-500" value="pr" {{($item->type_situation_judic=='pr')? 'checked':''}}>
                    </div>
                    <div class="mt-2 mr-3">
                        <label for="">P.I</label>
                        <input type="checkbox" name="type_situation_judic[]" class="type_situation_judic input ml-1 border border-gray-500" value="pi" {{($item->type_situation_judic=='pi')? 'checked':''}}>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-2">
            <label for="">Débiteur entreprise :</label>
            <div class="grid  justify-items-center">
                <div class="flex flex-row ">
                    <div class="mt-2 mr-3 ">
                        <label for="">En activité</label>
                        <input type="checkbox" name="type_situation_judic[]" class="type_situation_judic input ml-1 border border-gray-500" value="enactivit" {{($item->type_situation_judic=='enactivit')? 'checked':''}}>
                    </div>
                    <div class="mt-2 mr-3">
                        <label for="">R.J</label>
                        <input type="checkbox" name="type_situation_judic[]" class="type_situation_judic input ml-1 border border-gray-500" value="rj" {{($item->type_situation_judic=='rj')? 'checked':''}}>
                    </div>
                    <div class="mt-2 mr-3">
                        <label for="">L.J</label>
                        <input type="checkbox" name="type_situation_judic[]" class="type_situation_judic input ml-1 border border-gray-500" value="lj" {{($item->type_situation_judic=='lj')? 'checked':''}}>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-2">
            <label for="">Commentaires</label>
            <textarea name="comment_stj[]" class="input w-full border border-gray-500" placeholder="Commentaires...">{{$item->comment}}</textarea>
        </div>
    </div>
</div> 