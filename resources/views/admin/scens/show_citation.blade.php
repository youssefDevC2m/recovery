@extends('admin.admin')
@section('title')
    Modifier Citation
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex">
        <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a
            href="#/" class="breadcrumb--active">Modifier Citation</a>
    </div>
@endsection
@section('content')
    <form action="" id="form_data">
        <input type="hidden" name="id_cit" value="{{ $Citation->id }}">
        <input type="hidden" name="id_hist" value="{{ $Citation->id_hist_scen }}">
        <div class="intro-x w-full box my-2">
            <div class="p-5 grid grid-cols-12 gap-4 row-gap-3 font-semibold">
                <div class="col-span-12 sm:col-span-6">
                    <label>Validation pièces</label>
                    <div class="flex flex-row">
                        <div class="">
                            <label for="">Oui</label>
                            <input required type="radio" name="vald_pieces"
                                class="input w-full border border-gray-400 mt-2 flex-1"
                                {{ $Citation->vald_pieces == 1 ? 'checked' : '' }} value="1"
                                style="transform: translate(0px, 4px);">
                        </div>
                        <div class="ml-3">
                            <label for="">Non</label>
                            <input required type="radio" name="vald_pieces"
                                class="input w-full border border-gray-400 mt-2 flex-1"
                                {{ $Citation->vald_pieces == 0 ? 'checked' : '' }} value="0"
                                style="transform: translate(0px, 4px);">
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6"> </div>
                <div class="col-span-12 sm:col-span-6">
                    <label>Provision</label>
                    <input required type="text" class="input w-full border border-gray-400 mt-2 flex-1" name="provi"
                        value="{{ $Citation->provi }}" placeholder="Provision">
                </div>

                <div class="col-span-12 sm:col-span-6">
                    <label>Avocat</label>
                    <select name="avocat" class="input w-full border border-gray-400 mt-2 flex-1" required>
                        <option value="">Sélectionnez un avocat</option>
                        @foreach ($partner->where('type_partn', 'Avocat') as $item)
                            <option value="{{ $item->id }}" {{ $Citation->avocat == $item->id ? 'selected' : '' }}>
                                {{ $item->prenom_part . ' ' . $item->nom_part }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-span-12 sm:col-span-6">
                    <label>Date début</label>
                    <input required type="date" name="date_debut_plant" class="input w-full border mt-2 flex-1"
                        value="{{ date('Y-m-d', strtotime($Citation->date_debut_plant)) }}">
                </div>

                <div class="col-span-12 sm:col-span-6">
                    <label>Date fin</label>
                    <input required type="date" name="date_fin_plant" class="input w-full border mt-2 flex-1"
                        value="{{ date('Y-m-d', strtotime($Citation->date_fin_plant)) }}">
                </div>
            </div>
        </div>
        <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold CitaionHtml">
            <div class="accordion__pane active">
                <a href="javascript:;"
                    class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg">
                    <span class="text-xl">
                        Citation Directe
                    </span>
                </a>
                <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
                    <div class="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <label for="">Référence</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="cit_ref"
                                placeholder="Référence" value="{{ $Citation->cit_ref }}">
                        </div>
                        <div class="mt-2">
                            <label for="">Code Judiciare</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="cit_cod_jud"
                                placeholder="Code Judiciare" value="{{ $Citation->cit_cod_jud }}">
                        </div>
                        <div class="mt-2">
                            <label for="">Tribunal Compétente</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="cit_trib_comp"
                                placeholder="Tribunal Compétente" value="{{ $Citation->cit_trib_comp }}">
                        </div>
                    </div>

                    <div class="w-full text-center my-3">
                        <h1 class="text-lg">Les Audiences</h1>
                    </div>

                    <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-2">
                        <div class="sm:mt-2">
                            <label for="">Client</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="cit_client"
                                placeholder="Client" value="{{ $Citation->cit_client }}">
                        </div>
                        <div class="sm:mt-2">
                            <label for="">Adversaire</label>
                            <input type="text" class="input w-full mt-2 border border-gray-500" name="cit_advers"
                                placeholder="Adversaire" value="{{ $Citation->cit_advers }}">
                        </div>
                    </div>

                    <div class="w-full">
                        <div class="mt-2">
                            <label for="">Sujet</label>
                            <textarea name="cit_sujet" class="input w-full mt-2 border border-gray-500" placeholder="Sujet ..."
                                style="
                                height: 100px;
                            ">{{ $Citation->cit_sujet }}</textarea>
                        </div>
                    </div>

                    <div class=" my-3">
                        <h1 class="text-xl">Pièces Jointes de conviction</h1>
                    </div>

                    <div class="w-full audiences_list">
                        @foreach ($CitAudiences as $index => $item)
                            <div class="shadow-lg my-3 p-5 box">

                                <input type="hidden" name="indeXaudi[]" value="1">
                                <div class="grid grid-cols-2">
                                    <div>
                                        <h1 class="text-lg">Les Audiences</h1>
                                    </div>
                                    @if ($index == 0)
                                        <div class="grid justify-items-end">
                                            <div class="tooltip duplicat_audi" title="Ajouter">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                    stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                                                    class="feather feather-plus-circle mx-auto hover:text-theme-9">
                                                    <circle cx="12" cy="12" r="10"></circle>
                                                    <line x1="12" y1="8" x2="12" y2="16">
                                                    </line>
                                                    <line x1="8" y1="12" x2="16" y2="12">
                                                    </line>
                                                </svg>
                                            </div>
                                        </div>
                                    @else
                                        <div class="grid justify-items-end">
                                            <div class="kill_audi">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                    viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                    stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                                                    class="feather feather-x-circle mx-auto hover:text-theme-6">
                                                    <circle cx="12" cy="12" r="10"></circle>
                                                    <line x1="15" y1="9" x2="9" y2="15">
                                                    </line>
                                                    <line x1="9" y1="9" x2="15" y2="15">
                                                    </line>
                                                </svg>
                                            </div>
                                        </div>
                                    @endif

                                </div>

                                <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-clos-1 gap-2">
                                    <div class="mt-2">
                                        <label class="ml-2" for="">Date</label>
                                        <input type="date" class="input w-full mt-2 border border-gray-500"
                                            name="cit_date_audc[]"
                                            value="{{ date('Y-m-d', strtotime($item->cit_date_audc)) }}">
                                    </div>
                                    <div class="mt-2">
                                        <label class="ml-2" for="">L’heure</label>
                                        <input type="time" class="input w-full mt-2 border border-gray-500"
                                            name="cit_heur_audc[]"
                                            value="{{ date('H:i', strtotime($item->cit_heur_audc)) }}">
                                    </div>
                                    <div class="mt-2">
                                        <label class="ml-2" for="">Lieu</label>
                                        <input type="text" class="input w-full mt-2 border border-gray-500"
                                            name="cit_lieu_audc[]" placeholder="lieu"
                                            value="{{ $item->cit_lieu_audc }}">
                                    </div>
                                </div>

                                <div class="w-full">
                                    <div class="mt-2">
                                        <label class="ml-2" for="">Résumé</label>
                                        <textarea name="cit_resume[]" class="input w-full mt-2 border border-gray-500" placeholder="Résumé ..."
                                            style="height: 100px;">{{ $item->cit_resume }}</textarea>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>

                    <div class="w-full text-center">
                        <h1 class="text-lg my-2">Le Jugement</h1>
                        <textarea name="cit_jugemen" class="input w-full mt-2 border border-gray-500" placeholder="Le Jugement ..."
                            style="height: 100px;">{{ $Citation->cit_jugemen }}</textarea>
                    </div>
                    <div class="grid lg:justify-items-end lg:mt-0 mt-3 my-2">
                        <div class="flex flex-row">
                            <label class="text-gray-600">Phase D’appel</label>
                            <input type="checkbox" {{ !empty($Citation->cit_phas_app) ? 'checked' : '' }}
                                class="input input--switch border ml-2 cit_phas_app" name="cit_phas_app">
                        </div>
                    </div>
                </div>
            </div>
            @if (!empty($Citation->cit_phas_app) || $Citation->cit_phas_app == 1)
                <div class="accordion__pane phase_appel">
                    <a href="javascript:;"
                        class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg">
                        <span class="text-xl">
                            Phase D’appel
                        </span>
                    </a>

                    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
                        <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-2">
                            <div class="my-3">
                                <label for="">Code D’appel</label>
                                <input type="text" name="cit_code_app" value="{{ $Citation->cit_code_app }}"
                                    class="input w-full mt-2 border border-gray-500" placeholder="Code D’appel">
                            </div>
                            <div class="my-3">
                                <label for="">Date D’appel</label>
                                <input type="date" name="cit_date_app"
                                    class="input w-full mt-2 border border-gray-500" placeholder="Date D’appel"
                                    value="{{ date('Y-m-d', strtotime($Citation->cit_date_app)) }}">
                            </div>
                        </div>
                        <div class="w-full text-center">
                            <h1 class="text-lg my-2">Le Jugement</h1>
                        </div>

                        <div class="w-full pha_audiences_list">
                            @foreach ($PhaCitAudiences as $index2 => $item1)
                                <div class="shadow-lg my-3 p-5 box">
                                    <input type="hidden" name="pha_indeXaudi[]" value="1">
                                    <div class="grid grid-cols-2">
                                        <div>
                                            <h1 class="text-lg">Les Audiences</h1>
                                        </div>
                                        @if ($index2 == 0)
                                            <div class="grid justify-items-end">
                                                <div class="tooltip pha_duplicat_audi" title="Ajouter">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-plus-circle mx-auto hover:text-theme-9">
                                                        <circle cx="12" cy="12" r="10"></circle>
                                                        <line x1="12" y1="8" x2="12"
                                                            y2="16">
                                                        </line>
                                                        <line x1="8" y1="12" x2="16"
                                                            y2="12">
                                                        </line>
                                                    </svg>
                                                </div>
                                            </div>
                                        @else
                                            <div class="grid justify-items-end">
                                                <div class="pha_kill_audi">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                                        viewBox="0 0 24 24" fill="none" stroke="currentColor"
                                                        stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
                                                        class="feather feather-x-circle mx-auto hover:text-theme-6">
                                                        <circle cx="12" cy="12" r="10"></circle>
                                                        <line x1="15" y1="9" x2="9"
                                                            y2="15"></line>
                                                        <line x1="9" y1="9" x2="15"
                                                            y2="15"></line>
                                                    </svg>
                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                    <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-clos-1 gap-2">
                                        <div class="mt-2">
                                            <label class="ml-2" for="">Date</label>
                                            <input type="date" class="input w-full mt-2 border border-gray-500"
                                                name="pha_cit_date_audc[]"
                                                value="{{ date('Y-m-d', strtotime($item1->pha_cit_date_audc)) }}">
                                        </div>
                                        <div class="mt-2">
                                            <label class="ml-2" for="">L’heure</label>
                                            <input type="time" class="input w-full mt-2 border border-gray-500"
                                                name="pha_cit_heur_audc[]"
                                                value="{{ date('H:i', strtotime($item1->pha_cit_heur_audc)) }}">
                                        </div>
                                        <div class="mt-2">
                                            <label class="ml-2" for="">Lieu</label>
                                            <input type="text" class="input w-full mt-2 border border-gray-500"
                                                name="pha_cit_lieu_audc[]" value="{{ $item1->pha_cit_lieu_audc }}"
                                                placeholder="lieu">
                                        </div>
                                    </div>

                                    <div class="w-full">
                                        <div class="mt-2">
                                            <label class="ml-2" for="">Résumé</label>
                                            <textarea name="pha_cit_resume[]" class="input w-full mt-2 border border-gray-500" placeholder="Résumé ..."
                                                style="
                                            height: 100px;
                                        ">{{ $item1->pha_cit_resume }}</textarea>
                                        </div>
                                    </div>

                                </div>
                            @endforeach

                        </div>

                        <div class="w-full">
                            <label for="">Le Jugement définitive</label>
                            <textarea name="cit_jugemen_difinit" class="input w-full mt-2 border border-gray-500" placeholder="Le Jugement ..."
                                style="height: 100px;">{{ $Citation->cit_jugemen_difinit }}</textarea>
                        </div>

                    </div>

                </div>
            @endif

        </div>

        <div class="mt-3 w-full grid justify-items-center">
            <div class="box w-64 grid justify-items-center shadow-lg">
                <button class="button w-24 bg-theme-1 text-white my-5 save">Enregistrer</button>
            </div>
        </div>
    </form>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('body').on('click', '.duplicat_audi', function() {
                $.ajax({
                    url: `{{ route('duplicat_audi') }}`,
                    type: 'post',
                    dataType: 'json',
                    success: function(data) {
                        $('.audiences_list').append(data);
                    }
                });
            });
            $('body').on('click', '.pha_duplicat_audi', function() {
                $.ajax({
                    url: `{{ route('pha_duplicat_audi') }}`,
                    type: 'post',
                    dataType: 'json',
                    success: function(data) {
                        $('.pha_audiences_list').append(data);
                    }
                });
            });
            $("body").on('click', '.kill_audi', function() {
                $(this).parent().parent().parent().remove();
            });
            $("body").on('click', '.pha_kill_audi', function() {
                $(this).parent().parent().parent().remove();
            });

            $("body").on('click', '.save', function() {
                event.preventDefault();
                var form_data = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url: `{{ route('edit_citation') }}`,
                    type: 'post',
                    dataType: 'json',
                    data: form_data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data) {
                        window.location.replace(data);
                        // console.log(data);
                    },
                    error: function(error) {
                        console.log(error);
                    },
                });
            });
            $("body").on('change', '.cit_phas_app', function() {
                if ($(this).is(":checked")) {
                    $.ajax({
                        url: `{{ route('form_citation_phase_app') }}`,
                        type: 'post',
                        dataType: 'json',
                        success: function(data) {
                            $('.CitaionHtml').append(data);
                        },
                        error: function(error) {
                            console.log(error);
                        },
                    });
                } else {
                    $('.phase_appel').remove();
                }
            });
        });
    </script>
@endsection
