<div class="dropdown result_content" data-placement="top" id='row{{$index}}'>
    <div class="dropdown-toggle report-timeline relative hover:zoom-in">
        <div class="intro-y relative flex items-center mb-3">
            <div class="report-timeline__image">
                <div class="w-10 h-10 flex-none image-fit rounded-full overflow-hidden">
                    <img alt="Midone Tailwind HTML Admin Template" src="{{ asset('dist/images/profile-1.jpg') }}">
                </div>
            </div>
            <div class="box px-5 py-3 ml-4 flex-1 ">
                <div class="flex items-center">
                    <div class="font-medium date_deroul_aud">{{date('d/m/Y',strtotime($date_result))}}</div>
                    
                </div>
                <p class="result_aud">{{$result}}</p>
                <input type="hidden" class="date_result_val" name="date_result[]" value="{{date('Y-m-d',strtotime($date_result))}}">
                <input type="hidden" class="result_val" name="result[]" value="{{$result}}">
            </div>
        </div>
        <div class="dropdown-box w-48">
            <div class="dropdown-box__content box dark:bg-dark-1 p-2">
                <a href="javascript:void(0)" data-result = "{{$result}}" data-date ="{{date('Y-m-d',strtotime($date_result))}}" data-index = "{{$index}}" class="flex items-center text-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-theme-6 text-theme-6 hover:text-white dark:hover:bg-dark-2 rounded-md delete">
                    <span class="font-semibold ">Supprimer</span>
                </a>
                <a href="javascript:void(0)" data-result="{{$result}}" data-date="{{date('Y-m-d',strtotime($date_result))}}" data-index="{{$index}}" class="flex items-center text-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md edit">
                    <span class="">Modifier</span>
                </a>
            </div>
        </div>
    </div>
    
</div>
