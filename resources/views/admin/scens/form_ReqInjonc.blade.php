@extends('admin.admin')
@section('title')
Ajouter Des Injonctions De Payer
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ajouter Des Injonctions De Payer</a> </div>
@endsection
@section('content')
<form action="" id="form_data">
    <input type="hidden" name="id_hist" value="{{$id_hist}}">
    <div class="intro-x w-full box my-2">
        <div class="p-5 grid grid-cols-12 gap-4 row-gap-3 font-semibold">
            <div class="col-span-12 sm:col-span-6">
                <label>Validation pièces</label>
                <div class="flex flex-row">
                    <div class="">
                        <label for="">Oui</label>
                        <input required type="radio" name="vald_pieces"
                            class="input w-full border border-gray-400 mt-2 flex-1" {{($vald_pieces==1)? 'checked' : ''}} value="1"
                            style="transform: translate(0px, 4px);">
                    </div>
                    <div class="ml-3">
                        <label for="">Non</label>
                        <input required type="radio" name="vald_pieces"
                            class="input w-full border border-gray-400 mt-2 flex-1" {{($vald_pieces==0)? 'checked' : ''}} value="0"
                            style="transform: translate(0px, 4px);">
                    </div>
                </div>
            </div>
            <div class="col-span-12 sm:col-span-6"> </div>
            <div class="col-span-12 sm:col-span-6">
                <label>Provision</label>
                <input required type="text" class="input w-full border border-gray-400 mt-2 flex-1"  name="provi" value="{{$provi}}"
                    placeholder="Provision">
            </div>

            <div class="col-span-12 sm:col-span-6">
                <label>Avocat</label>
                <select name="avocat" class="input w-full border border-gray-400 mt-2 flex-1" required>
                    <option value="">Sélectionnez un avocat</option>
                    @foreach ($partner->where('type_partn','Avocat') as $item)
                        <option value="{{ $item->id }}" {{($avocat==$item->id)? 'selected' : ''}}>{{ $item->prenom_part . ' ' . $item->nom_part }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-span-12 sm:col-span-6">
                <label>Date début</label>
                <input required type="date" name="date_debut_plant" class="input w-full border mt-2 flex-1"
                    value="{{ date('Y-m-d',strtotime($date_debut_plant)) }}">
            </div>

            <div class="col-span-12 sm:col-span-6">
                <label>Date fin</label>
                <input required type="date" name="date_fin_plant" class="input w-full border mt-2 flex-1"
                    value="{{ date('Y-m-d',strtotime($date_fin_plant)) }}">
            </div>
        </div>
    </div>
    <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold reqinjoncDpayHtml" >
        <div class="accordion__pane active reqProc_dephtml"> 
            <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
                <span class="text-xl">
                    PROCÉDURE DE DÉPÔT
                </span>    
            </a>
            <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
                <div class="mt-1">
                    <textarea name="proc_dep_req_inj_pay" class="input w-full border border-gray-500 h-32" placeholder="Procédure De Dépôt"></textarea>
                </div>
                <div class="grid lg:justify-items-end lg:mt-0 mt-3 my-2">
                    <div class="flex flex-row">
                        <label class="text-gray-600">Les Procédures</label>
                        <input type="checkbox" class="input input--switch border ml-2 les_proc_req_inj_pay" name="les_proc_req_inj_pay"> 
                    </div>
                </div>
            </div>
        </div>
        <div class='prcreq_contain'></div>
    </div>
    <div class="grid justify-items-center">
        <div class="flex lg:flex-row md:flex-row flex-col ">
            <button class="button w-24 mr-1 mb-2 bg-theme-1 text-white mt-4 save">Enregistrer</button> 
        </div>
    </div>
</form>
@endsection
@section('script')
<script>
    var huissier = '';
    var proc_exec_huissierr="";
</script>
    <script src="{{asset('dist/js/param.js')}}"></script>
    
    <script>
        $(document).ready(function(){

            $('body').on('click','.save',function(){
                event.preventDefault();
                // alert();
                var form_data= new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:"{{route('add_ReqInjonc')}}",
                    data:form_data,
                    dataType:'json',
                    type:'post',
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        Swal.fire(
                        'Validé!',
                        'les détails sont enregistrés avec succès !',
                        'success'
                        ).then((result) =>{
                            window.location.replace(data);
                            // console.log(data);
                        });
                       
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });

            var req_procs_Notifhtml =
            `
            
                <div class="accordion__pane req_procs_Notifhtml"> 
                    <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
                        <span class="text-xl">
                            PROCÉDURES DE NOTIFICATION
                        </span>    
                    </a>
                    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
                        <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
                            <div class="mt-2">
                                <label for="">Date De Dépôt</label>
                                <input type="date" name="date_dep_notif_req_inj_pay" class="input w-full border border-gray-500 " value="{{date('Y-m-d')}}">
                            </div>
                            <div class="mt-2">
                                <label for="">Numéro Du Dossier De Notification</label>
                                <input type="text" name="num_doss_notif_req_inj_pay" class="input w-full border border-gray-500 " placeholder="Numéro Du Dossier De Notification">
                            </div>
                            <div class="mt-2">
                                <label for="">Huissier Désigné</label>
                                <select name="huissier_des_req_inj_pay" class="input w-full border border-gray-500 ">
                                    <option value='0'>Sélectionnez un mandataire</option>
                                    @foreach($partner as $item)
                                        <option value='{{$item->id}}'>{{$item->nom_part}} {{$item->prenom_part}}</option>
                                    @endforeach
                                </select>
                                {{-- <input type="text" name="huissier_des_req_inj_pay" class="input w-full border border-gray-500" placeholder="Huissier Désigné"> --}}
                            </div>
                        </div>
                        <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                            <div class="mt-2">
                                <label for="">Résultat De La Notification</label>
                                <input type="text" name="reslt_notif_req_inj_pay" class="input w-full border border-gray-500" placeholder="Résultat De La Notification">
                            </div>
                            <div class="mt-2">
                                <label for="">Date D'appel</label>
                                <input type="date" name="date_appl_notif_req_inj_pay" class="input w-full border border-gray-500" value="` + curr + `">
                            </div>
                        </div>
                        <div class="mt-2">
                            <label for="">Procédure</label>
                            <textarea name="proces_notif_req_inj_pay" class="input w-full border border-gray-500 h-32" placeholder="Procédure..."></textarea>
                        </div>
                        <div class="grid lg:justify-items-end my-3">
                            <div class="flex flex-row">
                                <label class="text-gray-600">Procédures D’exécution</label>
                                <input type="checkbox" class="input input--switch border ml-2 swt_proc_exe_req_inj_pay" name="swt_proc_exe_req_inj_pay"> 
                            </div>
                        </div>
                    </div>
                </div>
            
            `;

            $('body').on('change','.les_proc_req_inj_pay',function(){
                 
                if($(this).is(":checked")){
                    $('.prcreq_contain').append(reqLes_procshtml);
                    $('.reqLes_procshtml').show('slow');
                }else{
                    $('.reqLes_procshtml').slideUp(300);
                    $('.reqLes_procshtml').html('');

                    $('.reqLes_procs_Apphtml').slideUp(300);
                    $('.reqLes_procs_Apphtml').html('');

                    $('.req_procs_Notifhtml').slideUp(300);
                    $('.req_procs_Notifhtml').html('');

                    $('.req_procs_exehtml').slideUp(300);
                    $('.req_procs_exehtml').html('');
                }
            });
            $('body').on('change','.les_proc_app_req_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.prcreq_contain').append(reqLes_procs_Apphtml);
                    $('.reqLes_procs_Apphtml').show('slow');
                }else{
                    $('.reqLes_procs_Apphtml').slideUp(300);
                    $('.reqLes_procs_Apphtml').html('');

                    $('.req_procs_Notifhtml').slideUp(300);
                    $('.req_procs_Notifhtml').html('');

                    $('.req_procs_exehtml').slideUp(300);
                    $('.req_procs_exehtml').html('');
                }
            });

            $('body').on('change','.swt_proc_notif_req_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.prcreq_contain').append(req_procs_Notifhtml);
                    $('.req_procs_Notifhtml').show('slow');
                }else{
                    $('.req_procs_Notifhtml').slideUp(300);
                    $('.req_procs_Notifhtml').html('');

                    $('.req_procs_exehtml').slideUp(300);
                    $('.req_procs_exehtml').html('');
                }
            });

            $('body').on('change','.swt_proc_exe_req_inj_pay',function(){
                 console.log(req_procs_exehtml);
                if($(this).is(":checked")){
                    $('.prcreq_contain').append(req_procs_exehtml);
                    $('.req_procs_exehtml').show('slow');
                }else{
                    $('.req_procs_exehtml').slideUp(300);
                    $('.req_procs_exehtml').html('');
                }
            });
        });
    </script>
@endsection