
<div class="">
    <form action="" id="form_data">
        <div class="intro-x w-full box my-2">
            <div class="p-5 grid grid-cols-12 gap-4 row-gap-3 font-semibold">
                <div class="col-span-12 sm:col-span-6">
                    <label>Validation pièces</label>
                    <div class="flex flex-row">
                        <div class="">
                            <label for="">Oui</label>
                            <input required type="radio" name="vald_pieces"
                                class="input w-full border border-gray-400 mt-2 flex-1" {{($vald_pieces==1)? 'checked' : ''}} value="1"
                                style="transform: translate(0px, 4px);">
                        </div>
                        <div class="ml-3">
                            <label for="">Non</label>
                            <input required type="radio" name="vald_pieces"
                                class="input w-full border border-gray-400 mt-2 flex-1" {{($vald_pieces==0)? 'checked' : ''}} value="0"
                                style="transform: translate(0px, 4px);">
                        </div>
                    </div>
                </div>
                <div class="col-span-12 sm:col-span-6"> </div>
                
                <div class="col-span-12 sm:col-span-6">
                    <label>Date début</label>
                    <input required type="date" name="date_debut_plant" class="input w-full border-gray-400 border mt-2 flex-1"
                        value="{{ date('Y-m-d',strtotime($date_debut_plant)) }}">
                </div>
        
                <div class="col-span-12 sm:col-span-6">
                    <label>Date fin</label>
                    <input required type="date" name="date_fin_plant" class="input w-full border-gray-400 border mt-2 flex-1"
                        value="{{ date('Y-m-d',strtotime($date_fin_plant)) }}">
                </div>
                <div class="col-span-12 sm:col-span-6">
                    <label>Provision</label>
                    <input required type="text" class="input w-full border border-gray-400 mt-2 flex-1"  name="provi" value="{{$provi}}"
                        placeholder="Provision">
                </div>
            </div>
        </div>
        <input type="hidden" name="id_hist" value="{{ $id_hist }}">
        <div class="intro-x box w-full  mt-3">
            <div class="grid md:grid-cols-1 sm:grid-cols-1 lg:grid-cols-3 gap-2 p-3">
                <div>
                    <label for="" class="font-semibold">Référence</label>
                    <input type="text" class="input w-full border" name="ref" placeholder="Référence">
                </div>
                <div>
                    <label for="" class="font-semibold">Tribunal Compétente</label>
                    <input type="text" class="input w-full border" name="tribunal_competant"
                        placeholder="Tribunal Compétente">
                </div>
                <div>
                    <label for="" class="font-semibold">Le Nom De Juge</label>
                    <input type="text" class="input w-full border" name="non_juge" placeholder="Le Nom De Juge">
                </div>
            </div>
        </div>
        <div class="my-2">
            <h1 class="text-lg font-bold">Les parties:</h1>
        </div>
        <div class="intro-x p-3 box ">
            <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
                <div class="w-full">
                    <div class="my-2">
                        <label for="" class="font-semibold">Le Tireur</label>
                        <input type="text" class="input w-full border"
                            value="{{ $debiteur->type_dtbr == 'preso' ? $debiteur->nom . ' ' . $debiteur->prenom : $debiteur->nom }}"
                            name="le_tireur" placeholder="Le Tireur">
                    </div>
                    <div>
                        <label for="" class="">L’adresse</label>
                        <input type="text" class="input w-full border" name="address_tireur" placeholder="L’adresse">
                    </div>
                    <div>
                        <label for="" class="">Cin</label>
                        <input type="text"{{ $debiteur->type_dtbr == 'preso' ? $debiteur->cin : '' }}
                            class="input w-full border" name="cin_treur" placeholder="Numéro carte d'identité">
                    </div>
                    <div class="my-2">
                        <label for="" class="">L’Avocat </label>
                        <input type="text" class="input w-full border" name="avocat_tireur"
                            placeholder="L’Avocat De Tireur">
                    </div>
                </div>
                <div class="w-full">
                    <div class="my-2">
                        <label for="" class="font-semibold">Le Bénéficiaire</label>
                        <input type="text" class="input w-full border"
                            value="{{ $client->type_cli == 'perso' ? $client->nom . ' ' . $client->prenom : $client->nom }}"
                            name="benificiaire" placeholder="Le Bénéficiaire">
                    </div>
                    <div class="col-span-12 sm:col-span-6">
                        <label>Avocat</label>
                        <select name="avoca_benif" class="input w-full border" required>
                            <option value="">Sélectionnez un avocat</option>
                            @foreach ($partner->where('type_partn','Avocat') as $item)
                                <option value="{{ $item->id }}" {{($avocat==$item->id)? 'selected' : ''}}>{{ $item->prenom_part . ' ' . $item->nom_part }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="grid lg:grid-cols-3 lg:grid-cols-1 lg:grid-cols-1">
                <div class="col-span-1">
                    <label for="" class="font-semibold">Montant Global De la Créance</label>
                    <input type="number" min="0" step="0.1" class="input w-full border"
                        name="montant_creance" placeholder="Montant Global De la Créance">
                </div>
            </div>
        </div>
        <div class="my-2">
            <h1 class="text-lg font-bold">Procédure De Dépôt:</h1>
        </div>
        <div class="intro-x p-3 box">
            <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
                <div class="my-2">
                    <label for="" class="font-semibold">Numéro Du Dossier</label>
                    <input type="text" class="input w-full border" value="{{ $dossier->ref }}"
                        placeholder="Numéro Du Dossier" disabled>
                </div>
                <div class="my-2">
                    <label for="" class="font-semibold">Date De Dépôt</label>
                    <input type="date" class="input w-full border" name="date_depot" value="{{ date('Y-m-d') }}">
                </div>
                <div class="my-2">
                    <label for="" class="font-semibold">Huissier Désigné</label>
                    <input type="text" class="input w-full border" name="huissier_designie"
                        placeholder="Huissier Désigné">
                </div>
                <div class="grid lg:grid-cols-6 md:grid-cols-1 sm:grid-cols-1 gap-2 my-2 place-items-center">
                    <div class="col-span-3">
                        <label for="" class="font-semibold">Acceptation De La Demande :</label>
                        <input type="radio" class="input w-full border border-gray-500" name="accep_demande"
                            value="1" style="transform: translateY(3px);">
                    </div>
                    <div class="col-span-3">
                        <label for="" class="font-semibold">Refus De La Demande :</label>
                        <input type="radio" class="input w-full border border-gray-500" checked
                            name="accep_demande" value="0" style="transform: translateY(3px);">
                    </div>
                </div>
                <div class="my-2 col-span-2">
                    <label for="" class="font-semibold">Résultat De La Notification</label>
                    <textarea name="reslt_notif" class="input w-full border" placeholder="Résultat De La Notification..."></textarea>
                </div>
            </div>
        </div>
        <div class="my-2">
            <h1 class="text-lg font-bold">Déroulement d’audience :</h1>
        </div>
        <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
            <div class="intro-x p-3 box">
                <div class="my-2 lg:w-2/4">
                    <label for="" class="font-semibold">Date</label>
                    <input type="date" class="input w-full border date_result" name=""
                        value="{{ date('Y-m-d') }}">
                </div>
                <div class="my-2">
                    <label for="" class="font-semibold">Résultat</label>
                    <textarea name="" class="input w-full border result" placeholder="Résultat..."></textarea>
                </div>
                <center>
                    <div>
                        <button class="button w-24 mr-1 mb-2 bg-theme-33 text-white mt-4 add_deroul">Ajouter</button>
                        <button class="button w-24 mr-1 mb-2 bg-theme-9 text-white mt-4 edit_deroul hidden"
                            data-id=''>Modifier</button>
                    </div>
                </center>
            </div>
            <div class="overflow-y-scroll overflow-x-none h-64 ">
                <div class="result_list p-2 overflow-x-none">

                </div>
            </div>
        </div>
        <div class="box mt-3">
            <div class="p-8">
                <label for="" class="font-semibold">Pièces jointes</label>
                <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                    <div class="flex flex-wrap px-4 file_block">

                    </div>
                    <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                        <button
                            class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                stroke-linecap="round" stroke-linejoin="round"
                                class="feather feather-file-text w-4 h-4 mr-2">
                                <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                <polyline points="14 2 14 8 20 8"></polyline>
                                <line x1="16" y1="13" x2="8" y2="13"></line>
                                <line x1="16" y1="17" x2="8" y2="17"></line>
                                <polyline points="10 9 9 9 8 9"></polyline>
                            </svg>
                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span>
                        </button>
                        <input type="file"
                            class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0"
                            name="file_val[]" data-stp="not">
                    </div>
                </div>
            </div>
        </div>
        <div class="my-3">
            <center><button class=" button w-24 mr-1 mb-2 bg-theme-1 text-white my-2 save">Enregistrer</button>
            </center>
        </div>
    </form>
</div>
