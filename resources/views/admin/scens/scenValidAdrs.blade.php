<div class="accordion intro-y box px-5 py-8 mt-5 font-semibold validAdrs_chek">
    @foreach ($ValidAdrs as $item)
    <div class="accordion__pane  "> 
        <div class="grid grid-cols-2  hover:bg-gray-400 rounded-lg ">
            <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
                <span class="text-xl">
                    Validation d'adresse
                </span>    
            </a>
            <div class="grid justify-items-end flex items-center">
                <div title="" class=" w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 mr-2  kill_valid_adrs"> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                </div>   
            </div>
        </div>
        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
            <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="">Date</label>
                    <input type="date" name="date_valid_adres[]" class="input w-full border border-gray-500 mt-2" value="{{date('Y-m-d',strtotime($item->date_valid_adres))}}">
                </div>
                <div class="mt-2">
                    <label for="">Effectuée par</label>
                    <input type="text" name="effect_par[]" class="input w-full border border-gray-500 mt-2" value="{{$item->effect_par}}" placeholder="Effectuée par">
                </div>
            </div>
            <div class="mt-2">
                <div class="">
                    <label for="">Débiteur</label>
                </div>
                <div class="grid justify-items-center">
                    <div class="flex flex-row">
                        <div class="mt-2">
                            <label for="">DAI</label>
                            <input type="checkbox" name="type_debit[]" {{($item->type_debit=='DAI')? 'checked' : ''}} class="input w-full border border-gray-500 ml-1 type_debit" value="DAI">
                        </div>
                        <div class="mt-2 mx-3">
                            <label for="">NPAI</label>
                            <input type="checkbox" name="type_debit[]" {{($item->type_debit=='NPAI')? 'checked' : ''}} class="input w-full border border-gray-500 ml-1 type_debit" value="NPAI">
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-2">
                <label for="">Commentaire</label>
                <textarea name="comment_vali_adrs[]" class="input w-full border border-gray-500 h-32" placeholder="Commentaire...">{{$item->comment_vali_adrs}}</textarea>
            </div>
        </div>
    </div>
    @endforeach
    <div class="added_valid_adrs"></div>
</div>