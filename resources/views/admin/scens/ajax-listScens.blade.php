@if (count($data) > 0 && $type)
    <div class="intro-y box px-5 mt-5 font-semibold">

        <div class="text-xl font-semibold pt-5 text-gray-600">
            <label for="">
                {{ $type == 'cheque_sn_provis_block' ? 'Cheque Sans Provision' : '' }}
                {{ $type == 'injonction_pay_block' ? 'Injonction De Payer' : '' }}
                {{ $type == 'req_injonction_pay_block' ? 'Requete en Injonction De Payer' : '' }}
                {{ $type == 'citation_dir_block' ? 'Citation directe' : '' }}
            </label>
        </div>
        
        <div class=" grid justify-items-center ">
            <div class="p-3 mt-2 w-full overflow-y-scroll"style="height: 170px;">
                {{-- {{$data}} --}}
                @foreach ($data as $index => $value)
                    <div
                        class="flex items-center py-3 {{ count($data) == 1 ? 'border-b border-t' : '' }} {{ count($data) != $index + 1 ? 'border-b' : '' }}  border-gray-500">
                        <input 
                            type="radio" name="{{ $type }}"
                            class="input border border-gray-600 border-2 mr-2 radio" id="val"
                            value="{{ $value->id }}"
                            
                        >
                        <div class="w-full text-center">
                            <label for="val">
                                {{ $type == 'cheque_sn_provis_block' ? 'Référence ' . $value->ref_st_cheqe : '' }}
                                {{ $type == 'injonction_pay_block' ? 'Référence ' . $value->ref_inj_p : '' }}
                                {{ $type == 'req_injonction_pay_block' ? 'Date création '.date('Y-m-d H:i',strtotime($value->created_at)) : '' }}
                                {{ $type == 'citation_dir_block' ? 'Référence '.$value->cit_ref : '' }}
                            </label>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="mt-2 mb-5 w-full gird justify-items-center">
                <center>
                    <div class="gird place-items-center text-white">
                        <button class="button bg-theme-9 valid_proc">Continue</button>
                        <button class="button bg-theme-6 cncel_proc">Annuler</button>
                    </div>
                </center>
            </div>

        </div>
        
    </div>
@else
    <div class="intro-y box px-5 mt-5 font-semibold">
        <center>
            <div class="text-xl font-semibold py-5 text-gray-600">
                <label  for="">
                    {{ $type  ? 'Aucune information' : '' }}
                </label>    
            </div>
        </center>
    </div>
@endif

