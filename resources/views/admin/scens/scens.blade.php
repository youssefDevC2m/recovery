@extends('admin.admin')
@section('title')
Scénarios
@endsection
@section('menu-title')
<style>
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Scénarios</a> </div>
@endsection
@section('content')
<form action="" id="form_data">
    <input type="hidden" name="id_doss" value="{{$id_doss}}">
    <input type="hidden" name="id_scenH" value="{{(!empty($histo->id))? $histo->id : 0}}">
    <div class="intro-y box px-5 py-8 mt-5 font-semibold">
        <label for="" class="text-xl text-gray-600">Prise de contact</label>
        <div class="grid lg:grid-cols-4 md:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="" class="">Médiateur</label>
                {{-- <input type="text" class="input border w-full" placeholder=""> --}}
                <select name="mediateur" id="" class="tail-select w-full mt-2 "  data-search="true">
                    <option value="0">Sélectionner un médiateur</option>
                    @foreach ($agent as $item)
                        <option value="{{$item->id}}" {{( isset($histo->mediateur) && $histo->mediateur==$item->id)? 'selected' : ''}}>{{$item->nom.' '.$item->prenom}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mt-2">
                <label for="" class="">Agence</label>
                <input type="text" name="agence" class="input border w-full" value="{{(isset($histo->agence))? $histo->agence : ''}}" placeholder="Entrer le nom de l'aagence">
            </div>
            <div class="mt-2">
                <label for="">Date de remise</label>
                <input type="date" name="date_remise" class="input border w-full" value="{{(isset($histo->date_remise))? $histo->date_remise :date('Y-m-d')}}"> 
            </div>
            <div class="mt-2">
                <label for="">Date de retour</label>
                <input type="date" name="date_retoure" class="input border w-full" value="{{(isset($histo->date_retoure))? $histo->date_retoure :date('Y-m-d')}}"> 
            </div>
        </div>
    </div>
    <div class="intro-y box px-5 py-8 mt-5 font-semibold" >
        <label for="" class="text-xl text-gray-600">Durée</label>
        <div class="grid {{(!empty($doss->date_fin_doss))? 'lg:grid-cols-3' : 'lg:grid-cols-2'}}  md:grid-cols-1 sm:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Date de Debut</label>
                <input type="datetime-local" name="date_db_doss" class="input border w-full" value="{{(isset($doss->date_db_doss))? date('Y-m-d\TH:i',strtotime($doss->date_db_doss)) :date('Y-m-d\TH:i')}}"> 
            </div>
            <div class="mt-2">
                <label for="">Date de Fin</label>
                <input type="datetime-local" name="date_fin_doss" class="input border w-full" value="{{(isset($doss->date_fin_doss))? date('Y-m-d\TH:i',strtotime($doss->date_fin_doss)) :date('Y-m-d\TH:i')}}"> 
            </div>
            @if (!empty($doss->date_fin_doss))
            <div class="mt-2">
                <label for="" class="font-semibold">Durée</label>
                <input 
                type="text" 
                class="input w-full border" 
                {{-- name="anncDiff[]"  --}}
                value="<?php 
                $dossCont = new App\Http\Controllers\dossierController();
                $diff=$dossCont->dateDifference(date('Y-m-d\TH:i',strtotime($doss->date_fin_doss)), date('Y-m-d\TH:i',strtotime($doss->date_db_doss)));
                echo $diff;

                ?>">
            </div>    
            @endif
            
        </div>
    </div>
    {{-- situation amiable --}}
    <div id="top_chaine"></div>
    <div class="{{(!empty($histo->id) && Auth::user()->acs_phas_ami == 1)? '' : 'hidden'}}">
        <div class="text-2xl mt-5 font-semibold ml-3 w-full text-center">
            <span>Phase Amiable</span>
        </div>
        <div class="intro-y box px-5 py-8 mt-5 font-semibold  {{(!empty($histo->id))? '' : 'hidden'}} ">
            
            <div class="mt-3"> 
    
                <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
                    <div class="mx-2">
                        <div><label class="text-xl text-gray-600">Type d'intervention</label></div>
                    </div>
                    {{-- <div class="grid lg:justify-items-end">
                        <div class="flex flex-row">
                            <label class="text-xl text-gray-600">Situation judiciaire</label>
                            <input type="checkbox" class="input input--switch border ml-2 mt-1 situJ" name="situJ" {{(isset($histo->injoncDpay) && $histo->injoncDpay==1 || isset($histo->cheque_ref) && $histo->cheque_ref==1 || isset($histo->reqinjoncDpay) && $histo->reqinjoncDpay==1 || isset($histo->SituationDirect) && $histo->SituationDirect==1)? 'checked' : ''}}> 
                        </div>
                    </div> --}}
                </div>
    
                <div class="bg-gray-300 p-4 mt-3 rounded-lg shadow-inner">
                    <div class="grid justify-items-center box p-3 shadow-lg">
                        {{-- <div class="flex lg:flex-row flex-col"> --}}
                        <div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1">
                            
                            <div class="flex items-center text-gray-700 m-2 {{(isset($histo->courr_is_sel) && $histo->courr_is_sel==1)? 'rounded-lg px-2 py-2 bg-gray-400' : ""}}"> 
                                <input type="checkbox" class="input border border-gray-600 border-2 mr-2 ck_courr" id="horizontal-checkbox-chris-evans2" name="courr_is_sel" {{(isset($histo->courr_is_sel) && $histo->courr_is_sel==1)? 'checked' : ""}}> 
                                <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans2">Notification</label> 
                                <input type="number" min="0" name="nombre_courr_max" placeholder="Nbr" data-theme="light" class="input border w-16 ml-3 tooltip nombre_courr_max {{(isset($histo->courr_is_sel) && $histo->courr_is_sel==1)? '' : "hidden"}}" value="{{(isset($histo->nombre_courr_max))? $histo->nombre_courr_max : 1}}" data-trigger="click"  title="Nombre de tentatives de messagerie">
                            </div>
    
                            <div class="flex items-center text-gray-700 m-2 {{(isset($histo->valid_adrs_is_sel) && $histo->valid_adrs_is_sel==1)? 'rounded-lg px-2 py-2 bg-gray-400' : ""}}"> 
                                <input type="checkbox" class="input border border-gray-600 border-2 mr-2 valid_adrs_is_sel" id="horizontal-checkbox-chris-evans5" name="valid_adrs_is_sel" {{(isset($histo->valid_adrs_is_sel) && $histo->valid_adrs_is_sel==1)? 'checked' : ""}}> 
                                <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans5">Validation d'adresse</label> 
                                <input type="number" min="0" name="valid_adrs_max" placeholder="Nbr" data-theme="light" class="input border w-16 ml-3 tooltip valid_adrs_max {{(isset($histo->valid_adrs_is_sel) && $histo->valid_adrs_is_sel==1)? '' : "hidden"}}" value="{{(isset($histo->nombre_valid_adrs_max))? $histo->nombre_valid_adrs_max : 1}}" data-trigger="click"  title="Nombre de tentatives">
                            </div>
                            
                            <div class="flex items-center text-gray-700 m-2 {{(isset($histo->convo_is_sel) && $histo->convo_is_sel==1)? 'rounded-lg px-2 py-2 bg-gray-400' : ""}} "> 
                                <input type="checkbox" class="input border border-gray-600 border-2 mr-2 ck_convo" id="horizontal-checkbox-chris-evans6" name="convo_is_sel" {{(isset($histo->convo_is_sel) && $histo->convo_is_sel==1)? 'checked' : ""}}> 
                                <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans6">Convocation</label> 
                                <input type="number" min="0" name="nombre_convo_max" placeholder="Nbr" data-theme="light" class="input border w-16 ml-3 tooltip nombre_convo_max {{(isset($histo->convo_is_sel) && $histo->convo_is_sel==1)? '' : "hidden"}}" value="{{(isset($histo->nombre_convo_max))? $histo->nombre_convo_max : 1}}" data-trigger="click"  title="Nombre de visites à domicile">
                            </div>
    
                            <div class="flex items-center text-gray-700 m-2 {{(isset($histo->tel_is_sel) && $histo->tel_is_sel==1)? 'rounded-lg px-2 py-2 bg-gray-400' : ""}} "> 
                                <input type="checkbox" class="input border border-gray-600 border-2 mr-2 ck_tele" id="horizontal-checkbox-chris-evans" name="{{(Auth::user()->role=='Administrateur')? 'tel_is_sel' : ''}}" {{(isset($histo->tel_is_sel) && $histo->tel_is_sel==1)? 'checked' : ""}} {{(Auth::user()->role=='Administrateur')? '' : 'disabled'}}> 
                                <input type="hidden" name="{{(Auth::user()->role!='Administrateur')? 'tel_is_sel' : ''}}" value="1">
                                <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans">Contact téléphonique</label> 
                                @if (Auth::user()->role=='Administrateur')
                                    <input type="number" min="0" name="nombre_tele_max" placeholder="Nbr" data-theme="light" class="input border w-16 ml-3 tooltip nombre_tele_max {{(isset($histo->tel_is_sel) && $histo->tel_is_sel==1)? '' : "hidden"}}" value="{{(isset($histo->nombre_tele_max))? $histo->nombre_tele_max : 1}}" data-trigger="click"  title="Nombre de tentatives téléphoniques">
                                @endif
                                @if (Auth::user()->role!='Administrateur')
                                    <input type="hidden"  name="nombre_tele_max" value="{{(isset($histo->nombre_tele_max))? $histo->nombre_tele_max : 1}}">
                                @endif
                            </div>
    
                            <div class="flex items-center text-gray-700 m-2 {{(isset($histo->visit_is_sel) && $histo->visit_is_sel==1)? 'rounded-lg px-2 py-2 bg-gray-400' : ""}}"> 
                                <input type="checkbox" class="input border border-gray-600 border-2 mr-2 ck_visit" id="horizontal-checkbox-chris-evans3" name="visit_is_sel" {{(isset($histo->visit_is_sel) && $histo->visit_is_sel==1)? 'checked' : ""}}> 
                                <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans3">Visite domiciliaire</label> 
                                <input type="number" min="0" name="nombre_visit_max" placeholder="Nbr" data-theme="light" class="input border w-16 ml-3 tooltip nombre_visit_max {{(isset($histo->visit_is_sel) && $histo->visit_is_sel==1)? '' : "hidden"}}" value="{{(isset($histo->nombre_visit_max))? $histo->nombre_visit_max : 1}}" data-trigger="click"  title="Nombre de visites à domicile">
                            </div>
    
                            <div class="flex items-center text-gray-700 m-2 {{(isset($histo->medi_is_sel) && $histo->medi_is_sel==1)? 'rounded-lg px-2 py-2 bg-gray-400' : ""}}">
                                <input type="checkbox" class="input border border-gray-600 border-2 mr-2 ck_medi" id="horizontal-checkbox-chris-evans0" name="medi_is_sel" {{(isset($histo->medi_is_sel) && $histo->medi_is_sel==1)? 'checked' : ""}}> 
                                <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans0">Médiations</label> 
                                <input type="number" min="0" name="nombre_medi_max" placeholder="Nbr" data-theme="light" class="input border w-16 ml-3 tooltip nombre_medi_max {{(isset($histo->medi_is_sel) && $histo->medi_is_sel==1)? '' : "hidden"}}" value="{{(isset($histo->nombre_medi_max))? $histo->nombre_medi_max : 1}}" data-trigger="click"  title="Nombre de nombre de médiations">
                            </div>
    
                            <div class="flex items-center text-gray-700 m-2 {{(isset($histo->situJud_is_sel) && $histo->situJud_is_sel==1)? 'rounded-lg px-2 py-2 bg-gray-400' : ""}}">
                                <input type="checkbox" class="input border border-gray-600 border-2 mr-2 ck_situJud" id="horizontal-checkbox-chris-evans26" name="situJud_is_sel" {{(isset($histo->situJud_is_sel) && $histo->situJud_is_sel==1)? 'checked' : ""}}> 
                                <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans26">Situation Judiciaire</label> 
                                <input type="number" min="0" name="nombre_situJud_max" placeholder="Nbr" data-theme="light" class="input border w-16 ml-3 tooltip nombre_situJud_max {{(isset($histo->situJud_is_sel) && $histo->situJud_is_sel==1)? '' : "hidden"}}" value="{{(isset($histo->nombre_situJud_max))? $histo->nombre_situJud_max : 1}}" data-trigger="click"  title="Nombre de tentative">
                            </div>
                            <div  class="flex items-center text-gray-700 m-2 {{(isset($histo->solvaDebit_is_sel) && $histo->solvaDebit_is_sel==1)? 'rounded-lg px-2 py-2 bg-gray-400' : ""}}">
                                <input type="checkbox" class="input border border-gray-600 border-2 mr-2 ck_solvaDebit" id="horizontal-checkbox-chris-evans27" name="solvaDebit_is_sel" {{(isset($histo->solvaDebit_is_sel) && $histo->solvaDebit_is_sel==1)? 'checked' : ""}}> 
                                <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans27">Solvabilité du débiteur</label> 
                                <input type="number" min="0" name="nombre_solvaDebit_max" placeholder="Nbr" data-theme="light" class="input border w-16 ml-3 tooltip nombre_solvaDebit_max {{(isset($histo->solvaDebit_is_sel) && $histo->solvaDebit_is_sel==1)? '' : "hidden"}}" value="{{(isset($histo->nombre_solvaDebit_max))? $histo->nombre_solvaDebit_max : 1}}" data-trigger="click"  title="Nombre de tentative">
                            </div>
                        </div>
                    </div>
                </div>
                
                {{-- <div class="bg-gray-300 p-8 mt-3 rounded-lg shadow-inner interventions" visibility: {{(isset($histo->injoncDpay) && $histo->injoncDpay==1 || isset($histo->cheque_ref) && $histo->cheque_ref==1 || isset($histo->reqinjoncDpay) && $histo->reqinjoncDpay==1 || isset($histo->SituationDirect) && $histo->SituationDirect==1)? '' : 'hidden'}}>
                    <div class="box shadow-lg">
                        <div class="p-3">
                            <label for="" class="text-lg">Debiteur particulier</label>
                            <div class="grid justify-items-center">
                                <div class="flex lg:flex-row flex-col">
                                    <div class="flex flex-row p-2 mr-2">
                                        <label for="cheque_ref">Plante Cheque Sans Provision</label>
                                        <input type="checkbox" class="input border border-gray-500 border-1 ml-1 cheque_ref" style="margin-top: 0.1rem" name="cheque_ref" {{(isset($histo->cheque_ref) && $histo->cheque_ref==1)? 'checked' : ''}} id="cheque_ref">
                                    </div>
                                    <div class="flex flex-row p-2 mr-2">
                                        <label for="">Injonction De Payer</label>
                                        <input type="checkbox" class="input border border-gray-500 border-1 ml-1 injoncDpay" style="margin-top: 0.1rem" name="injoncDpay" {{(isset($histo->injoncDpay) && $histo->injoncDpay==1)? 'checked' : ''}}>
                                    </div>
                                    <div class="flex flex-row p-2  mr-2">
                                        <label for="">Requete en Injonction De Payer</label>
                                        <input type="checkbox" class="input border border-gray-500 border-1 ml-1 reqinjoncDpay" style="margin-top: 0.1rem" name="reqinjoncDpay" {{(isset($histo->reqinjoncDpay) && $histo->reqinjoncDpay==1)? 'checked' :''}} >
                                    </div>
                                    <div class="flex flex-row p-2  mr-2">
                                        <label for="">Citation Direct</label>
                                        <input type="checkbox" class="input border border-gray-500 border-1 ml-1 SituationDirect" style="margin-top: 0.1rem" name="SituationDirect" {{(isset($histo->SituationDirect) && $histo->SituationDirect==1)? 'checked' : ''}}  >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
        
        <div class="sendMailExists">
            @if (count($scen_courr)>0)
            <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold mail_block">
                @foreach ($scen_courr as $item)
                    @include('admin.scens.scenCourrier')
                @endforeach
                <div class="editmail"></div>
            </div>
           
            <div class="w-full px-3 w-full grid justify-items-center -intro-y">
                <div class="bg-white rounded-b-full w-32 grid justify-items-center px-3 pt-2 pb-3 shadow-lg" >
                    <a class="button w-24 rounded-full mb-3 bg-theme-14 text-theme-10 add_courrier" href="javascript:;">Plus</a>
                </div>
            </div>
            @endif
        </div>
    
        <div class="validAdrsExists">
            @if (count($ValidAdrs)>0)
            
            @include('admin.scens.scenValidAdrs')
            <div class="w-full px-3 w-full grid justify-items-center -intro-y">
                <div class="bg-white rounded-b-full w-32 grid justify-items-center px-3 pt-2 pb-3 shadow-lg" >
                    <a class="button w-24 rounded-full mb-3 bg-theme-14 text-theme-10 add_vld_adr" href="javascript:;">Plus</a>
                </div>
            </div>
            @endif
        </div>
    
        <div class="convocationExists">
            @if (count($SenConvo)>0)
                @include('admin.scens.scenConvo')
                <div class="w-full px-3 w-full grid justify-items-center -intro-y">
                    <div class="bg-white rounded-b-full w-32 grid justify-items-center px-3 pt-2 pb-3 shadow-lg" >
                        <a class="button w-24 rounded-full mb-3 bg-theme-14 text-theme-10 add_convo" href="javascript:;">Plus</a>
                    </div>
                </div>
            @endif
        </div>
    
        <div class="contactTeleExict">
            @if (count($scen_tele)>0)
            <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold tele_block">
                @foreach ($scen_tele as $item)
                    @include('admin.scens.scenTele')
                @endforeach
                <div class="editphone">
                    <div></div>
                </div>
            </div>
            <div class="w-full px-3 w-full grid justify-items-center -intro-y">
                <div class="bg-white rounded-b-full w-32 grid justify-items-center px-3 pt-2 pb-3 shadow-lg" >
                    <a class="button w-24 rounded-full mb-3 bg-theme-14 text-theme-10 add_tele " href="javascript:;">Plus</a>
                </div>
            </div>
            @endif
        </div>
        
        <div class="visitexcist">
            @if (count($SenVisDom)>0)
            <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold">
                @foreach ($SenVisDom as $item)
                    @include('admin.scens.scenVisiteDomic')
                @endforeach
                <div class="editvisite"></div>
            </div>
            <div class="w-full px-3 w-full grid justify-items-center -intro-y">
                <div class="bg-white rounded-b-full w-32 grid justify-items-center px-3 pt-2 pb-3 shadow-lg" >
                    <a class="button w-24 rounded-full mb-3 bg-theme-14 text-theme-10 add_visite" href="javascript:;">Plus</a>
                </div>
            </div>     
            @endif
        </div>
        
        <div class="mediaExcist">
            @if (count($SenMedi)>0)
            <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold">
                @foreach ($SenMedi as $item)
                    @include('admin.scens.scenMedi')  
                @endforeach
                <div class="added_medi"></div>
            </div> 
            <div class="w-full px-3 w-full grid justify-items-center -intro-y">
                <div class="bg-white rounded-b-full w-32 grid justify-items-center px-3 pt-2 pb-3 shadow-lg" >
                    <a class="button w-24 rounded-full mb-3 bg-theme-14 text-theme-10 add_medi" href="javascript:;">Plus</a>
                </div>
            </div>
            @endif
        </div>
    
        <div class="SituJudicExcist">
            @if (count($SituJudic)>0)
            <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold">
                @foreach ($SituJudic as $item)
                    @include('admin.scens.scenSituJurid')
                @endforeach
                <div class="added_stj"></div>
            </div>
            <div class="w-full px-3 w-full grid justify-items-center -intro-y">
                <div class="bg-white rounded-b-full w-32 grid justify-items-center px-3 pt-2 pb-3 shadow-lg" >
                    <a class="button w-24 rounded-full mb-3 bg-theme-14 text-theme-10 add_stj" href="javascript:;">Plus</a>
                </div>
            </div>   
            @endif
        </div>
    
        <div class="solvDebitExcist"> 
            @if (count($solvab_debi)>0)
            <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold">
                @foreach ($solvab_debi as $item)
                    @include('admin.scens.scenSvable')
                @endforeach
                <div class="added_solvation"></div>
            </div>
            <div class="w-full px-3 w-full grid justify-items-center -intro-y">
                <div class="bg-white rounded-b-full w-32 grid justify-items-center px-3 pt-2 pb-3 shadow-lg" >
                    <a class="button w-24 rounded-full mb-3 bg-theme-14 text-theme-10 add_solvaDeb" href="javascript:;">Plus</a>
                </div>
            </div>
            @endif
            
        </div>
    </div>
    
    {{-- situation judice --}}
    <div class="judicexcist"></div>
    
    <div class="{{(!empty($histo->id) && Auth::user()->acs_phas_judis == 1)? '' : 'hidden'}} tribun" >
        <div class="text-2xl mt-5 font-semibold ml-3  w-full text-center">
            <span>Phase judiciaire</span>
        </div>
        <div class="intro-x box w-full mt-5 shadow-lg p-5">
            <div class="grid grid-cols-2">
                <div>
                    <span class="text-xl text-gray-600 font-semibold">Plainte Cheque Sans Provision</span>
                </div>
                <div class="grid justify-items-end" style="transform: translateY(6px)">
                    <div>
                        <a class="button w-24 mt-2 bg-theme-33 text-white" href="{{route('plante_Cheque',[(!empty($histo->id))? $histo->id : 0])}}">Continue</a>
                    </div> 
                </div>
            </div>
        </div>

        <div class="intro-x box w-full mt-5 shadow-lg p-5">
            <div class="grid grid-cols-2">
                <div>
                    <span class="text-xl text-gray-600 font-semibold">Injonction De Payer</span>
                </div>
                <div class="grid justify-items-end" style="transform: translateY(6px)">
                    <div>
                        <a class="button w-24 mt-2 bg-theme-33 text-white" href="{{route('injonc_payer',[(!empty($histo->id))? $histo->id : 0])}}">Continue</a>
                    </div> 
                </div>
            </div>
        </div>

        <div class="intro-x box w-full mt-5 shadow-lg p-5">
            <div class="grid grid-cols-2">
                <div>
                    <span class="text-xl text-gray-600 font-semibold">Requete en Injonction De Payer</span>
                </div>
                <div class="grid justify-items-end" style="transform: translateY(6px)">
                    <div>
                        <a class="button w-24 mt-2 bg-theme-33 text-white" href="{{route('req_injonc_payer',[(!empty($histo->id))? $histo->id : 0])}}">Continue</a>
                    </div> 
                </div>
            </div>
        </div>

        <div class="intro-x box w-full mt-5 shadow-lg p-5">
            <div class="grid grid-cols-2">
                <div>
                    <span class="text-xl text-gray-600 font-semibold">Citation directe</span>
                </div>
                <div class="grid justify-items-end" style="transform: translateY(6px)">
                    <div>
                        <a class="button w-24 mt-2 bg-theme-33 text-white" href="{{route('citation_list',[(!empty($histo->id))? $histo->id : 0])}}">Continue</a>
                    </div> 
                </div>
            </div>
        </div>
        <div class="intro-x box shadow-lg w-full p-5 mt-5">
            <div class="grid grid-cols-2">
                <div>
                    <p class="text-xl text-gray-600 font-semibold">Saisie sur le fond de commerce <span class="text-gray-500">(FC)</span></p>
                </div>
                <div class="grid justify-items-end" style="transform: translateY(6px)">
                    <div>
                        <a class="button w-24 mt-2 bg-theme-33 text-white" href="{{route('saisie_fond_commerce_list',[(!empty($histo->id))? $histo->id : 0])}}">Continue</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="intro-x box shadow-lg w-full p-5 mt-5">
            <div class="grid grid-cols-2">
                <div>
                    <p class="text-xl text-gray-600 font-semibold">Saisie conservatoire tiers détenteur  <span class="text-gray-500">(TD)</span></p>
                </div>
                <div class="grid justify-items-end" style="transform: translateY(6px)">
                    <div>
                        <a class="button w-24 mt-2 bg-theme-33 text-white" href="{{route('saisie_tiers_detonado_list',[(!empty($histo->id))? $histo->id : 0])}}">Continue</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="intro-x box shadow-lg w-full p-5 mt-5">
            <div class="grid grid-cols-2">
                <div>
                    <span class="text-xl text-gray-600 font-semibold">Saisie arrêt bancaire</span>
                </div>
                <div class="grid justify-items-end" style="transform: translateY(6px)">
                    <div>
                        <a class="button w-24 mt-2 bg-theme-33 text-white" href="{{route('saisie_arret_bancaire_list',[(!empty($histo->id))? $histo->id : 0])}}">Continue</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="intro-x box shadow-lg w-full p-5 mt-5">
            <div class="grid grid-cols-2">
                <div>
                    <span class="text-xl text-gray-600 font-semibold">Saisie Actions /Parts sociales</span>
                </div>
                <div class="grid justify-items-end" style="transform: translateY(6px)">
                    <div>
                        <a class="button w-24 mt-2 bg-theme-33 text-white" href="{{route('saisie_actions_parts_soc_list',[(!empty($histo->id))? $histo->id : 0])}}">Continue</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="intro-x box shadow-lg w-full p-5 mt-5">
            <div class="grid grid-cols-2">
                <div>
                    <span class="text-xl text-gray-600 font-semibold">Saisie Mobilier</span>
                </div>
                <div class="grid justify-items-end" style="transform: translateY(6px)">
                    <div>
                        <a class="button w-24 mt-2 bg-theme-33 text-white" href="{{route('saisie_moblier_list',[(!empty($histo->id))? $histo->id : 0])}}">Continue</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="intro-x box shadow-lg w-full p-5 mt-5">
            <div class="grid grid-cols-2">
                <div>
                    <span class="text-xl text-gray-600 font-semibold">Saisie Immobilier</span>
                </div>
                <div class="grid justify-items-end" style="transform: translateY(6px)">
                    <div>
                        <a class="button w-24 mt-2 bg-theme-33 text-white" href="{{route('saisie_immoblier_list',[(!empty($histo->id))? $histo->id : 0])}}">Continue</a>
                    </div>
                </div>
            </div>
        </div>

        {{-- --------------------------------------------------------------- --}}
        {{-- <div class="intro-y box px-5 py-8 mt-5 font-semibold">
            <div class="mt-3"> 

                <div class="grid lg:grid-cols-2 md:grid-cols-1 gap-2">
                    <div class="mx-2">
                        <div><label class="text-xl text-gray-600">Type de procédure</label></div>
                    </div>
                </div>

                <div class="bg-gray-300 p-4 mt-3 rounded-lg shadow-inner">
                    <div class="grid justify-items-center box p-3 shadow-lg">
                        <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 ">

                            <div class="flex items-center text-gray-700 m-2 ">
                                <input type="radio" class="input border border-gray-600 border-2 mr-2 citation_dir_rang" name="nameis" data-bilong = "citation_dir_block">
                                <label for="">Citation directe</label>
                            </div>

                            <div class="flex items-center text-gray-700 m-2 ">
                                <input type="radio" class="input border border-gray-600 border-2 mr-2 plant_scam_range" name="nameis" data-bilong = "plant_scam_block">
                                <label for="">Plainte d’escroquerie</label>
                            </div>

                            <div class="flex items-center text-gray-700 m-2 ">
                                <input type="radio" class="input border border-gray-600 border-2 mr-2 injonction_pay_range" name="nameis" data-bilong ="injonction_pay_block">
                                <label for="">Injonction de payer</label>
                            </div>

                            <div class="flex items-center text-gray-700 m-2 ">
                                <input type="radio" class="input border border-gray-600 border-2 mr-2 cheque_sn_provis_range" name="nameis" data-bilong ="cheque_sn_provis_block">
                                <label for="">Plainte chèque sans provision</label>
                            </div>

                            <div class="flex items-center text-gray-700 m-2 ">
                                <input type="radio" class="input border border-gray-600 border-2 mr-2 req_injonction_pay_range" name="nameis" data-bilong ="req_injonction_pay_block">
                                <label for="">Requête en injonction de payer</label>
                            </div>

                            <div class="flex items-center text-gray-700 m-2 ">
                                <input type="radio" class="input border border-gray-600 border-2 mr-2 ses_conerv_fon_com_rang" name="nameis" data-bilong ="ses_conerv_fon_com_block">
                                <label for="">Saisie conservatoire sur le fond de commerce</label>
                            </div>

                            <div class="flex items-center text-gray-700 m-2 ">
                                <input type="radio" class="input border border-gray-600 border-2 mr-2 ses_conserv_bien_imob_rang" name="nameis" data-bilong ="ses_conserv_bien_imob_block">
                                <label for="">Saisie conservatoire de bien immobilier</label>
                            </div>

                            <div class="flex items-center text-gray-700 m-2 ">
                                <input type="radio" class="input border border-gray-600 border-2 mr-2 ses_conserv_bien_mob_rang" name="nameis" data-bilong ="ses_conserv_bien_mob_block">
                                <label for="">Saisie conservatoire de bien mobilier</label>
                            </div>

                            <div class="flex items-center text-gray-700 m-2 ">
                                <input type="radio" class="input border border-gray-600 border-2 mr-2 ses_arret_bank_rang" name="nameis" data-bilong ="ses_arret_bank_block">
                                <label for="">Saisie arrêt bancaire</label>
                            </div>

                            <div class="flex items-center text-gray-700 m-2 ">
                                <input type="radio" class="input border border-gray-600 border-2 mr-2 ses_arret_bank_entr_main_tier_rang" name="nameis" data-bilong ="ses_arret_bank_entr_main_tier_block">
                                <label for="">Saisie entre les mains des tiers</label>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div> --}}
        
        <div class="list_situ_jurid">
        </div>

        <div class="proc_range_form">
        </div>

        <div class="exist_ranges">
            @include('admin.scens.exist_ranges')
        </div>
        {{-- --------------------------------------------------------------- --}}
    </div>        

    <div class="grid justify-items-center">
        <div class="flex lg:flex-row md:flex-row flex-col ">
            <button class="button w-24 mr-1 mb-2 {{(!empty($histo->id))? 'bg-theme-1' : 'bg-theme-33'}} bg-theme-1 text-white mt-4 save">{{(!empty($histo->id))? 'Enregistrer' : 'Continuer'}}</button> 
        </div>
    </div>

</form>
@endsection
@section('script')
    <script>
        var huissier='';
        var proc_exec_huissierr="";
    </script>
    <script src="{{asset('dist/js/param.js')}}"></script>
    <script>
        $("body").on('click','.cncel_proc',function(){
            event.preventDefault();
            $('.list_situ_jurid').html('');
            $('.citation_dir_rang,.plant_scam_range,.injonction_pay_range,.cheque_sn_provis_range,.req_injonction_pay_range,.ses_conerv_fon_com_rang,.ses_conserv_bien_imob_rang,.ses_conserv_bien_mob_rang,.ses_arret_bank_rang,.ses_arret_bank_entr_main_tier_rang').prop('checked',false);
                        
        });
    </script>
    <script>
        $(document).ready(()=>{
            $bilongs = '';
            $('.citation_dir_rang,.plant_scam_range,.injonction_pay_range,.cheque_sn_provis_range,.req_injonction_pay_range,.ses_conerv_fon_com_rang,.ses_conserv_bien_imob_rang,.ses_conserv_bien_mob_rang,.ses_arret_bank_rang,.ses_arret_bank_entr_main_tier_rang').change(function(){
                $bilongs = $(this).data('bilong');
                if($(this).is(':checked')){
                    if($bilongs == 'injonction_pay_block' || $bilongs == 'req_injonction_pay_block' || $bilongs == 'cheque_sn_provis_block' || $bilongs == 'citation_dir_block'){
                        $.ajax({
                            url : `{{route('get_list_situ_jurid')}}`,
                            type :'post', 
                            dataType : 'json',
                            data:{
                                bilongs:$bilongs,
                                id:`{{(!empty($histo->id))? $histo->id : 0}}`},
                            success:function(data){
                                $('.list_situ_jurid').html(data);
                                console.log(data);
                                
                            },
                            error:function(error){
                                console.log(error);
                            },
                        });
                    }else{
                        $.ajax({
                            url : `{{route('get_range_form')}}`,
                            type :'post', 
                            dataType : 'json',
                            data:{
                                bilongs:$bilongs,
                            },
                            success:function(data){
                                $('.list_situ_jurid').html(data);
                                
                            },
                            error:function(error){
                                console.log(error);
                            },
                        });
                    }
                }else{
                    $('.list_situ_jurid').html("");
                }
            });
            $("body").on("click", '.valid_proc', () => {
                event.preventDefault();
                $radio = $('.radio:checked').val();
                if(typeof $radio !== 'undefined') 
                    $.ajax({
                        url:`{{route('get_range_form')}}`,
                        type:'post',
                        dataType:'json',
                        data : {
                            id:$radio,
                            bilongs:$bilongs,
                           
                        },
                        success:(data)=>{
                            $('.proc_range_form').html(data)
                            $('.list_situ_jurid').html('');
                            console.log(data);
                            
                        },
                        error:(error)=>{
                            console.log(error);
                        },
                    });
            });
            
        });
        
    </script>
    <script>
        $(document).ready(() => {
            $("body").on('click', '.valid_rang', () => {
                event.preventDefault();
                $.ajax({
                    url: `{{ route('create_range') }}`,
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id_histo_from_range : $('.id_histo_from_range').val(),
                        belong :  $('.belong').val(),
                        date_1 :  $('.date_1').val(),
                        date_2 :  $('.date_2').val(),
                        id_doss : {{$id_doss}}
                    },
                    success: (data) => {
                        // console.log(data);
                        $('.proc_range_form').html('');
                        $('.citation_dir_rang,.plant_scam_range,.injonction_pay_range,.cheque_sn_provis_range,.req_injonction_pay_range,.ses_conerv_fon_com_rang,.ses_conserv_bien_imob_rang,.ses_conserv_bien_mob_rang,.ses_arret_bank_rang,.ses_arret_bank_entr_main_tier_rang').prop('checked',false);
                        $('.exist_ranges').html(data);
                        $('.list_situ_jurid').html('');
                    },
                    error: (error) => {
                        console.log(error);
                    },
                });
               
            });
            
        });
    </script>
    <script>
        $("body").on('click','.delete_range', function(){
            $this = $(this);
            event.preventDefault();
             
            $.ajax({
                url : `{{route('delete_range')}}`,
                type : 'post',
                dataType : 'json',
                data : {
                    id:$this.data('id_range'),
                    id_doss : {{$id_doss}}
                },
                success:function(data){
                    // console.log(data);
                    $('.exist_ranges').html(data);
                    // $('.delete_range').parent().parent().hide("normal");
                },
                error:function(error){
                    console.log(error);
                },
            });
        });
        $("body").on('click','.update_range', function(){
            $this = $(this);
            event.preventDefault();
            $.ajax({
                url : `{{route('update_range')}}`,
                type : 'post',
                dataType : 'json',
                data : {
                    id:$this.data('id_range'),
                    date_1_:$(`.date_1_${$this.data('id_range')}`).val(),
                    date_2_:$(`.date_2_${$this.data('id_range')}`).val(),
                    id_doss : {{$id_doss}}
                },
                success:function(data){
                    // console.log(data);
                    $('.exist_ranges').html(data);
                    
                },
                error:function(error){
                    console.log(error);
                },
            });
        });
    </script>
    
    <script>
        var judicexcist = `
            <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold conatains_judic_chek">
                <div class="accordion__pane  "> 
                    <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg" >
                        <span class="text-xl">
                            PLAINTE CHEQUE SANS PROVISION
                        </span>    
                    </a>
                    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
                        <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                            <div class="mt-2">
                                <label for="">Référence</label>
                                <input type="text" name="ref_st_cheqe" class="input w-full border border-gray-500 mt-2" placeholder="Entrez un référence">
                            </div>
                            <div class="mt-2">
                                <label for="">Tribunal spécialisé</label>
                                <input type="text" name="tribunal_spese" class="input w-full border border-gray-500 mt-2" placeholder="Entrez tribunal spécialisé">
                            </div>
                        </div>
                        <div class="my-2">
                            <label for="" class="text-lg ">les partie</label>
                        </div>
                        <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
                            <div class="">
                                <div class="mt-2">
                                    <label for="">Le Tireur</label>
                                    <input type="text" name="tireur" class="input w-full border border-gray-500 mt-2" placeholder="Entrez le tireur">
                                </div>
                                <div class="mt-2">
                                    <label for="">Adresse</label>
                                    <input type="text" name="adresse_cheq" class="input w-full border border-gray-500 mt-2" placeholder="Entrez une adresse">
                                </div>
                                <div class="mt-2">
                                    <label for="">C.i.n</label>
                                    <input type="text" name="cin_cheq" class="input w-full border border-gray-500 mt-2" placeholder="Entrez le cin de tireur">
                                </div>
                                <div class="mt-2">
                                    <label for="">le mandataire</label>
                                    <select name="procureur_tir" class="input w-full border border-gray-500 mt-2">
                                        <option value='0'>Sélectionnez un mandataire</option>
                                        @foreach($partner as $item)
                                            <option value='{{$item->id}}'>{{$item->nom_part}} {{$item->prenom_part}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="">
                                <div class="mt-2">
                                    <label for="">Bénéficiaire</label>
                                    <input type="text" name="benefic" class="input w-full border border-gray-500 mt-2" placeholder="Entrez le bénéficiaire">
                                </div>
                                <div class="mt-2">
                                    <label for="">le mandataire</label>
                                    <input type="text" name="procureur_tir_benif" class="input w-full border border-gray-500 mt-2" placeholder="Entrez le procureur de bénéficiaire">
                                </div>
                                <div class="mt-2">
                                    <label for="">Chargé De Suivi</label>
                                    <input type="text" name="charge_suiv" class="input w-full border border-gray-500 mt-2" placeholder="Entrez Le Chargé De Suivi">
                                </div>
                            </div>
                        </div>
                        <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
                            <div class="mt-2">
                                <label for="">La Banque</label>
                                <input type="text" class="input w-full border border-gray-500 mt-2" placeholder="Entrez la banque" name="banque_tire" >
                            </div>
                            <div class="mt-2">
                                <label for="">Adresse</label>
                                <input type="text" class="input w-full border border-gray-500 mt-2" placeholder="Entrez l'adresse" name="adrs_tire" >
                            </div>
                            <div class="mt-2">
                                <label for="">Numéro du compte</label>
                                <input type="text" class="input w-full border border-gray-500 mt-2" placeholder="Entrez le numéro de compte" name="n_compt" >
                            </div>
                        </div>
                        <div class="my-5">
                            <label for="" class="text-lg">Informations sur chèque</label>
                        </div>
                        <div class="cheqs">
                            <div class="grid lg:grid-cols-4 md:grid-cols-1 sm:grid-cols-1 gap-1 border-b pb-3 ">
                            <input type="hidden" name="cheque_nb[]" value="1">
                                <div class="mt-2">
                                    <label for="">Montant du chèque</label>
                                    <input type="number" class="input w-full border border-gray-500 mt-2"  min="0" step="0.5" name="montant_cheq[]" placeholder="Entrez le montant du chèque">
                                </div>
                                <div class="mt-2">
                                    <label for="">Numéro du chèque</label>
                                    <input type="number" class="input w-full border border-gray-500 mt-2"  min="0" step="0.5" name="n_cheq[]" placeholder="Saisissez le numéro du chèque">
                                </div>
                                <div class="mt-2">
                                    <label for="">Date D’émission </label>
                                    <input type="date" class="input w-full border border-gray-500 mt-2" name="date_emission[]" value="` + curr + `" >
                                </div>
                                <div class="mt-2">
                                    <label for="">Motif</label>
                                    <input type="text" class="input w-full border border-gray-500 mt-2" name="motif[]" placeholder="Entrez le motif">
                                </div>
                            </div>
                        </div>
                        
                        <div class="grid justify-items-center my-3">
                            <div class="flex flex-row hover:text-theme-33 add_cheq">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle mx-auto"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                                <span class="" style="margin-top: 0.1rem">Plus</span>
                            </div>
                        </div>
                        <div class="mt-2 grid lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-2">
                            <div>
                                <label for="">Montant Global </label>
                                <input type="number" name="montant_glob" step="0.1" min="0" class="input w-full border border-gray-500 mt-2 " placeholder=" Montant Global" id="">
                            </div>
                        </div>
                        <div class="grid lg:justify-items-end lg:mt-0 mt-3 my-2">
                            <div class="flex flex-row">
                                <label class="text-gray-600">La Phase De Première Instance </label>
                                <input type="checkbox" class="input input--switch border ml-2 prem_inst" name="prem_inst"> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
        $(document).ready(function(){
            
            var kill_nb=0;
            $('body').on('change','.this_file',function(input){
                var file = $(this).get(0).files[0];
                if(file){
                    var reader = new FileReader();
                    $(this).parent().parent().find('.file_block').append(
                        `
                        <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">
                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                <span class="w-3/5 file__icon file__icon--file mx-auto">
                                    <div class="file__icon__file-name"></div>
                                </span>
                                <span class="block font-medium mt-4 text-center truncate">`+file.name+`</span>
                            </div>
                            <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file`+kill_nb+`"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                            </div>
                        </div>
                        `
                    );
                   
                    $(this).parent().find('button').hide();
                    $(this).parent().append(`
                        <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                        </button>
                        <input type="file" name="file_val[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file`+kill_nb+`">
                    `);
                    kill_nb++;
                }
            });
            $("body").on('click tap','.kill_file,.kill_file_base',function(){
                $('.'+$(this).data('id')).attr('name' ,'');
                $(this).parent().toggle();
                
            });
        });
    </script>
    <script>
        $("body").ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
          
            $('body').on('click','.save',function(){
                event.preventDefault();
                var formData = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:"{{route('createScene')}}",
                    type:'post',
                    dataType:'json',
                    data:formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        console.log(data);
                        Swal.fire(
                        'Validé!',
                        'les détails sont enregistrés avec succès !',
                        'success'
                        ).then((result) =>{
                           location.reload(); 
                        });
                        // $('.tribun').show('show');
                        // $('.back').show("slow");
                        $("#top_chaine")[0].scrollIntoView();
                        
                        console.log(data);
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });
            var medihtml=`
                <div class="accordion__pane border-b border-gray-200 dark:border-dark-5 "> 
                    <div class="grid grid-cols-2  hover:bg-gray-400 rounded-lg ">
                        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2" >
                            <span class="text-xl">
                                Médiations
                            </span>    
                        </a>
                        <div class="grid justify-items-end flex items-center">
                            <div title="" class=" w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 mr-2  kill_medi"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                            </div>   
                        </div>
                    </div>
                    <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
                        <div class="grid lg:grid-cols-2 grid-cols-1 gap-2">
                            <div class="mt-2">
                                <label for="">Lieu</label>
                                <select name="lieu_med[]" class="input border border-gray-500 w-full">
                                    <option value="0">Sélectionnez un lieu</option>
                                    <option value="Au Cabinet">Au Cabinet</option>
                                    <option value="Chez le Débiteur(bureau)">Chez le Débiteur(bureau)</option>
                                    <option value="Chez le Client(bureau)">Chez le Client(bureau)</option>
                                </select>
                            </div>
                        </div>
                        <div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">
                            <div class="mt-2">
                                <label for="">Date et heure</label>
                                <input type="datetime-local" name="date_med[]" class="input border border-gray-500 w-full" value="` + now.toISOString().slice(0, 16) + `" >
                            </div>
                            <div class="mt-2">
                                <label for="">Adresse de médiation</label>
                                <input type="text" name="adress_med[]" class="input border border-gray-500 w-full" placeholder="Adresse de médiation">
                            </div>
                            <div class="mt-2">
                                <label for="">Médiateur</label>
                                <select name="mediateur[]" class="input border border-gray-500 w-full">
                                    <option value="0">Sélectionnez un médiateur</option>
                                    @foreach ($agent as $item)
                                        <option value="{{$item->id}}">{{$item->nom}} {{$item->prenom}}</option>  
                                    @endforeach
                                </select>
                            </div>
                            <div class="mt-2">
                                <label for="">Débiteur / Mandataire</label>
                                <input type="text" name="debiteur_mandataire[]" class="input border border-gray-500 w-full" value="{{$debiteur->nom}} {{$debiteur->prenom}}">
                             </div>
                        </div>
                        <div class="mt-2">
                            <label for="">Promesse</label>
                            <textarea name="promesse_medi[]" class="input border border-gray-500 w-full h-32" placeholder="promesses..."></textarea>
                        </div>
                        <div class="mt-2">
                            <label for="">Commentaires</label>
                            <textarea name="comment_medi[]" class="input border border-gray-500 w-full h-32" placeholder="Commentaires..."></textarea>
                        </div>
                    </div>
                </div>
            `;
            var medihtmloute=`
                <div class="accordion intro-y box px-5 py-8 mt-5 font-semibold">
                    <div class="accordion__pane border-b border-gray-200 dark:border-dark-5 "> 
                        <a href="javascript:;" class="accordion__pane__toggle font-medium block py-4 px-2 hover:bg-gray-400 rounded-lg " >
                            <span class="text-xl">
                                Médiations
                            </span>    
                        </a>
                        <div class="accordion__pane__content mt-3 text-gray-700 dark:text-gray-600 leading-relaxed">
                            <div class="grid lg:grid-cols-2 grid-cols-1 gap-2">
                                <div class="mt-2">
                                    <label for="">Lieu</label>
                                    <select name="lieu_med[]" class="input border border-gray-500 w-full">
                                        <option value="0">Sélectionnez un lieu</option>
                                        <option value="Au Cabinet">Au Cabinet</option>
                                        <option value="Chez le Débiteur(bureau)">Chez le Débiteur(bureau)</option>
                                        <option value="Chez le Client(bureau)">Chez le Client(bureau)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-2">
                                <div class="mt-2">
                                    <label for="">Date et heure</label>
                                    <input type="datetime-local" name="date_med[]" class="input border border-gray-500 w-full" value="` + now.toISOString().slice(0, 16) + `" >
                                </div>
                                <div class="mt-2">
                                    <label for="">Adresse de médiation</label>
                                    <input type="text" name="adress_med[]" class="input border border-gray-500 w-full" placeholder="Adresse de médiation">
                                </div>
                                <div class="mt-2">
                                    <label for="">Médiateur</label>
                                    <select name="mediateur[]" class="input border border-gray-500 w-full">
                                        <option value="0">Sélectionnez un médiateur</option>
                                        @foreach ($agent as $item)
                                            <option value="{{$item->id}}">{{$item->nom}} {{$item->prenom}}</option>  
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mt-2">
                                    <label for="">Débiteur / Mandataire</label>
                                    <input type="text" name="debiteur_mandataire[]" class="input border border-gray-500 w-full" value="{{$debiteur->nom}} {{$debiteur->prenom}}">
                                </div>
                            </div>
                            <div class="mt-2">
                                <label for="">Promesse</label>
                                <textarea name="promesse_medi[]" class="input border border-gray-500 w-full h-32" placeholder="promesses..."></textarea>
                            </div>
                            <div class="mt-2">
                                <label for="">Commentaires</label>
                                <textarea name="comment_medi[]" class="input border border-gray-500 w-full h-32" placeholder="Commentaires..."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="added_medi"></div>
                </div>
                
                <div class="w-full px-3 w-full grid justify-items-center -intro-y">
                    <div class="bg-white rounded-b-full w-32 grid justify-items-center px-3 pt-2 pb-3 shadow-lg" >
                        <a class="button w-24 rounded-full mb-3 bg-theme-14 text-theme-10 add_medi" href="javascript:;">Plus</a>
                    </div>
                </div>
            `; 
            var clk_nb_tel=(parseInt("{{count($scen_tele)}}")==0)? 1 : parseInt("{{count($scen_tele)}}");
            var clk_nb_courr=(parseInt("{{count($scen_courr)}}")==0)? 1 : parseInt("{{count($scen_courr)}}");
            var clk_nb_visit=(parseInt("{{count($SenVisDom)}}")==0)? 1 : parseInt("{{count($SenVisDom)}}");
            var clk_nb_valid_adrs=(parseInt("{{count($ValidAdrs)}}")==0)? 1 : parseInt("{{count($ValidAdrs)}}");
            var clk_nb_convo=(parseInt("{{count($SenConvo)}}")==0)? 1 : parseInt("{{count($SenConvo)}}");
            var clk_nb_medi=(parseInt("{{count($SenMedi)}}")==0)? 1 : parseInt("{{count($SenMedi)}}");
            var clk_nb_SituJudic=(parseInt("{{count($SituJudic)}}")==0)? 1 : parseInt("{{count($SituJudic)}}");
            var clk_nb_solve=(parseInt("{{count($solvab_debi)}}")==0)? 1 : parseInt("{{count($solvab_debi)}}");
            var append_tel = 1;
            $('.nombre_solvaDebit_max').keyup(function(){
                clk_nb_solve=(parseInt("{{count($solvab_debi)}}")==0)? 1 : parseInt("{{count($solvab_debi)}}");
                if("{{count($solvab_debi)}}">0 && "{{count($solvab_debi)}}" == $('.nombre_solvaDebit_max').val()){
                    // $('.added_solvation').toggle();
                    $('.added_solvation').html("");
                }
                if ("{{count($solvab_debi)}}">0 && $('.nombre_solvaDebit_max').val()==""){
                    $('.added_solvation').html("");
                }
                if("{{count($solvab_debi)}}" == 0){
                    $('.added_solvation').html("");
                }
            });
            $('.nombre_tele_max').keyup(function(){
                clk_nb_tel=(parseInt("{{count($scen_tele)}}")==0)? 1 : parseInt("{{count($scen_tele)}}");
                if("{{count($scen_tele)}}">0 && "{{count($scen_tele)}}" == $('.nombre_tele_max').val()){
                    // $('.editphone').toggle();
                    $('.editphone').html("");
                }
                if ("{{count($scen_tele)}}">0 && $('.nombre_tele_max').val()==""){
                    $('.editphone').html("");
                }
                if("{{count($scen_tele)}}" == 0){
                    $('.editphone').html("");
                }
            });
           
            
            $('.nombre_situJud_max').keyup(function(){
                clk_nb_SituJudic=(parseInt("{{count($SituJudic)}}")==0)? 1 : parseInt("{{count($SituJudic)}}");
                if("{{count($SituJudic)}}">0 && "{{count($SituJudic)}}" == $('.nombre_situJud_max').val()){
                    // $('.addedstJD').toggle();
                    $('.addedstJD').html("");
                }
                if ("{{count($SituJudic)}}">0 && $('.nombre_situJud_max').val()==""){
                    $('.addedstJD').html("");
                }
                if("{{count($SituJudic)}}" == 0){
                    $('.addedstJD').html("");
                }
            });
            $('.nombre_courr_max').keyup(function(){
                clk_nb_courr=(parseInt("{{count($scen_courr)}}")==0)? 1 : parseInt("{{count($scen_courr)}}");
                if("{{count($scen_tele)}}">0 && "{{count($scen_tele)}}" == $('.nombre_tele_max').val()){
                    // $('.editmail').toggle();
                    $('.editmail').html("");
                }
                if ("{{count($scen_tele)}}">0 && $('.nombre_tele_max').val()==""){
                    $('.editmail').html("");
                }
                
                if("{{count($scen_courr)}}" == 0){
                    $('.editmail').html("");
                }
            });

            $('.nombre_visit_max').keyup(function(){
                clk_nb_visit=(parseInt("{{count($SenVisDom)}}")==0)? 1 : parseInt("{{count($SenVisDom)}}");
                if("{{count($SenVisDom)}}">0 && "{{count($SenVisDom)}}" == $('.nombre_visit_max').val()){
                    // $('.editvisite').toggle();
                    $('.editvisite').html("");
                }

                if ("{{count($SenVisDom)}}">0 && $('.nombre_visit_max').val()==""){
                    $('.editvisite').html("");
                }

                if("{{count($SenVisDom)}}" == 0){
                    $('.editvisite').html("");
                }
            });
            $('.valid_adrs_max').keyup(function(){
                clk_nb_valid_adrs=(parseInt("{{count($ValidAdrs)}}")==0)? 1 : parseInt("{{count($ValidAdrs)}}");
                if("{{count($ValidAdrs)}}">0 && "{{count($ValidAdrs)}}" == $('.valid_adrs_max').val()){
                    // $('.added_valid_adrs').toggle();
                    $('.added_valid_adrs').html("");
                }
                if ("{{count($ValidAdrs)}}">0 && $('.valid_adrs_max').val()==""){
                    $('.added_valid_adrs').html("");
                }
                if("{{count($ValidAdrs)}}" == 0){
                    $('.added_valid_adrs').html("");
                }
            });
            $('.nombre_convo_max').keyup(function(){
                clk_nb_convo=(parseInt("{{count($SenConvo)}}")==0)? 1 : parseInt("{{count($SenConvo)}}");
                if("{{count($SenConvo)}}">0 && "{{count($SenConvo)}}" == $('.nombre_convo_max').val()){
                    // $('.added_convo').toggle();
                    $('.added_convo').html("");
                }
                if ("{{count($SenConvo)}}">0 && $('.nombre_convo_max').val()==""){
                    $('.added_convo').html("");
                }
                if("{{count($SenConvo)}}" == 0){
                    $('.added_convo').html("");
                }
            });
            $('.nombre_medi_max').keyup(function(){
                clk_nb_medi=(parseInt("{{count($SenMedi)}}")==0)? 1 : parseInt("{{count($SenMedi)}}");
                if("{{count($SenMedi)}}">0 && "{{count($SenMedi)}}" == $('.nombre_medi_max').val()){
                    // $('.added_medi').toggle();
                    $('.added_medi').html("");
                }
                if ("{{count($SenMedi)}}">0 && $('.nombre_medi_max').val()==""){
                    $('.added_medi').html("");
                }
                if("{{count($SenMedi)}}" == 0){
                    $('.added_medi').html("");
                }
            });
            $('.nombre_situJud_max').keyup(function(){
                clk_nb_SituJudic=(parseInt("{{count($SituJudic)}}")==0)? 1 : parseInt("{{count($SituJudic)}}");
                if("{{count($SituJudic)}}">0 && "{{count($SituJudic)}}" == $('.nombre_situJud_max').val()){
                    // $('.added_medi').toggle();
                    $('.added_stj').html("");
                }
                if ("{{count($SituJudic)}}">0 && $('.nombre_situJud_max').val()==""){
                    $('.added_stj').html("");
                }
                if("{{count($SituJudic)}}" == 0){
                    $('.added_stj').html("");
                }
            });
            $("body").on('click','.add_solvaDeb',function(){
                var tnt_nb_solv=($('.nombre_solvaDebit_max').val()>0)? $('.nombre_solvaDebit_max').val() : 0;
                // alert();
                if(tnt_nb_solv > clk_nb_solve ){
                    $('.added_solvation').append(solvaDebhtml);
                    clk_nb_solve++;
                }
            });
            $("body").on('click','.add_courrier',function(){
                var tnt_nb_courr=($('.nombre_courr_max').val()>0)? $('.nombre_courr_max').val() : 0;
                
                if(tnt_nb_courr > clk_nb_courr ){
                    $('.editmail').append(courrierhtml);
                    clk_nb_courr++;
                }
            });
            $("body").on('click','.add_stj',function(){
                var tnt_nb_situJud=($('.nombre_situJud_max').val()>0)? $('.nombre_situJud_max').val() : 0;
                if(tnt_nb_situJud > clk_nb_SituJudic ){
                    $('.added_stj').append(stj_innr_html);
                    clk_nb_SituJudic++;
                }
            });
            $("body").on('click tap','.add_tele',function(){
                var tnt_nb_tel=($('.nombre_tele_max').val()>0)? $('.nombre_tele_max').val() : 0;
                // console.log(tnt_nb_tel , clk_nb_tel);
                if(tnt_nb_tel > clk_nb_tel){
                    // console.log(teleHtml);
                    $('.editphone').append(teleHtml); 
                    clk_nb_tel++;
                }
            });
            $("body").on('click','.add_visite',function(){
                var tnt_nb_visit=($('.nombre_visit_max').val()>0)? $('.nombre_visit_max').val() : 0;
                if(tnt_nb_visit > clk_nb_visit){
                    // console.log(teleHtml);
                    $('.editvisite').append(visitehtml); 
                    clk_nb_visit++;
                }
            });
            $("body").on('click','.add_vld_adr',function(){
                var tnt_nb_valid=($('.valid_adrs_max').val()>0)? $('.valid_adrs_max').val() : 0;
                if(tnt_nb_valid > clk_nb_valid_adrs){
                    // console.log(teleHtml);
                    $('.added_valid_adrs').append(added_valid_adrshtml); 
                    clk_nb_valid_adrs++;
                }
            });
            $('body').on('click','.add_convo',function(){
                var tnt_nb_convo=($('.nombre_convo_max').val()>0)? $('.nombre_convo_max').val() : 0;
                if(tnt_nb_convo > clk_nb_convo ){
                    $('.added_convo').append(outer_convo);
                    clk_nb_convo++;
                }
            });
            $('body').on('click','.add_medi',function(){
                var tnt_nb_medi=($('.nombre_medi_max').val()>0)? $('.nombre_medi_max').val() : 0;
                if(tnt_nb_medi > clk_nb_medi ){
                    $('.added_medi').append(medihtml);
                    clk_nb_medi++;
                }
            });
            $('body').on('click', '.kill_tele', function() {
                $(this).parent().parent().parent().remove();
                $(this).parent().parent().html("");
                clk_nb_tel--;
            });

            $('body').on('click','.kill_sitatJudic',function(){
                $(this).parent().parent().parent().remove();
                $(this).parent().parent().html("");
                clk_nb_solve--;
            });
            
            $('body').on('click', '.kill_courr', function() {
                $(this).parent().parent().parent().remove();
                $(this).parent().parent().html("");
                clk_nb_courr--;
            });
            $('body').on('click', '.kill_visit', function() {
                $(this).parent().parent().parent().remove();
                $(this).parent().parent().html("");
                clk_nb_visit--;
            });
            $('body').on('click','.kill_valid_adrs',function(){
                $(this).parent().parent().parent().remove();
                $(this).parent().parent().html("");
                clk_nb_valid_adrs--;
            });
            $('body').on('click','.kill_convo',function(){
                $(this).parent().parent().parent().remove();
                $(this).parent().parent().html("");
                clk_nb_convo--;
            });
           
            $('body').on('click','.kill_medi',function(){
                $(this).parent().parent().parent().remove();
                $(this).parent().parent().html("");
                clk_nb_medi--;
            });
            $('body').on('click','.kill_stj',function(){
                $(this).parent().parent().parent().remove();
                $(this).parent().parent().html("");
                clk_nb_SituJudic--;
            });
            
            //////////////////////////////
            $('.ck_solvaDebit').change(function(){
                if ($(this).is(":checked")) {
                    $('.nombre_solvaDebit_max').show();
                    $('.solvDebitExcist').html(solvaDebhtmlouter);
                    $('.nombre_solvaDebit_max').val(1);
                    $(this).parent().addClass("rounded-lg px-2 py-2 bg-gray-400");
                }else{
                    if(clk_nb_solve>0){
                        Swal.fire({
                        title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                        showDenyButton: true,
                        // showCancelButton: true,
                        allowOutsideClick: false,
                        confirmButtonText: 'Continuez',
                        denyButtonText: `ignorer`,
                        }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                clk_nb_solve=0;
                                $('.nombre_solvaDebit_max').hide();
                                $('.solvDebitExcist').html("");
                                $('.nombre_solvaDebit_max').val("");
                                $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                            } else if (result.isDenied) {
                                $(this).prop("checked",true);
                            }
                        });
                    }else{
                        $('.nombre_solvaDebit_max').hide();
                        $('.solvDebitExcist').html("");
                        $('.nombre_solvaDebit_max').val("");
                        $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                    }
                }
            });
            ////////////////////////////////
            $('.ck_tele').change(function() {
                if ($(this).is(":checked")) {
                    $('.nombre_tele_max').show();
                    $('.contactTeleExict').html(teleHtmlouter);
                    $('.nombre_tele_max').val(1);
                    $(this).parent().addClass("rounded-lg px-2 py-2 bg-gray-400");
                } else {
                    if(clk_nb_tel>0){
                        Swal.fire({
                        title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                        showDenyButton: true,
                        // showCancelButton: true,
                        allowOutsideClick: false,
                        confirmButtonText: 'Continuez',
                        denyButtonText: `ignorer`,
                        }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                clk_nb_tel=0;
                                $('.contactTeleExict').html("");
                                $('.nombre_tele_max').hide();
                                $('.nombre_tele_max').val("");
                                $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                            } else if (result.isDenied) {
                                $(this).prop("checked",true);
                            }
                        });
                    }else{
                        $('.contactTeleExict').html("");
                        $('.nombre_tele_max').hide();
                        $('.nombre_tele_max').val("");
                        $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                    }
                }
            });
            $('.ck_courr').change(function() {
                if ($(this).is(":checked")) {
                    $('.nombre_courr_max').show();
                    $('.sendMailExists').html(outercourrierhtml);
                    $('.nombre_courr_max').val(1);
                    $(this).parent().addClass("rounded-lg px-2 py-2 bg-gray-400");
                } else {
                    if(clk_nb_courr>0){
                        Swal.fire({
                        title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                        showDenyButton: true,
                        // showCancelButton: true,
                        allowOutsideClick: false,
                        confirmButtonText: 'Continuez',
                        denyButtonText: `ignorer`,
                        }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                clk_nb_courr=0;
                                $('.sendMailExists').html("");
                                $('.nombre_courr_max').hide();
                                $('.nombre_courr_max').val("");
                                $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                            } else if (result.isDenied) {
                                $(this).prop("checked",true);
                            }
                        });
                    }else{
                        $('.sendMailExists').html("");
                        $('.nombre_courr_max').hide();
                        $('.nombre_courr_max').val("");
                        $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                    }
                    
                }
            });
            $('.ck_situJud').change(function(){
                if ($(this).is(":checked")) {
                    $('.nombre_situJud_max').show();
                    $('.SituJudicExcist').html(outerstj);
                    $('.nombre_situJud_max').val(1);
                    $(this).parent().addClass("rounded-lg px-2 py-2 bg-gray-400");
                }else{
                    if(clk_nb_SituJudic>0){
                        Swal.fire({
                        title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                        showDenyButton: true,
                        // showCancelButton: true,
                        allowOutsideClick: false,
                        confirmButtonText: 'Continuez',
                        denyButtonText: `ignorer`,
                        }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                clk_nb_visit=0;
                                $('.SituJudicExcist').html("");
                                $('.nombre_situJud_max').hide();
                                $('.nombre_situJud_max').val("");
                                $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                            } else if (result.isDenied) {
                                $(this).prop("checked",true);
                            }
                        });
                    }else{
                        $('.SituJudicExcist').html("");
                        $('.nombre_situJud_max').hide();
                        $('.nombre_situJud_max').val("");
                        $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                    }
                }
            });
            
            $('.ck_visit').change(function() {
                if ($(this).is(":checked")) {
                    $('.nombre_visit_max').show();
                    $('.visitexcist').html(outervisitehtml);
                    $('.nombre_visit_max').val(1);
                    $(this).parent().addClass("rounded-lg px-2 py-2 bg-gray-400");
                } else {
                    if(clk_nb_visit>0){
                        Swal.fire({
                        title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                        showDenyButton: true,
                        // showCancelButton: true,
                        allowOutsideClick: false,
                        confirmButtonText: 'Continuez',
                        denyButtonText: `ignorer`,
                        }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                clk_nb_visit=0;
                                $('.visitexcist').html("");
                                $('.nombre_visit_max').hide();
                                $('.nombre_visit_max').val("");
                                $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                            } else if (result.isDenied) {
                                $(this).prop("checked",true);
                            }
                        });
                    }else{
                        $('.visitexcist').html("");
                        $('.nombre_visit_max').hide();
                        $('.nombre_visit_max').val("");
                        $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                    }
                    
                }
            });
            $('.ck_convo').change(function(){
                if($('.ck_convo').is(":checked")){
                    $('.nombre_convo_max').show();
                    $('.convocationExists').html(outer_convo_html);
                    $('.nombre_convo_max').val(1);
                    $(this).parent().addClass("rounded-lg px-2 py-2 bg-gray-400");
                }else{
                    if(clk_nb_convo>0){
                        Swal.fire({
                        title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                        showDenyButton: true,
                        // showCancelButton: true,
                        allowOutsideClick: false,
                        confirmButtonText: 'Continuez',
                        denyButtonText: `ignorer`,
                        }).then((result)=>{
                            if (result.isConfirmed) {
                                $('.convocationExists').html("");
                                $('.nombre_convo_max').hide();
                                $('.nombre_convo_max').val("");
                                $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                            } else if (result.isDenied) {
                                $(this).prop("checked",true);
                            }
                        });
                        
                    }else{
                        $('.convocationExists').html("");
                        $('.nombre_convo_max').hide();
                        $('.nombre_convo_max').val("");
                        $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                    }
                }
            });
            $('.valid_adrs_is_sel').change(function(){
                if ($(this).is(":checked")) {
                    $('.valid_adrs_max').show();
                    $('.validAdrsExists').html(added_valid_adrshtml_outer);
                    $('.valid_adrs_max').val(1);
                    $(this).parent().addClass("rounded-lg px-2 py-2 bg-gray-400");
                } else {
                    if(clk_nb_valid_adrs>0){
                        Swal.fire({
                        title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                        showDenyButton: true,
                        // showCancelButton: true,
                        allowOutsideClick: false,
                        confirmButtonText: 'Continuez',
                        denyButtonText: `ignorer`,
                        }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                $('.valid_adrs_max').hide();
                                $('.valid_adrs_max').val("");
                                $('.validAdrsExists').html("");
                                $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                                clk_nb_valid_adrs=0;
                            } else if (result.isDenied) {
                                $(this).prop("checked",true);
                            }
                        });
                    }else{
                        $('.valid_adrs_max').hide();
                        $('.valid_adrs_max').val("");
                        $('.validAdrsExists').html("");
                        $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                    }
                   
                }
            });
            $("body").on('click','.ck_medi',function(){
                if ($(this).is(":checked")) {
                    $('.nombre_medi_max').show();
                    $('.mediaExcist').html(medihtmloute);
                    $('.nombre_medi_max').val(1);
                    $(this).parent().addClass("rounded-lg px-2 py-2 bg-gray-400");
                } else {
                    if(clk_nb_medi>0){
                        Swal.fire({
                        title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                        showDenyButton: true,
                        // showCancelButton: true,
                        allowOutsideClick: false,
                        confirmButtonText: 'Continuez',
                        denyButtonText: `ignorer`,
                        }).then((result) => {
                        /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                $('.nombre_medi_max').hide();
                                $('.nombre_medi_max').val("");
                                $('.mediaExcist').html("");
                                $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                                clk_nb_medi=0;
                            } else if (result.isDenied) {
                                $(this).prop("checked",true);
                            }
                        });
                    }else{
                        $('.nombre_medi_max').hide();
                        $('.nombre_medi_max').val("");
                        $('.mediaExcist').html("");
                        $(this).parent().removeClass("rounded-lg px-2 py-2 bg-gray-400");
                    }
                }
            });
            $('body').on('click','.mise_dem_par',function(){
                if($(this).is(":checked")){
                    $(this).parent().parent().find('.mise_dem_par').prop("checked",false)
                    $(this).prop("checked",true);
                }
            });
            $('body').on('click','.obtenu_par',function(){
                if($(this).is(":checked")){
                    $(this).parent().parent().find('.obtenu_par').prop("checked",false)
                    $(this).prop("checked",true);
                }
            });
            $("body").on('click','.type_situation_judic',function(){
                if($(this).is(":checked")){
                    $(this).parent().parent().parent().parent().parent().find('.type_situation_judic').prop("checked",false)
                    $(this).prop("checked",true);
                }
            });
        });
    </script>
    <script>
        
        $('.situJ').change(function(){
            if($(this).is(":checked")){
                $('.interventions').show( "slow" );

                if("{{empty($ScenJudicCheq)}}"){
                    if($('.cheque_ref').is(":checked")){
                        $('.judicexcist').html(judicexcist);
                        $('.judicexcist').show("slow");
                    }
                }else{
                    $('.judicexcist').show("slow");
                    $('.cheque_ref').prop('checked',true);
                }

                if("{{empty($Inj_De_Payer)}}"){
                    if($('.injoncDpay').is(":checked")){
                        $('.injoncDpayExcist').html(injoncDpayExcisthtml);
                        $('.injoncDpayExcist').show("slow");
                    }
                }else{
                    $('.injoncDpayExcist').show("slow");
                }


            }else{
                Swal.fire({
                title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                showDenyButton: true,
                // showCancelButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'Continuez',
                denyButtonText: `ignorer`,
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        $('.cheque_ref').prop('checked',false);
                        $('.injoncDpay').prop('checked',false);
                        $('.reqinjoncDpay').prop('checked',false);
                        $('.SituationDirect').prop('checked',false);

                        $('.interventions').slideUp(300);

                        $('.judicexcist').slideUp(300);
                        $('.injoncDpayExcist').slideUp(300);
                        $('.reqinjoncDpayExcist').slideUp(300);
                        $('.SituationDirectHtml').slideUp(300);
                    } else if (result.isDenied) {
                        $(this).prop("checked",true);
                    }
                });
            }
        });

        $('body').on('change','.cheque_ref',function(){
            if($(this).is(':checked')){
                $('.judicexcist').html(judicexcist);
                $('.judicexcist').show("slow");
            }else{
                Swal.fire({
                title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                showDenyButton: true,
                // showCancelButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'Continuez',
                denyButtonText: `ignorer`,
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        $('.judicexcist').slideUp(300);
                        $('.judicexcist').html("");
                    } else if (result.isDenied) {
                        $(this).prop("checked",true);
                    }
                });
            }
        });

        $('body').on('click','.add_cheq',function(){
            $('.cheqs').append(cheqs);
        });
        $('body').on('click','.add_creanc_info',function(){
            $('.info_creace').append(infocreacehtml);
        });
        $("body").on('click','.kill_cheq',function(){
            $(this).parent().parent().parent().slideUp(300);
            $(this).parent().parent().parent().html("");
        });
        $('body').on('click','.kill_creance',function(){
            $(this).parent().parent().parent().slideUp(300);
            $(this).parent().parent().parent().html("");
        });
        $('body').on('change','.prem_inst',function(){
            if($(this).is(":checked")){
                $('.conatains_judic_chek').append(prem_insthtml);
                $('.prem_insthtml').show('slow');
            }else{
                $('.prem_insthtml').slideUp(300);
                $('.prem_insthtml').html("");

                $('.phase_appelhtml').slideUp(300);
                $('.phase_appelhtml').html("");
            }
        });
        $('body').on('change','.proc_depo_inj_pay',function(){
            if($(this).is(":checked")){
                $('.injoncDpayHtml').append(proc_dephtml);
                $('.proc_dephtml').show('slow');
            }else{
                $('.proc_dephtml').slideUp(300);
                $('.proc_dephtml').html("");

                $('.proc_notifhtml').slideUp(300);
                $('.proc_notifhtml').html("");

                $('.proc_exehtml').slideUp(300);
                $('.proc_exehtml').html("");
                
                $('.proc_appel_inj_pay').slideUp(300);
                $('.proc_appel_inj_pay').html("");

                $('.proc_appelhtml').slideUp(300);
                $('.proc_appelhtml').html("");
            }
        });
        $('body').on('change','.proc_notif_inj_pay',function(){
            if($(this).is(":checked")){
                $('.injoncDpayHtml').append(proc_notifhtml);
                $('.proc_notifhtml').show('slow');
            }else{
                $('.proc_notifhtml').slideUp(300);
                $('.proc_notifhtml').html("");
                
                $('.proc_exehtml').slideUp(300);
                $('.proc_exehtml').html("");
                
                $('.proc_appel_inj_pay').slideUp(300);
                $('.proc_appel_inj_pay').html("");

                $('.proc_appelhtml').slideUp(300);
                $('.proc_appelhtml').html("");
            }
        });
        $('body').on('change','.proc_exec_inj_pay',function(){
            if($(this).is(":checked")){
                
                $('.injoncDpayHtml').append(proc_exehtml);
                $('.proc_exehtml').show('slow');
                
            }else{
                $('.proc_exehtml').slideUp(300);
                $('.proc_exehtml').html("");

            }
        });
        $('body').on('change','.proc_appel_inj_pay',function(){
            if($(this).is(":checked")){
                $('.injoncDpayHtml').append(proc_appelhtml);
                $('.proc_appelhtml').show('slow');
            }else{
                $('.proc_appelhtml').slideUp(300);
                $('.proc_appelhtml').html("");

                $('.proc_exehtml').slideUp(300);
                $('.proc_exehtml').html("");
            }
        });
        $('body').on('change','.phase_appel',function(){
            if($(this).is(":checked")){
                $('.conatains_judic_chek').append(phase_appelhtml);
                $('.phase_appelhtml').show('slow');
            }else{
                $('.phase_appelhtml').slideUp(300);
                $('.phase_appelhtml').html('');

                $('.proc_appelhtml').slideUp(300);
                $('.proc_appelhtml').html("");
            }
        });
        $('body').on('click','.type_debit',function(){
            if($(this).is(":checked")){
                $(this).parent().parent().find('.type_debit').prop('checked',false);
                $(this).prop("checked",true);
            }
        });
        $("body").on('change','.injoncDpay',function(){
            if($(this).is(":checked")){
                $('.injoncDpayExcist').html(injoncDpayExcisthtml);
                $('.injoncDpayExcist').show();
                $('.injoncDpayHtml').show('slow');
            }else{
                Swal.fire({
                title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                showDenyButton: true,
                // showCancelButton: true,
                allowOutsideClick: false,
                confirmButtonText: 'Continuez',
                denyButtonText: `ignorer`,
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        $('.injoncDpayHtml').hide('slow', function(){ $('.injoncDpayHtml').remove(); });
                        // setTimeout(() => {
                        //     $('.injoncDpayExcist').html('');
                        // }, 500);
                    } else if (result.isDenied) {
                        $(this).prop("checked",true);
                    }
                });
            }
        });
    </script>
    <script>
        $(document).ready(function(){
            $("body").on('change','.reqinjoncDpay',function(){
                    if($(this).is(":checked")){
                    $('.reqinjoncDpayExcist').html(reqinjoncDpayHtml);
                    $('.reqinjoncDpayExcist').show('slow');
                }else{
                    Swal.fire({
                    title: 'une fois que vous confirmez cette action, toutes les données seront effacées',
                    showDenyButton: true,
                    // showCancelButton: true,
                    allowOutsideClick: false,
                    confirmButtonText: 'Continuez',
                    denyButtonText: `ignorer`,
                    }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                        if (result.isConfirmed) {
                            $('.reqinjoncDpayExcist').slideUp(300);
                            setTimeout(() => {
                                $('.reqinjoncDpayExcist').html('');
                            }, 500);
                        } else if (result.isDenied) {
                            $(this).prop("checked",true);
                        }
                    });
                }
            });

            $('body').on('change','.les_proc_req_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.prcreq_contain').append(reqLes_procshtml);
                    $('.reqLes_procshtml').show('slow');
                }else{
                    $('.reqLes_procshtml').slideUp(300);
                    $('.reqLes_procshtml').html('');

                    $('.reqLes_procs_Apphtml').slideUp(300);
                    $('.reqLes_procs_Apphtml').html('');

                    $('.req_procs_Notifhtml').slideUp(300);
                    $('.req_procs_Notifhtml').html('');

                    $('.req_procs_exehtml').slideUp(300);
                    $('.req_procs_exehtml').html('');
                }
            });

            $('body').on('change','.les_proc_app_req_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.prcreq_contain').append(reqLes_procs_Apphtml);
                    $('.reqLes_procs_Apphtml').show('slow');
                }else{
                    $('.reqLes_procs_Apphtml').slideUp(300);
                    $('.reqLes_procs_Apphtml').html('');

                    $('.req_procs_Notifhtml').slideUp(300);
                    $('.req_procs_Notifhtml').html('');

                    $('.req_procs_exehtml').slideUp(300);
                    $('.req_procs_exehtml').html('');
                }
            });

            $('body').on('change','.swt_proc_notif_req_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.prcreq_contain').append(req_procs_Notifhtml);
                    $('.req_procs_Notifhtml').show('slow');
                }else{
                    $('.req_procs_Notifhtml').slideUp(300);
                    $('.req_procs_Notifhtml').html('');

                    $('.req_procs_exehtml').slideUp(300);
                    $('.req_procs_exehtml').html('');
                }
            });

            $('body').on('change','.swt_proc_exe_req_inj_pay',function(){
                if($(this).is(":checked")){
                    $('.prcreq_contain').append(req_procs_exehtml);
                    $('.req_procs_exehtml').show('slow');
                }else{
                    $('.req_procs_exehtml').slideUp(300);
                    $('.req_procs_exehtml').html('');
                }
            });
            $('body').on('change','.SituationDirect',function(){
                if($(this).is(":checked")){
                    $('.SituationDirectExcist').html(SituationDirectHtml);
                }else{
                    $('.SituationDirectHtml').slideUp(300);
                    setTimeout(() => {
                        $('.SituationDirectHtml').remove();
                    }, 500);
                }
            });
        });
        
    </script>
@endsection