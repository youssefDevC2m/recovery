<div class="shadow-lg my-3 p-5 box">
    <input type="hidden" name="indeXaudi[]" value="1">
    <div class="grid grid-cols-2">
        <div>
            <h1 class="text-lg">Les Audiences</h1>
        </div>
        <div class="grid justify-items-end">
            <div class="kill_audi">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle mx-auto hover:text-theme-6"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg>
            </div>
        </div>
    </div>
    <div class="grid lg:grid-cols-3 md:grid-cols-1 sm:grid-clos-1 gap-2">
        <div class="mt-2">
            <label class="ml-2" for="">Date</label>
            <input type="date" class="input w-full mt-2 border border-gray-500"
                name="cit_date_audc[]" value="{{ date('Y-m-d') }}">
        </div>
        <div class="mt-2">
            <label class="ml-2" for="">L’heure</label>
            <input type="time" class="input w-full mt-2 border border-gray-500"
                name="cit_heur_audc[]" value="{{ date('H:i') }}">
        </div>
        <div class="mt-2">
            <label class="ml-2" for="">Lieu</label>
            <input type="text" class="input w-full mt-2 border border-gray-500"
                name="cit_lieu_audc[]" placeholder="lieu">
        </div>
    </div>

    <div class="w-full">
        <div class="mt-2">
            <label class="ml-2" for="">Résumé</label>
            <textarea name="cit_resume[]" class="input w-full mt-2 border border-gray-500" placeholder="Résumé ..." style="
            height: 100px;
        "></textarea>
        </div>
    </div>
</div>