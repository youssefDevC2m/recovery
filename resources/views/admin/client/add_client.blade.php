@extends('admin.admin')
@section('title')
    Ajouter des clients
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i
            data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ajouter des
            clients</a> </div>
    <style>
        img {
            object-fit: cover;
            object-position: center center;
        }
    </style>
@endsection
@section('content')
    <form method="post" action="" id="form_data" enctype="multipart/form-data">
        <input type="hidden" name="user" class="user" value="{{ isset($user_temp->id) ? $user_temp->id : 0 }}">

        <div class="intro-y box px-5 py-8 mt-2">
            <div class="mt-3 w-full flex items-center justify-center">
                <div class="">
                    <div class="flex flex-row mt-2 ">
                        <span for="" class="mr-2 font-semibold">Particulier</span>
                        <input type="checkbox" class="input input--switch border swich">
                        <span for="" class="ml-2 font-semibold">Entreprise</span>
                    </div>
                    <input type="hidden" name="type_cli" class="type_cli" value="perso">
                </div>
            </div>
        </div>

        <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2">
            <div class="intro-y box px-5 py-8 mt-5 shadow-lg">
                <label for="" class="text-lg font-semibold my-3">Code Client</label>
                <input type="text" name="code_cli" class="input w-full border border-gray-500"
                    placeholder="Entrer un code client unique">
            </div>
        </div>

        <div class="intro-x box px-5 py-8 mt-5 ">
            <div><label for="" class="text-2xl font-semibold text-gray-600">Client infos</label></div>
            <div class="contant"></div>
        </div>
        <div class="box mt-3">
            <div class="p-8">
                <label for="" class="font-semibold">Pièces jointes</label>
                <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                    <div class="flex flex-wrap px-4 file_block">

                    </div>
                    <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                        <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2">
                                <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                <polyline points="14 2 14 8 20 8"></polyline>
                                <line x1="16" y1="13" x2="8" y2="13"></line>
                                <line x1="16" y1="17" x2="8" y2="17"></line>
                                <polyline points="10 9 9 9 8 9"></polyline>
                            </svg>
                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span>
                        </button>
                        <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0"
                            name="file_val[]" data-stp="not">
                    </div>
                </div>
            </div>
        </div>
        <div class="-intro-y  contact-info">
            <center>
                <div class="box px-5 py-4 mt-5">
                    <label for="" class="text-2xl font-semibold text-gray-600 ">Contacts de client</label>
                </div>
            </center>

            <div class="grid lg:grid-cols-4  sm:grid-cols-1 gap-2 font-semibold">
                <div class="box px-5 py-8 mt-5 ">
                    <div class="flex items-center ">
                        <label for="">Téléphone </label><i data-feather="plus-circle"
                            class="ml-3 hover:text-theme-9 add_tele"></i>
                    </div>
                    <div class="h-32 overflow-y-scroll">
                        <div class="grid grid-cols-1 tele_block mr-2">
                            <input type="text" placeholder="Entrer le téléphone" name="tele[]"
                                class=" input border w-full mt-2"
                                value="{{ isset($user_temp->tele) ? $user_temp->tele : '' }}">
                        </div>
                    </div>
                </div>
                <div class="box px-5 py-8 mt-5">
                    <div class="flex items-center ">
                        <label for="">E-mail</label><i data-feather="plus-circle"
                            class="ml-3 hover:text-theme-9 add_mail"></i>
                    </div>
                    <div class="h-32 overflow-y-scroll">
                        <div class="grid grid-cols-1 mail_block mr-2">
                            <input type="text" placeholder="mail@mail.com" name="mail[]"
                                class="input border w-full mt-2"
                                value="{{ isset($user_temp->email) ? $user_temp->email : '' }}">
                        </div>
                    </div>
                </div>
                {{-- <div class="box px-5 py-8 mt-5 hidden">
                <div class="flex items-center ">
                    <label for="">Fax</label><i data-feather="plus-circle" class="ml-3 hover:text-theme-9 add_fax"></i> 
                </div>
                <div class="h-32 overflow-y-scroll">
                    <div class="grid grid-cols-1 fax_block mr-2 ">
                        <input type="text" placeholder="Entrer le fax" name="fax[]" class="fax input border w-full mt-2">
                    </div>
                </div>
            </div> --}}
                <div class="box px-5 py-8 mt-5">
                    <div class="flex items-center ">
                        <label for="">Téléphone portable</label><i data-feather="plus-circle"
                            class="ml-3 hover:text-theme-9 add_Cellphone"></i>
                    </div>
                    <div class="h-32 overflow-y-scroll">
                        <div class="grid grid-cols-1 fax_block mr-2 ">
                            <input type="text" placeholder="Téléphone portable" name="cellPhone[]"
                                class="cellPhone input border w-full mt-2">
                        </div>
                    </div>
                </div>
                <div class="box px-5 py-8 mt-5 ">
                    <div class="flex items-center">
                        <label for="">Autre contact</label><i data-feather="plus-circle"
                            class="ml-3 hover:text-theme-9 add_cont"></i>
                    </div>
                    <div class="h-32 overflow-y-scroll">
                        <div class="grid grid-cols-1 add_o_contact mr-2">
                            <input type="text" placeholder="Entrer le un autre contact" name="contact[]"
                                class="contact input border w-full mt-2">
                        </div>
                    </div>
                </div>
            </div>

            <center>
                <div class="box px-5 py-4 mt-5">
                    <label for="" class="text-2xl font-semibold text-gray-600 cntr_com">Mandataires
                    </label>
                </div>
            </center>
            <div class="intro-y grid grid-cols-12 gap-5 mt-5">
                <div class="col-span-12 lg:col-span-3 box px-5 pt-8 mt-5">
                    <label for="">Mandataire</label>
                    <input type="text" placeholder="Entrer le nom de mandataire" name="mndatr_partic"
                        class="mndatr_partic input border w-full mt-2" value="">
                    <div class="mt-3 grid justify-items-center">
                        <button type="button"
                            class="Add_mandataire button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-33 text-white">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-save w-4 h-4 mr-2">
                                <path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path>
                                <polyline points="17 21 17 13 7 13 7 21"></polyline>
                                <polyline points="7 3 7 8 15 8"></polyline>
                            </svg>
                            Ajouter
                        </button>
                    </div>
                </div>
                <div class="intro-y col-span-12 lg:col-span-4">
                    <div class="tab-content">
                        <div class="tab-content__pane active" id="ticket">
                            <div class="pos__ticket box p-2 mt-5 h-64 overflow-y-scroll admin-client-mondate">
                                {{-- @include('admin.client.mondate') --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modate_cord">

            </div>
            {{-- <div class="grid lg:grid-cols-4  sm:grid-cols-1 gap-2 font-semibold">
                <div class="box px-5 py-8 mt-5 ">
                    <div class="flex items-center ">
                        <label for="">Téléphone </label><i data-feather="plus-circle"
                            class="ml-3 hover:text-theme-9 add_tele_mand"></i>
                    </div>
                    <div class="h-32 overflow-y-scroll">
                        <div class="grid grid-cols-1 tele_block_mand mr-2">
                            <input type="text" placeholder="Entrer le téléphone" name="tele_mand[]"
                                class=" input border w-full mt-2"
                                value="{{ isset($user_temp->tele) ? $user_temp->tele : '' }}">
                        </div>
                    </div>
                </div>
                <div class="box px-5 py-8 mt-5">
                    <div class="flex items-center ">
                        <label for="">E-mail</label><i data-feather="plus-circle"
                            class="ml-3 hover:text-theme-9 add_mail_mand"></i>
                    </div>
                    <div class="h-32 overflow-y-scroll">
                        <div class="grid grid-cols-1 mail_block_mand mr-2">
                            <input type="text" placeholder="mail@mail.com" name="mail_mand[]"
                                class="input border w-full mt-2"
                                value="{{ isset($user_temp->email) ? $user_temp->email : '' }}">
                        </div>
                    </div>
                </div>
                <div class="box px-5 py-8 mt-5 ">
                <div class="flex items-center ">
                    <label for="">Fax</label><i data-feather="plus-circle" class="ml-3 hover:text-theme-9 add_fax_mand"></i> 
                </div>
                <div class="h-32 overflow-y-scroll">
                    <div class="grid grid-cols-1 fax_block_mand mr-2 ">
                        <input type="text" placeholder="Entrer le fax" name="fax_mand[]" class="fax input border w-full mt-2">
                    </div>
                </div>
                </div>

                <div class="box px-5 py-8 mt-5 ">
                    <div class="flex items-center ">
                        <label for="">Téléphone portable</label><i data-feather="plus-circle"
                            class="ml-3 hover:text-theme-9 add_cell_phone_mand"></i>
                    </div>
                    <div class="h-32 overflow-y-scroll">
                        <div class="grid grid-cols-1 cell_phon_block_mand mr-2 ">
                            <input type="text" placeholder="Téléphone portable" name="cell_phone_mand[]"
                                class="cell_phone input border w-full mt-2">
                        </div>
                    </div>
                </div>

                <div class="box px-5 py-8 mt-5 ">
                    <div class="flex items-center">
                        <label for="">Autre contact</label><i data-feather="plus-circle"
                            class="ml-3 hover:text-theme-9 add_cont_mand"></i>
                    </div>
                    <div class="h-32 overflow-y-scroll">
                        <div class="grid grid-cols-1 add_o_contact_mand mr-2">
                            <input type="text" placeholder="Entrer le un autre contact" name="contact_mand[]"
                                class="contact input border w-full mt-2">
                        </div>
                    </div>
                </div>
            </div> --}}



        </div>
        <div class="-intro-y  px-5 py-4 mt-5">
            <center>
                <button type="button"
                    class="save button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round"
                        stroke-linejoin="round" class="feather feather-save w-4 h-4 mr-2">
                        <path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path>
                        <polyline points="17 21 17 13 7 13 7 21"></polyline>
                        <polyline points="7 3 7 8 15 8"></polyline>
                    </svg>
                    Enregistrer
                </button>
            </center>
        </div>
    </form>

    <div class=" client-perso hidden">
        <div class="-intro-x">
            <div class="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 gap-2 mt-2">
                <div>
                    <label for="" class="font-semibold">Nom client</label>
                    <input type="text" placeholder="Entrer le nom" name="nom" class="input border w-full mt-2"
                        value="{{ isset($user_temp->nom) ? $user_temp->nom : '' }}">
                </div>
                <div dir="rtl" style="display: none">
                    <label for="" class="font-semibold">نسب العميل</label>
                    <input type="text" placeholder=" أدخل الاسم العائلي " name="nom_ar" class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Prenom client</label>
                    <input type="text" placeholder="Entrer le nom" name="prenom" class="input border w-full mt-2"
                        value="{{ isset($user_temp->prenom) ? $user_temp->prenom : '' }}">
                </div>
                <div dir="rtl" style="display: none">
                    <label for="" class="font-semibold">اسم العميل</label>
                    <input type="text" placeholder="أدخل الاسم الشخصي " name="prenom_ar"
                        class="input border w-full mt-2">
                </div>
                <div class="hidden">
                    <label for="" class="font-semibold">Mandataire</label>
                    <input type="text" placeholder="Entrer le nom de mandataire" name="mndatr_partic"
                        class="input border w-full mt-2" value="">
                </div>
                <div>
                    <label for="" class="font-semibold">Enseigne Commerciale</label>
                    <input type="text" placeholder="Enseigne Commerciale" name="brand"
                        class="input border w-full mt-2" value="">
                </div>
            </div>
            <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2 mt-2">
                <div>
                    <label for="" class="font-semibold">Adresse 1</label>
                    <input type="text" placeholder="Entrer le adresse 1" name="adress1"
                        class="input border w-full mt-2"
                        value="{{ isset($user_temp->address) ? $user_temp->address : '' }}">
                </div>
                <div>
                    <label for="" class="font-semibold">Adresse 2</label>
                    <input type="text" placeholder="Entrer le adresse 2" name="adress2"
                        class="input border w-full mt-2">
                </div>
            </div>
            <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2 mt-2">
                <div>
                    <label for="" class="font-semibold">Adresse de travaille 1</label>
                    <input type="text" placeholder="Entrer le adresse 1" name="adressTrav1"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Adresse de travaille 2</label>
                    <input type="text" placeholder="Entrer le adresse 2" name="adressTrav2"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Secteur D'activité</label>
                    <input type="text" placeholder="Secteur D'activité" name="sec_a_act"
                        class="input border w-full mt-2">
                </div>
            </div>

            <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2 mt-2">
                <div>
                    <label for="" class="font-semibold">Ville</label>
                    <input type="text" placeholder="Entrer la nationalité" name="ville_perso"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Nationalité</label>
                    <input type="text" placeholder="Entrer la nationalité" name="nation"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Le genre</label>
                    <div class="mt-2">
                        <select class="input border w-full " name="gender">
                            <option value="Homme">Homme</option>
                            <option value="Femme">Femme</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2 mt-2">
                <div>
                    <label for="" class="font-semibold">Cin</label>
                    <input type="text" placeholder="Entrer le cin" name="cin" class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Passeport</label>
                    <input type="text" placeholder="Entrer le numéro passeport" name="pass_port"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Carte séjour</label>
                    <input type="text" placeholder="Entrer le numéro de carte séjour" name="carte_sejour"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Date De Partenariat</label>
                    <input type="date" value="{{ date('Y-m-d') }}" placeholder="Entrer la date de partenariat"
                        name="date_parten" class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Registre de commerce</label>
                    <input type="text" placeholder="Entrer le rc" name="rc_client" class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">ICE</label>
                    <input type="text" placeholder="Entrer l'ice'" name="ice" class="input border w-full mt-2">
                </div>
            </div>
        </div>
    </div>
    <div class=" client-company hidden">
        <div class="-intro-x">
            <div class="grid lg:grid-cols-2  sm:grid-cols-1 gap-2 ">
                <div>
                    <label for="" class="font-semibold">Raison sociale</label>
                    <input type="text" placeholder="Entrer la raison sociale" name="raison_sos"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Représentant Légal</label>
                    <input type="text" placeholder="Représentant Légal" name="rep_legal_camp"
                        class="input border w-full mt-2">
                </div>
                {{-- <div>
                    <label for="" class="font-semibold">Capitale</label>
                    <input type="number" min=0 step="0.1" placeholder="Entrer le capitale" name="capital"
                        class="input border w-full mt-2">
                </div> --}}
            </div>
            <div class="grid lg:grid-cols-2  sm:grid-cols-1 gap-2 ">
                <div class="hidden">
                    <label for="" class="font-semibold">Interlocuteur privilégier</label>
                    <input type="text" placeholder="Interlocuteur privilégier" name="iterloc_prev"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Capitale</label>
                    <input type="number" min=0 step="0.1" placeholder="Entrer le capitale" name="capital"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Qualité</label>
                    <input type="text" placeholder="Qualité" name="qualit" class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Adresse 1</label>
                    <input type="text" placeholder="Entrer le gérant 1" name="adres_sos1"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Adresse 2</label>
                    <input type="text" placeholder="Entrer le gérant 1" name="adres_sos2"
                        class="input border w-full mt-2">
                </div>

                <div>
                    <label for="" class="font-semibold">Gérant 1</label>
                    <input type="text" placeholder="Entrer le gérant 1" name="gerant1"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Gérant 2</label>
                    <input type="text" placeholder="Entrer le gérant 2" name="gerant2"
                        class="input border w-full mt-2">
                </div>


            </div>
            <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2">
                <div>
                    <label for="" class="font-semibold">Registre de commerce</label>
                    <input type="text" placeholder="Entrer le rc" name="rc" class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">ICE</label>
                    <input type="text" placeholder="Entrer le ice" name="ice_camp" class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Secteur D'activité</label>
                    <input type="text" placeholder="Entrer l'activité" name="activite"
                        class="input border w-full mt-2">
                </div>
                <div>
                    <label for="" class="font-semibold">Statut juridique</label>
                    <input type="text" placeholder="Entrer le statut juridique" name="statue_jurdq"
                        class="input border w-full mt-2">
                </div>

                <div>
                    <label for="" class="font-semibold">Encienneté</label>
                    <input type="date" placeholder="Encienneté" name="encenre_camp" class="input border w-full mt-2"
                        value="{{ date('Y-m-d') }}">
                </div>

                <div>
                    <label for="" class="font-semibold">Date de Partenariat</label>
                    <input type="date" placeholder="Date de Partenariat" name="date_paten"
                        class="input border w-full mt-2" value="{{ date('Y-m-d') }}">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $("body").ready(function() {
            $('.client').addClass('side-menu--active');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.save').click(function() {
                var formData = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url: "{{ route('createClient') }}",
                    type: 'post',
                    dataType: 'json',
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function(data) {
                        // console.log(data);
                        window.location.replace("{{ route('listClient') }}");

                    },
                    error: function(error) {
                        console.log(error);
                    },
                });
            });
            $('.contant').html($('.client-perso').html());
            $('.swich').change(function() {
                if ($(this).is(':checked')) {
                    $('.contant').html("");
                    $('.type_cli').val('entre');
                    $('.cntr_com').text("Interlocuteur privilégier");
                    $('.contant').html($('.client-company').html());
                } else {
                    $('.contant').html("");
                    $('.type_cli').val('perso');
                    $('.cntr_com').text("Mandataire");
                    $('.contant').html($('.client-perso').html());
                }
            });

            $('body').on('click', '.add_tele_mand', function() {
                $this = $(this).data('nbr');
                
                $('.tele_block_mand').append(`
                <div class='flex items-center'>
                <input type="text" placeholder="Entrer le téléphone" name="tele_mand${$this}[]" class="tele input border w-full mt-2 ">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });
            $("body").on('click', '.add_mail_mand', function() {
                $this = $(this).data('nbr');
                $('.mail_block_mand').append(`
                <div class='flex items-center'>
                    <input type="text" placeholder="mail@mail.com" name="mail_mand${$this}[]" class="input border w-full mt-2" spellcheck="false" data-ms-editor="true">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });
            
            $("body").on('click', '.add_cell_phone_mand', function() {
                $this = $(this).data('nbr');
                
                $('.cell_phon_block_mand').append(`
                <div class='flex items-center'>
                    <input type="text" placeholder="Téléphone portable" name="cell_phone_mand${$this}[]" class="cell_phone input border w-full mt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });
            $("body").on('click', '.add_cont_mand', function() {
                $this = $(this).data('nbr');
                
                $('.add_o_contact_mand').append(`
                <div class='flex items-center'>
                    <input type="text" placeholder="Entrer le un autre contact" name="contact_mand${$this}[]" class="contact input border w-full mt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);

            });

            $('body').on('click', '.add_tele', function() {
                $('.tele_block').append(`
                <div class='flex items-center'>
                <input type="text" placeholder="Entrer le téléphone" name="tele[]" class="tele input border w-full mt-2 ">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });

            $("body").on('click', '.add_mail', function() {
                $('.mail_block').append(`
                <div class='flex items-center'>
                    <input type="text" placeholder="mail@mail.com" name="mail[]" class="input border w-full mt-2" spellcheck="false" data-ms-editor="true">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });

            
            $("body").on('click', '.add_Cellphone', function() {
                $('.fax_block').append(`
                <div class='flex items-center'>
                    <input type="text" placeholder="Téléphone portable" name="cellPhone[]" class="cellPhone input border w-full mt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });
            $("body").on('click', '.add_cont', function() {
                $('.add_o_contact').append(`
                <div class='flex items-center'>
                    <input type="text" placeholder="Entrer le un autre contact" name="contact[]" class="contact input border w-full mt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });

            $("body").on('click tap', '.rm-tele', function() {
                $(this).parent().toggle();
                $(this).parent().html("");

            });
            var kill_nb = 0;

            $('body').on('change', '.this_file', function(input) {
                var file = $(this).get(0).files[0];
                // alert(file);
                if (file) {
                    var reader = new FileReader();
                    $(this).parent().parent().find('.file_block').append(
                        `
                        <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">
                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                <span class="w-3/5 file__icon file__icon--file mx-auto">
                                    <div class="file__icon__file-name"></div>
                                </span>
                                <span class="block font-medium mt-4 text-center truncate">` + file.name +
                        `</span>
                            </div>
                            <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file` +
                        kill_nb + `"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                            </div>
                        </div>
                        `
                    );

                    $(this).parent().find('button').hide();
                    $(this).parent().append(
                        `
                        <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                        </button>
                        <input type="file" name="file_val[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file` +
                        kill_nb + `">
                    `);
                    kill_nb++;
                }
            });

            $("body").on('click tap', '.kill_file,.kill_file_base', function() {
                $('.' + $(this).data('id')).attr('name', '');
                $(this).parent().toggle();

            });
            var id_cord = 0;
            $("body").on('click', '.Add_mandataire', function() {
                if ($('.mndatr_partic').val() != '') {
                    
                    $.ajax({
                        url: `{{ route('render_mondataireV') }}`,
                        dataType: 'json',
                        type: 'post',
                        data: {
                            mndatr_partic: $('.mndatr_partic').val(),
                            id_cord: id_cord
                        },
                        success: function(data) {
                            
                            $('.mndatr_partic').val("");
                            $('.modate_cord').append(data.view2);
                            $('.admin-client-mondate').append(data.view);
                            id_cord++;
                        },
                        error: function(error) {
                            console.log(error);
                        },
                    });
                }
            });

            $('body').on('click','.show_cords',function(){
                $this = $(this).data('cordon');
                $('.cordon_form').hide();
                $(`#${$this}`).show();
            });

            $('body').on('click', '.kill-mod', function() {
                $this = $(this).data('cordon');
                $(`#${$this}`).remove();
                console.log($(this).parent().parent().remove());
                
            });

        });
    </script>
@endsection
