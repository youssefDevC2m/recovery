<div id="cordon{{$id_cord}}" class="grid lg:grid-cols-4 sm:grid-cols-1 gap-2 font-semibold -intro-x cordon_form " style="display: none">
    <input type="hidden" name="nbr_cord[]" value="{{$id_cord}}">
    <div class="box px-5 py-8 mt-5 ">
        <div class="flex items-center ">
            <label for="">Téléphone </label>
            <svg data-nbr='{{$id_cord}}' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle ml-3 hover:text-theme-9 add_tele_mand"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
        </div>
        <div class="h-32 overflow-y-scroll">
            <div class="grid grid-cols-1 tele_block_mand mr-2">
                <input type="text" placeholder="Entrer le téléphone" name="tele_mand{{$id_cord}}[]" class=" input border w-full mt-2" value="">
            </div>
        </div>
    </div>
    <div class="box px-5 py-8 mt-5">
        <div class="flex items-center ">
            <label for="">E-mail</label>
            <svg data-nbr='{{$id_cord}}' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle ml-3 hover:text-theme-9 add_mail_mand"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
             
        </div>
        <div class="h-32 overflow-y-scroll">
            <div class="grid grid-cols-1 mail_block_mand mr-2">
                <input type="text" placeholder="mail@mail.com" name="mail_mand{{$id_cord}}[]" class="input border w-full mt-2" value="">
            </div>
        </div>
    </div>

    <div class="box px-5 py-8 mt-5 ">
        <div class="flex items-center ">
            <label for="">Téléphone portable</label>
            <svg data-nbr='{{$id_cord}}' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle ml-3 hover:text-theme-9 add_cell_phone_mand"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
        </div>
        <div class="h-32 overflow-y-scroll">
            <div class="grid grid-cols-1 cell_phon_block_mand mr-2 ">
                <input type="text" placeholder="Téléphone portable" name="cell_phone_mand{{$id_cord}}[]" class="cell_phone input border w-full mt-2">
            </div>
        </div>
    </div>

    <div class="box px-5 py-8 mt-5 ">
        <div class="flex items-center">
            <label for="">Autre contact</label>
            <svg data-nbr='{{$id_cord}}' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle ml-3 hover:text-theme-9 add_cont_mand"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
        </div>
        <div class="h-32 overflow-y-scroll">
            <div class="grid grid-cols-1 add_o_contact_mand mr-2">
                <input type="text" placeholder="Entrer le un autre contact" name="contact_mand{{$id_cord}}[]" class="contact input border w-full mt-2">
            </div>
        </div>
    </div>
    
</div>