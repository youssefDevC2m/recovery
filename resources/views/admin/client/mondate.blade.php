<a href="javascript:;"
    
    class="-intro-y flex items-center p-3 cursor-pointer transition duration-300 ease-in-out bg-white dark:bg-dark-3 hover:bg-gray-200 dark:hover:bg-dark-1 rounded-md">
    <div class="pos__ticket__item-name truncate mr-1">{{ $monName }}</div>
    <input type="hidden" name="name_mondate[]" value="{{ $monName }}">
    {{-- <div class="text-gray-600">x 1</div> --}}
    <div class="ml-auto flex flex-row">
        <svg data-cordon='cordon{{$id_cord}}' xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
            stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
            class="feather feather-edit w-4 h-4 text-gray-600 ml-2 hover:text-theme-1 show_cords">
            <path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path>
            <path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path>
            
        </svg>
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
            stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"
            data-cordon='cordon{{$id_cord}}'
            class="feather feather-trash-2 w-4 h-4 text-gray-600 ml-2 hover:text-theme-6 kill-mod ">
            <polyline points="3 6 5 6 21 6"></polyline>
            <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
            <line x1="10" y1="11" x2="10" y2="17"></line>
            <line x1="14" y1="11" x2="14" y2="17"></line>
        </svg>
    </div>
</a>
