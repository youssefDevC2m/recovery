<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        @page {
            margin: 10px;
        }
    </style>
</head>

<body style="width: 100%; height: 100%; border:3px dashed rgb(0, 14, 116); border-radius: 6px;">
    <center>
        <div>
            <img src="data:image/png;base64,{{ $data['logo'] }}" style="width: 20%; margin-top: 10px;">
        </div>
        <h1>Centre Juridique Droit et Devoir</h1>
        <h3>
            <span style="color: rgba(43, 43, 43, 0.837)">
                {{ $data['client']->type_cli == 'perso' ? 'Renseignement Client Commercant ' : 'Renseignement Client Entreprise' }}
            </span>
        </h3>
    </center>

    @if ($data['client']->type_cli == 'perso')
        {{-- cli --}}
        <div style="margin: 15px; width: 100%; margin-top: 100px;">
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td align="center">
                            <span>Nom/Prénom :</span>
                        </td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->prenom . ' ' . $data['client']->nom }}</span>
                        </td>
                        <td align="center">
                            <span>Enseigne Commerciale :</span>
                        </td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->brand }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <span>CIN</span>
                        </td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->cin }}</span>
                        </td>
                        <td align="center">
                            <span>RC :</span>
                        </td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->rc_client }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <span>ICE</span>
                        </td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->ice }}</span>
                        </td>
                        <td align="center">
                            <span>Date de création :</span>
                        </td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ date('Y-m-d', strtotime($data['client']->created_at)) }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <span>Secteur D'activité :</span>
                        </td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->sec_a_act }}</span>
                        </td>
                        <td align="center">
                            <span>Adresse Principal :</span>
                        </td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->adress1 }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <span>Date De Partenariat :</span>
                        </td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->date_parten }}</span>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>

                </tbody>
            </table>

        </div>
        {{-- end cli --}}
    @else
        {{-- company --}}
        <center>
            <h4> Raison sociale
                <span style="color: rgba(43, 43, 43, 0.837)">
                    {{$data['client']->raison_sos }}
                </span>    
            </h4>
        </center>
        <div style="margin: 15px; width: 100%; margin-top: 100px;">
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td align="center">Représentant Légal</td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->rep_legal_camp }}</span>
                        </td>
                        <td align="center">RC</td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->rc }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">ICE</td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->ice_camp }}</span>
                        </td>
                        <td align="center">Qualité</td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->qualit }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">Interlocuteur privilégier</td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->iterloc_prev }}</span>
                        </td>
                        <td align="center">Secteur d'activité</td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->activite }}</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">Encienneté</td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->encenre_camp }}</span>
                        </td>

                        <td align="center">Date de Partenariat</td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            <span>{{ $data['client']->date_paten }}</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        {{-- end company --}}
    @endif
    {{-- contact --}}
    <center>
        <div style="margin-top: 30px">
            <h1>Contact</h1>
        </div>

        <div>
            <table style="width: 100%;">
                <thead>
                    <tr>
                        <th></th>
                        <th align="center"><span>Téléphone</span></th>
                        <th align="center"><span>E-mail</span></th>
                        <th></th>
                    </tr>

                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            @foreach ($data['contact'] as $tele)
                                @if ($tele->tele == null)
                                    <?php continue; ?>
                                @else
                                    <span>{{ $tele->tele }}</span>
                                    <br>
                                @endif
                            @endforeach
                        </td>
                        <td align="center" style="border: 1px black solid;width: 25%; padding: 10px;">
                            @foreach ($data['contact'] as $mail)
                                @if ($mail->email == null)
                                    <?php continue; ?>
                                @else
                                    <span>{{ $mail->email }}</span>
                                    <br>
                                @endif
                            @endforeach
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>

    </center>
    {{-- end contact --}}
    {{-- dossiers --}}
    @if (Auth::user()->see_all_doss == 1)  
        <center>
            <div style="margin-top: 5px">
                <h1>Dossiers</h1>
            </div>    
        </center>
        
        <table style="width: 100%; border: 2px black solid; border-collapse: collapse; margin: 10px;">
            <thead>
                <tr>
                    <th style="border: 2px black solid;">Référence </th>
                    <th style="border: 2px black solid;">Client</th>
                    <th style="border: 2px black solid;">Débiteur</th>
                    <th style="border: 2px black solid;">Date D'overture</th>
                    <th style="border: 2px black solid;">Phase</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data['dossiers'] as $item)
                    <tr>
                        <td align="center" style="border: 2px black solid;">{{$item->ref}}</td>
                        <td align="center" style="border: 2px black solid;">{{(($item->type_cli=='perso')? $item->nomcli.' '.$item->prenomcli : $item->raison_sos)}}</td>
                        <td align="center" style="border: 2px black solid;">{{(($item->type_dtbr=='perso')? $item->nomdbtr.' '.$item->prenomdbtr : $item->nomdbtr)}}</td>
                        <td align="center" style="border: 2px black solid;">{{$item->date_ouverture}}</td>
                        <td align="center" style="border: 2px black solid;">
                            @if ($item->is_jurid == 0)
                                <div class="bg-theme-9 text-white rounded-lg">
                                    Amiable
                                </div>    
                            @else
                                <div class="bg-theme-12 text-white rounded-lg">
                                    Judiciaire
                                </div>
                            @endif    
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
    {{-- end dossiers --}}
</body>

</html>
