@extends('admin.admin')
@section('title')
Liste des clients
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Liste des clients</a> </div>
@endsection
@section('content')
@if (Auth::user()->add_cli == 1)
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-8">
        <a href="{{route('indexClient')}}" class="button text-white bg-theme-1 shadow-md mr-2">Ajouter un nouveau client</a>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="hidden md:block mx-auto text-gray-600"></div>
    </div>    
@endif

<div class="intro-y box px-5 py-8 mt-5">
    <div class="intro-y col-span-12 overflow-y-scroll xl:overflow-x-scroll" style="height: 30rem;">
        <table class="table table-report -mt-2 border-none" style="border-style: none !important;" id="table_id">
            <thead style="border-style: none !important;">
                <tr class="shadow-lg">
                    <th class="whitespace-no-wrap text-center " style="border-style: none !important;">id</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Type</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Nom</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Cin/R.c</th>
                    @if (Auth::user()->edit_cli == 1 || Auth::user()->delet_cli == 1 || Auth::user()->see_doss_cli == 1 || Auth::user()->read_only_cli == 1)
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Actions</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach ($clients as $item)
                <?php
                    $create = App\HistoriqueAct::where('id_action',$item->id)->where('where','client')->where('what','create')->orderby('id','desc')->first();
                    
                    $delete = App\HistoriqueAct::where('id_action',$item->id)->where('where','client')->where('what','delete')->orderby('id','desc')->first();
            
                    if(isset($create->id) && $create->id_user != Auth::user()->id && $create->id_valid <= 0 ){
                        continue;
                    }
            
                    if(isset($delete->id) && $delete->id_user == Auth::user()->id && $delete->id_valid == 0 ){
                        continue;
                    }
                ?>
                <tr class="shadow-md rounded-lg intro-y " >
                    <td class="w-16" align="center">
                        <div class="rounded-full p-3"  style="width: 5.4rem;">
                            <div class="text-center font-semibold">
                                {{$item->id}} 
                            </div> 
                        </div> 
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{(($item->type_cli=='perso')? 'Particulier' : 'Entreprise')}}
                            </div>
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{(($item->type_cli=='perso')? $item->nom.' '.$item->prenom : $item->raison_sos)}}
                            </div>
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{(($item->type_cli=='perso')? $item->cin : $item->rc)}}
                            </div>
                        </div>
                    </td>
                    @if (Auth::user()->edit_cli == 1 || Auth::user()->delet_cli == 1 || Auth::user()->see_doss_cli == 1 || Auth::user()->read_only_cli == 1)
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                @if (Auth::user()->edit_cli == 1)
                                    <a class="flex items-center mr-3" href="{{route('showClient',[$item->id])}}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Modifier </a>
                                @endif
                                @if (Auth::user()->read_only_cli == 1)
                                    <a class="flex items-center mr-3" href="{{route('showClient',[$item->id])}}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Aperçu </a>
                                @endif
                                @if (Auth::user()->delet_cli == 1 )
                                    <a class="flex items-center text-theme-6 mr-3 " href="javascript:;" data-toggle="modal" data-target="#delete-confirmation-modal{{$item->id}}"> 
                                        <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete 
                                    </a>
                                @endif
                                
                                <div class="dropdown"> 
                                    <button class="dropdown-toggle button px-2 mr-1 mb-2 bg-theme-33 text-white m-2"><i data-feather="plus"></i></button>
                                    <div class="dropdown-box w-40">
                                        <div class="dropdown-box__content box dark:bg-dark-1 p-2 overflow-y-auto " style="height: 106px;"> 
                                            @if (Auth::user()->see_doss_cli == 1)
                                                <a class="flex items-center hover:bg-gray-300 w-full p-3 rounded-lg" href="{{route('cliDossier',[$item->id])}}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Dossiers </a>
                                            @endif
                                            <a class="flex items-center hover:bg-gray-300 w-full p-3 rounded-lg" href="{{route('print_fich_cli',[$item->id])}}" target="blank"> <i data-feather="file" class="w-4 h-4 mr-1"></i> Imprimer </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    @endif
                </tr>
                 <!-- BEGIN: Delete Confirmation Modal -->
                 <div class="modal" id="delete-confirmation-modal{{$item->id}}">
                    <div class="modal__content">
                        <div class="p-5 text-center">
                            <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
                            <div class="text-3xl mt-5">Suppression !</div>
                            <div class="text-gray-600 mt-2">Voulez-vous vraiment supprimer ces enregistrements ? Il n'y pas de retour en arriere.</div>
                        </div>
                        <div class="px-5 pb-8 text-center">
                            <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Annuler</button>
                            <a class="button w-24 bg-theme-6 text-white" href="{{route('destroyClient',[$item->id])}}">Supprimer</a>
                        </div>
                    </div>
                </div>
                 <!-- END: Delete Confirmation Modal -->
                @endforeach
                
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
    <script src="{{asset('dist/js/dataTbl.js')}}"></script>
    <script>
        $("body").ready(function(){
            $('.client').addClass('side-menu--active');
        });
    </script>
@endsection