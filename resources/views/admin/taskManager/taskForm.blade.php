@extends('admin.admin')
@section('title')
Détails du partenaire
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Détails du partenaire</a> </div>
@endsection
@section('content')
<form action="" id="form_data" enctype="multipart/form-data">
    <div class="intro-x box w-full mt-5 font-semibold p-3">
        <div class="">
            <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-2">
                <div class="">
                    <label for="">Dossier</label>
                    <select name="doss" class="input doss w-full border border-gray-500 mt-2">
                        <option value="0">Sélectionnez un dossier</option>        
                        @foreach ($Dossier as $item)
                            <option value="{{$item->id}}">{{$item->ref}}</option>        
                        @endforeach
                    </select>
                </div>
                <div class="">
                    <label for="">Agent</label>
                    <select name="agent" class="input w-full border border-gray-500 mt-2">
                        <option value="0">Sélectionner un agent</option>
                        @forelse ($agent as $item)
                            <option value="{{$item->id}}">{{$item->nom." ".$item->prenom}}</option>        
                        @empty
                            <option value="0">Aucun Agent</option>    
                        @endforelse
                    </select>
                </div>
            </div>
            <div class="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 gap-2 mt-3">
                <div class="">
                    <label for="">Commande</label>
                    <select name="cmnd" class="input w-full border border-gray-500 mt-2">
                        <option value="Notification">Notification</option>  
                        <option value="Validation d'adresse">Validation d'adresse</option>  
                        <option value="Visite domiciliaire">Visite domiciliaire</option>  
                        <option value="Médiations">Médiations</option>
                    </select>
                </div>
                <div class="">
                    <label for="">Date débute</label>
                    <input type="datetime-local" name="date_dbTsk" value="{{date('Y-m-d\TH:i')}}" class="input w-full border border-gray-500 mt-2">
                </div>
                <div class="">
                    <label for="">Date fin</label>
                    <input type="datetime-local" name="date_FinTsk" value="{{date('Y-m-d\TH:i')}}" class="input w-full border border-gray-500 mt-2">
                </div>
            </div>
        </div>
    </div> 
    <div class="-intro-x box w-full mt-5 font-semibold p-3">
        <div class="w-full">
            <label for="">Fond & consigne</label>
            <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                <div class="flex flex-wrap px-4 file_block" >
                    
                </div>
                <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                    <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                        <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                    </button>
                    <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0" name="file_val[]" data-stp="not">
                </div>
            </div>
            <textarea name="remarque" class="input w-full h-32 border border-gray-500 mt-2" placeholder="Remarque..."></textarea>
        </div>
    </div>
    <div class="grid justify-items-center mt-5">
        <button class="save button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white"> 
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-activity w-4 h-4 mr-2"><polyline points="22 12 18 12 15 21 9 3 6 12 2 12"></polyline></svg>
            Enregistrer  
        </button>
    </div>
</form>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('body').on('click','.save',function(){
                event.preventDefault();
                var formData = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:`{{route('GestionTachesAdd')}}`,
                    type:'post',
                    dataType:'json',
                    data:formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        window.location.replace(`{{route('GestionTachesList')}}`);
                        // console.log(data);
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });
            
        });
    </script>
    <script>
        $('document').ready(function(){
            var kill_nb=0;
            $('body').on('change','.this_file',function(input){
                var file = $(this).get(0).files[0];
                if(file){
                    var reader = new FileReader();
                    $(this).parent().parent().find('.file_block').append(
                        `
                        <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">
                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                <span class="w-3/5 file__icon file__icon--file mx-auto">
                                    <div class="file__icon__file-name"></div>
                                </span>
                                <span class="block font-medium mt-4 text-center truncate">`+file.name+`</span>
                            </div>
                            <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file`+kill_nb+`"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                            </div>
                        </div>
                        `
                    );
                    $(this).parent().find('button').hide();
                    $(this).parent().append(`
                        <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                        </button>
                        <input type="file" name="file_val[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file`+kill_nb+`">
                    `);
                    kill_nb++;
                }
            });
            $("body").on('click tap','.kill_file,.kill_file_base',function(){
                $('.'+$(this).data('id')).attr('name' ,'');
                $(this).parent().toggle();
            });
        });
    </script>
    <script>
        $("body").ready(function(){
            $('.taskManager').addClass('side-menu--active');
        });
    </script>
@endsection

