@extends('admin.admin')
@section('title')
Liste des tâches
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Liste des tâches</a> </div>
@endsection
@section('content')
@if (Auth::user()->add_task == 1)
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-8">
        <a href="{{route('GestionTachesForm')}}" class="button text-white bg-theme-1 shadow-md mr-2">Ajouter une nouvelle tâche</a>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="hidden md:block mx-auto text-gray-600"></div>
    </div>    
@endif

<div class="intro-y box px-5 py-8 mt-5">
    <div class="intro-y col-span-12 overflow-y-scroll xl:overflow-x-scroll" style="height: 30rem;">
        <table class="table table-report -mt-2 border-none" style="border-style: none !important;" id="table_id">
            <thead style="border-style: none !important;">
                <tr class="shadow-lg">
                    <th class="whitespace-no-wrap text-center " style="border-style: none !important;">id</th>
                    <th class="whitespace-no-wrap text-center " style="border-style: none !important;">Dossier</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Agent</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Commande</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Date de création</th>

                    @if (Auth::user()->edit_task == 1 || Auth::user()->delet_task == 1 || Auth::user()->suivi_task == 1 || Auth::user()->see_dt_task == 1)
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Actions</th>
                    @endif
                    
                </tr>
            </thead>
            <tbody>
                @foreach ($task as $item)
                <tr class="shadow-md rounded-lg intro-y " >
                    <td class="w-16" align="center">
                        <div class="rounded-full p-3 {{(strtotime($item->date_fin) <= strtotime(date('Y-m-d H:i')) )? 'bg-theme-6 text-white tooltip' : ''}}" title="tâches expirées" style="width: 5.4rem;">
                            <div class="text-center font-semibold ">
                               {{$item->id}}
                            </div> 
                        </div> 
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{$item->ref}}
                            </div>
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{$item->nom}} {{$item->prenom}}
                            </div>
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{$item->commande}}
                            </div>
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{date('d-m-Y H:i',strtotime($item->created_at))}}
                            </div>
                        </div>
                    </td>
                    @if (Auth::user()->edit_task == 1 || Auth::user()->delet_task == 1 || Auth::user()->suivi_task == 1 || Auth::user()->see_dt_task == 1)
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                @if (Auth::user()->edit_task == 1)
                                    <a class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md" href="{{route('GestionTachesShow',[$item->id])}}"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-square w-4 h-4 mr-1"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                        Modifier 
                                    </a>    
                                @endif
                                @if (Auth::user()->see_dt_task == 1)
                                    <a class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md" href="{{route('GestionTachesShow',[$item->id])}}"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-square w-4 h-4 mr-1"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                        Aperçu 
                                    </a>
                                @endif
                                @if (Auth::user()->delet_task == 1)
                                    <a class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md" href="javascript:;" data-toggle="modal" data-target="#delete-confirmation-modal{{$item->id}}"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 w-4 h-4 mr-1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                        Delete 
                                    </a>
                                @endif
                                @if (Auth::user()->suivi_task == 1)
                                    <div class="flex justify-center items-center">
                                        <div class="dropdown"> 
                                            <button class="dropdown-toggle button inline-block bg-gray-200 text-gray-500">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus mx-auto"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>
                                            </button>
                                            <div class="dropdown-box w-40">
                                                <div class="dropdown-box__content box dark:bg-dark-1 p-2 overflow-y-auto h-auto"> 
                                                    <a class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md" href="{{route('Messenger.admin',[$item->id])}}"> 
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-square w-4 h-4 mr-1"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                                        Suivie 
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </td>
                    @endif
                </tr>

                <!-- BEGIN: Delete Confirmation Modal -->
                    <div class="modal" id="delete-confirmation-modal{{$item->id}}">
                        <div class="modal__content">
                            <div class="p-5 text-center">
                                <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
                                <div class="text-3xl mt-5">Suppression !</div>
                                <div class="text-gray-600 mt-2">Voulez-vous vraiment supprimer ces enregistrements ? Il n'y pas de retour en arriere.</div>
                            </div>
                            <div class="px-5 pb-8 text-center">
                                <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Annuler</button>
                                <a class="button w-24 bg-theme-6 text-white" href="{{route('GestionTachesDelete',[$item->id])}}">Supprimer</a>
                            </div>

                        </div>
                    </div>
                <!-- END: Delete Confirmation Modal -->
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/js/dataTbl.js')}}"></script>
<script>
    $("body").ready(function(){
        $('.taskManager').addClass('side-menu--active');
    });
</script>
@endsection