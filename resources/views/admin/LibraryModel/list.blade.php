<table class="table table-report -mt-2 border-none" style="border-style: none !important;" id="table_id">
    <thead style="border-style: none !important;">
        <tr class="shadow-lg">
            <th class="text-center whitespace-no-wrap">Référence</th>
            <th class="text-center whitespace-no-wrap">Description </th>
            <th class="text-center whitespace-no-wrap">Modèle</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($ModelLibraryt as $item)
            <tr class="shadow-md rounded-lg intro-y">
                <td class="text-center w-40">
                    {{ $item->ref }}
                </td>
                <td class="text-center w-64">
                    <div class="truncate ">
                        {{ $item->descrip }}
                    </div>
                </td>
                <td class=" table-report__action w-56">
                    <div class="flex justify-center items-center">
                        <div class="mx-2">
                            @if (empty($item->modle))
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                    stroke-linecap="round" stroke-linejoin="round"
                                    class="feather feather-file-minus mx-auto tooltip" title="aucun fichier">
                                    <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                    <polyline points="14 2 14 8 20 8"></polyline>
                                    <line x1="9" y1="15" x2="15" y2="15"></line>
                                </svg>
                            @else
                                <a href='{{ url($path . $item->modle) }}' target="_blank" class="tooltip"
                                    title="cliquez pour accéder au fichier">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                        viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                        stroke-linecap="round" stroke-linejoin="round"
                                        class="feather feather-file-text">
                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                        <polyline points="14 2 14 8 20 8"></polyline>
                                        <line x1="16" y1="13" x2="8" y2="13"></line>
                                        <line x1="16" y1="17" x2="8" y2="17"></line>
                                        <polyline points="10 9 9 9 8 9"></polyline>
                                    </svg>
                                </a>
                            @endif
                        </div>
                        <div>
                            <a class="tooltip" title="Supprimer" href="javascript:;" data-toggle="modal"
                                data-target="#delete-confirmation-modal{{ $item->id }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5"
                                    stroke-linecap="round" stroke-linejoin="round"
                                    class="feather feather-trash-2 mx-auto hover:text-theme-6">
                                    <polyline points="3 6 5 6 21 6"></polyline>
                                    <path
                                        d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
                                    </path>
                                    <line x1="10" y1="11" x2="10" y2="17"></line>
                                    <line x1="14" y1="11" x2="14" y2="17"></line>
                                </svg>
                            </a>
                        </div>
                    </div>
                </td>
            </tr>
            <!-- BEGIN: Delete Confirmation Modal -->
            <div class="modal" id="delete-confirmation-modal{{ $item->id }}">
                <div class="modal__content">
                    <div class="p-5 text-center">
                        <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Êtes-vous sûr? </div>
                        <div class="text-gray-600 mt-2">Voulez-vous vraiment supprimer ces enregistrements? Ce
                            processus ne peut pas être annulé.</div>
                    </div>
                    <div class="px-5 pb-8 text-center">
                        <button type="button" data-dismiss="modal"
                            class="button w-24 border text-gray-700 mr-1">Annuler</button>
                        <a class="button w-24 bg-theme-6 text-white delete" data-id='{{ $item->id }}'
                            data-dismiss="modal" href="{{route('delete_biblio',[$item->id])}}">Supprimer</a>
                    </div>
                </div>
            </div>
            <!-- END: Delete Confirmation Modal -->
        @endforeach
    </tbody>
</table>
<script src="{{ asset('dist/js/dataTbl.js') }}"></script>
