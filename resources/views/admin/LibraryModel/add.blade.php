@extends('admin.admin')
@section('title')
    Ajouter des modèles
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i
            data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ajouter des
            modèles</a> </div>
@endsection
@section('content')
    <form action="" id="form_data">
        <div class="-intro-x box mt-3 p-3">
            <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2 my-2">
                <div class="">
                    <label for="">Référence</label>
                    <input type="text" name="ref" class="input w-full border boder-gray-500" placeholder="Référence">
                </div>
                <div class="">
                    <label for="">Modèle</label>
                    <input type="file" name="modle" class="input w-full border boder-gray-500">
                </div>
            </div>
            <div class="w-full">
                <label for="">Description</label>
                <textarea name="descrip" placeholder='Description...' class="input w-full border boder-gray-500"></textarea>
            </div>
            <center>
                <div class="my-3">
                    <button class="button bg-theme-1 text-white save">Enregistrer</button>
                </div>
            </center>
        </div>
    </form>
    <div class="-intro-x box mt-3 p-3 result">
        @include('admin.LibraryModel.list')
    </div>
@endsection
@section('script')
    <script>
        $("body").ready(function() {
            $('.bib_model').addClass('side-menu--active');
            $('body').on('click','.save',function(){
                $('.show_load').show();
                event.preventDefault();
                var FromData = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:`{{route('bib_model_add')}}`,
                    dataType:'json',
                    type:'post',
                    data:FromData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        $('.show_load').hide('slow');
                        console.log(data);
                        $('.result').html(data.view);
                    },
                    error:function(error){
                        $('.show_load').hide('slow');
                        console.log(error);
                    },
                });
            });
        });
    </script>
@endsection
