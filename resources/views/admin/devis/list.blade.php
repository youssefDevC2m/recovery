@extends('admin.admin')
@section('title')
Liste des devis
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Liste des devis</a> </div>
@endsection
@section('content')
@if (Auth::user()->add_devis==1)
    <div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-8">
        <a href="{{route('addDevis')}}" class="button text-white bg-theme-1 shadow-md mr-2">Ajouter un nouveau devis</a>
        <div class="hidden md:block mx-auto text-gray-600"></div>
        <div class="hidden md:block mx-auto text-gray-600"></div>
    </div>
@endif
<div class="intro-y box px-5 py-8 mt-5">
    <div class="intro-y col-span-12 overflow-y-scroll xl:overflow-x-scroll" style="height: 30rem;">
        <table class="table table-report -mt-2 border-none" style="border-style: none !important;" id="table_id">
            <thead style="border-style: none !important;">
                <tr class="shadow-lg">
                    <th class="whitespace-no-wrap text-center " style="border-style: none !important;">id</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Ref dossier</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Date devis</th>
                    @if (Auth::user()->edit_devis==1 || Auth::user()->delet_devis==1 || Auth::user()->print_devis==1 || Auth::user()->see_devis==1 )
                        <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Actions</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach ($devis as $item)
                <?php
                    $create = App\HistoriqueAct::where('id_action',$item->id)
                    ->where('where','Devis')
                    ->where('what','create')
                    ->orderby('id','desc')
                    ->first();
                    
                    $delete = App\HistoriqueAct::where('id_action',$item->id)
                    ->where('where','Devis')
                    ->where('what','delete')
                    ->orderby('id','desc')
                    ->first();
                    
                    if(isset($create->id) && $create->id_user != Auth::user()->id && $create->id_valid <= 0 ){
                        continue;
                    }
                    if(isset($delete->id) && $delete->id_user == Auth::user()->id && $delete->id_valid == 0 ){
                        continue;
                    }
                ?>
                <tr class="shadow-md rounded-lg intro-y " >
                    <td class="w-16" align="center">
                        <div class="rounded-full p-3"  style="width: 5.4rem;">
                            <div class="text-center font-semibold">
                                {{$item->id}} 
                            </div> 
                        </div> 
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{$item->ref}}
                            </div>
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{date('d-m-Y',strtotime($item->date_devie))}}
                            </div>
                        </div>
                    </td>
                    @if (Auth::user()->edit_devis==1 || Auth::user()->delet_devis==1 || Auth::user()->print_devis==1 || Auth::user()->see_devis==1 )
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                @if(Auth::user()->edit_devis==1)
                                    <a class="flex items-center" href="{{route('showDevis',[$item->id])}}"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-square w-4 h-4 mr-1"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                        Modifier 
                                    </a>
                                @endif
                                @if(Auth::user()->see_devis==1)
                                    <a class="flex items-center" href="{{route('showDevis',[$item->id])}}"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-square w-4 h-4 mr-1"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                        Aperçu 
                                    </a>
                                @endif
                                @if(Auth::user()->delet_devis==1)
                                    <a class="flex items-center text-theme-6 mx-3" href="javascript:;" data-toggle="modal" data-target="#delete-confirmation-modal{{$item->id}}"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 w-4 h-4 mr-1"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                        Delete 
                                    </a>
                                @endif
                                @if(Auth::user()->print_devis==1)
                                    <a class="flex items-center" href="{{route('printDevie',[$item->id])}}" target="_blanck"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-1"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                        Imprimer
                                    </a>
                                @endif
                            </div>
                        </td>
                    @endif
                </tr>
                <!-- BEGIN: Delete Confirmation Modal -->
                    <div class="modal" id="delete-confirmation-modal{{$item->id}}">
                        <div class="modal__content">
                            <div class="p-5 text-center">
                                <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i> 
                                <div class="text-3xl mt-5">Suppression !</div>
                                <div class="text-gray-600 mt-2">Voulez-vous vraiment supprimer ces enregistrements ? Il n'y pas de retour en arriere.</div>
                            </div>
                            <div class="px-5 pb-8 text-center">
                                <button type="button" data-dismiss="modal" class="button w-24 border text-gray-700 mr-1">Annuler</button>
                                <a class="button w-24 bg-theme-6 text-white" href="{{route('deleteDevis',[$item->id])}}">Supprimer</a>
                            </div>

                        </div>
                    </div>
                <!-- END: Delete Confirmation Modal -->
                @endforeach
                
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/js/dataTbl.js')}}"></script>
<script>
    $("body").ready(function(){
        $('.devis').addClass('side-menu--active');
    });
</script>
@endsection