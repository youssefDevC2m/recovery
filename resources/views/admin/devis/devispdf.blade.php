
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        @page{
            margin: 10px;
        }
    </style>
</head>

<body style="width: 100%; height: 100%; border:3px dashed rgb(0, 14, 116); border-radius: 6px;">
    <center>
        <div>
            <img src="data:image/png;base64,{{$data['logo']}}" style="width: 20%; margin-top: 10px;">
        </div> 
        <h1>Centre Juridique Droit et Devoir</h1>  
        <h3><i>DEVIS N°</i><span style="color: rgba(109, 109, 109, 0.837)"> {{$data['devis']->ref}}-{{$data['devis']->id}}</span></h3> 
    </center>
    <div style="margin-top:20px; ">
        <table style="width: 100%;">
            <tbody>
                <tr>
                    <td align="">
                        <div style="margin-left: 20px ;font-size: 1.25rem;  line-height: 1.75rem;  ">
                          {{-- <b>Référence Dossier</b> <span style="border:2px solid black; padding:0.3rem">{{$data['devis']->ref}}</span>  --}}
                            <b>Date :</b> <span style="padding:0.3rem">{{date('d/m/Y H:i')}}</span>
                        </div>
                    </td>
                    <td align="right">
                        <div style="margin-right: 20px ;font-size: 1.25rem;  line-height: 1.75rem; ">
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div style="margin-left:20px;">
        <div>
            <h4>
                DEBITEUR: <span style="color: rgb(94, 94, 94);  line-height: 1.75rem; height: 30px;">{{strtoupper($data['devis']->nom.' '.$data['devis']->prenom)}} </span>
                <br>
                VILLE: <span style="color: rgb(94, 94, 94);  line-height: 1.75rem; height: 30px;">{{strtoupper($data['devis']->ville_adress_prv)}} </span>
                <br>
                MONTANT DES CREANCES : <span style="color: rgb(94, 94, 94);  line-height: 1.75rem; height: 30px;">{{number_format($data['mt_creance'], 2, ',', ' ')}}</span>
                <br>
                MONTANT RECLAMEE : <span style="color: rgb(94, 94, 94);  line-height: 1.75rem; height: 30px;">{{number_format($data['mt_reclame'], 2, ',', ' ')}}</span>
                <br>
                VALEUR : <span style="color: rgb(94, 94, 94);  line-height: 1.75rem; height: 30px;">{{ ($data['dtDoss']->type_pay) }}</span>
            </h4>
        </div>
         
    </div>
    <div style=" padding-left: 20px; padding-right: 20px;">
        <table style="width: 100%;">
            <thead>
                <tr>
                    <th align="left" style="font-size: 1.25rem; line-height: 1.75rem; height: 30px;">
                        Désignation
                    </th>
                    <th align="right" style="font-size: 1.25rem; line-height: 1.75rem; height: 30px;">
                        Montant
                    </th>

                </tr>
                
            </thead>
            <tbody>
                @php($mt_total=0)
                @foreach ($data['prods'] as $item)
                <tr style="font-size: 1.25rem; color: rgb(94, 94, 94);  line-height: 1.75rem; height: 30px;">
                    <td align="">
                        <div>
                           <b>{{$item->designation}} :</b> 
                        </div>
                    </td>
                    <td align="right">
                        <div style="">
                            @php($mt_total+=$item->montant)
                            {{number_format($item->montant, 2, ',', ' ')}} Mad
                        </div>
                    </td>
                </tr>  
                @endforeach
                <tr style="font-size: 1.25rem;  line-height: 1.75rem; height: 1000px;">
                    <td align="">
                        <div>
                           <b>Total :</b> 
                        </div>
                    </td>
                    <td align="right">
                        <div style="color: rgb(94, 94, 94);  line-height: 1.75rem; height: 30px;">
                            {{number_format($mt_total, 2, ',', ' ')}} Mad
                        </div>
                    </td>
                </tr>
                <tr style="font-size: 1.25rem;  line-height: 1.75rem; height: 1000px;">
                    <td style="width: 80%" align="">
                        <div style="">
                            Arreté le présent devis à la somme de : <b>{{ucfirst($data['nb']->ConvNumberLetter($mt_total,0,0))}} dhs</b>
                        </div>
                    </td>
                    <td align="">
                        {{-- <div>
                           <b>Arreté le présent devis à la somme de :</b> 
                        </div> --}}
                    </td>
                    
                </tr>
            </tbody>
        </table>
    </div>
    
</body>

</html>