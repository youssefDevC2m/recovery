@extends('admin.admin')
@section('title')
Ouverture Dossier
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Fermeture du dossier</a> </div>
@endsection
@section('content')
<div class="p-3">
    <label for="" class="text-lg font-semibold">Référence du dossier :<span class="text-gray-600">{{$dossier->ref}}</span></label>
</div>
<form method="post"  action="" id="form_data" enctype="multipart/form-data" class="">
    @csrf
    <input type="hidden" name="id_doss" value="{{$id_doss}}">
    <input type="hidden" name="id_ferm" value="{{(isset($FermDoss->id))? $FermDoss->id : '0'}}">
    <div class="intro-y box px-5 py-8 mt-2 font-semibold">
        
        <div class="">
            <label for="" class="text-lg">Position du dossier</label>
        </div>

        <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-2">
            <div class="mt-2">
                <label for="">Date De Fermeture</label>
                <input type="date" name="date_fermetur" class="input w-full border border-gray-500" value="{{(!empty($FermDoss->id) && isset($FermDoss->date_fermetur))? date('Y-m-d',strtotime($FermDoss->date_fermetur)) : date('Y-m-d')}}">
            </div>
        </div>

        <div class="bg-gray-400 my-3 p-5 rounded-lg grid ">
            <div class="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-2 p-5 box ">
                <div class="flex items-center text-gray-700 mt-2"> 
                    <input type="radio" value="Cloture pour payement" name="statuFerm" {{(!empty($FermDoss->id) && $FermDoss->statuFerm=='Cloture pour payement')? 'checked' : '' }} class="input border mr-2 border-gray-500" id="horizontal-checkbox-chris-evans-1"> 
                    <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans-1" >Cloture pour payement</label> 
                </div>
                <div class="flex items-center text-gray-700 mt-2"> 
                    <input type="radio" value="Cloture pour non resultat" name="statuFerm"{{(!empty($FermDoss->id) && $FermDoss->statuFerm=='Cloture pour non resultat')? 'checked' :'' }}  class="input border mr-2 border-gray-500" id="horizontal-checkbox-chris-evans-2"> 
                    <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans-2">Cloture pour non resultat</label> 
                </div>
                {{-- <div class="flex items-center text-gray-700 mt-3"> 
                    <input type="radio" value="Transfere au tribunal" name="statuFerm"{{(!empty($FermDoss->id) && $FermDoss->statuFerm=='Transfere au tribunal')? 'checked' : ''}}   class="input border mr-2 border-gray-500" id="horizontal-checkbox-chris-evans-3"> 
                    <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans-3">Transfere au tribunal</label> 
                </div> --}}
                <div class="flex items-center text-gray-700 mt-3"> 
                    <input type="radio" value="Retour au client" name="statuFerm" {{(!empty($FermDoss->id) && $FermDoss->statuFerm=='Retour au client')? 'checked' : ''}} class="input border mr-2 border-gray-500" id="horizontal-checkbox-chris-evans-4"> 
                    <label class="cursor-pointer select-none" for="horizontal-checkbox-chris-evans-4">Retour au client</label> 
                </div>
            </div>
        </div>

        <div class="mt-2">
            <label for="">Rapport de décision</label>
            <textarea name="rapportDecis" class="input w-full border border-gray-500 h-32" placeholder="Rapport de décision">{{(!empty($FermDoss->id))? $FermDoss->rapportDecis : ''}}</textarea>
        </div>
        
        <div class="">
            
            <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                <div class="flex flex-wrap px-4 file_block" >
                    @if (count($files)>0)
                        @foreach ($files as $item)
                            <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2">
                                <a href="{{asset($File_path->file_path.$item->file_name)}}" target="_blank">
                                    <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                        <span class="w-3/5 file__icon file__icon--file mx-auto">
                                            <div class="file__icon__file-name"></div>
                                        </span>
                                        <span class="block font-medium mt-4 text-center truncate">{{$item->org_file_name}}</span>
                                    </div>
                                </a>
                                <input type="text" name="" id="">
                                <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file_base " data-id="kill_file_base{{$item->id}}"> 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                                </div>
                            </div>
                            <input type="hidden" name="file_exc[]" value="{{$item->file_name}}" class="kill_file_base{{$item->id}}">
                        @endforeach
                    @endif
                </div>
                <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                    <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                        <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                    </button>
                    <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0" name="file_val[]" data-stp="not">
                </div>
            </div>

        </div>

        @if ( Auth::user()->see_fermetur_doss == 1)
            <div class="grid justify-items-center mt-5">
                <div class="flex lg:flex-row md:flex-row flex-col ">
                    <button class="button w-24 mr-1 mb-2 bg-theme-1 text-white  {{(!empty($FermDoss->id))? 'update' : 'save'}}">Enregistrer</button>
                    <a href="javascript:;" class="hidden button w-24 mr-1 mb-2 bg-theme-9 text-white back tooltip" data-theme="light"  title="Retour à la liste" onclick="event.preventDefault();window.location.replace('{{route('listDossier')}}')">Précédent</a> 
                </div>
            </div>    
        @endif
        
    </div>
   
</form>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.save').click(function(){
                event.preventDefault();
                var formData = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:"{{route('addFermInfos')}}",
                    type:"post",
                    dataType:'json',
                    data:formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        console.log(data);
                        $('.back').show("slow");
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });
            $('.update').click(function(){
                event.preventDefault();
                var formData = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:"{{route('updateFermInfos')}}",
                    type:"post",
                    dataType:'json',
                    data:formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        console.log(data);
                        $('.back').show("slow");
                    },
                    error:function(error){
                        console.log(error);
                    },
                });
            });
            var kill_nb=0;
            $('body').on('change','.this_file',function(input){
                var file = $(this).get(0).files[0];
                if(file){
                    var reader = new FileReader();
                    $(this).parent().parent().find('.file_block').append(
                        `
                        <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">
                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                <span class="w-3/5 file__icon file__icon--file mx-auto">
                                    <div class="file__icon__file-name"></div>
                                </span>
                                <span class="block font-medium mt-4 text-center truncate">`+file.name+`</span>
                            </div>
                            <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file`+kill_nb+`"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                            </div>
                        </div>
                        `
                    );
                   
                    $(this).parent().find('button').hide();
                    $(this).parent().append(`
                        <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                        </button>
                        <input type="file" name="file_val[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file`+kill_nb+`">
                    `);
                    kill_nb++;
                }
            });
            $("body").on('click tap','.kill_file,.kill_file_base',function(){
                $('.'+$(this).data('id')).attr('name' ,'');
                $(this).parent().toggle();
                
            });

        });
    </script>
@endsection