@extends('admin.admin')
@section('title')
Ouverture Dossier
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Ouverture Dossier</a> </div>
<style>
    img {
        object-fit: cover;
        object-position: center center;
    }    
</style>
@endsection

@section('content')
<style>
    .dropdown-inner{
        max-height: 100px !important;
    }
</style>
<form method="post"  action="" id="form_data" enctype="multipart/form-data" class="">
    @csrf
    <div class="intro-y box px-5 py-8 mt-5">
        <span class="text-xl font-semibolde text-gray-600">Type Débiteur</span>
        <div class="mt-3 w-full flex items-center justify-center"> 
            <div class="">
                <div class="flex flex-row mt-2 "> 
                    <span for="" class="mr-2 font-semibold">Particulier</span>
                    <input type="checkbox" class="input input--switch border swich"> 
                    <span for="" class="ml-2 font-semibold">Entreprise</span>
                </div> 
                <input type="hidden" name="type_dtbr" class="type_cli" value="perso">
            </div>
        </div>
    </div>
    @if (Auth::user()->role=='Administrateur' )
        <div class="intro-y box px-5 py-8 mt-5">
            <span class="text-xl font-semibolde text-gray-600">informations créancier</span>
            <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                <div class="mt-2"> 
                    <label for="" class="font-semibold ">Créancier</label>
                    <select data-search="true" class="tail-select w-full " name="creanc">
                        @foreach ($client as $item)
                            <option value="{{$item->id}}">{{($item->type_cli=='perso')? $item->nom." ".$item->prenom :  $item->raison_sos}}</option>   
                        @endforeach
                    </select> 
                </div>
                <div class="mt-2">
                    <label for="" class="font-semibold">Date de création</label>
                    <input type="datetime-local" name="date_ouverture" class="input w-full border border-gray-500 " value="{{date('Y-m-d\TH:i:s')}}">
                </div> 
            </div>
        </div>
    @elseif(Auth::user()->role=='Client')
        <input type="hidden" name="creanc" value="{{Auth::user()->id}}">
        <input type="datetime-local" name="date_ouverture" class="hidden" value="{{date('Y-m-d\TH:i:s')}}">
    @endif
    <div class="intro-y box px-5 py-8 mt-5 contant"></div>
    {{-- <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2 mt-2">
        <div class="intro-y box px-5 py-8 ">
            <div class="flex items-center">
                <span class="text-xl font-semibolde text-gray-600 ">informations banque</span>
                <i data-feather="plus-circle" class="ml-3 hover:text-theme-9"></i> 
            </div>
            <div class="w-full bg-gray-300 h-64 rounded-lg overflow-y-scroll">
                <div class="h-2" style=""></div>
                <div>
                    <div class="flex items-center p-3 h-8 m-2 hover:shadow-lg cursor-pointer transition duration-300 ease-in-out bg-white dark:bg-dark-3  dark:hover:bg-dark-1 rounded-md" style="">
                        <span class="pos__ticket__item-name truncate mr-1 font-normal text-gray-600"></span>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <div class=" mt-5 ">
        <div class="w-full my-3">
            <center>
                <button class="button rounded-full bg-theme-9 text-white hover:shadow-lg plus_banque" onclick="event.preventDefault()"><i data-feather="plus"></i></button>
            </center>
        </div>
        <div class="overflow-y-scroll bank bg-gray-400 rounded-lg" style="height: 12rem;">
            <div class="intro-y box px-5 py-8 ">
                <div class="grid lg:grid-cols-4 sm:grid-cols-4 gap-2 ">
                    <input type="hidden" name="nb_bank[]" value="1">
                    <div class="mt-2">
                        <label for="" class="font-semibold">Banque</label>
                        <input type="text" class="input border w-full mt-2 border-gray-500" name="banque[]" placeholder="Entrer le banque de debiteur">
                    </div>
                    <div class="mt-2">
                        <label for="" class="font-semibold">Agence</label>
                        <input type="text" class="input border w-full mt-2 border-gray-500" name="agence[]" placeholder="Entrer une agence">
                    </div>
                    <div class="mt-2">
                        <label for="" class="font-semibold">Ville</label>
                        <input type="text" class="input border w-full mt-2 border-gray-500" name="ville[]" placeholder="Entrer la ville">
                    </div>
                    <div class="mt-2">
                        <label for="" class="font-semibold">N°compte</label>
                        <input type="text" class="input border w-full mt-2 border-gray-500" name="n_compte[]" placeholder="Entrer le banque de debiteur">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=" mt-5 gerant_block">
        
    </div>
    <div class="-intro-y contact-info">
        <center>
            <div class="box px-5 py-4 mt-5">
                <label for="" class="text-2xl font-semibold text-gray-600 ">Contacts de débiteur</label>
            </div>   
        </center>
        <div class="grid lg:grid-cols-4  sm:grid-cols-1 gap-2 font-semibold">
            <div class="box px-5 py-8 mt-5 ">
                <div class="flex items-center ">
                    <label for="">Téléphone </label><i data-feather="plus-circle" class="ml-3 hover:text-theme-9 add_tele"></i> 
                </div>
                <div class="h-32 overflow-y-scroll">
                    <div class="grid grid-cols-1 tele_block mr-2">
                        <input type="text" placeholder="Entrer le téléphone" name="tele[]" class=" input border w-full mt-2">
                    </div> 
                </div>
            </div>
            <div class="box px-5 py-8 mt-5">
                <div class="flex items-center ">
                    <label for="">E-mail</label><i data-feather="plus-circle" class="ml-3 hover:text-theme-9 add_mail"></i> 
                </div>
                <div class="h-32 overflow-y-scroll">
                    <div class="grid grid-cols-1 mail_block mr-2">
                        <input type="text" placeholder="mail@mail.com" name="mail[]" class="input border w-full mt-2">
                    </div>
                </div>
            </div>
            <div class="box px-5 py-8 mt-5 ">
                <div class="flex items-center ">
                    <label for="">GSM</label><i data-feather="plus-circle" class="ml-3 hover:text-theme-9 add_fax"></i> 
                </div>
                <div class="h-32 overflow-y-scroll">
                    <div class="grid grid-cols-1 fax_block mr-2 ">
                        <input type="text" placeholder="Entrer le GSM" name="fax[]" class="fax input border w-full mt-2">
                    </div>
                </div>
            </div>
            <div class="box px-5 py-8 mt-5 ">
                <div class="flex items-center">
                    <label for="">Autre contact</label><i data-feather="plus-circle" class="ml-3 hover:text-theme-9 add_cont"></i> 
                </div>
                <div class="h-32 overflow-y-scroll">
                    <div class="grid grid-cols-1 add_o_contact mr-2">
                        <input type="text" placeholder="Entrer le un autre contact" name="contact[]" class="contact input border w-full mt-2">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="-intro-y  px-5 py-4 mt-5">
        <center>
            <button type="button" class="save button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white"> 
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-save w-4 h-4 mr-2"><path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path><polyline points="17 21 17 13 7 13 7 21"></polyline><polyline points="7 3 7 8 15 8"></polyline></svg>
                Enregistrer 
            </button>
        </center>
    </div>
</form> 
<div class="hidden hidden_gerant">
    <div class="w-full my-3">
        <center>
            <button class="button rounded-full bg-theme-9 text-white hover:shadow-lg plus_gerant" onclick="event.preventDefault()"><i data-feather="plus"></i></button>
        </center>
    </div>
    <div class="overflow-y-scroll bg-gray-400 rounded-lg gerant" style="height: 15rem;">
        <div class="intro-y box px-5 py-8 ">
            <div class="grid lg:grid-cols-4 sm:grid-cols-1 gap-2 ">
                <input type="hidden" value="1" name="nb_gerant[]">
                <div class="mt-2">
                    <label for="" class="font-semibold">Nom Gérant</label>
                    <input type="text" class="input border w-full mt-2 border-gray-500" name="nom_gerant[]" placeholder="Entrer le nom du gerant">
                </div>
                <div class="mt-2">
                    <label for="" class="font-semibold">Prenom Gérant</label>
                    <input type="text" class="input border w-full mt-2 border-gray-500" name="prenom_gerant[]" placeholder="Entrer le prenom du gerant">
                </div>
                <div class="mt-2">
                    <label for="" class="font-semibold">Alias Gérant</label>
                    <input type="text" class="input border w-full mt-2 border-gray-500" name="alias_gerant[]" placeholder="Entrer l'alias du gerant">
                </div>
                <div class="mt-2">
                    <label for="" class="font-semibold">Cin Gérant</label>
                    <input type="text" class="input border w-full mt-2 border-gray-500" name="cin_gerant[]" placeholder="Entrer la cin gerant">
                </div>
            </div>
            <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2">
                <div class="mt-2">
                    <label for="" class="font-semibold">Adresse Principal du Gérant</label>
                    <input type="text" class="input border w-full mt-2 border-gray-500" name="adress_pincipale_gerant[]" placeholder="Entrer l'Adresse Principal du Gérant">
                </div>
                <div class="mt-2">
                    <label for="" class="font-semibold">Ville</label>
                    <input type="text" class="input border w-full mt-2 border-gray-500" name="ville_gerant[]" placeholder="Entrer la ville">
                </div>
                <div class="my-2">
                    <label for="" class="font-semibold">Tel privé du gérant</label>
                    <input type="text" class="input border w-full mt-2 border-gray-500" name="tele_gerant[]" placeholder="Entrer le tel privé du gérant">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="client-perso hidden">
    <div>
        <span class="text-xl font-semibolde text-gray-600">Débiteur Particulier</span>
    </div>
    <div class="grid lg:grid-cols-4 sm:grid-cols-1 gap-2">
        <div class="mt-2">
            <label for="" class="font-semibold">Nom débiteur </label>
            <input type="text" name="nom" class="input w-full border border-gray-500 " placeholder="Entre le nom de debiteur">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Prenom débiteur </label>
            <input type="text" name="prenom" class="input w-full border border-gray-500 " placeholder="Entre le prenom de debiteur">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Alias </label>
            <input type="text" name="alias" class="input w-full border border-gray-500 " placeholder="Entre l'alias de debiteur">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Cin </label>
            <input type="text" name="cin" class="input w-full border border-gray-500 " placeholder="Entre le cin de debiteur">
        </div>
    </div>
    <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
        <div class="mt-2">
            <label for="" class="font-semibold">Employeur </label>
            <input type="text" name="employeur" class="input w-full border border-gray-500 " placeholder="Entre l'employeur de debiteur">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Activité </label>
            <input type="text" name="activite" class="input w-full border border-gray-500 " placeholder="Entre l'activité de debiteur">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Adresse privée de débiteur</label>
            <input type="text" name="adress_prv" class="input w-full border border-gray-500 " placeholder="Entre l'adresse privée de débiteur">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Ville </label>
            <input type="text" name="ville_adress_prv" class="input w-full border border-gray-500 " placeholder="Entre la ville">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Adresse professionnelle</label>
            <input type="text" name="adress_pro" class="input w-full border border-gray-500 " placeholder="Entre l'adresse professionnelle de débiteur">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Ville </label>
            <input type="text" name="ville_adress_pro" class="input w-full border border-gray-500 " placeholder="Entre la ville">
        </div>
    </div>
    <div class="">
        
    </div>
</div>   
<div class="client-company hidden">
    <div>
        <span class="text-xl font-semibolde text-gray-600">Débiteur Entreprise</span>
    </div>
    <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
        <div class="mt-2">
            <label for="" class="font-semibold">Nom de la société </label>
            <input type="text" name="nom" class="input w-full border border-gray-500 " placeholder="Entre le nom de debiteur">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Activité</label>
            <input type="text" name="activite" class="input w-full border border-gray-500" placeholder="Entre l'activité de la société">
        </div>
    </div>
    <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2">
        <div class="mt-2">
            <label for="" class="font-semibold">Forme juridique</label>
            <input type="text" name="forme_jurdq" class="input w-full border border-gray-500" placeholder="Entre la forme juridique">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">N°Registre de commerce</label>
            <input type="text" name="rc" class="input w-full border border-gray-500" placeholder="Entre le N°RC">
        </div>
        <div class="mt-2 hidden">
            <label for="" class="font-semibold">Identifiant fiscal</label>
            <input type="text" name="if" class="input w-full border border-gray-500" placeholder="Entre le IF">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">ICE</label>
            <input type="text" name="patent" class="input w-full border border-gray-500" placeholder="Entre l'ICE">
        </div>
    </div>
    <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
        <div class="mt-2">
            <label for="" class="font-semibold">Adresse du siège</label>
            <input type="text" name="adress_siege" class="input w-full border border-gray-500" placeholder="Entre la adresse du siège">
        </div>
        <div class="mt-2">
            <label for="" class="font-semibold">Ville</label>
            <input type="text" name="ville_entre" class="input w-full border border-gray-500" placeholder="Entre la ville">
        </div>
    </div>
</div>

@endsection
@section('script')
    <script>
        $("body").ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('body').on('click','.save',function(){
                event.preventDefault();
                var form_data = new FormData(document.getElementById('form_data'));
                $.ajax({
                    url:"{{route('createDossier')}}",
                    type:'post',
                    dataType:'json',
                    data:form_data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        // console.log(data);
                        window.location.replace("{{route('listDossier')}}");
                    },error:function(error){
                        console.log(error);
                    },
                });
            });
            $('.dossier').addClass('side-menu--active');
            $('.contant').html($('.client-perso').html());
            // $('.contant').html($('.client-company').html());
            $('.swich').change(function(){
                if($(this).is(':checked')){
                    $('.contant').html("");
                    $('.type_cli').val('entre');
                    $('.contant').html($('.client-company').html());
                    $('.gerant_block').html($('.hidden_gerant').html());
                }else{
                    $('.contant').html("");
                    $('.type_cli').val('perso');
                    $('.contant').html($('.client-perso').html());
                    $('.gerant_block').html("");
                }
            });
            $('body').on('click','.add_tele',function(){
                $('.tele_block').append(`
                <div class='flex items-center'>
                <input type="text" placeholder="Entrer le téléphone" name="tele[]" class="tele input border w-full mt-2 ">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });
            $("body").on('click','.add_mail',function(){
                $('.mail_block').append(`
                <div class='flex items-center'>
                    <input type="text" placeholder="mail@mail.com" name="mail[]" class="input border w-full mt-2" spellcheck="false" data-ms-editor="true">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });
            $("body").on('click','.add_fax',function(){
                $('.fax_block').append(`
                <div class='flex items-center'>
                    <input type="text" placeholder="Entrer le GSM" name="fax[]" class="fax input border w-full mt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });
            $("body").on('click','.add_cont',function(){
                $('.add_o_contact').append(`
                <div class='flex items-center'>
                    <input type="text" placeholder="Entrer le un autre contact" name="contact[]" class="contact input border w-full mt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 ml-2 hover:text-red-500 rm-tele"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                </div>
                `);
            });
            $("body").on('click tap','.rm-tele',function(){
               $(this).parent().toggle();
            });
            $("body").on('click tap','.remove_back',function(){
                event.preventDefault();
                console.log($(this).parent().parent().toggle());
               
            });
            $("body").on('click tap','.remove_gerant',function(){
                event.preventDefault();
                console.log($(this).parent().toggle());
               
            });
            $('.plus_banque').click(function(){
                $('.bank').append(
                    `
                    <div class="intro-y box px-5 py-8 mt-2">
                    <input type="hidden" name="nb_bank[]" value="1">
                        
                        <div class="">
                            <button class="button px-2 mr-1 mb-2 bg-theme-6 text-white remove_back"> 
                                <span class="w-5 h-5 flex items-center justify-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 mx-auto justify-self-start "><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                </span>
                            </button>
                        </div>   
                        <div class="grid lg:grid-cols-4 sm:grid-cols-4 gap-2 ">
                            <div class="mt-2">
                                <label for="" class="font-semibold">Banque</label>
                                <input type="text" class="input border w-full mt-2 border-gray-500" name="banque[]" placeholder="Entrer le banque de debiteur">
                            </div>
                            <div class="mt-2">
                                <label for="" class="font-semibold">Agence</label>
                                <input type="text" class="input border w-full mt-2 border-gray-500" name="agence[]" placeholder="Entrer une agence">
                            </div>
                            <div class="mt-2">
                                <label for="" class="font-semibold">Ville</label>
                                <input type="text" class="input border w-full mt-2 border-gray-500" name="ville[]" placeholder="Entrer la ville">
                            </div>
                            <div class="mt-2">
                                <label for="" class="font-semibold">N°compte</label>
                                <input type="text" class="input border w-full mt-2 border-gray-500" name="n_compte[]" placeholder="Entrer le banque de debiteur">
                            </div>
                        </div>
                    </div>
                    `
                );
            });
            $('body').on('click','.plus_gerant',function(){
                $('.gerant').append(`
                <div class="intro-y box px-5 py-8 mt-2">
                <input type="hidden" value="1" name="nb_gerant[]">
                    <button class="button px-2 mr-1 mb-2 bg-theme-6 text-white remove_gerant"> 
                        <span class="w-5 h-5 flex items-center justify-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2 mx-auto justify-self-start "><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                        </span>
                    </button>
                    <div class="grid lg:grid-cols-4 sm:grid-cols-1 gap-2 ">
                        <div class="mt-2">
                            <label for="" class="font-semibold">Nom Gérant</label>
                            <input type="text" class="input border w-full mt-2 border-gray-500" name="nom_gerant[]" placeholder="Entrer le nom du gerant">
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Prenom Gérant</label>
                            <input type="text" class="input border w-full mt-2 border-gray-500" name="prenom_gerant[]" placeholder="Entrer le prenom du gerant">
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Alias Gérant</label>
                            <input type="text" class="input border w-full mt-2 border-gray-500" name="alias_gerant[]" placeholder="Entrer l'alias du gerant">
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Cin Gérant</label>
                            <input type="text" class="input border w-full mt-2 border-gray-500" name="cin_gerant[]" placeholder="Entrer la cin gerant">
                        </div>
                    </div>
                    <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <label for="" class="font-semibold">Adresse Principal du Gérant</label>
                            <input type="text" class="input border w-full mt-2 border-gray-500" name="adress_pincipale_gerant[]" placeholder="Entrer l'Adresse Principal du Gérant">
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Ville</label>
                            <input type="text" class="input border w-full mt-2 border-gray-500" name="ville_gerant[]" placeholder="Entrer la ville">
                        </div>
                        <div class="my-2">
                            <label for="" class="font-semibold">Tel privé du gérant</label>
                            <input type="text" class="input border w-full mt-2 border-gray-500" name="tele_gerant[]" placeholder="Entrer le tel privé du gérant">
                        </div>
                    </div>
                </div>
                `);
            });
        });
    </script>
@endsection