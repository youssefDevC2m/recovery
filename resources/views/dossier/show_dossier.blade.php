@extends('admin.admin')
@section('title')
Détails du dossier
@endsection
@section('menu-title')
    <div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">recoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Détails du dossier</a> </div>
@endsection
@section('content')
<style>
    .dropdown-inner{
        max-height: 100px !important;
    }
</style>
   
    <form action="{{route('editDossier')}}" id="formdata">
        <input type="hidden" name="id_doss" value="{{$dossiers->id}}">
        <div class="intro-y box px-5 py-8 mt-5">
            <span class="text-xl font-semibolde text-gray-600">Dossier</span>
            <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                <div class="mt-2" >
                    <label for="" class="font-semibold">Date réception</label>
                    <input type="date" name="date_resep" class="input w-full border mt-2 date_resep" value="{{(!empty($dossiers->date_resep))? date('Y-m-d',strtotime($dossiers->date_resep)) : date('Y-m-d')}}">
                </div>
                <div class="mt-2" >
                    <label for="" class="font-semibold">Référence</label>
                    <input type="text" name="ref" class="input w-full border mt-2 ref" placeholder="Entrer le référence" value="{{(!empty($dossiers->ref))? $dossiers->ref : ''}}">
                </div>
            </div>
            <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2">
                <div class="mt-2" >
                    <label for="" class="font-semibold">Client</label>
                    <select name="id_client" class="input w-full border mt-2">
                        <option value="0">Sélectionnez un client</option>
                        @foreach ($client as $item)
                            <option value="{{$item->id}}" {{($item->id==$dossiers->id_client)? 'selected' : ''}}>{{(($item->type_cli=="perso")? $item->nom." ".$item->prenom : $item->raison_sos)}}</option>  
                        @endforeach
                    </select>
                </div>
                <div class="mt-2" >
                    <label for="" class="font-semibold">Débiteur</label>
                    <select name="id_dbtr" class="input w-full border mt-2">
                        {{-- <option value="0">Sélectionnez un débiteur</option> --}}
                        <option value="{{$debiteur->id}}">{{(($debiteur->type_dtbr=="perso")? $debiteur->nom." ".$debiteur->prenom : $debiteur->nom)}}</option>  
                    </select>
                </div>
                <div class="mt-2">
                    <label for="" class="font-semibold">Montant globale de créance</label>
                    <input type="" min="0" step="0.01" name="mt_glob_creance" class="input w-full border mt-2" placeholder="Entrer montant globale de créance" value="{{(!empty($dossiers->mt_glob_creance))?  number_format($dossiers->mt_glob_creance, 2, '.', ' ') : ''}}">
                </div>
            </div>
        </div>
        
        <div class="details_block">
            @if (count($details) > 0)
                @php($i=0)
                @foreach ($details as $item)
               
                <div class="intro-y 
                {{($item->date_ancent<=date('Y-m-d',strtotime('-48 months',strtotime(date('Y-m-d')))) && $item->type_pay=="Chèque")? 'bg-red-400 rounded-lg shadow-lg':'box'}}  
                {{($item->date_ancent<=date('Y-m-d',strtotime('-36 months',strtotime(date('Y-m-d')))) && $item->type_pay=="Facture")? 'bg-red-400 rounded-lg shadow-lg':'box'}}  
                {{($item->date_ancent<=date('Y-m-d',strtotime('-36 months',strtotime(date('Y-m-d')))) && $item->type_pay=="Effet")? 'bg-red-400 rounded-lg shadow-lg':'box'}}  
                {{($item->date_ancent<=date('Y-m-d',strtotime('-36 months',strtotime(date('Y-m-d')))) && $item->type_pay=="Autre")? 'bg-red-400 rounded-lg shadow-lg':'box'}}  
                px-5 py-8 mt-5">
                    <input type="hidden" name="details_block_nb[]" value="1">
                    <div class="w-full px-3 w-full grid justify-items-end">
                        <div title="" class=" w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_dt"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                        </div>
                    </div>
                    <div class="grid lg:grid-cols-4 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <div class="flex flex-row  items-center">
                                <label for="" class="font-semibold mr-2 ">Type</label>
                                {{-- <i data-feather="plus-circle"  class="text-gray-600 hover:text-theme-9 tooltip outline-none add_pay_type" href="javascript:;" data-theme="light" title="Ajouter un type de créance"></i> --}}
                            </div>  
                            
                            <select name="type_pay[]" class="input w-full border mt-1" style="margin-top: 5px;">
                                <option value="0">Selectionnez un type</option>    
                                {{-- @foreach ($typeCreance as $item1)
                                    <option value="{{$item1->id}}" {{ ($item->type_pay==$item1->id)? 'selected' : "" }}>{{$item1->designation}}</option>    
                                @endforeach --}}
                                <option value="Chèque" {{($item->type_pay=="Chèque")? 'selected' : ''}}>Chèque</option>    
                                <option value="Effet" {{($item->type_pay=="Effet")? 'selected' : ''}}>Effet</option>    
                                <option value="Facture" {{($item->type_pay=="Facture")? 'selected' : ''}}>Facture</option>    
                                <option value="Autre" {{($item->type_pay=="Autre")? 'selected' : ''}}>Autre</option>    
                            </select>
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Banque</label>
                            <select name="bank[]" class="input w-full border mt-2 banksel" >
                                <option value="0">Selectionnez une banque</option>
                                @foreach ($bank as $item1)
                                <option value="{{$item1->id}}" {{($item->bank==$item1->id)? 'selected' :""}}>{{$item1->banque}}</option> 
                                @endforeach
                            </select>
                            {{-- <input type="text" class="input w-full border mt-2" value="{{$item->bank}}" name="bank[]" placeholder="Entrer la banque"> --}}
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Numero</label>
                            <input type="text" class="input w-full border mt-2 numero" value="{{$item->numero}}" name="numero[]" placeholder="Entrer le numero">
                        </div>
                        
                        <div class="mt-2">
                            <label for="" class="font-semibold">Ville</label>
                            <input type="text" class="input w-full border mt-2" value="{{$item->ville}}"  name="ville[]" placeholder="Entrer la ville">
                        </div>
                    </div>
                    <div class="grid lg:grid-cols-3 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <label for="" class="font-semibold">Motif</label>
                            <select type="text" class="input w-full border mt-2" name="motif[]">
                                <option value="Insuffisance de provision" {{($item->motif=="Insuffisance de provision")? 'selected' : ''}}>Insuffisance de provision</option>
                                <option value="Signature non conforme" {{($item->motif=="Signature non conforme")? 'selected' : ''}}>Signature non conforme</option>
                                <option value="Opposition pour perte ou vol" {{($item->motif=="Opposition pour perte ou vol")? 'selected' : ''}}>Opposition pour perte ou vol</option>
                                <option value="Endos irrégulier" {{($item->motif=="Endos irrégulier")? 'selected' : ''}}>Endos irrégulier</option>
                                <option value="Non conformité de la somme en lettre et en chiffre" {{($item->motif=="Non conformité de la somme en lettre et en chiffre")? 'selected' : ''}}>Non conformité de la somme en lettre et en chiffre</option>
                                <option value="Décédé" {{($item->motif=="Décédé")? 'selected' : ''}}>Décédé</option>
                                <option value="Prescrit" {{($item->motif=="Prescrit")? 'selected' : ''}}>Prescrit</option>
                                <option value="Compte clôturé" {{($item->motif=="Compte clôturé")? 'selected' : ''}}>Compte clôturé</option>
                                <option value="Opposition de paiement" {{($item->motif=="Opposition de paiement")? 'selected' : ''}}>Opposition de paiement</option>
                                <option value="Autres" {{($item->motif=="Autres")? 'selected' : ''}}>Autres</option>
                            </select>
                            {{-- <input type="text" class="input w-full border mt-2" value="{{$item->motif}}" name="motif[]" placeholder="Entrer la motif"> --}}
                        </div>
                        <input type="hidden" name="id_dt[]" value="{{$item->id}}">
                        <div class="mt-2">
                            <label for="" class="font-semibold">Date réception</label>
                            <input type="date" class="input w-full border mt-2"  name="date_creation[]" value="{{date('Y-m-d',strtotime($item->date_creation))}}">
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Date création </label>
                            <input type="date" class="input w-full border mt-2"  name="date_ancent[]" value="{{date('Y-m-d',strtotime($item->date_ancent))}}">
                        </div>
                    </div>
                    <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <label for="" class="font-semibold">Ancienneté </label>
                            <input 
                            type="text" 
                            class="input w-full border mt-2" 
                            name="anncDiff[]" 
                            value="<?php 
                            $dossCont = new App\Http\Controllers\dossierController();
                            $diff=$dossCont->dateDifference(date('Y-m-d'),date('Y-m-d',strtotime($item->date_ancent)));
                            echo $diff;

                            ?>">
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Date de rejet</label>
                            <input type="date" class="input w-full border mt-2" name="date_rejet[]" value="{{date('Y-m-d',strtotime($item->date_rejet))}}"">
                        </div>
                    </div>
                    <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <label for="" class="font-semibold">Mt créance</label>
                            <input type="number" min="0" step="0.1" class="input w-full border mt-2" name="mt_creance[]" placeholder="Entrer le montant créance" value="{{$item->mt_creance}}">
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Mt réclamé</label>
                            <input type="number" min="0" step="0.1" class="input w-full border mt-2" name="mt_reclame[]" placeholder="Entrer le montant réclamé" value="{{$item->mt_reclame}}">
                        </div>
                    </div>
                    <div class="mt-3">
                        <label class="font-semibold">pièce jointe</label>
                        <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                            <div class="flex flex-wrap px-4 file_block" >
                                @foreach ($files as $file)
                                    @if ($file->id_detail==$item->id)
                                        <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2 ">
                                            <a href="{{url($file_join->file_path.$file->file_name)}}" target="_blank">
                                                <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                                    <span class="w-3/5 file__icon file__icon--file mx-auto">
                                                        <div class="file__icon__file-name"></div>
                                                    </span>
                                                    
                                                    <span class="block font-medium mt-4 text-center truncate">{{$file->org_file_name}}</span>
                                                </div>
                                            </a>
                                            <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file_simp"> 
                                                <input type="hidden" name="file_from_db{{$i}}[]" value="{{$file->org_file_name}}" class="details_box">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                                            </div>
                                        </div> 
                                    
                                    @endif
                                @endforeach
                            </div>
                            <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                                <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                    <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                                </button>
                                <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0" name="file_val{{$i}}[]" data-stp="{{$i}}">
                            </div>
                            @php($i++)
                        </div>
                    </div>
                    <div class="mt-2">
                        <label for="" class="font-semibold">Remarque</label>
                        <textarea name="remarque[]" id="" class="input w-full h-64 border mt-2" placeholder="Remarque..." value="">{{$item->remarque}}</textarea>
                    </div>
                </div>  
                @endforeach  
            @else 
                
                <div class="intro-y box px-5 py-8 mt-5 ">
                    <input type="hidden" name="details_block_nb[]" value="1">
                    <div class="flex flex-row mb-3"> 
                        <span class="text-xl font-semibolde text-gray-600 w-full">Détails Dossier</span>
                        <div class="mt-1 ml-3 w-full grid  justify-items-end">
                            <i data-feather="plus-circle" class="text-gray-600 hover:text-theme-9 tooltip outline-none add_dt" href="javascript:;" data-theme="light" title="Ajouter de détails"></i>
                        </div>
                    </div>
                    <div class="grid lg:grid-cols-4 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <div class="flex flex-row  items-center">
                                <label for="" class="font-semibold mr-2 ">Type</label>
                                {{-- <i data-feather="plus-circle"  class="text-gray-600 hover:text-theme-9 tooltip outline-none add_pay_type" href="javascript:;" data-theme="light" title="Ajouter un type de créance"></i> --}}
                            </div>  
                            
                            <select name="type_pay[]" class="input w-full border mt-1" style="margin-top: 5px;">
                                <option value="0">Selectionnez un type</option>    
                                {{-- @foreach ($typeCreance as $item)
                                    <option value="{{$item->id}}">{{$item->designation}}</option>    
                                @endforeach --}}
                                <option value="Chèque">Chèque</option>    
                                <option value="Effet">Effet</option>    
                                <option value="Facture">Facture</option>
                                <option value="Autre">Autre</option>
                            </select>
                        </div>
                        <input type="hidden" name="id_dt[]" value="-1">
                        <div class="mt-2">
                            <label for="" class="font-semibold">Banque</label>
                            <select name="bank[]" class="input w-full border mt-2 banksel">
                                <option value="0">Selectionnez une banque</option>
                                @foreach ($bank as $item)
                                   <option value="{{$item->id}}">{{$item->banque}}</option> 
                                @endforeach
                            </select>
                            {{-- <input type="text" class="input w-full border mt-2" name="bank[]" placeholder="Entrer la banque"> --}}
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Numero</label>
                            <input type="text" class="input w-full border mt-2 numero" name="numero[]" placeholder="Entrer le numero">
                        </div>
                        
                        <div class="mt-2">
                            <label for="" class="font-semibold">Ville</label>
                            <input type="text" class="input w-full border mt-2" name="ville[]" placeholder="Entrer la ville">
                        </div>
                    </div>
                    <div class="grid lg:grid-cols-4 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <label for="" class="font-semibold">Motif</label>
                            <select type="text" class="input w-full border mt-2" name="motif[]"><option value="Insuffisance de provision" >Insuffisance de provision</option>
                                <option value="Signature non conforme">Signature non conforme</option>
                                <option value="Opposition pour perte ou vol" >Opposition pour perte ou vol</option>
                                <option value="Endos irrégulier" >Endos irrégulier</option>
                                <option value="Non conformité de la somme en lettre et en chiffre" >Non conformité de la somme en lettre et en chiffre</option>
                                <option value="Décédé" >Décédé</option>
                                <option value="Prescrit" >Prescrit</option>
                                <option value="Compte clôturé" >Compte clôturé</option>
                                <option value="Opposition de paiement" >Opposition de paiement</option>
                                <option value="Autres" >Autres</option>
                            </select>
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Date réception</label>
                            <input type="date" class="input w-full border mt-2" name="date_creation[]" value="{{date('Y-m-d')}}">
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Date création </label>
                            <input type="date" class="input w-full border mt-2"  name="date_ancent[]" value="{{date('Y-m-d')}}">
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Date de rejet</label>
                            <input type="date" class="input w-full border mt-2" name="date_rejet[]" value="{{date('Y-m-d')}}">
                        </div>
                    </div>
                    <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                        <div class="mt-2">
                            <label for="" class="font-semibold">Mt créance</label>
                            <input type="number" min="0" step="0.1" class="input w-full border mt-2" name="mt_creance[]" placeholder="Entrer le montant créance">
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Mt réclamé</label>
                            <input type="number" min="0" step="0.1" class="input w-full border mt-2" name="mt_reclame[]" placeholder="Entrer le montant réclamé">
                        </div>
                    </div>
                    <div class="mt-3">
                        <label class="font-semibold">pièce jointe</label>
                        <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                            <div class="flex flex-wrap px-4 file_block" ></div>
                            <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                                <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                    <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                                </button>
                                <input type="file" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file0" name="file_val0[]" data-stp="not">
                            </div>
                        </div>
                    </div>
                    <div class="mt-2">
                        <label for="" class="font-semibold">Remarque</label>
                        <textarea name="remarque[]" id="" class="input w-full h-64 border mt-2" placeholder="Remarque..."></textarea>
                    </div>
                </div>
            @endif
        </div>
        <div class="w-full px-3 w-full grid justify-items-center">
            <div class="bg-white rounded-b-full w-32 grid justify-items-center px-3 pt-2 pb-3 shadow-lg" >
                <a class="button w-24 rounded-full mb-3 bg-theme-18 text-theme-9 add_dt " href="javascript:;">Plus</a>
            </div>
        </div>
        <div class="box -intro-y px-3 py-3 mt-2">
            <div>
                <label for="" class="font-semibold">Observation</label>
                <textarea name="observation" id="" class="input w-full h-64 border mt-2"  placeholder="Observation...">{{$dossiers->observation}}</textarea>
            </div>
            @if (Auth::user()->read_only_doss == 0)
                <div class="grid justify-items-center">
                    <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-theme-1 text-white save"> 
                        <i data-feather="save" class="w-4 h-4 mr-2"></i> Enregistrer
                    </button>
                </div>    
            @endif
            
        </div>
    </form>
    
@endsection

@section('script')
    <script>
       
        $(document).ready(function(){
            var file_nb="{{(count($details)>0)? count($details)-1 : 0}}";
            var kill_nb=0;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // $('body').on('change','.banksel',function(){
            //     // console.log($(this).parent().parent().html());
            //     var content=$(this).parent().parent();
            //     $.ajax({
            //         url:"{{route('get_bank_info')}}",
            //         data:{id_b:$(this).val()},
            //         type:"post",
            //         dataType:"json",
            //         success:function(data){
            //             if(data!=0){
            //                 // console.log(data.n_compte);
            //                 content.find('.numero').val(data.n_compte);
            //                 // console.log($(this).parent().parent().html());
            //             }
            //         },
            //         error:function(error){
            //             console.log(error);
            //         },
            //     });
                
            // });
            $('.save').click(function(){
                event.preventDefault();
                var formdata=new FormData(document.getElementById('formdata'));
                
                $.ajax({
                    url:"{{route('editDossier')}}",
                    type:'post',
                    dataType:'json',
                    data:formdata,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success:function(data){
                        // console.log(data);

                        if(data.data!=0){
                            window.location.replace("{{route('listDossier')}}");
                            // console.log(data.data);
                        }else{
                            alert(data.err);
                        }
                        
                    },error:function(error){
                        console.log(error);
                    },
                });
            }); 
            $('body').on('change','.this_file',function(input){
                var file = $(this).get(0).files[0];
                 
                if(file){
                    var reader = new FileReader();
                    
                    $(this).parent().parent().find('.file_block').append(
                        `
                        <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2  opacity-50 ">
                            <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                <span class="w-3/5 file__icon file__icon--file mx-auto">
                                    <div class="file__icon__file-name"></div>
                                </span>
                                <span class="block font-medium mt-4 text-center truncate">`+file.name+`</span>
                            </div>
                            <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file " data-id="kill_file`+kill_nb+`"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                            </div>
                        </div>
                    
                        `
                    );
                   
                    $(this).parent().find('button').hide();
                    var next_step=(($(this).data('stp')!="not")? $(this).data('stp'): file_nb);
                    $(this).parent().append(`
                        <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                        </button>
                        <input type="file" name="file_val`+(($(this).data('stp')!="not")? $(this).data('stp'): file_nb)+`[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file`+kill_nb+`" data-stp="`+next_step+`">
                        
                    `);
                    
                    console.log((($(this).data('stp')!="not")? $(this).data('stp'): file_nb));
                }
            });
            $("body").on('click tap','.kill_file',function(){
                $('.'+$(this).data('id')).attr('name' ,'');
                $(this).parent().toggle();
                
            });
            $("body").on('click tap','.kill_file_simp',function(){
                $(this).parent().toggle();
                console.log($(this).parent().find('input[type="hidden"]').attr('name',''));
                // console.log($(this).parent().html());
            });
            $("body").on('click tap','.add_dt',function(){
                file_nb++;
                $('.details_block').append(
                    `
                   
                    <div class="intro-y box px-5 py-8 mt-5 ">
                        <input type="hidden" name="details_block_nb[]"  value="1">
                        <div class="w-full px-3 w-full grid justify-items-end">
                            <div title="" class="tooltip w-5 h-5 flex items-center justify-center  rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_dt"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                            </div>
                        </div>
                        <div class="grid lg:grid-cols-4 sm:grid-cols-1 gap-2">
                            <div class="mt-2">
                                <label for="" class="font-semibold">Type</label>
                                <select name="type_pay[]" class="input w-full border mt-2">
                                    <option value="0">Selectionnez un type</option>    
                                    <option value="Chèque" >Chèque</option>    
                                    <option value="Effet" >Effet</option>    
                                    <option value="Facture" >Facture</option>
                                    <option value="Autre">Autre</option>
                                </select>
                            </div>
                            <input type="hidden" name="id_dt[]" value="-1">
                            <div class="mt-2">
                                <label for="" class="font-semibold">Banque</label>
                                <select name="bank[]" class="input w-full border mt-2 banksel" >
                                    <option value="0">Selectionnez une banque</option>
                                    @foreach ($bank as $item)
                                    <option value="{{$item->id}}">{{$item->banque}}</option> 
                                    @endforeach
                                </select>
                            </div>
                            <div class="mt-2">
                                <label for="" class="font-semibold">Numero</label>
                                <input type="text" class="input w-full border mt-2 numero" name="numero[]" placeholder="Entrer le numero">
                            </div>
                            <div class="mt-2">
                                <label for="" class="font-semibold">Ville</label>
                                <input type="text" class="input w-full border mt-2" name="ville[]" placeholder="Entrer la ville">
                            </div>
                        </div>
                        <div class="grid lg:grid-cols-4 sm:grid-cols-1 gap-2">
                            <div class="mt-2">
                                <label for="" class="font-semibold">Motif</label>
                                <select type="text" class="input w-full border mt-2" name="motif[]">
                                    <option value="Insuffisance de provision" >Insuffisance de provision</option>
                                    <option value="Signature non conforme">Signature non conforme</option>
                                    <option value="Opposition pour perte ou vol" >Opposition pour perte ou vol</option>
                                    <option value="Endos irrégulier" >Endos irrégulier</option>
                                    <option value="Non conformité de la somme en lettre et en chiffre" >Non conformité de la somme en lettre et en chiffre</option>
                                    <option value="Décédé" >Décédé</option>
                                    <option value="Prescrit" >Prescrit</option>
                                    <option value="Compte clôturé" >Compte clôturé</option>
                                    <option value="Opposition de paiement" >Opposition de paiement</option>
                                    <option value="Autres" >Autres</option>
                                </select>
                            </div>
                            <div class="mt-2">
                                <label for="" class="font-semibold">Date réception</label>
                                <input type="date" class="input w-full border mt-2" name="date_creation[]" value="{{date('Y-m-d')}}">
                            </div>
                            <div class="mt-2">
                                <label for="" class="font-semibold">Date création</label>
                                <input type="date" class="input w-full border mt-2"  name="date_ancent[]" value="{{date('Y-m-d')}}">
                            </div>
                            <div class="mt-2">
                                <label for="" class="font-semibold">Date de rejet</label>
                                <input type="date" class="input w-full border mt-2" name="date_rejet[]" value="{{date('Y-m-d')}}">
                            </div>
                        </div>
                        <div class="grid lg:grid-cols-2 sm:grid-cols-1 gap-2">
                            <div class="mt-2">
                                <label for="" class="font-semibold">Mt créance</label>
                                <input type="number" min="0" step="0.1" class="input w-full border mt-2" name="mt_creance[]" placeholder="Entrer le montant créance">
                            </div>
                            <div class="mt-2">
                                <label for="" class="font-semibold">Mt réclamé</label>
                                <input type="number" min="0" step="0.1" class="input w-full border mt-2" name="mt_reclame[]" placeholder="Entrer le montant réclamé">
                            </div>
                        </div>
                        <div class="mt-3">
                            <label class="font-semibold">pièce jointe</label>
                            <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                                <div class="flex flex-wrap px-4 file_block" ></div>
                                <div class="px-4 pb-4 flex items-center justify-center cursor-pointer relative w-full">
                                    <button class="button w-32 mr-2 mb-2 flex items-center justify-center bg-gray-200 text-gray-600"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text w-4 h-4 mr-2"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                                        <span class="text-theme-1 dark:text-theme-10 mr-1">Ajouter</span> 
                                    </button>
                                    <input type="file" name="file_val`+file_nb+`[]" class="w-full h-full top-0 left-0 absolute opacity-0 this_file kill_file`+kill_nb+`" data-stp="not">
                                </div>
                            </div>
                        </div>
                        <div class="mt-2">
                            <label for="" class="font-semibold">Remarque</label>
                            <textarea name="remarque[]" id="" class="input w-full h-64 border mt-2" placeholder="Remarque..."></textarea>
                        </div>
                    </div>
                    
                    `
                   
                ); 
                
            });
            $(document).on('click','.kill_dt',function(){
                $(this).parent().parent().toggle();
                $(this).parent().parent().html("");
                var c=0;
                $('.details_box').each(function(index){
                    $(this).attr('name','file_from_db'+index+"[]");
                });
                file_nb--;
            });
           
        });
        
    </script>
@endsection