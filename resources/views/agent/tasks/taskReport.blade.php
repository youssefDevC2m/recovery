<?php 
use Carbon\Carbon;

?>
<div class="col-span-12 md:col-span-6 xl:col-span-4 xxl:col-span-12 mt-3 xxl:mt-8" id="app1">
    <div class="intro-x flex items-center h-10">
        <h2 class="text-lg font-medium truncate mr-5">
            Nouvelles tâches
        </h2>
    </div>
    @forelse ($task as $item)
        <div class="mt-5">
            <div class="intro-x">
                <div class="box px-5 py-3 mb-3 flex items-center zoom-in">
                    <div class="w-10 h-10 flex-none image-fit rounded-full overflow-hidden">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-inbox mt-2 mx-auto"><polyline points="22 12 16 12 14 15 10 15 8 12 2 12"></polyline><path d="M5.45 5.11L2 12v6a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2v-6l-3.45-6.89A2 2 0 0 0 16.76 4H7.24a2 2 0 0 0-1.79 1.11z"></path></svg>
                    </div>
                    <div class="ml-4 mr-auto">
                        <div class="font-medium">Commande : {{$item->commande}}</div>
                        <div class="text-gray-600 text-xs">{{date('m-d-Y H:i',strtotime($item->created_at))}}</div>
                    </div>
                    <div class="text-theme-9">Réf : {{strtoupper($item->ref)}}</div>
                </div>
            </div>
        </div>
    @empty
        <div class="w-full box grid justify-items-center my-3">
            <div class="p-4">
                <span class="text-lg text-gray-700">Aucune nouvelle</span>
            </div>
        </div>
    @endforelse
    <a href="{{route('task_list')}}" class="intro-x w-full block text-center rounded-md py-3 border border-dotted border-theme-15 dark:border-dark-5 text-theme-16 dark:text-gray-600">Liste des commandes</a> 
</div>

