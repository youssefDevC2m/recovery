@extends((Auth::user()->role!='Agent')?'admin.admin':'agent.main')
@section('title')
Détails du partenaire
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> 
    <a href="{{url('/')}}" class="">recoveryApp</a> 
    <i data-feather="chevron-right" class="breadcrumb__icon"></i> 
    <a href="{{route((Auth::user()->role!='Agent')?'GestionTachesList':'task_list' )}}" class="breadcrumb">Liste des tâches</a>
    <i data-feather="chevron-right" class="breadcrumb__icon"></i> 
    <a href="#/" class="breadcrumb--active">Détails du partenaire</a> 
</div>
@endsection
@section('content')

<form action="" id="form_data" enctype="multipart/form-data">
    <input type="hidden" name="id_task" value="{{$task->id}}">
    <div class="grid lg:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-2" >
        <div>
            
            <div class="intro-x box w-full mt-5 font-semibold p-3">
                <div class="grid justify-items-left my-2 {{(strtotime($task->date_fin) >= strtotime(date('Y-m-d H:i')) )? 'hidden' : ''}}">
                    <div class="px-2 py-1 rounded-lg text-white w-32 text-center" style="background: red;">
                        <span>tâches expirées</span>
                    </div>
                </div> 
                <div>
                    <label for="">Agent</label>
                    <input type="text" name="" value="{{$task->nom}} {{$task->prenom}}" class="input w-full border border-gray-500 mt-2" disabled>
                </div>
                <div class="">
                    <div class="">
                        <label for="">Dossier</label>
                        <select name="doss" class="input doss w-full border border-gray-500 mt-2" disabled>
                            @foreach ($Dossier as $item)
                                @if ($item->id==$task->id_doss)
                                    <option value="{{$item->id}}" {{($item->id==$task->id_doss)? 'selected' : ''}}>{{$item->ref}}</option>        
                                    @php
                                        break;
                                    @endphp
                                @endif
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="">
                        <label for="">Commande</label>
                        <select name="cmnd" class="input w-full border border-gray-500 mt-2" disabled>
                            <option >{{$task->commande}}</option>  
                        </select>
                    </div>
                    <div class="">
                        <label for="">Date débute</label>
                        <input type="datetime-local" disabled name="date_dbTsk" value="{{date('Y-m-d\TH:i',strtotime($task->date_db))}}" class="input w-full border border-gray-500 mt-2">
                    </div>
                    <div class="">
                        <label for="">Date fin</label>
                        <input type="datetime-local" disabled name="date_FinTsk" value="{{date('Y-m-d\TH:i',strtotime($task->date_fin))}}" class="input w-full border border-gray-500 mt-2">
                    </div>
                </div>
            </div> 
            <div class="-intro-x box w-full mt-5 font-semibold p-3">
                <div class="w-full">
                    <label for="">Fond & consigne</label>
                    @if (count($files)>0)
                    <div class="border-2 border-dashed dark:border-dark-5 rounded-md mt-3 pt-4">
                        <div class="flex flex-wrap px-4 file_block" >
                            @foreach ($files as $item)
                                <div class="w-32 h-32 relative image-fit cursor-pointer zoom-in my-3 rounded-lg mx-2">
                                    <a href="{{asset($File_path->file_path.$item->file_name)}}" target="_blank">
                                        <div class="file rounded-lg px-5 pt-8 pb-5 px-3 sm:px-5 relative ">
                                            <span class="w-3/5 file__icon file__icon--file mx-auto">
                                                <div class="file__icon__file-name"></div>
                                            </span>
                                            <span class="block font-medium mt-4 text-center truncate">{{$item->org_file_name}}</span>
                                        </div>
                                    </a>
                                     
                                    <div title="" class=" w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 kill_file_base " data-id="kill_file_base{{$item->id}}"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> 
                                    </div>
                                </div>
                                <input type="hidden" name="file_exc[]" value="{{$item->file_name}}" class="kill_file_base{{$item->id}}">
                            @endforeach
                        </div>
                    </div>
                    @endif
                    <textarea name="remarque" class="input w-full h-32 border border-gray-500 mt-2" disabled placeholder="Remarque...">{{$task->remarque}}</textarea>
                </div>
            </div>
        </div>
        
    {{-- chat star --}}
        <div id="app"> 
            <div class="intro-y col-span-12 lg:col-span-8 xxl:col-span-9 mt-5">
                <div class="chat__box box">
                    <!-- BEGIN: Chat Active -->
                    <div class="h-full flex flex-col" style="">
                        <div class="flex flex-col sm:flex-row border-b border-gray-200 dark:border-dark-5 px-5 py-4">
                            <div class="flex items-center">
                                <div class="ml-3 mr-auto">
                                    <div class="font-medium text-base">Messagerie</div>
                                    <div class="text-gray-600 text-xs sm:text-sm">Messagerie en temps réel</div>
                                    
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="px-5 pt-5" >
                                <div class="relative "  >
                                    {{-- <div class="flex items-end float-left mb-4 bg-gray-200 dark:bg-dark-5 px-4 py-3 text-gray-700 dark:text-gray-300 rounded-r-md rounded-t-md">
                                        Lorem ipsum sit amen dolor, lorem ipsum sit amen dolor 
                                        <div class="mt-1 text-xs text-gray-600">2 mins ago</div>
                                    </div>
                                    <div class="flex items-end float-left mb-4 bg-gray-200 dark:bg-dark-5 px-4 py-3 text-gray-700 dark:text-gray-300 rounded-r-md rounded-t-md">
                                        Lorem ipsum sit amen dolor, lorem ipsum sit amen dolor 
                                        <div class="mt-1 text-xs text-theme-25">1 mins ago</div>
                                    </div> --}}
                                    @if (Auth::user()->role=='Agent')
                                   
                                        <messages-comp
                                        :user="{{auth()->user()}}"
                                        :convers="{{$Conver}}"
                                        taskagent={{$task->id_agent}}
                                        idtask={{$task->id}}
                                        routsend="{{route('send')}}"
                                        routype="{{route('type')}}"
                                        reloadlink="{{route('reload-msgAgent')}}"
                                        taskid={{$task->id}}
                                        ></messages-comp>
                                    @else
                                        <messages-comp
                                        :user="{{auth()->user()}}"
                                        :convers="{{$Conver}}"
                                        taskagent={{$task->id_agent}}
                                        idtask={{$task->id}}
                                        routsend="{{route('send-admin')}}"
                                        routype="{{route('type-admin')}}"
                                        reloadlink="{{route('reload-msgAdmin')}}"
                                        taskid={{$task->id}}
                                        ></messages-comp>
                                    @endif 
                                    
                                </div>
                            </div>
                        </div>
                        {{-- @if (Auth::user()->role=='Agent')
                            <chat-input
                            :user="{{auth()->user()}}"
                            routsend="{{route('send')}}"
                            routype="{{route('type')}}"
                            taskid={{$task->id}}
                            ></chat-input> 
                        @else
                            <chat-input
                            :user="{{auth()->user()}}"
                            routsend="{{route('send-admin')}}"
                            routype="{{route('type-admin')}}"
                            taskid={{$task->id}}
                            ></chat-input> 
                        @endif  --}}
                        
                    </div>
                    <!-- END: Chat Active -->
                    
                </div>
            </div>
        </div>
    {{-- chat end --}}
    </div>
    
    
</form>
@endsection
@section('script')
<script src="{{asset('js/app.js')}}" defer></script>
@endsection

