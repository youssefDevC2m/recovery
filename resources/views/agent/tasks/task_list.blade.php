@extends('agent.main')
@section('title')
Liste des tâches
@endsection
@section('menu-title')
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> 
    <a href="{{url('/')}}" class="">recoveryApp</a>
    <i data-feather="chevron-right" class="breadcrumb__icon"></i>
    <a href="{{url('/')}}" class="">Tableau de bord</a> 
    <i data-feather="chevron-right" class="breadcrumb__icon"></i> 
    <a href="#/" class="breadcrumb--active">Liste des tâches</a> </div>
@endsection
@section('content')
<div class="intro-y col-span-12 flex flex-wrap sm:flex-no-wrap items-center mt-8">
    {{-- <a href="{{route('GestionTachesForm')}}" class="button text-white bg-theme-1 shadow-md mr-2">Ajouter une nouvelle tâche</a> --}}
    <div class="hidden md:block mx-auto text-gray-600"></div>
    <div class="hidden md:block mx-auto text-gray-600"></div>
</div>
<div class="intro-y box px-5 py-8 mt-5">
    <div class="intro-y col-span-12 overflow-y-scroll xl:overflow-x-scroll" style="height: 30rem;">
        <table class="table table-report -mt-2 border-none" style="border-style: none !important;" id="table_id">
            <thead style="border-style: none !important;">
                <tr class="shadow-lg">
                    <th class="whitespace-no-wrap text-center " style="border-style: none !important;">id</th>
                    <th class="whitespace-no-wrap text-center " style="border-style: none !important;">Dossier</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Commande</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Date de création</th>
                    <th class="text-center whitespace-no-wrap " style="border-style: none !important;">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($task as $item)
                <tr class="shadow-md rounded-lg intro-y " >
                    <td class="w-16" align="center">
                        <div class="rounded-full p-3 {{(strtotime($item->date_fin) <= strtotime(date('Y-m-d H:i')) )? 'bg-theme-6 text-white tooltip' : ''}}" title="tâches expirées"   style="width: 5.4rem;">
                            <div class="text-center font-semibold">
                               {{$item->id}}
                            </div> 
                        </div> 
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{$item->ref}}
                            </div>
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{$item->commande}}
                            </div>
                        </div>
                    </td>
                    <td class="w-40">
                        <div class="">
                            <div class="text-center">
                                {{date('d-m-Y H:i',strtotime($item->created_at))}}
                            </div>
                        </div>
                    </td>
                    
                    <td class="table-report__action w-56">
                        <div class="flex justify-center items-center">
                            <a class="flex items-center" href="{{route('show_task',[$item->id])}}"> 
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-square w-4 h-4 mr-1"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>
                                Aperçu 
                            </a>
                        </div>
                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script src="{{asset('dist/js/dataTbl.js')}}"></script>
<script>
    $("body").ready(function(){
        $('.taskManager').addClass('side-menu--active');
    });
</script>
@endsection