
<nav class="side-nav">
    <a href="" class="intro-x flex items-center pl-5 pt-4">
        <img alt="Midone Tailwind HTML Admin Template" class="w-6" src="{{asset('dist/images/logo.svg')}}">
        <span class="hidden xl:block text-gray-300 text-lg ml-3"> recovery<span class="font-medium text-white">App</span> </span>
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul class="">
        <li>
            <a href="{{route('home')}}" id="bord" class="dbord side-menu side-menu">
                <div class="side-menu__icon"> <i data-feather="monitor"></i> </div>
                <div class="side-menu__title"> Tableau de bord </div>
            </a>
        </li>
        <li class="">
            <a href="javascript:;" id="client" class="client side-menu side-menu">
                <div class="side-menu__icon"><i  data-feather="users"></i> </div>
                <div class="side-menu__title">Clients<i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{route('indexClient')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="user-plus"></i> </div>
                        <div class="side-menu__title"> Ajouter un client</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('listClient')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                        <div class="side-menu__title">Liste des client </div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;" id="dossier" class="dossier side-menu side-menu">
                <div class="side-menu__icon"><i  data-feather="folder"></i> </div>
                <div class="side-menu__title">Dossier<i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{route('ouvertureDoss')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="folder-plus"></i> </div>
                        <div class="side-menu__title"> Ouverture dossier</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('listDossier')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                        <div class="side-menu__title"> Liste des dossiers</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;" id="devis" class="devis side-menu side-menu">
                <div class="side-menu__icon"><i  data-feather="folder"></i> </div>
                <div class="side-menu__title">Devis<i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{route('addDevis')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="folder-plus"></i> </div>
                        <div class="side-menu__title"> Ajouter un devis</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('listDevis')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                        <div class="side-menu__title"> Liste des devis</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;" id="reglement" class="reglement side-menu side-menu">
                <div class="side-menu__icon"><i  data-feather="folder"></i> </div>
                <div class="side-menu__title">Règlements<i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{route('addReglementp')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="folder-plus"></i> </div>
                        <div class="side-menu__title"> Ajouter Un Règlement</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('listReglementp')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                        <div class="side-menu__title"> Liste Des Règlements</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;" id="charge" class="charge side-menu side-menu">
                <div class="side-menu__icon"><i  data-feather="briefcase"></i> </div>
                <div class="side-menu__title">Charges<i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{route('addIndex')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="plus-square"></i> </div>
                        <div class="side-menu__title"> Ajouter Les Charges</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('listCharge')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                        <div class="side-menu__title"> Liste Des Charges</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;" id="partners" class="partners side-menu side-menu">
                <div class="side-menu__icon"><i  data-feather="users"></i> </div>
                <div class="side-menu__title">Les Partenaires<i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{route('partForm')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="user-plus"></i> </div>
                        <div class="side-menu__title"> Ajouter Les Partenaires</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('listForm')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                        <div class="side-menu__title"> Liste Des Partenaires</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;" id="taskManager" class="taskManager side-menu side-menu">
                <div class="side-menu__icon"><i  data-feather="layers"></i> </div>
                <div class="side-menu__title">Gestionnaire des tâches<i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{route('GestionTachesList')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="message-square"></i> </div>
                        <div class="side-menu__title"> Ajouter une tâche</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('GestionTachesForm')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                        <div class="side-menu__title">Liste des tâches </div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="javascript:;" id="parametre" class="users side-menu side-menu">
                <div class="side-menu__icon"><i  data-feather="user"></i> </div>
                <div class="side-menu__title">Utilisateurs<i data-feather="chevron-down" class="side-menu__sub-icon"></i> </div>
            </a>
            <ul class="">
                <li>
                    <a href="{{route('register_index')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="user-plus"></i> </div>
                        <div class="side-menu__title"> Ajouter un utilisateur</div>
                    </a>
                </li>
                <li>
                    <a href="{{route('users_list')}}" id="bord" class="side-menu side-menu">
                        <div class="side-menu__icon"> <i data-feather="list"></i> </div>
                        <div class="side-menu__title">Liste des utilisateurs </div>
                    </a>
                </li>
            </ul>
        </li>
        
        {{-- <li>
            <a href="{{route('indexSettings')}}" id="bord" class="side-menu side-menu param">
                <div class="side-menu__icon"> <i data-feather="settings"></i> </div>
                <div class="side-menu__title"> Paramètres </div>
            </a>
        </li> --}}
    </ul>
</nav>
