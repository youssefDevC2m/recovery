<?php 
$tasks = \App\taskManagment::select('task_managments.*','dossiers.ref')
->join('dossiers','dossiers.id','task_managments.id_doss')->where('id_agent',Auth::user()->id)
->where('valid_task',1)
->whereNull('task_ag_vu')
->take(5)
->get();
// dd($tasks);
$diff = new App\Http\Controllers\dossierController();
?>
<div class="top-bar">
    @yield('menu-title')
    <!-- BEGIN: Search -->
        <div class="intro-x relative mr-3 sm:mr-6">
            <div class="search">
                <input type="text" class="search__input w-full input placeholder-theme-13 findg" placeholder="Rcherche...">
                <i data-feather="search" class="search__icon dark:text-gray-300"></i> 
            </div>
            <div class="search-result rounded-lg shadow-lg w-full">
                <div class="search-result__content">
                    <div class="flex">
                        <div class="flex-1"></div>
                        <div class="flex-1"></div>
                        <div class="flex-1"></div>
                        <div class="flex-1"></div>
                        <div class="flex-1"></div>
                        <div class="flex-1">
                            <div  class="w-8 h-8 bg-red-300 text-red-500 flex items-center justify-center rounded-full stop"><i class="w-4 h-4 items-end" data-feather="x-circle"></i>  </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- END: Search -->
    
    <div class="intro-x dropdown mr-auto sm:mr-6">
        <div class="dropdown-toggle notification notif_bull cursor-pointer {{(count($tasks) > 0)?'notification--bullet' :''}}"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell notification__icon dark:text-gray-300"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg> </div>
        <div class="notification-content pt-2 dropdown-box" id="_b3vrq130n" data-popper-placement="bottom-end" style="position: absolute; inset: 0px auto auto 0px; transform: translate(-330px, 20px);">
            <div class="notification-content__box dropdown-box__content box dark:bg-dark-6">
                <div class="notification-content__title">Notifications</div>
                <div class="my-3 grid justify-items-center {{(count($tasks) > 0)? 'hidden': ''}}">
                    <div>
                        <span>Aucune notification</span>
                    </div>
                </div>
                
                <div class="depased_block h-64 overflow-y-scroll " >
                    @foreach ($tasks as $item)
                    <div class="depased rounded-lg lg:shadow-lg md:shadow-lg show_task" data-id="{{$item->id}}" data-link="{{route('show_task',[$item->id])}}">
                        <div class="cursor-pointer relative flex items-center my-2 p-3">
                            <a id='#\' href="{{route('show_task',[$item->id])}}" >
                                <div class="ml-2 overflow-hidden">
                                    <div class="flex items-center">
                                        <a href="3\"   class="font-medium truncate mr-5">
                                            <span>Dossier : {{$item->ref}}</span>
                                        </a> 
                                        <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap">
                                            {{$item->commande}}
                                        </div>
                                    </div>
                                    <div class="w-full truncate text-gray-700">
                                        <span class="font-medium ">Date début :</span> <span class="text-gray-600">{{date('d-m-Y H:i',strtotime($item->date_db))}}</span> 
                                        <br>
                                        <span class="font-medium ">Date fin :</span><span class="text-gray-600">{{date('d-m-Y H:i',strtotime($item->date_fin))}}</span> 
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    {{-- <div class="depased rounded-lg lg:shadow-lg md:shadow-lg p-2">
                    </div>    --}}
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    
    
    <!-- BEGIN: Account Menu -->
    <div class="intro-x dropdown w-8 h-8 hidden xl:block lg:block md:block sm:block relative ">
        <div class="dropdown-toggle w-8 h-8 rounded-full overflow-hidden shadow-lg bg-blue-400 image-fit zoom-in lg:block">
            <img class="rounded-full w-16 h-16" src="{{(!empty(Auth::user()->photo)? asset(App\File_path::where('type','users')->first()->file_path.Auth::user()->photo) : asset(App\File_path::where('type','users')->first()->file_path.'link.jpg'))}}" alt="">

        </div>
        <div class="dropdown-box w-56 bg-theme-38 rounded-lg ">
            <div class="dropdown-box__content border-b-2 border-gray-600 dark:bg-dark-6 text-white mx-2">
                <div class="p-1 py-2 dark:border-dark-3">
                    <div class="font-medium ">
                        <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdown">
                            <span class="dropdown-item grid justify-items-right">
                                <div class="text-lg">
                                    {{Auth::user()->nom}} {{Auth::user()->prenom}}
                                </div>
                                <span class="text-gray-400">{{Auth::user()->email}}</span>
                            </span>
                        </div>
                    </div>
                    {{-- <input type="hidden" id="id_auth" value="{{Auth::user()->nom}}" > --}}
                    
                    {{-- <div class="text-xs text-theme-41 dark:text-gray-600"></div> --}}
                </div>
            </div>
            {{--  --}}
            <div class="dropdown-box__content text-white rounded-lg hover:bg-blue-700 mx-2">
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <div class="p-1 dark:border-dark-3 ">
                        <div class="font-medium">
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <span class="dropdown-item">{{ __('Logout') }}</span> 
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </a>    
            </div>
            <div class="border-b-2 border-gray-600 mx-2 my-2"></div>
            <div class="dropdown-box__content text-white rounded-lg hover:bg-blue-700 mx-2 mb-2">
                <a href="{{ route('edit_userindex',[Auth::user()->id])}}">
                    <div class="p-1 dark:border-dark-3 ">
                        <div class="font-medium">
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <span class="dropdown-item">profile</span> 
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            {{--  --}}
        </div>
        <div class="block w-3 h-3 bg-theme-9 absolute right-0 top-0 rounded-full border-2 border-white"></div>

    </div>
    <!-- END: Account Menu -->
</div>
<script src="{{asset('dist/js/app.js')}}"></script>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // $('body').on('click','.vu_anc',function(){
    //     event.preventDefault();
    //     var thiss=$(this);
    //     $.ajax({
    //         url:"{{route('vu_anc')}}",
    //         data:{id_dt:thiss.data('id')},
    //         dataType:'json',
    //         type:'post',
    //         success:function(data){
    //             window.location.replace(thiss.attr('href'));
    //             // console.log(data);
    //         },
    //         error:function(error){
    //             console.log(error);
    //         },
    //     });
    // });
    $('body').on('click','.show_task',function(){
        event.preventDefault();
        var thiss=$(this);
        $.ajax({
            url:"{{route('vu_task')}}",
            data:{id_task:thiss.data('id')},
            dataType:'json',
            type:'post',
            success:function(data){
                location.replace(thiss.data('link'));
                // console.log(data);
            },
            error:function(error){
                console.log(error);
            },
        });
    });
        // $.ajax({
        //     url:"",
        //     type:'post',
        //     dataType:'json',
        //     success:function(data){
        //         a=0;
        //         console.log(data.count);
        //         if(data.count!=0){
        //             $('.notif_bull').addClass('notification--bullet');
        //             if(data.count>3){$('.unpayed_pack').addClass('overflow-auto h-64')};
        //             $.each(data.data,function(index,o){
        //                 url='';
        //                 v="{{--route('',['id'])--}}";
        //                 url=v.replace('id',o.alert_pay);
        //                 // console.log(url);
                        
        //                 $('.unpayed_client').append(`
        //                 <div class="cursor-pointer relative flex items-center my-2">
        //                         <a id='`+a+`' href="">
        //                         <div class="w-12 h-12 flex-none image-fit mr-1">
        //                             <img alt="Midone Tailwind HTML Admin Template" class="rounded-full" `+((o.image!=null)?`src="{{asset('files/clients')}}/`+ o.image +` `:`src="{{asset('dist/images/img_avatar3.png')}}`)+`">
        //                             <div class="w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white"></div>
        //                         </div>
                                
        //                             <div class="ml-2 overflow-hidden">
        //                                 <div class="flex items-center">
        //                                     <a href="" id="`+a+`t" class="font-medium truncate mr-5">`+((o.type_indv_sos==0)?o.nom_raisonsocial +' '+o.prenom : o.nom_raisonsocial) +`</a> 
        //                                     <div class="text-xs text-gray-500 ml-auto whitespace-no-wrap"> `+ ((o.nb_j_pre_pay==0)? 1 : Math.abs(o.nb_j_pre_pay)) +` mois crédit</div>
        //                                 </div>
        //                                 <div class="w-full truncate text-gray-600">Cette personne doit payer son loyer</div>
        //                             </div>
        //                         </a>
        //                     </div>
        //                 `);
        //                 $('#'+a).attr('href',url);
        //                 $('#'+a+'t').attr('href',url);
        //                 a++;
        //                 $('#'+a).attr('href',url);
        //                 $('#'+a+'t').attr('href',url);
        //             });
        //         }
        //         // console.log(data);
        //     },error:function(error){
        //         console.log(error);
        //     },
        // });
        if("{{Auth::user()->nom}}"==null){
            window.location.replace("{{route('out')}}");
        }
    });
</script>
