
<div class="mobile-menu md:hidden">
            <div class="mobile-menu-bar">
                <a href="" class="flex mr-auto">
                    <img alt="Midone Tailwind HTML Admin Template" class="w-6" src="{{ asset('dist/images/logo.svg') }}">
                </a>
                <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
            </div>
              <ul class="border-t border-theme-24 py-5 hidden">
                <li class="">
                    <a href="{{route('home')}}"  class="menu menu--active">
                        <div class="menu__icon"> <i data-feather="home"></i></div>
                        <div class="menu__title"> Tableau de bord </div>
                    </a>
                </li>
                <li class="">
                    <a href="javascript:;"  class="menu ">
                        <div class="menu__icon"> <i data-feather="users"></i> </div>
                        <div class="menu__title">Utilisateurs<i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        <li class="menu">
                            <a href="{{route('register_index')}}" id="bord" class="side-menu__sub-icon">
                                <div class="menu__title">Ajouter un utilisateur</div>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('users_list')}}" class="menu">
                                <div class="menu__title">Liste des utilisateurs</div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;"  class="menu ">
                        <div class="menu__icon"> <i data-feather="folder"></i> </div>
                        <div class="menu__title">Dossier<i data-feather="chevron-down" class="menu__sub-icon"></i> </div>
                    </a>
                    <ul class="">
                        <li class="menu">
                            <a href="{{route('ouvertureDoss')}}" id="bord" class="side-menu__sub-icon">
                                <div class="menu__title">Ajouter un utilisateur</div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"  class="menu menu--active">
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                        <div class="menu__icon"> <i data-feather="home"></i></div>
                        <div class="menu__title"> Déconnexion </div>
                    </a>
                </li>
            </ul>
</div>