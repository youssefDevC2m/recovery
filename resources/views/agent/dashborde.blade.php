@extends('agent.main')

@section('title')
Tableaux de borde 
@endsection

@section('menu-title')
<style>
    .report-box__indicator
    {padding-right: 0.50rem !important;}
</style>
<div class="-intro-x breadcrumb mr-auto hidden sm:flex"> <a href="" class="">RecoveryApp</a> <i data-feather="chevron-right" class="breadcrumb__icon"></i> <a href="#/" class="breadcrumb--active">Tableau de bord</a> </div>
@endsection

@section('content')

    <div class="">
          <!-- BEGIN: General Report -->
        <div class="col-span-12 mt-8">
            <div class="intro-y flex items-center h-10">
                <h2 class="text-lg font-medium truncate mr-5">
                Rapport général
                </h2>
                <a href="{{ route('AgentIndex') }}" class="ml-auto flex text-theme-1 dark:text-theme-10"> <i data-feather="refresh-ccw" class="w-4 h-4 mr-3"></i> Recharger les données </a>
            </div>
            {{-- <div class=""> 
                <div class="tiny-slider  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal"> 
                    
                    
                </div> 
            </div>  --}}
            <div class="" >
                <div class="grid grid-cols-1 gap-4 mt-5">

                    <div class="intro-y">
                        <div class="report-box zoom-in">
                            <div class="box p-5">
                                <div class="flex">
                                    <span class="relative">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell report-box__icon text-yellow-400"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg>
                                    </span>
                                    <div class="ml-auto ">
                                        <div class="report-box__indicator bg-yellow-500  cursor-pointer text-center " >
                                            <a href="javascript:;" data-theme="light" class="tooltip" title="nombre des dossiers">1</a> 
                                        </div>
                                    </div>
                                </div>
                                <div class="text-3xl font-bold leading-8 mt-6">Tâches</div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        @include('agent.tasks.taskReport')
        <!-- END: General Report -->
    </div>
    
    <script>
        $('.dbord').attr('class','side-menu side-menu--active');
    </script>
@endsection