<!DOCTYPE html>

<html lang="en" class="light">
    <!-- BEGIN: Head -->
    <head>
        <meta charset="utf-8">
        <link href="{{asset('dist/images/logo.svg')}}" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="LEFT4CODE">
        
        @yield('head')

        <title>@yield('title')</title>
        <!-- BEGIN: CSS Assets-->

        <link rel="stylesheet" href="{{asset('dist/css/app.css')}}" />
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
       
        <!-- END: CSS Assets-->
        <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
    </head>
    <!-- END: Head -->
    <body class="app">
        <!-- BEGIN: Mobile Menu -->
        {{-- @include('admin.layout.mobilenav') --}}
        
          
        <!-- END: Mobile Menu -->
        <div class="flex">
            <!-- BEGIN: Side Menu -->
            @include('admin.layout.side')
            <!-- END: Side Menu -->
            
            <!-- BEGIN: Content -->
            <div class="content">
            @include('admin.layout.nav')
            @yield('content')
            </div>
            <!-- END: Content -->
        </div>

        <!-- BEGIN: JS Assets-->
        {{-- @yield('script') --}}
        
        <!-- END: JS Assets-->
        {{-- <script src="{{asset('dist/js/app.js')}}"></script> --}}
        <!-- get all parameters -->
		<script>
            $(document).keydown(function (event) {
                if (event.keyCode == 123) { // Prevent F12
                    return false;
                }else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
                    return false;
                }else if (event.ctrlKey  && event.keyCode == 85) { // Prevent Ctrl+u        
                    return false;
                }
            });
            // $(document).on("contextmenu", function (e) {        
            //     e.preventDefault();
            // });
		</script>
        
    </body>
    
    
</html>