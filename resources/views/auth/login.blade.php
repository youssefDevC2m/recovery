 
@extends('auth.auth')

@section('title')
    Admin Login
@endsection

@section('content')

<!-- BEGIN: Login Form -->
<div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
    <div class="my-auto mx-auto xl:ml-20 bg-white xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-auto lg:w-2/4 xl:w-auto">
        <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
        Se Connecter.
        </h2>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="intro-x lg:mt-8">
                <input class="focs intro-x login__input m:w-auto input input--lg border border-gray-300 block @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" placeholder="Entrer votre email" autofocus type="email" required autocomplete="email" >
              
                @if (isset($message))
                     {{$message}}
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                    
                @endif
                   
                 
                <input id="password" type="password" class="intro-x login__input sm:w-auto input input--lg border border-gray-300 block mt-4 @error('password') is-invalid @enderror"  placeholder="Entrer votre mot de pass"  name="password"  required autocomplete="current-password">
                {{-- @error('message')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror --}}
            </div>
            <div class="intro-x flex text-gray-700 dark:text-gray-600 text-xs sm:text-sm mt-4">
                <div class="flex items-center mr-auto">
                    <input class="input border mr-2 sm:w-auto" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="cursor-pointer select-none" for="remember-me">Remember me</label>
                </div>
                <a href="#/">Mot de passe oublié?</a> 
            </div>
            <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                <button type="submit" style="background-color: #2a97ad;" class="button button--lg w-full xl:w-32 text-white xl:mr-3 align-top">Login</button>
                {{-- <!--<a href="{{url('/register')}}" class="button button--lg w-full xl:w-32 text-gray-700 border border-gray-300 dark:border-dark-5 dark:text-gray-300 mt-3 xl:mt-0 align-top">register now</a>--> --}}
            </div>
            {{-- <div class="intro-x mt-10 xl:mt-24 text-gray-700 dark:text-gray-600 text-center xl:text-left">
                En vous inscrivant, vous acceptez nos
                <br>
                <a class="text-theme-1 dark:text-theme-10" href="#/">Termes et conditions</a> et <a class="text-theme-1 dark:text-theme-10" href="#/">Politique de confidentialité</a> 
            </div> --}}
        </form>
    </div>
</div>
<!-- END: Login Form -->

<script>
    // $( ".focs" ).focus();
</script>
@endsection
