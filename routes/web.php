<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/','Auth\LoginController@index')->name('out');

Route::prefix('/admin')->middleware(['IsAdmin'])->group(function(){
Route::get('/', 'HomeController@index')->name('home');

//users
Route::get('/register_index','Auth\RegisterController@register_index')->name('register_index');
Route::post('/add_user','Auth\RegisterController@add_user')->name('add_user');
Route::get('/users_list','Auth\RegisterController@users_list')->name('users_list');
Route::get('/edit_userindex/{id}','Auth\RegisterController@edit_userindex')->name('edit_userindex');
Route::get('/delete_user/{id}','Auth\RegisterController@delete_user')->name('delete_user');
Route::post('/edit_user','Auth\RegisterController@edit_user')->name('edit_user');
// historique activite
Route::get('/admin_see_all_acts','ActiviesController@admin_see_all_acts')->name('admin_see_all_acts');
Route::post('/aprouv_act','ActiviesController@aprouv_act')->name('aprouv_act');
Route::post('/reject_act','ActiviesController@reject_act')->name('reject_act');
// client
Route::get('/indexClient','ClientController@indexClient')->name('indexClient');
Route::get('/listClient','ClientController@listClient')->name('listClient');
Route::get('/showClient/{id}','ClientController@showClient')->name('showClient');
Route::post('/createClient','ClientController@createClient')->name('createClient');
Route::post('/editClient','ClientController@editClient')->name('editClient');
Route::get('/destroyClient/{id}','ClientController@destroyClient')->name('destroyClient');
Route::get('/clientUser/{id}','ClientController@clientUser')->name('clientUser');
Route::get('/cli-dossiers/{id}','dossierController@cliDossier')->name('cliDossier');
Route::get('/print_fich_cli/{id}','ClientController@print_fich_cli')->name('print_fich_cli');
Route::post('/add_mandataire-v','ClientController@render_mondataireV')->name('render_mondataireV');
// dossier
Route::get('/ouvertureDoss','dossierController@ouvertureDoss')->name('ouvertureDoss');
Route::post('/createDossier','dossierController@createDossier')->name('createDossier');
Route::get('/listDossier','dossierController@listDossier')->name('listDossier');
Route::get('/showDossier/{id}','dossierController@showDossier')->name('showDossier');
Route::get('/showDebiteur/{id}','dossierController@showDebiteur')->name('showDebiteur');
Route::get('/destroyDossier/{id}','dossierController@destroyDossier')->name('destroyDossier');
Route::post('/editDebiteurDossier','dossierController@editDebiteurDossier')->name('editDebiteurDossier');
Route::post('/editDossier',"dossierController@editDossier")->name('editDossier');
Route::post('/createScene','dossierController@createScene')->name('createScene');
Route::get('/fermetureDoss/{id}','dossierController@fermetureDoss')->name('fermetureDoss');
Route::post('/addFermInfos','dossierController@addFermInfos')->name('addFermInfos');
Route::post('/updateFermInfos','dossierController@updateFermInfos')->name('updateFermInfos');
Route::post('/get_bank_info','dossierController@get_bank_info')->name('get_bank_info');
Route::post('/vu_anc','dossierController@vu_anc')->name('vu_anc');
Route::get('/list_alert_anc','dossierController@list_alert_anc')->name('list_alert_anc');
// Scénarios
Route::get('/SceneList/{id}','dossierController@SceneList')->name('SceneList');
Route::get('/plante_Cheque/{id}','dossierController@plante_Cheque')->name('plante_Cheque');
Route::get('/form_Cheque/{id}','dossierController@form_Cheque')->name('form_Cheque');
Route::get('/show_Cheque/{id}','dossierController@show_Cheque')->name('show_Cheque');
Route::post('/add_cheques_ref','dossierController@add_cheques_ref')->name('add_cheques_ref');
Route::post('/edit_cheques_ref','dossierController@edit_cheques_ref')->name('edit_cheques_ref');
Route::get('/delete_cheques/{id}','dossierController@delete_cheques')->name('delete_cheques');
Route::get('/injonc_payer/{id}','dossierController@injonc_payer')->name('injonc_payer');
Route::get('/form_injonc_pay/{id}','dossierController@form_injonc_pay')->name('form_injonc_pay');
Route::post('/add_injonc_pay_ref','dossierController@add_injonc_pay_ref')->name('add_injonc_pay_ref');
Route::get('/show_injoction/{id}','dossierController@show_injoction')->name('show_injoction');
Route::post('/edit_injonc_pay_ref','dossierController@edit_injonc_pay_ref')->name('edit_injonc_pay_ref');
Route::get('/delete_injoction{id}','dossierController@delete_injoction')->name('delete_injoction');
Route::get('/req_injonc_payer/{id}','dossierController@req_injonc_payer')->name('req_injonc_payer');
Route::get('/form_ReqInjonc_pay/{id}','dossierController@form_ReqInjonc_pay')->name('form_ReqInjonc_pay');
Route::post('/add_ReqInjonc','dossierController@add_ReqInjonc')->name('add_ReqInjonc');
Route::get('/show_ReqInjoction/{id}','dossierController@show_ReqInjoction')->name('show_ReqInjoction');
Route::post('/edit_ReqInjonc','dossierController@edit_ReqInjonc')->name('edit_ReqInjonc');
Route::get('/delete_Reqinjoction/{id}','dossierController@delete_Reqinjoction')->name('delete_Reqinjoction');
Route::post('/get_list_situ_jurid','dossierController@get_list_situ_jurid')->name('get_list_situ_jurid');
Route::post('/get_range_form','dossierController@get_range_form')->name('get_range_form');
Route::post('/create_range','dossierController@create_range')->name('create_range');
Route::post('/delete_range','dossierController@delete_range')->name('delete_range');
Route::post('/update_range','dossierController@update_range')->name('update_range');
Route::get('/citation_list/{id}','dossierController@citation_list')->name('citation_list');
Route::get('/citation_form/{id}','dossierController@citation_form')->name('citation_form');
Route::post('/duplicat_audi','dossierController@duplicat_audi')->name('duplicat_audi');
Route::post('/save_citation_dir','dossierController@save_citation_dir')->name('save_citation_dir');
Route::post('/pha_duplicat_audi','dossierController@pha_duplicat_audi')->name('pha_duplicat_audi');
Route::post('/form_citation_phase_app','dossierController@form_citation_phase_app')->name('form_citation_phase_app');
Route::get('/delete_citation/{id}','dossierController@delete_citation')->name('delete_citation');
Route::get('/show_citation/{id}','dossierController@show_citation')->name('show_citation');
Route::post('/edit_citation','dossierController@edit_citation')->name('edit_citation');

Route::get('/saisie_fond_commerce_list/{id}','dossierController@saisie_fond_commerce_list')->name('saisie_fond_commerce_list');
Route::get('/saisie_fond_commerce_form/{id}','dossierController@saisie_fond_commerce_form')->name('saisie_fond_commerce_form');
Route::post('/render_fond_comerc_deroul','dossierController@render_fond_comerc_deroul')->name('render_fond_comerc_deroul');
Route::post('/add_fond_commerce_form','dossierController@add_fond_commerce_form')->name('add_fond_commerce_form');
Route::get('/delete_font_comerc/{id}','dossierController@delete_font_comerc')->name('delete_font_comerc');
Route::get('/show_fond_commerce_form/{id}','dossierController@show_fond_commerce_form')->name('show_fond_commerce_form');
Route::post('/update_fond_commerce_form','dossierController@update_fond_commerce_form')->name('update_fond_commerce_form');
Route::get('/saisie_tiers_detonad_form/{id}','dossierController@saisie_tiers_detonad_form')->name('saisie_tiers_detonad_form');
Route::get('/saisie_tiers_detonado_list/{id}','dossierController@saisie_tiers_detonado_list')->name('saisie_tiers_detonado_list');
Route::post('/add_saisie_tiers_detonado','dossierController@add_saisie_tiers_detonado')->name('add_saisie_tiers_detonado');
Route::get('/delete_saisie_tiers_detonado/{id}','dossierController@delete_saisie_tiers_detonado')->name('delete_saisie_tiers_detonado');
Route::get('/show_saisie_tiers_detonado/{id}','dossierController@show_saisie_tiers_detonado')->name('show_saisie_tiers_detonado');
Route::post('/update_saisie_tiers_detonado','dossierController@update_saisie_tiers_detonado')->name('update_saisie_tiers_detonado');
Route::get('/saisie_arret_bancaire_list/{id}','dossierController@saisie_arret_bancaire_list')->name('saisie_arret_bancaire_list');
Route::get('/saisie_arret_bancair_form/{id}','dossierController@saisie_arret_bancair_form')->name('saisie_arret_bancair_form');
Route::post('/add_saisie_arret_bancaire','dossierController@add_saisie_arret_bancaire')->name('add_saisie_arret_bancaire');
Route::get('/show_saisie_arret_bancaire/{id}','dossierController@show_saisie_arret_bancaire')->name('show_saisie_arret_bancaire');
Route::post('/update_saisie_arret_bancaire','dossierController@update_saisie_arret_bancaire')->name('update_saisie_arret_bancaire');
Route::get('/delete_saisie_arret_bancaire/{id}','dossierController@delete_saisie_arret_bancaire')->name('delete_saisie_arret_bancaire');
Route::get('/saisie_actions_parts_soc_list/{id}','dossierController@saisie_actions_parts_soc_list')->name('saisie_actions_parts_soc_list');
Route::get('/saisie_action_parts_form/{id}','dossierController@saisie_action_parts_form')->name('saisie_action_parts_form');
Route::post('/add_saisie_actions_parts','dossierController@add_saisie_actions_parts')->name('add_saisie_actions_parts');
Route::get('/show_saisie_actions_parts/{id}','dossierController@show_saisie_actions_parts')->name('show_saisie_actions_parts');
Route::post('/update_saisie_actions_parts','dossierController@update_saisie_actions_parts')->name('update_saisie_actions_parts');
Route::get('/delete_saisie_actions_parts/{id}','dossierController@delete_saisie_actions_parts')->name('delete_saisie_actions_parts');
Route::get('/saisie_moblier_list/{id}','dossierController@saisie_moblier_list')->name('saisie_moblier_list');
Route::get('/saisie_mobilier_form/{id}','dossierController@saisie_mobilier_form')->name('saisie_mobilier_form');
Route::post('/add_saisie_mobilier','dossierController@add_saisie_mobilier')->name('add_saisie_mobilier');
Route::get('/show_saisie_mobilier/{id}','dossierController@show_saisie_mobilier')->name('show_saisie_mobilier');
Route::post('/update_saisie_mobilier','dossierController@update_saisie_mobilier')->name('update_saisie_mobilier');
Route::get('/delete_saisie_mobilier/{id}','dossierController@delete_saisie_mobilier')->name('delete_saisie_mobilier');
Route::get('/saisie_immoblier_list/{id}','dossierController@saisie_immoblier_list')->name('saisie_immoblier_list');
Route::get('/saisie_immobilier_form/{id}','dossierController@saisie_immobilier_form')->name('saisie_immobilier_form');
Route::post('add_saisie_immobilier','dossierController@add_saisie_immobilier')->name('add_saisie_immobilier');
Route::get('/show_saisie_immobilier/{id}','dossierController@show_saisie_immobilier')->name('show_saisie_immobilier');
Route::post('/update_saisie_immobilier','dossierController@update_saisie_immobilier')->name('update_saisie_immobilier');
Route::get('/delete_saisie_immobilier/{id}','dossierController@delete_saisie_immobilier')->name('delete_saisie_immobilier');
//devis
Route::get('/addDevis','DevisController@addIndex')->name('addDevis');
Route::post('/grate','DevisController@store')->name('storeDevis');
Route::get('/listDevis','DevisController@index')->name('listDevis');
Route::get('/showDevis/{id}','DevisController@showDevis')->name('showDevis');
Route::post('/editDevis','DevisController@editDevis')->name('editDevis');
Route::get('/printDevis/{id}','DevisController@printDevie')->name('printDevie');
Route::get('/deleteDevis/{id}','DevisController@deleteDevis')->name('deleteDevis');
Route::post('/get_list_value/','DevisController@get_list_value')->name('get_list_value');
//reglement
Route::get('/addReglementp','ReglementController@addReglementp')->name('addReglementp');
Route::get('/listReglementp','ReglementController@listReglementp')->name('listReglementp');
Route::post('/storeRegle','ReglementController@storeRegle')->name('storeRegle');
Route::post('/get_cli_reg','ReglementController@get_cli_reg')->name('get_cli_reg');
Route::get('/showRglement/{id}','ReglementController@showRglement')->name('showRglement');
Route::post('/updateRegle','ReglementController@updateRegle')->name('updateRegle');
Route::get('/deleteRegle/{id}','ReglementController@deleteRegle')->name('deleteRegle');
//charges
Route::get('/addCharges','ChargeController@addIndex')->name('addIndex');
Route::post('/storeCharge','ChargeController@storeCharge')->name('storeCharge');
Route::get('/listCharge','ChargeController@listCharge')->name('listCharge');
Route::get('/showCharge/{id}','ChargeController@showCharge')->name('showCharge');
Route::post('/updateCharge','ChargeController@updateCharge')->name('updateCharge');
Route::get('/deleteCharge/{id}','ChargeController@deleteCharge')->name('deleteCharge');
// Partenaires
Route::get('/partForm','PratnerController@partForm')->name('partForm');
Route::get('/listForm','PratnerController@listForm')->name('listForm');
Route::post('/GetDebit','PratnerController@GetDebit')->name('GetDebit');
Route::get('/creatPart','PratnerController@creatPart')->name('creatPart');
Route::get('/showPart/{id}','PratnerController@showPart')->name('showPart');
Route::get('/editPart','PratnerController@editPart')->name('editPart');
Route::get('/deletePart/{id}','PratnerController@deletePart')->name('deletePart');
Route::get('/part_work/{id}','PratnerController@part_work')->name('part_work');
Route::get('/formDetail/{id}','PratnerController@formDetail')->name('formDetail');
Route::post('/addDetail','PratnerController@addDetail')->name('addDetail');
Route::get('/showDetail/{id}','PratnerController@showDetail')->name('showDetail');
Route::post('/editDetail','PratnerController@editDetail')->name('editDetail');
Route::get('/deleteDetail/{id}','PratnerController@deleteDetail')->name('deleteDetail');
Route::post('/getAgents','PratnerController@getAgents')->name('getAgents');
// Task managment
Route::get('/GestionTachesList','TaskController@GestionTachesList')->name('GestionTachesList');
Route::get('/GestionTachesForm','TaskController@GestionTachesForm')->name('GestionTachesForm');
Route::post('/GestionTachesAdd','TaskController@GestionTachesAdd')->name('GestionTachesAdd');
Route::get('/GestionTachesDelete/{id}','TaskController@GestionTachesDelete')->name('GestionTachesDelete');
Route::get('/GestionTachesShow/{id}','TaskController@GestionTachesShow')->name('GestionTachesShow');
Route::post('/GestionTachesEdit','TaskController@GestionTachesEdit')->name('GestionTachesEdit');
Route::get('/Messenger-admin/{id}','AgentConroller@show_task')->name('Messenger.admin');
Route::post('/send-admin','AgentConroller@send')->name('send-admin');
Route::post('/type-admin','AgentConroller@typingAg')->name('type-admin');
Route::post('/reload-msg','AgentConroller@reloadmsg')->name("reload-msgAdmin");
Route::post('/task_exp_vu','TaskController@task_exp_vu')->name('task_exp_vu');
Route::get('/clear', function() {
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    //  Artisan::call('storage:link');
    return "Cleared!";
});
// factures
Route::get('/facture','factureController@addFacture')->name('addFacture');
Route::get('/factuerContn/{id_client}/{id_fact}','factureController@factuerContn')->name('factuerContn');
Route::post('/factuer-laststep','factureController@factuerlaststep')->name('factuerlaststep');
Route::get('/listFacture','factureController@listFacture')->name('listFacture');
Route::post('/facture-add','factureController@createFacture')->name('createFacture');
Route::get('/deleteFacture/{id}','factureController@deleteFacture')->name('deleteFacture');
Route::get('/showFacture/{id}','factureController@showFacture')->name('showFacture');
Route::post('/updateFacture','factureController@updateFacture')->name('updateFacture');
Route::get('/print-facture/{id}','factureController@print')->name('print');
// 
Route::get('/bib_model_index','ModelLibrarytController@bib_model_index')->name('bib_model_index');
Route::post('/bib_model_add','ModelLibrarytController@bib_model_add')->name('bib_model_add');
Route::get('/delete_biblio/{id}','ModelLibrarytController@delete_biblio')->name('delete_biblio');

route::get('/db',function(){
    // dd(
    //   \App\File_path::where('type','users')->update([
    //        'file_path'=>'/public/files/users/'
    //     ]),
    //     \App\File_path::where('type','joints')->update([
    //         'file_path'=>'/public/files/joints/'
    //     ])
    // );
	// dd(DB::statement("
	// CREATE TABLE `factures` (
	// `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	// `doss` INT(11) NULL DEFAULT NULL,
	// `type_facture` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	// `date_facture` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	// `remarque` TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	// `montant_total` DOUBLE(8,2) NULL DEFAULT NULL,
	// `created_at` TIMESTAMP NULL DEFAULT NULL,
	// `updated_at` TIMESTAMP NULL DEFAULT NULL,
	// PRIMARY KEY (`id`) USING BTREE
	// )
	// COLLATE='utf8mb4_unicode_ci'
	// ENGINE=InnoDB
	// AUTO_INCREMENT=3
	// ;	
	// "),
    dd(
        DB::statement("ALTER TABLE `clients`
        ADD COLUMN `brand` VARCHAR(255) NULL DEFAULT NULL AFTER `id_user`,
        ADD COLUMN `sec_a_act` VARCHAR(255) NULL DEFAULT NULL AFTER `brand`,
        ADD COLUMN `date_parten` VARCHAR(255) NULL DEFAULT NULL AFTER `sec_a_act`;
        ")
    );
    
		
});



});
Route::get('/test', function () {
    // $user = [
    //     'name' => 'Harsukh Makwana',
    //     'info' => 'Laravel & Python Devloper'
    // ];
    // \Mail::to('amhile22youssef@gmail.com')->send(new \App\Mail\NewMail());
    // dd("success");
// })->name('send_valid_resMail');

dd(
    DB::statement("ALTER TABLE `dossiers`
	ADD COLUMN `date_db_doss` DATETIME NULL DEFAULT NULL"),
    DB::statement("ALTER TABLE `dossiers`
	ADD COLUMN `date_fin_doss` DATETIME NULL DEFAULT NULL ")
);

});
Route::prefix('/agent')->middleware(['IsAgent'])->group(function(){
    Route::get('/','AgentConroller@index')->name('AgentIndex');
    Route::get('/task_list','AgentConroller@task_list')->name('task_list');
    Route::get('/show_task/{id}','AgentConroller@show_task')->name('show_task');
    Route::post('/vu_task','AgentConroller@vu_task')->name('vu_task');
    Route::post('/send','AgentConroller@send')->name('send');
    Route::post('/type','AgentConroller@typingAg')->name('type');
    Route::post('/reload-msg','AgentConroller@reloadmsg')->name("reload-msgAgent");
    // Route::get('notif/',
    // function(){
    //     return event(new App\Events\NotificationEv("tettete"));
    // });
    Route::get('/clear', function() {
        Artisan::call('config:clear');
        Artisan::call('config:cache');
        Artisan::call('view:clear');
        //  Artisan::call('storage:link');
        return "Cleared!";
    });
});

